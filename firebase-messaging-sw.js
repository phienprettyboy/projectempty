﻿// Import and configure the Firebase SDK
// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup
importScripts('/App/FireBase/firebase-app.js');
importScripts('/App/FireBase/firebase-messaging.js');
importScripts('/App/FireBase/firebase-database.js');
importScripts('/App/FireBase/initFireBase.js');
if (firebase.messaging.isSupported()) {
    messaging = firebase.messaging();
    messaging.setBackgroundMessageHandler(function (payload) {
        console.log('[firebase-messaging-sw.js] Received background message ', payload);
        var data = JSON.parse(payload);
        const title = data.title;
        const options = {
            icon: 'http://businessportal.vn/assets/static/images/logo.png',
            body:data.body
        };
        self.registration.showNotification(title, options);
    });
}
