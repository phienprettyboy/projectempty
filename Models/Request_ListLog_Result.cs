//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Request_ListLog_Result
    {
        public string RequestSign_ID { get; set; }
        public string GroupName { get; set; }
        public Nullable<int> IsTypeDuyet { get; set; }
        public Nullable<bool> IsHide { get; set; }
        public Nullable<int> STTQT { get; set; }
        public string QTName { get; set; }
        public Nullable<bool> IsHideQT { get; set; }
        public string RequestQT_ID { get; set; }
    }
}
