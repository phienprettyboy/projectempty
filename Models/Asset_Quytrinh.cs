//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Asset_Quytrinh
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Asset_Quytrinh()
        {
            this.Asset_Quytrinh_Nhom = new HashSet<Asset_Quytrinh_Nhom>();
            this.Asset_QuytrinhPB = new HashSet<Asset_QuytrinhPB>();
        }
    
        public string QT_ID { get; set; }
        public string Tenquytrinh { get; set; }
        public string Mota { get; set; }
        public string Congty_ID { get; set; }
        public string Phongban_ID { get; set; }
        public bool Trangthai { get; set; }
        public int STT { get; set; }
        public Nullable<int> LoaiQT { get; set; }
        public Nullable<bool> IsDuyetPB { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Asset_Quytrinh_Nhom> Asset_Quytrinh_Nhom { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Asset_QuytrinhPB> Asset_QuytrinhPB { get; set; }
    }
}
