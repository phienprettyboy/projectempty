//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Car_Theodoi
    {
        public string Theodoixe_ID { get; set; }
        public string PhieuDX_ID { get; set; }
        public string LenhDX_ID { get; set; }
        public string Nhansu_ID { get; set; }
        public string Congty_ID { get; set; }
        public string DieuxeFollow_ID { get; set; }
        public Nullable<int> STT { get; set; }
    
        public virtual Car_LenhDieuXe Car_LenhDieuXe { get; set; }
        public virtual Car_PhieuDatXe Car_PhieuDatXe { get; set; }
    }
}
