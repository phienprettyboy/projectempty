//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Suniform_GetIDChucVu_Phongban_Result
    {
        public string tenPhongban { get; set; }
        public string Phongban_ID { get; set; }
        public string tenChucVu { get; set; }
        public string Nguoinhan { get; set; }
        public string Chucvunguoinhan { get; set; }
        public string Kho_ID { get; set; }
        public string chucVuID { get; set; }
    }
}
