//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Uniform_CapphatMFile
    {
        public string Filedinhkem_ID { get; set; }
        public string CapphatM_ID { get; set; }
        public string TenFile { get; set; }
        public string File_Path { get; set; }
        public int STT { get; set; }
        public Nullable<System.DateTime> NgayTai { get; set; }
        public Nullable<double> size { get; set; }
    
        public virtual Uniform_Capphat Uniform_Capphat { get; set; }
    }
}
