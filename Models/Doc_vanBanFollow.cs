//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Doc_vanBanFollow
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Doc_vanBanFollow()
        {
            this.Doc_vanBanFollowUser = new HashSet<Doc_vanBanFollowUser>();
            this.Doc_vanBanTaiLieu = new HashSet<Doc_vanBanTaiLieu>();
        }
    
        public string vanBanFollow_ID { get; set; }
        public string vanBanMasterID { get; set; }
        public string NhanSu_ID { get; set; }
        public string NhomDuyetVanBan_ID { get; set; }
        public string Noidung { get; set; }
        public bool IsType { get; set; }
        public bool IsDuyet { get; set; }
        public bool IsView { get; set; }
        public Nullable<System.DateTime> NgayView { get; set; }
        public Nullable<System.DateTime> NgayDuyet { get; set; }
        public int Thutu { get; set; }
        public string vanBanTrangthai_ID { get; set; }
        public bool hanXuly { get; set; }
        public Nullable<System.DateTime> hanXulyNgay { get; set; }
        public string NguoiTao { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }
        public string Noidungtralai { get; set; }
        public bool IsQT { get; set; }
        public string Hanhvi { get; set; }
    
        public virtual Doc_vanBanFollow Doc_vanBanFollow1 { get; set; }
        public virtual Doc_vanBanFollow Doc_vanBanFollow2 { get; set; }
        public virtual Doc_vanBanMaster Doc_vanBanMaster { get; set; }
        public virtual Doc_vanBanTrangthai Doc_vanBanTrangthai { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Doc_vanBanFollowUser> Doc_vanBanFollowUser { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Doc_vanBanTaiLieu> Doc_vanBanTaiLieu { get; set; }
    }
}
