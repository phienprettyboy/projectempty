//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Shrm_GetNSChucnang_Result
    {
        public string ThietlapChucnangNS_ID { get; set; }
        public string Congty_ID { get; set; }
        public string NhanSu_ID { get; set; }
        public string TenNut { get; set; }
        public string TenChucnang { get; set; }
    }
}
