//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class PlanTrip_GetKehoachCongtac_Result
    {
        public string ThoigianCT { get; set; }
        public string KehoachCT_ID { get; set; }
        public int STT { get; set; }
        public System.DateTime ThoigianTu { get; set; }
        public string Noidungcongviec { get; set; }
        public string Kethop { get; set; }
        public Nullable<System.DateTime> ThoigianDen { get; set; }
        public string KehoachCT_ID1 { get; set; }
        public string Congty_ID { get; set; }
        public string MauForm_ID { get; set; }
        public string SoKyhieu { get; set; }
        public string Noilap { get; set; }
        public Nullable<System.DateTime> Ngaylap { get; set; }
        public string Kinhgui { get; set; }
        public string Tentoila { get; set; }
        public string NhanSu_ID { get; set; }
        public string Noicongtac { get; set; }
        public string Thuchien { get; set; }
        public Nullable<double> Dutrukinhphi { get; set; }
        public string Phuongtiendilai { get; set; }
        public string Duyet1 { get; set; }
        public string Duyet2 { get; set; }
        public string Duyet3 { get; set; }
        public Nullable<int> Trangthai { get; set; }
        public Nullable<int> STT1 { get; set; }
        public string PT_Follow_ID { get; set; }
        public string Nguoikycuoicung { get; set; }
        public Nullable<bool> IsTraLai { get; set; }
        public string NguoiLap { get; set; }
        public string fullName { get; set; }
        public string ten { get; set; }
        public string anhDaiDien { get; set; }
        public string anhThumb { get; set; }
        public string tenChucVuNL { get; set; }
        public string tenPbnguoiLap { get; set; }
    }
}
