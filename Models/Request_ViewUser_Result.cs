//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Request_ViewUser_Result
    {
        public string Xem_ID { get; set; }
        public string RequestMaster_ID { get; set; }
        public string NguoiXem { get; set; }
        public Nullable<System.DateTime> NgayXem { get; set; }
        public bool IsApp { get; set; }
        public string token_id { get; set; }
        public string IP { get; set; }
        public Nullable<System.DateTime> NgayXemCuoi { get; set; }
        public bool IsAppCuoi { get; set; }
        public string token_idCuoi { get; set; }
        public string IPCuoi { get; set; }
        public int LanXem { get; set; }
        public string fullName { get; set; }
        public string ten { get; set; }
        public string anhDaiDien { get; set; }
        public string anhThumb { get; set; }
        public string tenToChuc { get; set; }
        public string tenChucVu { get; set; }
    }
}
