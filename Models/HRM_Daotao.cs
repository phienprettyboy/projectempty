//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class HRM_Daotao
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HRM_Daotao()
        {
            this.HRM_DaotaoPhongban = new HashSet<HRM_DaotaoPhongban>();
            this.HRM_DaotaoThamgia = new HashSet<HRM_DaotaoThamgia>();
            this.HRM_DaotaoThamgia1 = new HashSet<HRM_DaotaoThamgia>();
        }
    
        public string Daotao_ID { get; set; }
        public string Congty_ID { get; set; }
        public string NhomDaotao_ID { get; set; }
        public string Khoahoc_ID { get; set; }
        public System.DateTime Tungay { get; set; }
        public System.DateTime Denngay { get; set; }
        public string Thoiluong { get; set; }
        public int Doituong { get; set; }
        public int Hinhthuc { get; set; }
        public string DonviThuchien { get; set; }
        public string NguoiPhutrach { get; set; }
        public string NguoiPhutrach_ID { get; set; }
        public double Hocphi { get; set; }
        public double Chiphi { get; set; }
        public string GiangVien { get; set; }
        public string Diadiem { get; set; }
        public int Trangthai { get; set; }
        public string Ghichu { get; set; }
        public int STT { get; set; }
        public string Nguoitao { get; set; }
    
        public virtual HRM_TD_Khoahoc HRM_TD_Khoahoc { get; set; }
        public virtual HRM_TD_NhomDaotao HRM_TD_NhomDaotao { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_DaotaoPhongban> HRM_DaotaoPhongban { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_DaotaoThamgia> HRM_DaotaoThamgia { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_DaotaoThamgia> HRM_DaotaoThamgia1 { get; set; }
    }
}
