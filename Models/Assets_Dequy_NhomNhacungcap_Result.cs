//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Assets_Dequy_NhomNhacungcap_Result
    {
        public string NhomNCC_ID { get; set; }
        public string Name { get; set; }
        public string TenNhom { get; set; }
        public Nullable<bool> Trangthai { get; set; }
        public Nullable<int> STT { get; set; }
        public Nullable<int> Lvl { get; set; }
    }
}
