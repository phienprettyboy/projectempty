//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Home_TaskTeam_Result
    {
        public string tenCongty { get; set; }
        public string NhanSu_ID { get; set; }
        public string ten { get; set; }
        public string fullName { get; set; }
        public string phone { get; set; }
        public string mail { get; set; }
        public string anhDaiDien { get; set; }
        public int STT { get; set; }
        public string anhThumb { get; set; }
        public string tenToChuc { get; set; }
        public string tenChucVu { get; set; }
        public Nullable<int> soCV { get; set; }
        public Nullable<int> soCVDL { get; set; }
        public Nullable<int> soCVHT { get; set; }
        public Nullable<int> soCVQH { get; set; }
        public string congviecs { get; set; }
    }
}
