//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class GetLichKhoaByYear_Result
    {
        public string LichKhoa_ID { get; set; }
        public string Congty_ID { get; set; }
        public string Tuan { get; set; }
        public Nullable<System.DateTime> TuNgay { get; set; }
        public Nullable<System.DateTime> DenNgay { get; set; }
        public bool IsLock { get; set; }
        public string NguoiKhoa { get; set; }
        public Nullable<System.DateTime> NgayKhoa { get; set; }
        public bool IsAuto { get; set; }
        public bool IsAll { get; set; }
        public Nullable<int> ThuKhoa { get; set; }
        public Nullable<System.DateTime> GioKhoa { get; set; }
        public Nullable<int> ThuMo { get; set; }
        public Nullable<System.DateTime> GioMo { get; set; }
        public string anhDaiDien { get; set; }
        public string fullName { get; set; }
        public string tenToChuc { get; set; }
    }
}
