//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Srequest_Get_Formsign_Result
    {
        public string FormSign_ID { get; set; }
        public string Team_ID { get; set; }
        public string Form_ID { get; set; }
        public string GroupName { get; set; }
        public Nullable<int> IsTypeDuyet { get; set; }
        public bool IsSkipOffline { get; set; }
        public string IsTypeGroup { get; set; }
        public Nullable<bool> IsSkipGroup { get; set; }
        public int STT { get; set; }
        public string Nguoitao { get; set; }
        public string Ngaytao { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string fullName { get; set; }
        public string anhThumb { get; set; }
        public string ten { get; set; }
        public string tenChucVu { get; set; }
        public string tenPhongban { get; set; }
    }
}
