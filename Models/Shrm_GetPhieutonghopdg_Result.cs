//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Shrm_GetPhieutonghopdg_Result
    {
        public string PhieutonghopDG_ID { get; set; }
        public int Loaitonghop { get; set; }
        public Nullable<int> Thoigiantonghop { get; set; }
        public Nullable<int> Namtonghop { get; set; }
        public string Congty_ID { get; set; }
        public string Nguoilap { get; set; }
        public System.DateTime Ngaylap { get; set; }
        public string Sophieu { get; set; }
        public int Trangthai { get; set; }
        public int STT { get; set; }
        public string Follow_ID { get; set; }
        public string Nguoinhancuoi { get; set; }
        public string UyQuyen_ID { get; set; }
        public bool IsPhathanh { get; set; }
        public string tenCongty { get; set; }
    }
}
