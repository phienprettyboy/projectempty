//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class DataStore_ListMenu_Result
    {
        public string MenuItem_ID { get; set; }
        public string Name { get; set; }
        public string TenMenu { get; set; }
        public string Capcha { get; set; }
        public Nullable<int> STT { get; set; }
        public string Icon { get; set; }
        public Nullable<bool> IsShow { get; set; }
        public Nullable<int> Lvl { get; set; }
    }
}
