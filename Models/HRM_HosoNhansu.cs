//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class HRM_HosoNhansu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HRM_HosoNhansu()
        {
            this.HRM_BangCap = new HashSet<HRM_BangCap>();
            this.HRM_BaohiemXH = new HashSet<HRM_BaohiemXH>();
            this.HRM_CanhanThaydoi = new HashSet<HRM_CanhanThaydoi>();
            this.HRM_Danhgia = new HashSet<HRM_Danhgia>();
            this.HRM_HopdongLD = new HashSet<HRM_HopdongLD>();
            this.HRM_HosoNhansuACERuot = new HashSet<HRM_HosoNhansuACERuot>();
            this.HRM_HosoNhansuQTHD = new HashSet<HRM_HosoNhansuQTHD>();
            this.HRM_HosoTiepnhan = new HashSet<HRM_HosoTiepnhan>();
            this.HRM_KhenthuongKL = new HashSet<HRM_KhenthuongKL>();
            this.HRM_MatchAccount = new HashSet<HRM_MatchAccount>();
            this.HRM_Nghithaisan = new HashSet<HRM_Nghithaisan>();
            this.HRM_PhancongCongtac = new HashSet<HRM_PhancongCongtac>();
            this.HRM_QuatrinhCongtac = new HashSet<HRM_QuatrinhCongtac>();
            this.HRM_QuyetDinh = new HashSet<HRM_QuyetDinh>();
            this.HRM_SetNhansuForm = new HashSet<HRM_SetNhansuForm>();
        }
    
        public string HosoNhansu_ID { get; set; }
        public string Congty_ID { get; set; }
        public string MaUngtuyen_ID { get; set; }
        public string MaNhansu { get; set; }
        public string CapNhansu { get; set; }
        public Nullable<System.DateTime> NgayvaoCongty { get; set; }
        public string Hodem { get; set; }
        public string Ten { get; set; }
        public int Gioitinh { get; set; }
        public string Bidanh { get; set; }
        public string Tenthuonggoi { get; set; }
        public Nullable<System.DateTime> Ngaysinh { get; set; }
        public string Tai { get; set; }
        public string Nguyenquan { get; set; }
        public string NoidangkyHKTT { get; set; }
        public string NoioHientai { get; set; }
        public string LoaiChungthuc { get; set; }
        public string SoChungthuc { get; set; }
        public Nullable<System.DateTime> NgayChungthuc { get; set; }
        public string NoicapChungthuc { get; set; }
        public Nullable<System.DateTime> NgayhethanChungthuc { get; set; }
        public string DTNharieng { get; set; }
        public string Didong { get; set; }
        public string Email { get; set; }
        public string Quoctich { get; set; }
        public string Dantoc { get; set; }
        public string Tongiao { get; set; }
        public Nullable<int> Tinhtranghonnhan { get; set; }
        public string ThanhphanGiadinh { get; set; }
        public string ThanhphanBanthan { get; set; }
        public string TrinhdoVanhoa { get; set; }
        public string TrinhdoNgoaingu { get; set; }
        public string Xeploai { get; set; }
        public string Noidaotao { get; set; }
        public string Khoa { get; set; }
        public string Chuyennganh { get; set; }
        public string Loaihinh { get; set; }
        public Nullable<int> Namtotnghiep { get; set; }
        public string Trinhdochuyenmon { get; set; }
        public Nullable<System.DateTime> NgayketnapDang { get; set; }
        public string NoiketnapDang { get; set; }
        public Nullable<System.DateTime> NgayketnapDoan { get; set; }
        public string NoiketnapDoan { get; set; }
        public string TinhtrangSuckhoe { get; set; }
        public string Chieucao { get; set; }
        public string Cannang { get; set; }
        public string NghenghiepChuyenmon { get; set; }
        public string Capbac { get; set; }
        public Nullable<double> LuongchinhHiennay { get; set; }
        public Nullable<System.DateTime> Ngaynhapngu { get; set; }
        public Nullable<System.DateTime> Ngayxuatngu { get; set; }
        public string Lydo { get; set; }
        public string HotenBo { get; set; }
        public Nullable<int> NamsinhBo { get; set; }
        public string NghenghiepBo { get; set; }
        public string TruocCMT8Bo { get; set; }
        public string TrongkhangchienBo { get; set; }
        public string Tunam1995Bo { get; set; }
        public string HotenMe { get; set; }
        public Nullable<int> NamsinhMe { get; set; }
        public string NghenghiepMe { get; set; }
        public string TruocCMT8Me { get; set; }
        public string TrongkhangchienMe { get; set; }
        public string Tunam1995Me { get; set; }
        public string Khenthuong { get; set; }
        public string Kyluat { get; set; }
        public string KhicanBaotinCho { get; set; }
        public string TheoSoDienthoai { get; set; }
        public string TheoDiachi { get; set; }
        public string Anhdaidien { get; set; }
        public string Noikhai { get; set; }
        public System.DateTime NgayKhai { get; set; }
        public int TrangthaiNS { get; set; }
        public bool TrangthaiHS { get; set; }
        public int STT { get; set; }
        public string Nguoitao { get; set; }
        public Nullable<System.DateTime> Ngaynghiviec { get; set; }
        public string Lydonghiviec { get; set; }
        public string Ghichunghiviec { get; set; }
        public string MaChamcong { get; set; }
        public bool IsDelete { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_BangCap> HRM_BangCap { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_BaohiemXH> HRM_BaohiemXH { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_CanhanThaydoi> HRM_CanhanThaydoi { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_Danhgia> HRM_Danhgia { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_HopdongLD> HRM_HopdongLD { get; set; }
        public virtual HRM_HosoUngTuyen HRM_HosoUngTuyen { get; set; }
        public virtual HT_TD_Congty HT_TD_Congty { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_HosoNhansuACERuot> HRM_HosoNhansuACERuot { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_HosoNhansuQTHD> HRM_HosoNhansuQTHD { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_HosoTiepnhan> HRM_HosoTiepnhan { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_KhenthuongKL> HRM_KhenthuongKL { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_MatchAccount> HRM_MatchAccount { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_Nghithaisan> HRM_Nghithaisan { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_PhancongCongtac> HRM_PhancongCongtac { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_QuatrinhCongtac> HRM_QuatrinhCongtac { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_QuyetDinh> HRM_QuyetDinh { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_SetNhansuForm> HRM_SetNhansuForm { get; set; }
    }
}
