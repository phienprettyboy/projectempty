//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Doc_NhomDuyetPB
    {
        public string NhomDuyetPB_ID { get; set; }
        public string NhomDuyetVanBan_ID { get; set; }
        public string NguoiDuyet { get; set; }
        public string NguoiTao { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }
        public bool IsActive { get; set; }
        public string Phongban_ID { get; set; }
    
        public virtual Doc_NhomDuyet Doc_NhomDuyet { get; set; }
    }
}
