//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Shrm_ListTDHS_Result
    {
        public string ChungthuCN_ID { get; set; }
        public string Congty_ID { get; set; }
        public string TenChungthuc { get; set; }
        public bool Trangthai { get; set; }
        public int STT { get; set; }
    }
}
