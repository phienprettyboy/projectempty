//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Asset_PhieuThatthoatD
    {
        public string PhieuThatthoatD_ID { get; set; }
        public string PhieuThatthoatM_ID { get; set; }
        public string TheTaisan_ID { get; set; }
        public string TenTaisan { get; set; }
        public string SoSerial { get; set; }
        public string Taisan_ID { get; set; }
        public string NuocSanxuat { get; set; }
        public Nullable<int> NamSanxuat { get; set; }
        public Nullable<int> Thoigiansudung { get; set; }
        public string Quycach { get; set; }
        public double Giatri { get; set; }
        public double Giatriconlai { get; set; }
        public double Giatridenbu { get; set; }
        public string Tinhtrang { get; set; }
        public string Ghichu { get; set; }
        public int STT { get; set; }
    
        public virtual Asset_PhieuThatthoatM Asset_PhieuThatthoatM { get; set; }
        public virtual Asset_TheTaiSan Asset_TheTaiSan { get; set; }
    }
}
