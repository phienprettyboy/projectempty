//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Calendar_GetNguoithamdu_Result
    {
        public string Nguoithamdu_ID { get; set; }
        public string Thamdu_ID { get; set; }
        public string NhanSu_ID { get; set; }
        public string fullName { get; set; }
        public string ten { get; set; }
        public string anhThumb { get; set; }
    }
}
