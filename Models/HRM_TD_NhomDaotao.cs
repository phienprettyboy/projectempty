//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class HRM_TD_NhomDaotao
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HRM_TD_NhomDaotao()
        {
            this.HRM_Daotao = new HashSet<HRM_Daotao>();
        }
    
        public string NhomDaotao_ID { get; set; }
        public string Congty_ID { get; set; }
        public string TenNhom { get; set; }
        public bool Trangthai { get; set; }
        public int STT { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_Daotao> HRM_Daotao { get; set; }
    }
}
