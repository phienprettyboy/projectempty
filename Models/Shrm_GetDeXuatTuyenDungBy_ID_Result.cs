//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Shrm_GetDeXuatTuyenDungBy_ID_Result
    {
        public string Dexuat_ID { get; set; }
        public string Congty_ID { get; set; }
        public string Vitri_ID { get; set; }
        public string Phongban_ID { get; set; }
        public int Soluong { get; set; }
        public string Noilamviec_ID { get; set; }
        public Nullable<double> Luongtu { get; set; }
        public Nullable<double> Luongden { get; set; }
        public System.DateTime HantuyenTu { get; set; }
        public System.DateTime HantuyenDen { get; set; }
        public string LydoTuyen { get; set; }
        public string Motacongviec { get; set; }
        public Nullable<int> TuoiTu { get; set; }
        public Nullable<int> TuoiDen { get; set; }
        public Nullable<int> Gioitinh { get; set; }
        public string Chieucao { get; set; }
        public string Cannang { get; set; }
        public string Trinhdo_ID { get; set; }
        public string Kinhnghiem_ID { get; set; }
        public string Nguoilap_ID { get; set; }
        public System.DateTime Ngaylap { get; set; }
        public int Trangthai { get; set; }
        public string Tendexuat { get; set; }
        public Nullable<int> Hinhthuclamviec { get; set; }
        public string Follow_ID { get; set; }
        public string Nguoikycuoicung { get; set; }
        public Nullable<bool> IsTraLai { get; set; }
        public Nullable<bool> isRoleDuyet { get; set; }
        public string fullName { get; set; }
        public string anhThumb { get; set; }
        public string ten { get; set; }
        public string tenChucVu { get; set; }
        public string tenPhongban { get; set; }
        public string TenVitri { get; set; }
        public string TenDiadanh { get; set; }
        public string TenTrinhdohocvan { get; set; }
    }
}
