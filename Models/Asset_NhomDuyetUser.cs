//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Asset_NhomDuyetUser
    {
        public string NhomDuyetUser_ID { get; set; }
        public string NhomDuyet_ID { get; set; }
        public string NguoiDuyet { get; set; }
        public string NguoiTao { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public int STT { get; set; }
    
        public virtual Asset_NhomDuyet Asset_NhomDuyet { get; set; }
    }
}
