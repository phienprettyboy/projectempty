//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Task_Tag
    {
        public string DuanTagID { get; set; }
        public string DuanID { get; set; }
        public string CommentID { get; set; }
        public string NguoiDuocTag { get; set; }
        public string NguoiTao { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }
    
        public virtual Task_Comment Task_Comment { get; set; }
    }
}
