//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class HRM_TonghopTuyendung
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public HRM_TonghopTuyendung()
        {
            this.HRM_KetquaTrungtuyen = new HashSet<HRM_KetquaTrungtuyen>();
            this.HRM_TonghopTuyendungSend = new HashSet<HRM_TonghopTuyendungSend>();
        }
    
        public string TonghopTuyendung_ID { get; set; }
        public string Sophieu { get; set; }
        public System.DateTime Ngaylap { get; set; }
        public string Congty_ID { get; set; }
        public string Nguoilap { get; set; }
        public Nullable<System.DateTime> Ngayxulycuoi { get; set; }
        public int Trangthai { get; set; }
        public int STT { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_KetquaTrungtuyen> HRM_KetquaTrungtuyen { get; set; }
        public virtual HT_TD_Congty HT_TD_Congty { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<HRM_TonghopTuyendungSend> HRM_TonghopTuyendungSend { get; set; }
    }
}
