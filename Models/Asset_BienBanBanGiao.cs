//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Asset_BienBanBanGiao
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Asset_BienBanBanGiao()
        {
            this.Asset_BienBanBanGiaoFile = new HashSet<Asset_BienBanBanGiaoFile>();
            this.Asset_BienBanTaiSan = new HashSet<Asset_BienBanTaiSan>();
        }
    
        public string PhieuBangiao_ID { get; set; }
        public string Congty_ID { get; set; }
        public string TenCongty { get; set; }
        public string Sophieu { get; set; }
        public System.DateTime Ngaylap { get; set; }
        public string NguoiBangiao_ID { get; set; }
        public string NguoiBangiao { get; set; }
        public string ChucvuNguoiBG { get; set; }
        public string Phongban_ID { get; set; }
        public string NguoiXacnhan_ID { get; set; }
        public string NguoiXacnhan { get; set; }
        public string ChucvuNguoiXN { get; set; }
        public string PhongbanNguoiXN { get; set; }
        public string NguoiNhan_ID { get; set; }
        public string NguoiNhan { get; set; }
        public string ChucvuNguoinhan { get; set; }
        public string PhongbanNN_ID { get; set; }
        public Nullable<int> Loaibienban { get; set; }
        public Nullable<System.DateTime> Ngayduyet { get; set; }
        public string BBG_Status { get; set; }
        public bool IsHuy { get; set; }
        public string NguoiHuy { get; set; }
        public Nullable<System.DateTime> NgayHuy { get; set; }
        public bool IsUse { get; set; }
        public Nullable<System.DateTime> NgayNgung { get; set; }
        public string PhieuDexuatM_ID { get; set; }
        public Nullable<int> LoaiCapphat { get; set; }
        public Nullable<System.DateTime> Ngayxacnhan { get; set; }
        public bool IsNhanho { get; set; }
        public bool IsXacnhan { get; set; }
        public string PhieuSuachuaM_ID { get; set; }
        public string QT_ID { get; set; }
        public Nullable<bool> IsXacnhan3 { get; set; }
        public string NhomDuyet_ID { get; set; }
        public string Nguoiduyet_ID { get; set; }
        public Nullable<bool> IsQuytrinh { get; set; }
        public string QT_Nhom_ID { get; set; }
        public Nullable<bool> Bangiaocanhan { get; set; }
        public string GhichuPrint { get; set; }
        public string BophanQL_ID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Asset_BienBanBanGiaoFile> Asset_BienBanBanGiaoFile { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Asset_BienBanTaiSan> Asset_BienBanTaiSan { get; set; }
    }
}
