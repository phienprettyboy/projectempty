//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class HRM_TD_NgaynghileD
    {
        public string NgaynghileD_ID { get; set; }
        public string NgaynghileM_ID { get; set; }
        public System.DateTime Ngaynghi { get; set; }
        public bool Tinhcong { get; set; }
        public bool Lap { get; set; }
        public string CongtyApdung_ID { get; set; }
        public int STT { get; set; }
    
        public virtual HRM_TD_NgaynghileM HRM_TD_NgaynghileM { get; set; }
    }
}
