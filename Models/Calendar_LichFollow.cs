//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Calendar_LichFollow
    {
        public string LichhopFollow_ID { get; set; }
        public string LichhopTuan_ID { get; set; }
        public string NhanSu_ID { get; set; }
        public string NhomDuyetLich_ID { get; set; }
        public string Noidung { get; set; }
        public bool IsType { get; set; }
        public bool IsDuyet { get; set; }
        public bool IsView { get; set; }
        public Nullable<System.DateTime> NgayView { get; set; }
        public Nullable<System.DateTime> NgayDuyet { get; set; }
        public string NguoiTao { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }
        public string QT_ID { get; set; }
        public Nullable<int> Thutu { get; set; }
        public string Congty_ID { get; set; }
    
        public virtual Calendar_LichhopTuan Calendar_LichhopTuan { get; set; }
    }
}
