//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Task_TagDuanCongViec
    {
        public string TagDuanID { get; set; }
        public string TaskTagKey_ID { get; set; }
        public string DuanID { get; set; }
        public string CongviecID { get; set; }
        public string Nguoithem { get; set; }
        public System.DateTime Ngaythem { get; set; }
        public int STT { get; set; }
    
        public virtual Task_Congviec Task_Congviec { get; set; }
        public virtual Task_Duan Task_Duan { get; set; }
        public virtual Task_TagKey Task_TagKey { get; set; }
    }
}
