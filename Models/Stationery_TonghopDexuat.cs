//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Stationery_TonghopDexuat
    {
        public string TonghopDexuat_ID { get; set; }
        public string TonghopM_ID { get; set; }
        public string PhieudexuatM_ID { get; set; }
        public int STT { get; set; }
    
        public virtual Stationery_TonghopM Stationery_TonghopM { get; set; }
    }
}
