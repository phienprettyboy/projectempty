//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class App_Media_ListAlbum_Result
    {
        public string Albums_ID { get; set; }
        public string AlbumsTen { get; set; }
        public string Nguoitao { get; set; }
        public System.DateTime Ngaytao { get; set; }
        public string Mota { get; set; }
        public int STT { get; set; }
        public double LuotView { get; set; }
        public string Congty_ID { get; set; }
        public string fullName { get; set; }
        public string ten { get; set; }
        public string anhDaiDien { get; set; }
        public string anhThumb { get; set; }
        public string tenToChuc { get; set; }
        public string tenChucVu { get; set; }
        public Nullable<int> IsPhoto { get; set; }
        public string Duongdan { get; set; }
        public Nullable<int> Chiase { get; set; }
    }
}
