//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Request_Download
    {
        public string Download_ID { get; set; }
        public string FileID { get; set; }
        public string NguoiTai { get; set; }
        public Nullable<System.DateTime> NgayTai { get; set; }
        public bool IsApp { get; set; }
        public string token_id { get; set; }
        public string IP { get; set; }
        public Nullable<System.DateTime> NgayTaiCuoi { get; set; }
        public bool IsAppCuoi { get; set; }
        public string token_idCuoi { get; set; }
        public string IPCuoi { get; set; }
        public int Lantai { get; set; }
    
        public virtual Request_MasterFile Request_MasterFile { get; set; }
    }
}
