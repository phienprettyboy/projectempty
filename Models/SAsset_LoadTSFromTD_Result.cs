//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class SAsset_LoadTSFromTD_Result
    {
        public string Taisan_ID { get; set; }
        public string TenTaisan { get; set; }
        public string LoaiTaisan_ID { get; set; }
        public string Donvitinh_ID { get; set; }
        public string TenDonvi { get; set; }
        public int Thangkhauhao { get; set; }
    }
}
