//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Sdoc_ListVBLog_data_Result
    {
        public string vanBanFollow_ID { get; set; }
        public string vanBanMasterID { get; set; }
        public string NhanSu_ID { get; set; }
        public string NhomDuyetVanBan_ID { get; set; }
        public string Noidung { get; set; }
        public bool IsType { get; set; }
        public bool IsDuyet { get; set; }
        public bool IsView { get; set; }
        public Nullable<System.DateTime> NgayView { get; set; }
        public Nullable<System.DateTime> NgayDuyet { get; set; }
        public int Thutu { get; set; }
        public string vanBanTrangthai_ID { get; set; }
        public bool hanXuly { get; set; }
        public Nullable<System.DateTime> hanXulyNgay { get; set; }
        public string NguoiTao { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }
        public string Noidungtralai { get; set; }
        public bool IsQT { get; set; }
        public string Hanhvi { get; set; }
        public string NhomDuyetVanBan_ID1 { get; set; }
        public string fullName { get; set; }
        public string anhDaiDien { get; set; }
        public string ten { get; set; }
        public string anhThumb { get; set; }
        public string enFullName { get; set; }
        public string tenChucVu { get; set; }
        public string tenToChuc { get; set; }
        public string froles { get; set; }
        public string tenTrangthaiNext { get; set; }
        public string vanBanTrangthaiNext_ID { get; set; }
        public string Users { get; set; }
    }
}
