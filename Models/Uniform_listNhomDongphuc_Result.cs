//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Uniform_listNhomDongphuc_Result
    {
        public string NhomDP_ID { get; set; }
        public string TenNhom { get; set; }
        public string Parent_ID { get; set; }
        public string Congty_ID { get; set; }
        public bool Trangthai { get; set; }
        public int STT { get; set; }
    }
}
