//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Shrm_ListUyquyenPTH_Result
    {
        public string UyQuyen_ID { get; set; }
        public string Congty_ID { get; set; }
        public string Noidung { get; set; }
        public string NguoiUQ_ID { get; set; }
        public string NguoiduocUQ_ID { get; set; }
        public System.DateTime NgayUQ { get; set; }
        public Nullable<System.DateTime> NgayhetUQ { get; set; }
        public bool Trangthai { get; set; }
        public int STT { get; set; }
    }
}
