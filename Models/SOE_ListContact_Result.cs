//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class SOE_ListContact_Result
    {
        public bool HienThiEmail { get; set; }
        public bool HienThiMobile { get; set; }
        public string NhanSu_ID { get; set; }
        public string ten { get; set; }
        public string hoDem { get; set; }
        public Nullable<System.DateTime> ngaySinh { get; set; }
        public string tenTruyCap { get; set; }
        public string maNhanSu { get; set; }
        public string anhThumb { get; set; }
        public string anhDaiDien { get; set; }
        public string chucVuID { get; set; }
        public string phone { get; set; }
        public string mail { get; set; }
        public int gioiTinh { get; set; }
        public string nickName { get; set; }
        public int Status { get; set; }
        public string fullName { get; set; }
        public string tenCongty { get; set; }
        public string roles { get; set; }
        public string froles { get; set; }
        public string phongbans { get; set; }
        public string tenToChuc { get; set; }
        public string tenChucVu { get; set; }
        public string chucvus { get; set; }
        public string kiemnghiems { get; set; }
        public string thutuPhongBan { get; set; }
        public int STT { get; set; }
    }
}
