//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Snews_Listnhomduyettb_Result
    {
        public string NhomDuyetThongBao_ID { get; set; }
        public string NhomDuyetThongBao_Ten { get; set; }
        public string tenPhongban { get; set; }
        public Nullable<bool> IsDefault { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }
        public Nullable<int> countUser { get; set; }
    }
}
