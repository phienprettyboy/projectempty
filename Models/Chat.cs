//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Chat
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Chat()
        {
            this.Chat_Member = new HashSet<Chat_Member>();
            this.Chat_Message = new HashSet<Chat_Message>();
        }
    
        public string ChatID { get; set; }
        public string tenNhom { get; set; }
        public Nullable<int> loaiNhom { get; set; }
        public System.DateTime ngayLap { get; set; }
        public string nguoiLap { get; set; }
        public string nguoiChat { get; set; }
        public int trangThai { get; set; }
        public string hinhNen { get; set; }
        public System.DateTime ngayCapNhat { get; set; }
        public bool nhom { get; set; }
        public string congtyID { get; set; }
        public string DuanID { get; set; }
    
        public virtual HT_KB_Nhansu HT_KB_Nhansu { get; set; }
        public virtual HT_KB_Nhansu HT_KB_Nhansu1 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Chat_Member> Chat_Member { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Chat_Message> Chat_Message { get; set; }
    }
}
