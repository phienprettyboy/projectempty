//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class OS_SendHub
    {
        public string sendHubID { get; set; }
        public string nguoiGui { get; set; }
        public string tenNguoiGui { get; set; }
        public string tenNguoiNhan { get; set; }
        public string nguoiNhan { get; set; }
        public string icon { get; set; }
        public string tieuDe { get; set; }
        public string noiDung { get; set; }
        public string noiDungNoti { get; set; }
        public int loai { get; set; }
        public Nullable<System.DateTime> ngayGui { get; set; }
        public bool doc { get; set; }
        public Nullable<System.DateTime> ngayDoc { get; set; }
        public string idKey { get; set; }
        public string webView { get; set; }
        public Nullable<bool> App { get; set; }
        public string groupID { get; set; }
        public string DuanID { get; set; }
        public string CongviecID { get; set; }
        public Nullable<byte> IsType { get; set; }
        public Nullable<bool> IsDelete { get; set; }
    }
}
