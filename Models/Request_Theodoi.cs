//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Request_Theodoi
    {
        public string Theodoi_ID { get; set; }
        public string RequestMaster_ID { get; set; }
        public string NhanSu_ID { get; set; }
        public string Nguoitao { get; set; }
        public Nullable<System.DateTime> Ngaytao { get; set; }
        public Nullable<bool> Trangthai { get; set; }
        public Nullable<int> IsType { get; set; }
        public Nullable<int> STT { get; set; }
        public Nullable<bool> IsApp { get; set; }
        public string token_id { get; set; }
        public string IP { get; set; }
        public bool IsNoty { get; set; }
        public Nullable<bool> IsMail { get; set; }
        public string Nguoicapnhat { get; set; }
        public Nullable<System.DateTime> Ngaycapnhat { get; set; }
        public string tokencapnhat_id { get; set; }
        public string IPCapnhat { get; set; }
    
        public virtual Request_Master Request_Master { get; set; }
    }
}
