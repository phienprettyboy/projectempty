//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Sadmin_Getdiadiem_Result
    {
        public string Diadiem_ID { get; set; }
        public string Diadiem_Ten { get; set; }
        public string Diadiem_Tenen { get; set; }
        public bool on_off { get; set; }
        public int thutu { get; set; }
        public string Congty_ID { get; set; }
        public System.DateTime Ngaytao { get; set; }
        public string Nguoitao { get; set; }
        public bool ThuocCongty { get; set; }
    }
}
