//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Srequest_Get_Khaibaoteam_Result
    {
        public string Team_ID { get; set; }
        public string NhomTeam_ID { get; set; }
        public string Team_Name { get; set; }
        public string Mota { get; set; }
        public string Nguoiquanly { get; set; }
        public string Nguoitao { get; set; }
        public Nullable<System.DateTime> Ngaytao { get; set; }
        public Nullable<int> Isloai { get; set; }
        public string Congty_ID { get; set; }
        public Nullable<bool> Trangthai { get; set; }
        public Nullable<int> STT { get; set; }
        public string fullName { get; set; }
        public string anhThumb { get; set; }
        public string ten { get; set; }
        public string tenChucVu { get; set; }
        public string tenPhongban { get; set; }
    }
}
