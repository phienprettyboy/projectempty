//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Task_Duan_ListTailieuGanday_Result
    {
        public string TailieuID { get; set; }
        public string ThumucID { get; set; }
        public string FileID { get; set; }
        public string Duongdan { get; set; }
        public string CongviecID { get; set; }
        public string DuanID { get; set; }
        public string Tenfile { get; set; }
        public string NguoiTao { get; set; }
        public Nullable<System.DateTime> NgayTao { get; set; }
        public bool IsPublic { get; set; }
        public int IsType { get; set; }
        public string Dinhdang { get; set; }
        public Nullable<double> Dungluong { get; set; }
        public bool IsDelete { get; set; }
        public Nullable<bool> IsImage { get; set; }
        public string fullName { get; set; }
        public string ten { get; set; }
        public string anhDaiDien { get; set; }
        public string anhThumb { get; set; }
        public string tenToChuc { get; set; }
        public string tenChucVu { get; set; }
    }
}
