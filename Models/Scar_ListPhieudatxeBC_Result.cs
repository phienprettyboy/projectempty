//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Scar_ListPhieudatxeBC_Result
    {
        public string PhieuDX_ID { get; set; }
        public string PhieuDX_Sophieu { get; set; }
        public string PhieuDX_Nguoisudungxe { get; set; }
        public string PhieuDX_ChucvuNguoidung { get; set; }
        public string PhieuDX_PhongbanNguoidung { get; set; }
        public System.DateTime PhieuDX_Ngaylap { get; set; }
        public string PhieuDX_NguoiDX { get; set; }
        public string PhieuDX_TenNguoiDX { get; set; }
        public string PhieuDX_ChucvuNguoiDX { get; set; }
        public string PhieuDX_Phongban { get; set; }
        public System.DateTime PhieuDX_FromDate { get; set; }
        public System.DateTime PhieuDX_ToDate { get; set; }
        public string PhieuDX_Bienxe { get; set; }
        public string PhieuDX_Nguoilai { get; set; }
        public string PhieuDX_Loaixe { get; set; }
        public Nullable<int> PhieuDX_Chongoi { get; set; }
        public string PhieuDX_Lydo { get; set; }
        public string PhieuDX_Noicongtac { get; set; }
        public string PhieuDX_Hanhtrinh { get; set; }
        public string PhieuDX_Ghichu { get; set; }
        public string Congty_ID { get; set; }
        public string PhieuDX_OtoID { get; set; }
        public string PhieuDX_Dienthoai { get; set; }
        public int thutu { get; set; }
        public bool on_off { get; set; }
        public string PhieuDX_Trangthai { get; set; }
        public Nullable<bool> IsHuy { get; set; }
        public string Nguoihuy { get; set; }
        public Nullable<System.DateTime> Ngayhuy { get; set; }
        public Nullable<int> Songuoi { get; set; }
        public Nullable<int> VeCauduong { get; set; }
        public Nullable<int> ChiphiXangxe { get; set; }
        public Nullable<int> SoKm { get; set; }
        public Nullable<int> Thayphieu { get; set; }
        public string QT_ID { get; set; }
        public Nullable<bool> IsQuytrinh { get; set; }
        public string Nhomduyet_ID { get; set; }
        public string Nguoiduyet_ID { get; set; }
        public string Nguoigui_ID { get; set; }
        public Nullable<System.DateTime> Ngaygui { get; set; }
        public string QT_NhomID { get; set; }
        public string NhomduyetPB_ID { get; set; }
        public string Nguoiduyetcuoi { get; set; }
        public string Nhomduyetcuoi { get; set; }
        public string QT_NhomIDcuoi { get; set; }
        public string NhanSu_ID { get; set; }
        public string fullName { get; set; }
    }
}
