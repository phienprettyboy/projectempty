//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class HRM_QuatrinhBH
    {
        public string QuatrinhBH_ID { get; set; }
        public string BaohiemXH_ID { get; set; }
        public Nullable<int> TuThang { get; set; }
        public Nullable<int> TuNam { get; set; }
        public Nullable<bool> Hinhthuc { get; set; }
        public string Lydo { get; set; }
        public Nullable<double> Mucdong { get; set; }
        public Nullable<double> CongtyDong { get; set; }
        public Nullable<double> NguoilaodongNop { get; set; }
        public Nullable<int> STT { get; set; }
    
        public virtual HRM_BaohiemXH HRM_BaohiemXH { get; set; }
    }
}
