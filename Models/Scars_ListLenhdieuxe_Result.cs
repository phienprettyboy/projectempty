//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Scars_ListLenhdieuxe_Result
    {
        public string NhanSu_ID { get; set; }
        public string LenhDX_Nguoitao { get; set; }
        public System.DateTime LenhDX_FromDate { get; set; }
        public Nullable<System.DateTime> LenhDX_ToDate { get; set; }
        public Nullable<System.DateTime> Hanxuly { get; set; }
        public string tenChucVu { get; set; }
        public string tenPhongban { get; set; }
        public string LenhDX_ID { get; set; }
        public string Nhomduyet_ID { get; set; }
        public string QT_NhomID { get; set; }
        public string ten { get; set; }
        public string nguoisd { get; set; }
        public string anhThumb { get; set; }
        public string LenhDX_So { get; set; }
        public string Congty_ID { get; set; }
        public string LenhDX_Noicongtac { get; set; }
        public string LenhDX_Nguoilaixe { get; set; }
        public string LenhDX_Bienso { get; set; }
        public Nullable<bool> IsQuytrinh { get; set; }
        public string PhieuDX_ID { get; set; }
        public string QT_ID { get; set; }
        public string Nhomduyet { get; set; }
        public string DieuxeFollow_ID { get; set; }
        public string PhieuDX_Sophieu { get; set; }
        public System.DateTime PhieuDX_Ngaylap { get; set; }
        public string nguoigui { get; set; }
        public Nullable<System.DateTime> Ngaygui { get; set; }
        public Nullable<System.DateTime> LenhDX_Ngaylap { get; set; }
        public string LenhDX_Trangthai { get; set; }
        public Nullable<int> Loaiduyet { get; set; }
        public int thutu { get; set; }
        public Nullable<int> LenhDX_LoaiDat { get; set; }
        public Nullable<int> Thaylenh { get; set; }
        public string Nhomduyetcuoi { get; set; }
        public string QT_NhomIDcuoi { get; set; }
        public string Nguoiduyetcuoi { get; set; }
        public Nullable<int> LoaiDuyetcuoi { get; set; }
        public Nullable<bool> duyetlai { get; set; }
        public Nullable<bool> TrangthaiQT { get; set; }
        public Nullable<int> Quahan { get; set; }
        public Nullable<int> Quangay { get; set; }
    }
}
