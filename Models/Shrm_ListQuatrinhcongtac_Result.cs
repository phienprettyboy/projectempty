//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Shrm_ListQuatrinhcongtac_Result
    {
        public string QuatrinhCongtac_ID { get; set; }
        public string Congty_ID { get; set; }
        public int Tuthang { get; set; }
        public int Tunam { get; set; }
        public int Denthang { get; set; }
        public int Dennam { get; set; }
        public string HosoNhansu_ID { get; set; }
        public string Vitricongtac { get; set; }
        public string Donvicongtac { get; set; }
        public string Noicongtac { get; set; }
        public string Nguoithamchieu { get; set; }
        public string SDTNguoithamchieu { get; set; }
        public string Ghichu { get; set; }
        public int STT { get; set; }
        public string Nguoitao { get; set; }
        public string ten { get; set; }
        public string anhThumb { get; set; }
        public string fullName { get; set; }
        public string tenChucVuNS { get; set; }
        public string tenToChuc { get; set; }
        public string Files { get; set; }
    }
}
