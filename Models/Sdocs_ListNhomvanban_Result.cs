//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SRequest.Models
{
    using System;
    
    public partial class Sdocs_ListNhomvanban_Result
    {
        public string NhomvanbanID { get; set; }
        public string tenNhom { get; set; }
        public string maSo { get; set; }
        public int thutu { get; set; }
        public bool on_off { get; set; }
        public string Congty_ID { get; set; }
        public int nam { get; set; }
        public double soHienTai { get; set; }
    }
}
