﻿angular.module('ManageteamCtr', [])
    .controller("ManageteamCtr", ['$scope', '$rootScope', '$http', '$filter', 'Upload', '$interval', '$state', '$stateParams', '$sce', function ($scope, $rootScope, $http, $filter, Upload, $interval, $state, $stateParams, $sce) {
        // Setup ======================================
        $scope.trustAsHtml = function (html) {
            return $sce.trustAsHtml(html);
        };
        $scope.filterValue = function ($event) {
            if (isNaN(String.fromCharCode($event.keyCode))) {
                $event.preventDefault();
            }
        };
        function groupBy(list, props) {
            return list.reduce((a, b) => {
                (a[b[props]] = a[b[props]] || []).push(b);
                return a;
            }, {});
        }
        $rootScope.bgColor = [
            "#F8E69A", "#AFDFCF", "#F4B2A3", "#9A97EC", "#CAE2B0", "#8BCFFB", "#CCADD7"
        ];
        var ColorChats = [
            {
                "color": "#04D215"
            },
            {
                "color": "#B0DE09"
            }, {
                "color": "#0D52D1"
            },
            {
                "color": "#8A0CCF"
            },
            {
                "color": "#F8FF01"
            },
            {
                "color": "#FCD202"
            },
            {
                "color": "#FF9E01"
            },
            {
                "color": "#FF6600"
            },

            {
                "color": "#FF0F00"
            }, {
                "color": "#CD0D74"
            }, {
                "color": "#754DEB"
            }, {
                "color": "#FFCCFF"
            }, {
                "color": "#FF99CC"
            }, {
                "color": "#FF6699"
            }, {
                "color": "#FF3366"
            }, {
                "color": "#FF3333"
            }, {
                "color": "#990066"
            }, {
                "color": "#660066"
            }, {
                "color": "#660033"
            }, {
                "color": "#660000"
            }, {
                "color": "#DDDDDD"
            }, {
                "color": "#999999"
            }, {
                "color": "#333333"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }
        ];
        $scope.opition = {
            year: new Date().getFullYear(),
            month: new Date().getMonth() + 1,
            Congty_ID: null,
            tenCongty: null,
            Team_ID: null,
            Team_Name: null,
            NhanSu_ID: $rootScope.login.u.NhanSu_ID,
            forms: [],
            teams: [],
            typeforms: [],
            ngaylaptu: null,
            ngaylapden: null,
            ngayhttu: null,
            ngayhtden: null,
            Status: null,
            roles: null,
            nhansus: [],
            isType: 1,
            isTypeGroup: 3,
            isQuaHanXL: null,
            statusDX: null,
            statusDG: null,
            statusQH: null,
            View: 1,
            search: "",
            numPerPage: 100,
            currentPage: 1,
            sort: 5,
            ob: "desc",
            Total: 0,
            Pages: [100, 250, 500, 1000]
        };
        $scope.List_Status = [
            { value: 0, text: 'Mới tạo' },
            { value: 1, text: 'Đang trình' },
            { value: 2, text: 'Hoàn thành' },
        ];
        $scope.List_Roles = [
            { value: 1, text: 'Người tạo' },
            { value: 2, text: 'Người theo dõi' },
            { value: 3, text: 'Người quản lý' },
            { value: 4, text: 'Người duyệt' },
        ];
        $scope.ListSort = [
            { id: 0, text: 'A-Z' },
            { id: 1, text: 'Z-A' },
            { id: 2, text: 'Ngày lập cũ nhất' },
            { id: 3, text: 'Ngày lập mới nhất' },
            { id: 4, text: 'Ngày tạo cũ nhất' },
            { id: 5, text: 'Ngày tạo mới nhất' },
            { id: 6, text: 'Ngày cập nhật cũ nhất' },
            { id: 7, text: 'Ngày cập nhật mới nhất' },
            { id: 8, text: 'Số ngày quá hạn nhiều nhất' },
            { id: 9, text: 'Số ngày quá hạn ít nhất' },
        ];
        $scope.setSort = function (s) {
            $scope.opition.sort = s.id;
            $scope.Bind_List_Report_Dexuat(true);
        };
        $scope.IsShowMap = true;
        //=============================================

        //Filter ====================================================================
        $scope.goCongty = function (id) {
            $scope.opition.Congty_ID = id;
            $scope.Bind_List_Tudien();
        };

        $scope.completeForm_AdvSearch = function (string, m, $event, i) {
            $scope.opition.Focus1 = false;
            $scope.opition.Focus2 = false;
            $scope.opition.Focus3 = false;
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            if (!string) {
                string = "";
            }
            //string = string.replace("@", "");
            m["Focus" + i] = !m["Focus" + i];
            var output = [];
            angular.forEach($scope.Tudien_Report[1], function (u) {
                if ((u.Form_Name || "").toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    output.push(u);
                }
            });
            $scope.Forms_advsearch = output;
            if ($scope.opition.forms.length > 0) {
                angular.forEach($scope.opition.forms, function (item) {
                    var i = $scope.Forms_advsearch.findIndex(x => x.Form_ID === item.Form_ID);
                    if (i !== -1) {
                        $scope.Forms_advsearch.splice(i, 1);
                    }
                });
            }
        };
        $scope.clickAddForm_AdvSearch = function (u) {
            $scope.opition.forms.push(u);

            var i = $scope.Forms_advsearch.findIndex(x => x.Form_ID === u.Form_ID);
            if (i !== -1) {
                $scope.Forms_advsearch.splice(i, 1);
            }
            $scope.opition.Focus = false;
            $scope.opition.searchForm = "";
        }

        $scope.completeTeam_AdvSearch = function (string, m, $event, i) {
            $scope.opition.Focus = false;
            $scope.opition.Focus2 = false;
            $scope.opition.Focus3 = false;
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            if (!string) {
                string = "";
            }
            //string = string.replace("@", "");
            m["Focus" + i] = !m["Focus" + i];
            var output = [];
            angular.forEach($scope.Tudien_Report[2], function (u) {
                if ((u.Team_Name || "").toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    output.push(u);
                }
            });
            $scope.Teams_advsearch = output;
            if ($scope.opition.teams.length > 0) {
                angular.forEach($scope.opition.teams, function (item) {
                    var i = $scope.Teams_advsearch.findIndex(x => x.Team_ID === item.Team_ID);
                    if (i !== -1) {
                        $scope.Teams_advsearch.splice(i, 1);
                    }
                });
            }
        };
        $scope.clickAddTeam_AdvSearch = function (u) {
            $scope.opition.teams.push(u);

            var i = $scope.Teams_advsearch.findIndex(x => x.Team_ID === u.Team_ID);
            if (i !== -1) {
                $scope.Teams_advsearch.splice(i, 1);
            }
            $scope.opition.Focus1 = false;
            $scope.opition.searchTeam = "";
        }

        $scope.completeTypeForm_AdvSearch = function (string, m, $event, i) {
            $scope.opition.Focus = false;
            $scope.opition.Focus1 = false;
            $scope.opition.Focus3 = false;
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            if (!string) {
                string = "";
            }
            //string = string.replace("@", "");
            m["Focus" + i] = !m["Focus" + i];
            var output = [];
            angular.forEach($scope.Tudien_Report[4], function (u) {
                if ((u.Type_Form || "").toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    output.push(u);
                }
            });
            $scope.TypeForms_advsearch = output;
            if ($scope.opition.typeforms.length > 0) {
                angular.forEach($scope.opition.typeforms, function (item) {
                    var i = $scope.TypeForms_advsearch.findIndex(x => x.Type_ID === item.Type_ID);
                    if (i !== -1) {
                        $scope.Typeforms_advsearch.splice(i, 1);
                    }
                });
            }
        };
        $scope.clickAddTypeform_AdvSearch = function (u) {
            $scope.opition.typeforms.push(u);

            var i = $scope.Typeforms_advsearch.findIndex(x => x.Type_ID === u.Type_ID);
            if (i !== -1) {
                $scope.Typeforms_advsearch.splice(i, 1);
            }
            $scope.opition.Focus2 = false;
            $scope.opition.searchTypeform = "";
        }

        $scope.completeNhansu_AdvSearch = function (string, m, $event, i) {
            $scope.opition.Focus = false;
            $scope.opition.Focus1 = false;
            $scope.opition.Focus2 = false;
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            if (!string) {
                string = "";
            }
            //string = string.replace("@", "");
            m["Focus" + i] = !m["Focus" + i];
            var output = [];
            angular.forEach($scope.Tudien_Report[3], function (u) {
                if ((u.fullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    output.push(u);
                }
            });
            $scope.Nhansus_advsearch = output;
            if ($scope.opition.nhansus.length > 0) {
                angular.forEach($scope.opition.nhansus, function (item) {
                    var i = $scope.Nhansus_advsearch.findIndex(x => x.NhanSu_ID === item.NhanSu_ID);
                    if (i !== -1) {
                        $scope.Nhansus_advsearch.splice(i, 1);
                    }
                });
            }
        };
        $scope.clickAddNhansu_AdvSearch = function (u) {
            $scope.opition.nhansus.push(u);

            var i = $scope.Nhansus_advsearch.findIndex(x => x.NhanSu_ID === u.NhanSu_ID);
            if (i !== -1) {
                $scope.Nhansus_advsearch.splice(i, 1);
            }
            $scope.opition.Focus3 = false;
            $scope.opition.searchNhansu = "";
        };

        $scope.removetag = function (us, us_id, idx) {
            us.splice(idx, 1);
        };
        $scope.Clear_Filter = function () {
            $scope.opition = {
                year: new Date().getFullYear(),
                month: new Date().getMonth() + 1,
                //Congty_ID: null,
                //tenCongty: null,
                //Team_ID: null,
                //Team_Name: null,
                NhanSu_ID: $rootScope.login.u.NhanSu_ID,
                forms: [],
                teams: [],
                typeforms: [],
                ngaylaptu: null,
                ngaylapden: null,
                ngayhttu: null,
                ngayhtden: null,
                Status: null,
                roles: null,
                nhansus: [],
                isType: 1,
                isTypeGroup: $scope.opition.isTypeGroup,
                isQuaHanXL: null,
                statusDX: null,
                statusDG: $scope.opition.statusDG,
                statusQH: $scope.opition.statusQH,
                search: "",
                numPerPage: $scope.opition.numPerPage,
                currentPage: $scope.opition.currentPage,
                sort: 5,
                ob: "desc",
                Total: $scope.opition.Total,
                Pages: [100, 250, 500, 1000]
            };
        };
        $scope.Click_RenderTeam = function (cty, team, user) {
            if (cty) {
                $scope.opition.Congty_ID = cty.Congty_ID;
                
                $scope.opition.Team_ID = null;
                $scope.opition.Team_Name = null;
                $scope.opition.teams = [];

                $scope.opition.NhanSu_ID_focus = null;
                $scope.opition.fullName_focus = null;
                $scope.opition.nhansus = [];
            }
            if (team) {
                $scope.opition.Team_ID = team.Team_ID;
                $scope.opition.Team_Name = team.Team_Name;
                $scope.IsPeopleManage = team.IsPeopleManage;
                $scope.opition.teams = [];
                $scope.opition.teams.push({ Team_ID: team.Team_ID, Team_Name: team.Team_Name});

                $scope.opition.NhanSu_ID_focus = null;
                $scope.opition.fullName_focus = null;
                $scope.opition.nhansus = [];
            }
            if (user) {
                $scope.opition.Team_ID = team.Team_ID;
                $scope.opition.NhanSu_ID_focus = user.NhanSu_ID;
                $scope.opition.fullName_focus = user.fullName;
                $scope.opition.nhansus = [];
                $scope.opition.nhansus.push({ NhanSu_ID: user.NhanSu_ID, fullName: user.fullName });
            }
            //$scope.requestCount();
            $scope.Bind_List_ReportAmCharts_Cty();
            $scope.AmCharts_Team = [];
            $scope.AmCharts_User = [];
            if ($scope.opition.Congty_ID) $scope.Bind_List_ReportAmCharts_Team();
            if ($scope.opition.Team_ID) $scope.Bind_List_ReportAmCharts_User();
            $scope.Bind_List_Count_Dexuat();
            $scope.Bind_List_Report_Dexuat(true);
        };
        $scope.Bind_List_ReportAmCharts_Cty = function () {
            $http({
                method: 'POST',
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_ReportAmCharts", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.AmCharts_Cty = data[0];
                }
            });
        };
        $scope.Bind_List_ReportAmCharts_Team = function () {
            $http({
                method: 'POST',
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_ReportAmCharts_Team", pas: [
                        { "par": "Congty_IDs", "va": $scope.opition.Congty_ID },
                        { "par": "NhanSu_ID", "va": $scope.opition.NhanSu_ID },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.AmCharts_Team = data[0];
                }
            });
        };
        $scope.Bind_List_ReportAmCharts_User = function () {
            $http({
                method: 'POST',
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_ReportAmCharts_User", pas: [
                        { "par": "Congty_IDs", "va": $scope.opition.Congty_ID },
                        { "par": "Team_IDs", "va": $scope.opition.Team_ID },
                        { "par": "NhanSu_ID", "va": $scope.opition.NhanSu_ID },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.AmCharts_User = data[0];
                }
            });
        };

        var datacharts = [];
        var vt = 2;
        var cname = "fullName";

        $scope.setChart = function (v) {
            vt = v;
            $scope.BindChartData(v, 100, datacharts, cname);
        };

        $scope.ShowChart = function (col, type) {
            if (type === 0) {
                $scope.nameView = "Công ty";
            }
            else if (type === 1) {
                $scope.nameView = "Team";
            }
            else if (type === 2) {
                $scope.nameView = "Nhân sự";
            }
            $scope.opition.View = type
            $scope.col = col || "soDX";
            datacharts = [];
            $("#chartdivPop").empty();
            $("#ModalChart").modal("show");
            switch ($scope.opition.View) {
                case 0:
                    cname = "tenCongty";
                    $scope.AmCharts_Cty.forEach(function (r, i) {
                        var o = { tenCongty: r.tenCongty, soDX: r[col || "soDX"] };
                        datacharts.push(o);
                    });
                    datacharts.sort(function (a, b) { return b.soDX - a.soDX }).forEach(function (m, j) {
                        if (j >= ColorChats.length - 1) {
                            j = ColorChats.length - 1;
                        }
                        m.color = ColorChats[j].color;
                    });
                    $scope.BindChartData(vt, 100, datacharts, cname);
                    break;
                case 1:
                    cname = "Team_Name";
                    $scope.AmCharts_Team.forEach(function (r, i) {
                        var o = { Team_Name: r.Team_Name, soDX: r[col || "soDX"] };
                        datacharts.push(o);
                    });
                    datacharts.sort(function (a, b) { return b.soDX - a.soDX }).forEach(function (m, j) {
                        if (j >= ColorChats.length - 1) {
                            j = ColorChats.length - 1;
                        }
                        m.color = ColorChats[j].color;
                    });
                    $scope.BindChartData(vt, 100, datacharts, cname);
                    break;
                case 2:
                    cname = "fullNameNS";
                    $scope.AmCharts_User.forEach(function (r, i) {
                        var o = { fullNameNS: r.fullNameNS, soDX: r[col || "soDX"] };
                        datacharts.push(o);
                    });
                    datacharts.sort(function (a, b) { return b.soDX - a.soDX }).forEach(function (m, j) {
                        if (j >= ColorChats.length - 1) {
                            j = ColorChats.length - 1;
                        }
                        m.color = ColorChats[j].color;
                    });
                    $scope.BindChartData(vt, 100, datacharts, cname);
                    break;
            }
        };
        $scope.BindChartData = function (type, ti, data, ten) {
            var time = ti || 100;
            var fontsize = 14;
            id = "chartdivPop";
            $("#" + id).empty();
            switch (type) {
                case 0:
                    setTimeout(function () {
                        AmCharts.makeChart(id, {
                            "theme": "none", "fontSize": fontsize,
                            "rotate": false,
                            "type": "serial",
                            "startDuration": 2,
                            "dataProvider": data,
                            "valueAxes": [{
                                "position": "left",
                                "title": "Số phiếu đề xuất " + $scope.nameView,
                            }],
                            "graphs": [{
                                "balloonText": "[[category]]: <b>[[value]]</b>",
                                "fillColorsField": "color", "labelText": "[[value]]",
                                "fillAlphas": 1,
                                "lineAlpha": 0.1,
                                "type": "column",
                                "valueField": "soDX"
                            }],
                            "colorField": "color",
                            "depth3D": 20,
                            "angle": 30,
                            "chartCursor": {
                                "categoryBalloonEnabled": false,
                                "cursorAlpha": 0,
                                "zoomable": false
                            },
                            "categoryField": ten,
                            "categoryAxis": {
                                "gridPosition": "start",
                                "labelRotation": 90,
                            },
                            "export": {
                                "enabled": true, "fileName": "Phiếu đề xuất"
                            }
                        });
                    }, time);
                    break;
                case 1:
                    setTimeout(function () {
                        AmCharts.makeChart(id, {
                            "type": "pie", "outlineThickness": 2, "outlineAlpha": 1,
                            "startDuration": 1,
                            "theme": "none", "fontSize": fontsize,
                            "addClassNames": true,
                            "legend": {
                                "position": "right",
                            },
                            "innerRadius": "25%",
                            "graphs": [{
                                "balloonText": "[[category]]: <b>[[value]]</b>",
                                "fillColorsField": "color", "labelText": "[[value]]",
                                "fillAlphas": 1,
                                "lineAlpha": 0.1,
                                "type": "column",
                                "valueField": "percent"
                            }],
                            "colorField": "color",
                            "depth3D": 10,
                            "angle": 10,
                            "defs": {
                                "filter": [{
                                    "id": "shadow",
                                    "width": "200%",
                                    "height": "200%",
                                    "feOffset": {
                                        "result": "offOut",
                                        "in": "SourceAlpha",
                                        "dx": 0,
                                        "dy": 0
                                    },
                                    "feGaussianBlur": {
                                        "result": "blurOut",
                                        "in": "offOut",
                                        "stdDeviation": 5
                                    },
                                    "feBlend": {
                                        "in": "SourceGraphic",
                                        "in2": "blurOut",
                                        "mode": "normal"
                                    }
                                }]
                            },
                            "dataProvider": data,
                            "valueField": "soDX",
                            "titleField": ten,
                            "export": {
                                "enabled": true, "fileName": "Congviec"
                            }
                        });
                    }, time);
                    break;
                case 2:
                    setTimeout(function () {
                        AmCharts.makeChart(id, {
                            "theme": "none", "fontSize": fontsize,
                            "rotate": true,
                            "type": "serial",
                            "startDuration": 2,
                            "dataProvider": data,
                            "valueAxes": [{
                                "position": "left",
                                "title": "Số phiếu đề xuất",
                            }],
                            "graphs": [{
                                "balloonText": "[[category]]: <b>[[value]]</b>",
                                "fillColorsField": "color", "labelText": "[[value]]",
                                "fillAlphas": 1,
                                "lineAlpha": 0.1,
                                "type": "column",
                                "valueField": "soDX"
                            }],
                            "depth3D": 20,
                            "angle": 1,
                            "chartCursor": {
                                "categoryBalloonEnabled": false,
                                "cursorAlpha": 0,
                                "zoomable": false
                            },
                            "categoryField": ten,
                            "categoryAxis": {
                                "gridPosition": "start",
                                "labelRotation": 90,
                            },
                            "export": {
                                "enabled": true, "fileName": "Congviec"
                            }
                        });
                    }, time);
                    break;
                case 3:
                    setTimeout(function () {
                        AmCharts.makeChart(id, {
                            "theme": "none", "fontSize": fontsize,
                            "type": "radar",
                            "startDuration": 2,
                            "dataProvider": data,
                            "valueAxes": [{
                                "position": "left",
                                "title": "Số phiếu đề xuất"
                            }],
                            "graphs": [{
                                "balloonText": "[[category]]: <b>[[value]]</b>",
                                "fillColorsField": "color", "labelText": "[[value]]",
                                "bullet": "round",
                                "lineThickness": 2,
                                "valueField": "soDX"
                            }],
                            "categoryField": ten,
                            "export": {
                                "enabled": true, "fileName": "Congviec"
                            }
                        });
                    }, time);
                    break;
                case 4:
                    setTimeout(function () {
                        AmCharts.makeChart(id, {
                            "type": "radar",
                            "theme": "none", "fontSize": fontsize,
                            "dataProvider": data,
                            "valueAxes": [{
                                "gridType": "circles",
                                "minimum": 0,
                                "autoGridCount": false,
                                "axisAlpha": 0.2,
                                "fillAlpha": 0.05,
                                "fillColor": "#FFFFFF",
                                "gridAlpha": 0.08,
                                "guides": [{
                                    "angle": 225,
                                    "fillAlpha": 0.3,
                                    "fillColor": "#0066CC",
                                    "tickLength": 0,
                                    "toAngle": 315,
                                    "toValue": 14,
                                    "value": 0,
                                    "lineAlpha": 0,

                                }, {
                                    "angle": 45,
                                    "fillAlpha": 0.3,
                                    "fillColor": "#CC3333",
                                    "tickLength": 0,
                                    "toAngle": 135,
                                    "toValue": 14,
                                    "value": 0,
                                    "lineAlpha": 0,
                                }],
                                "position": "left"
                            }],
                            "startDuration": 1,
                            "graphs": [{
                                "balloonText": "[[category]]: <b>[[value]]</b>",
                                "fillColorsField": "color", "labelText": "[[value]]",
                                "bullet": "round",
                                "lineThickness": 2,
                                "valueField": "soDX"
                            }],
                            "categoryField": ten,
                            "export": {
                                "enabled": true, "fileName": "Congviec"
                            }
                        });
                    }, time);
                    break;
            }
        };

        $scope.goInfoRQ = function (r) {
            if ($scope.RequestMaster_ID != r.RequestMaster_ID)
                $scope.RequestMaster_ID = r.RequestMaster_ID;
            else {
                $scope.Uid = generateUUID();
            }
        };
        $scope.setStatusDX = function (t) {
            if (t)
                $scope.opition.statusDX = t;
            else
                $scope.opition.statusDX = null;
            $scope.Bind_List_Report_Dexuat(true);
        };
        $scope.BindSearch = function () {
            $scope.Bind_List_Count_Dexuat();
            $scope.Bind_List_Report_Dexuat(true);
        };

        $scope.GroupByData = function (gr) {
            $scope.opition.isTypeGroup = gr;
            $scope.Bind_List_Report_Dexuat(true);
        };
        //===========================================================================

        //Bind list Report =========================================
        $scope.Bind_List_TreeCty = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_List_TreeCty", pas: [
                        { "par": "Congty_ID", "va": $scope.opition.Congty_ID || $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    angular.forEach(data[0], function (r) {
                        if (r.ListTeam) {
                            r.ListTeam = JSON.parse(r.ListTeam);
                            angular.forEach(r.ListTeam, function (m) {
                                if (m.IsPeopleManage && m.IsPeopleManage == '1') {
                                    m.IsPeopleManage = true;
                                }
                                else  {
                                    m.IsPeopleManage = false;
                                }
                                if (m.ListUser) {
                                    m.ListUser = JSON.parse(m.ListUser);
                                }
                            })
                        }
                    });
                    $rootScope.TreeCty = data[0];
                }
            });
        };
        $scope.Bind_List_Tudien = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_List_Tudien_Report", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "congtys", "va": $scope.opition.Congty_ID },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    data[0].unshift({ Congty_ID: null, tenCongty: '--- Tất cả ---' });
                    data[1].unshift({ Form_ID: null, Form_Name: '--- Đề xuất trực tiếp ---' });
                    $scope.Tudien_Report = data;
                }
            });
        };
        $scope.Bind_List_Count_Dexuat = function (f) {
            if ($rootScope.roles_Root || $rootScope.roles_Admin) {
                $scope.opition.isType = 0;
            }
            let forms = '';
            if ($scope.opition.forms.length > 0) {
                angular.forEach($scope.opition.forms, function (r) {
                    forms += r.Form_ID + ',';
                })
                forms = forms.slice(0, -1);
            }
            let teams = '';
            if ($scope.opition.teams.length > 0) {
                angular.forEach($scope.opition.teams, function (r) {
                    teams += r.Team_ID + ',';
                })
                teams = teams.slice(0, -1);
            }
            let typeforms = '';
            //if ($scope.opition.typeforms.length > 0) {
            //    angular.forEach($scope.opition.typeforms, function (r) {
            //        typeforms += r.Type_ID + ',';
            //    })
            //    typeforms = typeforms.slice(0, -1);
            //}
            let nhansus = '';
            if ($scope.opition.nhansus.length > 0) {
                angular.forEach($scope.opition.nhansus, function (r) {
                    nhansus += r.NhanSu_ID + ',';
                })
                nhansus = nhansus.slice(0, -1);
            }
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_List_Count_Dexuat", pas: [
                        { "par": "Congty_ID", "va": $scope.opition.Congty_ID },
                        { "par": "NhanSu_ID", "va": $scope.opition.NhanSu_ID },
                        { "par": "forms", "va": forms },
                        { "par": "teams", "va": teams },
                        { "par": "typeforms", "va": typeforms },
                        { "par": "ngaylaptu", "va": $scope.opition.ngaylaptu },
                        { "par": "ngaylapden", "va": $scope.opition.ngaylapden },
                        { "par": "ngayhttu", "va": $scope.opition.ngayhttu },
                        { "par": "ngayhtden", "va": $scope.opition.ngayhtden },
                        { "par": "Status", "va": $scope.opition.Status },
                        { "par": "roles", "va": $scope.opition.roles },
                        { "par": "nhansus", "va": nhansus },
                        { "par": "isType", "va": $scope.opition.isType },
                        { "par": "isQuaHanXL", "va": $scope.opition.isQuaHanXL },
                        { "par": "statusDX", "va": $scope.opition.statusDX },
                        { "par": "statusQH", "va": $scope.opition.statusQH },
                        { "par": "p", "va": $scope.opition.currentPage },
                        { "par": "pz", "va": $scope.opition.numPerPage },
                        { "par": "sort", "va": $scope.opition.sort },
                        { "par": "ob", "va": $scope.opition.ob },
                        { "par": "s", "va": $scope.opition.search },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.CountDX = data;
                }
            });
        };
        $scope.Bind_List_Report_Dexuat = function (f) {
            $('.menu-task').removeClass('show');
            $scope.loadding = true;
            swal.showLoading();
            if ($rootScope.roles_Root || $rootScope.roles_Admin) {
                $scope.opition.isType = 0;
            }
            if (f) {
                $("#ManageteamCtr .row-content").animate({ scrollTop: 0 }, 400);
            }
            let forms = '';
            if ($scope.opition.forms.length > 0) {
                angular.forEach($scope.opition.forms, function (r) {
                    forms += r.Form_ID + ',';
                })
                forms = forms.slice(0, -1);
            }
            let teams = '';
            if ($scope.opition.teams.length > 0) {
                angular.forEach($scope.opition.teams, function (r) {
                    teams += r.Team_ID + ',';
                })
                teams = teams.slice(0, -1);
            }
            let typeforms = '';
            //if ($scope.opition.typeforms.length > 0) {
            //    angular.forEach($scope.opition.typeforms, function (r) {
            //        typeforms += r.Type_ID + ',';
            //    })
            //    typeforms = typeforms.slice(0, -1);
            //}
            let nhansus = '';
            if ($scope.opition.nhansus.length > 0) {
                angular.forEach($scope.opition.nhansus, function (r) {
                    nhansus += r.NhanSu_ID + ',';
                })
                nhansus = nhansus.slice(0, -1);
            }
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_List_Report_Dexuat_Filter", pas: [
                        { "par": "Congty_ID", "va": $scope.opition.Congty_ID },
                        { "par": "NhanSu_ID", "va": $scope.opition.NhanSu_ID },
                        { "par": "forms", "va": forms },
                        { "par": "teams", "va": teams },
                        { "par": "typeforms", "va": typeforms },
                        { "par": "ngaylaptu", "va": $scope.opition.ngaylaptu },
                        { "par": "ngaylapden", "va": $scope.opition.ngaylapden },
                        { "par": "ngayhttu", "va": $scope.opition.ngayhttu },
                        { "par": "ngayhtden", "va": $scope.opition.ngayhtden },
                        { "par": "Status", "va": $scope.opition.Status },
                        { "par": "roles", "va": $scope.opition.roles },
                        { "par": "nhansus", "va": nhansus },
                        { "par": "isType", "va": $scope.opition.isType },
                        { "par": "isQuaHanXL", "va": $scope.opition.isQuaHanXL },
                        { "par": "statusDX", "va": $scope.opition.statusDX },
                        { "par": "statusQH", "va": $scope.opition.statusQH },
                        { "par": "p", "va": $scope.opition.currentPage },
                        { "par": "pz", "va": $scope.opition.numPerPage },
                        { "par": "sort", "va": $scope.opition.sort },
                        { "par": "ob", "va": $scope.opition.ob },
                        { "par": "s", "va": $scope.opition.search },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    angular.forEach(data[0], function (r) {
                        r.Nguoiduyets = '';
                        if (r.ListSignUser) {
                            r.ListSignUser = JSON.parse(r.ListSignUser);
                            r.ListSignUser.forEach(function (m) {
                                r.Nguoiduyets += m.fullName + ' (' + m.tenToChuc + ')' + ', ';
                            });
                        }
                    });
                    let arr = [];
                    //Group by
                    switch ($scope.opition.isTypeGroup) {
                        case 0:
                            data[0].forEach(function (td) {
                                td.color = renderColor(td.Tiendo);
                                td.txtcolor = renderTxtColor(td.Tiendo);
                            });
                            //Order by
                            switch ($scope.opition.sort) {
                                case 0:
                                    data[0] = $filter('orderBy')(data[0], 'Title');
                                    break;
                                case 1:
                                    data[0] = $filter('orderBy')(data[0], '-Title');
                                    break;
                                case 2:
                                    data[0] = $filter('orderBy')(data[0], 'Ngaylap');
                                    break;
                                case 3:
                                    data[0] = $filter('orderBy')(data[0], '-Ngaylap');
                                    break;
                                case 4:
                                    data[0] = $filter('orderBy')(data[0], 'Ngaytao');
                                    break;
                                case 5:
                                    data[0] = $filter('orderBy')(data[0], '-Ngaytao');
                                    break;
                                case 6:
                                    data[0] = $filter('orderBy')(data[0], 'Ngaycapnhat');
                                    break;
                                case 7:
                                    data[0] = $filter('orderBy')(data[0], '-Ngaycapnhat');
                                    break;
                                case 8:
                                    data[0] = $filter('orderBy')(data[0], 'SoNgayHan');
                                    break;
                                case 9:
                                    data[0] = $filter('orderBy')(data[0], '-SoNgayHan');
                                    break;
                            }
                            break;
                        case 1:
                            data[0] = groupBy(data[0], 'Form_ID');
                            for (let f in data[0]) {
                                data[0][f].forEach(function (td) {
                                    td.color = renderColor(td.Tiendo);
                                    td.txtcolor = renderTxtColor(td.Tiendo);
                                });
                                //Order by
                                switch ($scope.opition.sort) {
                                    case 0:
                                        data[0][f] = $filter('orderBy')(data[0][f], 'Title');
                                        break;
                                    case 1:
                                        data[0][f] = $filter('orderBy')(data[0][f], '-Title');
                                        break;
                                    case 2:
                                        data[0][f] = $filter('orderBy')(data[0][f], 'Ngaylap');
                                        break;
                                    case 3:
                                        data[0][f] = $filter('orderBy')(data[0][f], '-Ngaylap');
                                        break;
                                    case 4:
                                        data[0][f] = $filter('orderBy')(data[0][f], 'Ngaytao');
                                        break;
                                    case 5:
                                        data[0][f] = $filter('orderBy')(data[0][f], '-Ngaytao');
                                        break;
                                    case 6:
                                        data[0][f] = $filter('orderBy')(data[0][f], 'Ngaycapnhat');
                                        break;
                                    case 7:
                                        data[0][f] = $filter('orderBy')(data[0][f], '-Ngaycapnhat');
                                        break;
                                    case 8:
                                        data[0][f] = $filter('orderBy')(data[0][f], 'SoNgayHan');
                                        break;
                                    case 9:
                                        data[0][f] = $filter('orderBy')(data[0][f], '-SoNgayHan');
                                        break;
                                }
                                arr.push({ group_form: f, name_group_form: data[0][f][0].Form_Name, list_ns: data[0][f] });
                            }
                            data[0] = arr;
                            break;
                        case 2:
                            data[0] = groupBy(data[0], 'Team_ID');
                            for (let f in data[0]) {
                                data[0][f].forEach(function (td) {
                                    td.color = renderColor(td.Tiendo);
                                    td.txtcolor = renderTxtColor(td.Tiendo);
                                });
                                //Order by
                                switch ($scope.opition.sort) {
                                    case 0:
                                        data[0][f] = $filter('orderBy')(data[0][f], 'Title');
                                        break;
                                    case 1:
                                        data[0][f] = $filter('orderBy')(data[0][f], '-Title');
                                        break;
                                    case 2:
                                        data[0][f] = $filter('orderBy')(data[0][f], 'Ngaylap');
                                        break;
                                    case 3:
                                        data[0][f] = $filter('orderBy')(data[0][f], '-Ngaylap');
                                        break;
                                    case 4:
                                        data[0][f] = $filter('orderBy')(data[0][f], 'Ngaytao');
                                        break;
                                    case 5:
                                        data[0][f] = $filter('orderBy')(data[0][f], '-Ngaytao');
                                        break;
                                    case 6:
                                        data[0][f] = $filter('orderBy')(data[0][f], 'Ngaycapnhat');
                                        break;
                                    case 7:
                                        data[0][f] = $filter('orderBy')(data[0][f], '-Ngaycapnhat');
                                        break;
                                    case 8:
                                        data[0][f] = $filter('orderBy')(data[0][f], 'SoNgayHan');
                                        break;
                                    case 9:
                                        data[0][f] = $filter('orderBy')(data[0][f], '-SoNgayHan');
                                        break;
                                }
                                arr.push({ group_team: f, name_group_team: data[0][f][0].Team_Name, list_ns: data[0][f] });
                            }
                            data[0] = arr;
                            break;
                        default:
                            data[0] = groupBy(data[0], 'Form_ID');
                            for (let f in data[0]) {
                                let data_team_by_id = groupBy(data[0][f], 'Team_ID');
                                let arr_team_3 = [];
                                for (let t in data_team_by_id) {
                                    data_team_by_id[t].forEach(function (td) {
                                        td.color = renderColor(td.Tiendo);
                                        td.txtcolor = renderTxtColor(td.Tiendo);
                                    });
                                    //Order by
                                    switch ($scope.opition.sort) {
                                        case 0:
                                            data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], 'Title');
                                            break;
                                        case 1:
                                            data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], '-Title');
                                            break;
                                        case 2:
                                            data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], 'Ngaylap');
                                            break;
                                        case 3:
                                            data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], '-Ngaylap');
                                            break;
                                        case 4:
                                            data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], 'Ngaytao');
                                            break;
                                        case 5:
                                            data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], '-Ngaytao');
                                            break;
                                        case 6:
                                            data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], 'Ngaycapnhat');
                                            break;
                                        case 7:
                                            data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], '-Ngaycapnhat');
                                            break;
                                        case 8:
                                            data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], 'SoNgayHan');
                                            break;
                                        case 9:
                                            data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], '-SoNgayHan');
                                            break;
                                    }
                                    arr_team_3.push({ group_team: t, name_group_team: data_team_by_id[t][0].Team_Name, list_ns: data_team_by_id[t] });
                                };
                                data_team_by_id = arr_team_3;
                                arr.push({ group_form: f, name_group_form: data[0][f][0].Form_Name, list_team: data_team_by_id });
                            }
                            data[0] = arr;
                            break;
                    }
                    $scope.Data_Report_Dexuat = data[0];
                    if (f || !$scope.opition.noOfPages) {
                        $scope.opition.Total = data[1][0].c;
                        $scope.opition.noOfPages = Math.ceil(data[1][0].c / $scope.opition.numPerPage);
                    }
                }
            }).then(function (response) {
                $scope.loadding = false;
                closeswal();
            });
        };
        //===================================================

        $scope.Refresh = function () {
            $scope.opition = {
                year: new Date().getFullYear(),
                month: new Date().getMonth() + 1,
                Congty_ID: null,
                tenCongty: null,
                Team_ID: null,
                Team_Name: null,
                NhanSu_ID: $rootScope.login.u.NhanSu_ID,
                forms: [],
                teams: [],
                typeforms: [],
                ngaylaptu: null,
                ngaylapden: null,
                ngayhttu: null,
                ngayhtden: null,
                Status: null,
                roles: null,
                nhansus: [],
                isType: 1,
                isTypeGroup: 3,
                isQuaHanXL: null,
                statusDX: null,
                statusDG: null,
                statusQH: null,
                search: "",
                numPerPage: 100,
                currentPage: 1,
                sort: 5,
                ob: "desc",
                Total: 0,
                Pages: [100, 250, 500, 1000]
            };
            $scope.Bind_List_Count_Dexuat()
            $scope.Bind_List_Report_Dexuat(true);
            $scope.Bind_List_ReportAmCharts_Cty();
            $scope.Bind_List_ReportAmCharts_Team();
            $scope.AmCharts_User = [];
        };
        $scope.initOne = function () {
            $rootScope.link = "manageteam";
            //$scope.opition.Congty_ID = $stateParams.id;
            $scope.Bind_List_Count_Dexuat();
            $scope.$watch('opition.currentPage', $scope.Bind_List_Report_Dexuat);
            $scope.Bind_List_TreeCty();
            $scope.Bind_List_ReportAmCharts_Cty();
            $scope.Bind_List_ReportAmCharts_Team();
            $scope.AmCharts_User = [];
            $scope.Bind_List_Tudien();
        };
        $scope.initOne();

        $scope.SaveSetup = function () {
            $http({
                method: 'POST',
                url: "Request/Update_SetupManage",
                data: {
                    t: $rootScope.login.tk, model: $scope.dataSetupManage
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: res.data.ms
                    });
                    return false;
                }
                $("#Modal_SetupManage").modal("hide");
                showtoastr('Đã cập nhật cấu hình thành công!.');
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        $scope.SetupManage = function () {
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_Get_SettingManage", pas: [
                        { "par": "Congty_ID", "va": $scope.opition.Congty_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    angular.forEach(data, function (r) {
                        if (!r.IsNumDevice || r.IsNumDevice == '') r.IsNumDevice = 3;
                        if (!r.IsVerifySign) r.IsVerifySign = false;
                        if (!r.IsSkipWeb) r.IsSkipWeb = false;
                        if (!r.IsOneDevice) r.IsOneDevice = false;
                    });
                    $scope.dataSetupManage = data;
                    $scope.utitle = "Thiết lập quản lý";
                    $("#Modal_SetupManage").modal("show");
                }
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        //============== Modal Export ===============
        $scope.openModalExport = function (type) {
            $scope.Data_Report_Dexuat_All = [];
            if (type === 2) {
                $scope.loadding = true;
                swal.showLoading();
                if ($rootScope.roles_Root || $rootScope.roles_Admin) {
                    $scope.opition.isType = 0;
                }
                let forms = '';
                if ($scope.opition.forms.length > 0) {
                    angular.forEach($scope.opition.forms, function (r) {
                        forms += r.Form_ID + ',';
                    })
                    forms = forms.slice(0, -1);
                }
                let teams = '';
                if ($scope.opition.teams.length > 0) {
                    angular.forEach($scope.opition.teams, function (r) {
                        teams += r.Team_ID + ',';
                    })
                    teams = teams.slice(0, -1);
                }
                let typeforms = '';
                //if ($scope.opition.typeforms.length > 0) {
                //    angular.forEach($scope.opition.typeforms, function (r) {
                //        typeforms += r.Type_ID + ',';
                //    })
                //    typeforms = typeforms.slice(0, -1);
                //}
                let nhansus = '';
                if ($scope.opition.nhansus.length > 0) {
                    angular.forEach($scope.opition.nhansus, function (r) {
                        nhansus += r.NhanSu_ID + ',';
                    })
                    nhansus = nhansus.slice(0, -1);
                }
                $http({
                    method: "POST",
                    url: "Home/CallProc",
                    data: {
                        t: $rootScope.login.tk, proc: "Srequest_List_Report_Dexuat_All", pas: [
                            { "par": "Congty_ID", "va": $scope.opition.Congty_ID || $rootScope.login.u.congtyID },
                            { "par": "NhanSu_ID", "va": $scope.opition.NhanSu_ID },
                            { "par": "forms", "va": forms },
                            { "par": "teams", "va": teams },
                            { "par": "typeforms", "va": typeforms },
                            { "par": "ngaylaptu", "va": $scope.opition.ngaylaptu },
                            { "par": "ngaylapden", "va": $scope.opition.ngaylapden },
                            { "par": "ngayhttu", "va": $scope.opition.ngayhttu },
                            { "par": "ngayhtden", "va": $scope.opition.ngayhtden },
                            { "par": "Status", "va": $scope.opition.Status },
                            { "par": "roles", "va": $scope.opition.roles },
                            { "par": "nhansus", "va": nhansus },
                            { "par": "isType", "va": $scope.opition.isType },
                            { "par": "isQuaHanXL", "va": $scope.opition.isQuaHanXL },
                            { "par": "statusDX", "va": $scope.opition.statusDX },
                            { "par": "p", "va": $scope.opition.currentPage },
                            { "par": "pz", "va": $scope.opition.numPerPage },
                            { "par": "sort", "va": $scope.opition.sort },
                            { "par": "ob", "va": $scope.opition.ob },
                            { "par": "s", "va": $scope.opition.search },
                        ]
                    },
                    contentType: 'application/json; charset=utf-8'
                }).then(function (res) {
                    $scope.loadding = false;
                    if (!$rootScope.checkToken(res)) return false;
                    if (res.data.error !== 1) {
                        var data = JSON.parse(res.data.data);
                        angular.forEach(data[0], function (r) {
                            r.Nguoiduyets = '';
                            if (r.ListSignUser) {
                                r.ListSignUser = JSON.parse(r.ListSignUser);
                                r.ListSignUser.forEach(function (m) {
                                    r.Nguoiduyets += m.fullName + ' (' + m.tenToChuc + ')' + ', ';
                                });
                            }
                        });
                        let arr = [];
                        //Group by
                        switch ($scope.opition.isTypeGroup) {
                            case 0:
                                data[0].forEach(function (td) {
                                    td.color = renderColor(td.Tiendo);
                                    td.txtcolor = renderTxtColor(td.Tiendo);
                                });
                                //Order by
                                switch ($scope.opition.sort) {
                                    case 0:
                                        data[0] = $filter('orderBy')(data[0], 'Title');
                                        break;
                                    case 1:
                                        data[0] = $filter('orderBy')(data[0], '-Title');
                                        break;
                                    case 2:
                                        data[0] = $filter('orderBy')(data[0], 'Ngaylap');
                                        break;
                                    case 3:
                                        data[0] = $filter('orderBy')(data[0], '-Ngaylap');
                                        break;
                                    case 4:
                                        data[0] = $filter('orderBy')(data[0], 'Ngaytao');
                                        break;
                                    case 5:
                                        data[0] = $filter('orderBy')(data[0], '-Ngaytao');
                                        break;
                                    case 6:
                                        data[0] = $filter('orderBy')(data[0], 'Ngaycapnhat');
                                        break;
                                    case 7:
                                        data[0] = $filter('orderBy')(data[0], '-Ngaycapnhat');
                                        break;
                                    case 8:
                                        data[0] = $filter('orderBy')(data[0], 'SoNgayHan');
                                        break;
                                    case 9:
                                        data[0] = $filter('orderBy')(data[0], '-SoNgayHan');
                                        break;
                                }
                                break;
                            case 1:
                                data[0] = groupBy(data[0], 'Form_ID');
                                for (let f in data[0]) {
                                    data[0][f].forEach(function (td) {
                                        td.color = renderColor(td.Tiendo);
                                        td.txtcolor = renderTxtColor(td.Tiendo);
                                    });
                                    //Order by
                                    switch ($scope.opition.sort) {
                                        case 0:
                                            data[0][f] = $filter('orderBy')(data[0][f], 'Title');
                                            break;
                                        case 1:
                                            data[0][f] = $filter('orderBy')(data[0][f], '-Title');
                                            break;
                                        case 2:
                                            data[0][f] = $filter('orderBy')(data[0][f], 'Ngaylap');
                                            break;
                                        case 3:
                                            data[0][f] = $filter('orderBy')(data[0][f], '-Ngaylap');
                                            break;
                                        case 4:
                                            data[0][f] = $filter('orderBy')(data[0][f], 'Ngaytao');
                                            break;
                                        case 5:
                                            data[0][f] = $filter('orderBy')(data[0][f], '-Ngaytao');
                                            break;
                                        case 6:
                                            data[0][f] = $filter('orderBy')(data[0][f], 'Ngaycapnhat');
                                            break;
                                        case 7:
                                            data[0][f] = $filter('orderBy')(data[0][f], '-Ngaycapnhat');
                                            break;
                                        case 8:
                                            data[0][f] = $filter('orderBy')(data[0][f], 'SoNgayHan');
                                            break;
                                        case 9:
                                            data[0][f] = $filter('orderBy')(data[0][f], '-SoNgayHan');
                                            break;
                                    }
                                    arr.push({ group_form: f, name_group_form: data[0][f][0].Form_Name, list_ns: data[0][f] });
                                }
                                data[0] = arr;
                                break;
                            case 2:
                                data[0] = groupBy(data[0], 'Team_ID');
                                for (let f in data[0]) {
                                    data[0][f].forEach(function (td) {
                                        td.color = renderColor(td.Tiendo);
                                        td.txtcolor = renderTxtColor(td.Tiendo);
                                    });
                                    //Order by
                                    switch ($scope.opition.sort) {
                                        case 0:
                                            data[0][f] = $filter('orderBy')(data[0][f], 'Title');
                                            break;
                                        case 1:
                                            data[0][f] = $filter('orderBy')(data[0][f], '-Title');
                                            break;
                                        case 2:
                                            data[0][f] = $filter('orderBy')(data[0][f], 'Ngaylap');
                                            break;
                                        case 3:
                                            data[0][f] = $filter('orderBy')(data[0][f], '-Ngaylap');
                                            break;
                                        case 4:
                                            data[0][f] = $filter('orderBy')(data[0][f], 'Ngaytao');
                                            break;
                                        case 5:
                                            data[0][f] = $filter('orderBy')(data[0][f], '-Ngaytao');
                                            break;
                                        case 6:
                                            data[0][f] = $filter('orderBy')(data[0][f], 'Ngaycapnhat');
                                            break;
                                        case 7:
                                            data[0][f] = $filter('orderBy')(data[0][f], '-Ngaycapnhat');
                                            break;
                                        case 8:
                                            data[0][f] = $filter('orderBy')(data[0][f], 'SoNgayHan');
                                            break;
                                        case 9:
                                            data[0][f] = $filter('orderBy')(data[0][f], '-SoNgayHan');
                                            break;
                                    }
                                    arr.push({ group_team: f, name_group_team: data[0][f][0].Team_Name, list_ns: data[0][f] });
                                }
                                data[0] = arr;
                                break;
                            default:
                                data[0] = groupBy(data[0], 'Form_ID');
                                for (let f in data[0]) {
                                    let data_team_by_id = groupBy(data[0][f], 'Team_ID');
                                    let arr_team_3 = [];
                                    for (let t in data_team_by_id) {
                                        data_team_by_id[t].forEach(function (td) {
                                            td.color = renderColor(td.Tiendo);
                                            td.txtcolor = renderTxtColor(td.Tiendo);
                                        });
                                        //Order by
                                        switch ($scope.opition.sort) {
                                            case 0:
                                                data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], 'Title');
                                                break;
                                            case 1:
                                                data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], '-Title');
                                                break;
                                            case 2:
                                                data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], 'Ngaylap');
                                                break;
                                            case 3:
                                                data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], '-Ngaylap');
                                                break;
                                            case 4:
                                                data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], 'Ngaytao');
                                                break;
                                            case 5:
                                                data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], '-Ngaytao');
                                                break;
                                            case 6:
                                                data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], 'Ngaycapnhat');
                                                break;
                                            case 7:
                                                data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], '-Ngaycapnhat');
                                                break;
                                            case 8:
                                                data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], 'SoNgayHan');
                                                break;
                                            case 9:
                                                data_team_by_id[t] = $filter('orderBy')(data_team_by_id[t], '-SoNgayHan');
                                                break;
                                        }
                                        arr_team_3.push({ group_team: t, name_group_team: data_team_by_id[t][0].Team_Name, list_ns: data_team_by_id[t] });
                                    };
                                    data_team_by_id = arr_team_3;
                                    arr.push({ group_form: f, name_group_form: data[0][f][0].Form_Name, list_team: data_team_by_id });
                                }
                                data[0] = arr;
                                break;
                        }
                        $scope.Data_Report_Dexuat_All = data[0];
                    }
                }).then(function (response) {
                    $scope.loadding = false;
                    closeswal();
                });
            }
            $scope.Tudien_Report_copy = angular.copy($scope.Tudien_Report);
            $scope.Tudien_Report_copy[0] = $scope.Tudien_Report_copy[0].find(x => x.Congty_ID == $rootScope.login.u.congtyID);
            $scope.listData_Filter_Column = [
                { "id": 1, "name": "Mã đề xuất", "column": "RequestNo" },
                { "id": 2, "name": "Tên đề xuất", "column": "Title" },
                { "id": 3, "name": "Người tạo", "column": "fulName_nguoitao" },
                { "id": 4, "name": "Ngày lập", "column": "Ngaylap" },
                { "id": 5, "name": "Trạng thái", "column": "Trangthai" },
                { "id": 6, "name": "Tiến độ", "column": "Tiendo" },
                { "id": 7, "name": "Người duyệt", "column": "Nguoiduyets" },
                { "id": 8, "name": "Ngày hoàn thành", "column": "Ngayhoanthanh" },
                { "id": 9, "name": "Số giờ hạn", "column": "IsSLA" },
            ]
            $scope.listData_Column = [];
            $scope.dtitle = "Xuất file tổng hợp đề xuất";
            $scope.title_export = "DANH SÁCH TỔNG HỢP ĐỀ XUẤT";
            $("#ModalExport").modal("show");
        };
        $scope.push_Filter_DataColumn = function (item) {
            if (item != null) {
                $scope.listData_Filter_Column.push(item);
            }
            else {
                $scope.listData_Column.filter(a => $scope.listData_Filter_Column.filter(b => b.id === a.id).length == 0).forEach(function (r) {
                    $scope.listData_Filter_Column.push(r);
                });
            }
            $scope.listData_Column = $scope.listData_Column.filter(a => $scope.listData_Filter_Column.filter(b => b.id === a.id).length == 0);
        };
        $scope.remove_rowDataColumn = function (item) {
            if (item != null) {
                $scope.listData_Column.push(item);
            }
            else {
                $scope.listData_Filter_Column.filter(a => $scope.listData_Column.filter(b => b.id === a.id).length == 0).forEach(function (r) {
                    $scope.listData_Column.push(r);
                });
            }
            $scope.listData_Filter_Column = $scope.listData_Filter_Column.filter(a => $scope.listData_Column.filter(b => b.id === a.id).length == 0);
            $scope.listData_Column = $filter('orderBy')($scope.listData_Column, 'id');
        };
        function renderTable() {
            let data = [];
            if ($scope.Data_Report_Dexuat_All && $scope.Data_Report_Dexuat_All.length > 0) {
                data = angular.copy($scope.Data_Report_Dexuat_All);
            }
            else if ($scope.Data_Report_Dexuat && $scope.Data_Report_Dexuat.length > 0) {
                data = angular.copy($scope.Data_Report_Dexuat);
            }
            if (data) {
                switch ($scope.opition.isTypeGroup) {
                    case 0:
                        data.forEach(function (ns) {
                            if (ns.Ngaylap) {
                                ns.Ngaylap = new Date(ns.Ngaylap);
                                ns.Ngaylap = moment(ns.Ngaylap).format("DD/MM/YYYY");
                            }
                            if (ns.Ngayhoanthanh) {
                                ns.Ngayhoanthanh = new Date(ns.Ngayhoanthanh);
                                ns.Ngayhoanthanh = moment(ns.Ngayhoanthanh).format("DD/MM/YYYY");
                            }
                        });
                        break;
                    case 1:
                        data.forEach(function (form) {
                            form.list_ns.forEach(function (ns) {
                                if (ns.Ngaylap) {
                                    ns.Ngaylap = new Date(ns.Ngaylap);
                                    ns.Ngaylap = moment(ns.Ngaylap).format("DD/MM/YYYY");
                                }
                                if (ns.Ngayhoanthanh) {
                                    ns.Ngayhoanthanh = new Date(ns.Ngayhoanthanh);
                                    ns.Ngayhoanthanh = moment(ns.Ngayhoanthanh).format("DD/MM/YYYY");
                                }
                            });
                        });
                        break;
                    case 2:
                        data.forEach(function (team) {
                            team.list_ns.forEach(function (ns) {
                                if (ns.Ngaylap) {
                                    ns.Ngaylap = new Date(ns.Ngaylap);
                                    ns.Ngaylap = moment(ns.Ngaylap).format("DD/MM/YYYY");
                                }
                                if (ns.Ngayhoanthanh) {
                                    ns.Ngayhoanthanh = new Date(ns.Ngayhoanthanh);
                                    ns.Ngayhoanthanh = moment(ns.Ngayhoanthanh).format("DD/MM/YYYY");
                                }
                            });
                        });
                        break;
                    default:
                        data.forEach(function (form) {
                            form.list_team.forEach(function (team) {
                                team.list_ns.forEach(function (ns) {
                                    if (ns.Ngaylap) {
                                        ns.Ngaylap = new Date(ns.Ngaylap);
                                        ns.Ngaylap = moment(ns.Ngaylap).format("DD/MM/YYYY");
                                    }
                                    if (ns.Ngayhoanthanh) {
                                        ns.Ngayhoanthanh = new Date(ns.Ngayhoanthanh);
                                        ns.Ngayhoanthanh = moment(ns.Ngayhoanthanh).format("DD/MM/YYYY");
                                    }
                                });
                            });

                        });
                        break;
                }
            }
            let data_filter = $scope.listData_Filter_Column;
            let countdatafilter = data_filter.length - 1;
            var htmltable = "";
            htmltable = "<font face='Times New Roman'>";
            htmltable += "<div><table border='0' colspan='2' cellpadding='10' style=''><tbody><tr><td style='padding:0px 15px;'><img src='" + $rootScope.fileUrl + ($rootScope.login.u.logo == null ? '' : $rootScope.login.u.logo) + "' height='50'/></td>";
            htmltable += "<td style='text-align: left' colspan='" + countdatafilter + "'><div><b style='font-size: 18px'>" + ($scope.Tudien_Report_copy[0].tenCongty == null ? '' : $scope.Tudien_Report_copy[0].tenCongty) + "</b></div> <div style='font-size: 16px'>" + ($scope.Tudien_Report_copy[0].tenkhongdau == null ? '' : $scope.Tudien_Report_copy[0].tenkhongdau) + "</div> <div>" + ($scope.Tudien_Report_copy[0].diaChi == null ? '' : $scope.Tudien_Report_copy[0].diaChi) + "</div></td></tbody ></table>";
            htmltable += "<table border='0' cellpadding='5' style=''><tbody><tr><td colspan='" + data_filter.length + "'><hr/></td></tr></tbody></table></div>";
            htmltable += "<div><table border='0' cellpadding='10' style='min-width:1000px; margin:auto;'><thead><tr><th colspan='" + countdatafilter + "' align='center'><div class='col-md-12'><h3 style='font-weight:bold;text-align:center'>" + $scope.title_export + "</h3></div></th></tr></thead></table>";
            htmltable += "<table border='1' cellpadding='10' style='min-width:1000px; margin:auto; font-size:14px;border-collapse:collapse'><thead>";
            htmltable += "<tr style='vertical-align: middle'>";
            if (data_filter != null && data_filter.length > 0) {
                htmltable += "<td style='font-weight:bold;font-size:14px;width:50px;text-align: center;'>STT</td>";
                data_filter.forEach(function (cl) {
                    htmltable += "<td style='font-weight:bold;font-size:14px;width:150px !important;text-align: center;'>" + cl.name + "</td>";
                });
                htmltable += "</tr></thead>";
                let index_form_th = 1;
                if (data.length > 0) {
                    data.forEach(function (form) {
                        let countFilter = data_filter.length + 1;
                        let index_team = 1;
                        if ($scope.opition.isTypeGroup === 0 || $scope.opition.isTypeGroup === 1 || $scope.opition.isTypeGroup === 2) {
                            htmltable += "<tbody>";
                        }
                        switch ($scope.opition.isTypeGroup) {
                            case 0:
                                htmltable += "<tr>";
                                htmltable += "<th width='50' style='border-top: 1px solid #000 !important;border-left: 1px solid #000;' align='center'>" + index_form_th + "</th>";
                                data_filter.forEach(function (cl) {
                                    if (cl.column != 'Tiendo' && cl.column != 'Trangthai' && cl.column != 'IsSLA') {
                                        if (form[cl.column] != null) {
                                            if (cl.column === "Title" || cl.column === "fulName_nguoitao" || cl.column === "Nguoiduyets") htmltable += "<td style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: left;'>" + form[cl.column] + "</td>";
                                            else htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + form[cl.column] + "</td>";
                                        }
                                        else htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'></td>";
                                    }
                                    else if (cl.column === "Trangthai") {
                                        let trangthai = ''
                                        if (form[cl.column] === 1) trangthai = "Mới lập";
                                        else if (form[cl.column] === 2) trangthai = "Đang trình";
                                        else if (form[cl.column] === 3) trangthai = "Hoàn thành";
                                        htmltable += "<td style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + trangthai + "</td>";
                                    }
                                    else if (cl.column === "Tiendo") {
                                        if (form[cl.column] != null) {
                                            htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + form[cl.column] + "%</td>";
                                        }
                                        else {
                                            htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'></td>";
                                        }
                                    }
                                    else if (cl.column === "IsSLA") {
                                        if (form[cl.column] != null) {
                                            let IsSLA = "";
                                            if (form['IsSLAXL']) {
                                                IsSLA = form['IsSLAXL'] + "/" + form[cl.column];
                                            }
                                            else {
                                                IsSLA = form[cl.column];
                                            }
                                            htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + IsSLA + "</td>";
                                        }
                                        else {
                                            htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'></td>";
                                        }
                                    }
                                });
                                htmltable += "</tr>";
                                index_form_th = index_form_th + 1;
                                break;
                            case 1:
                                htmltable += "<tr><th align='left' colspan='" + countFilter + "'>" + form.name_group_form + "</th></tr>";
                                let index_1 = 1;
                                if (form.list_ns) {
                                    form.list_ns.forEach(function (ns) {
                                        htmltable += "<tr>";
                                        htmltable += "<th width='50' style='border-top: 1px solid #000 !important;border-left: 1px solid #000;' align='center'>" + index_1 + "</th>";
                                        data_filter.forEach(function (cl) {
                                            if (cl.column != 'Tiendo' && cl.column != 'Trangthai' && cl.column != 'IsSLA') {
                                                if (ns[cl.column] != null) {
                                                    if (cl.column === "Title" || cl.column === "fulName_nguoitao" || cl.column === "Nguoiduyets") htmltable += "<td style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: left;'>" + ns[cl.column] + "</td>";
                                                    else htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + ns[cl.column] + "</td>";
                                                }
                                                else htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'></td>";
                                            }
                                            else if (cl.column === "Trangthai") {
                                                let trangthai = ''
                                                if (ns[cl.column] === 1) trangthai = "Mới lập";
                                                else if (ns[cl.column] === 2) trangthai = "Đang trình";
                                                else if (ns[cl.column] === 3) trangthai = "Hoàn thành";
                                                htmltable += "<td style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + trangthai + "</td>";
                                            }
                                            else if (cl.column === "Tiendo") {
                                                if (ns[cl.column] != null) {
                                                    htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + ns[cl.column] + "%</td>";
                                                }
                                                else {
                                                    htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'></td>";
                                                }
                                            }
                                            else if (cl.column === "IsSLA") {
                                                if (ns[cl.column] != null) {
                                                    let IsSLA = "";
                                                    if (ns['IsSLAXL']) {
                                                        IsSLA = ns['IsSLAXL'] + "/" + ns[cl.column];
                                                    }
                                                    else {
                                                        IsSLA = ns[cl.column];
                                                    }
                                                    htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + IsSLA + "</td>";
                                                }
                                                else {
                                                    htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'></td>";
                                                }
                                            }
                                        });
                                        htmltable += "</tr>";
                                        index_1 = index_1 + 1;
                                    });
                                }
                                break;
                            case 2:
                                htmltable += "<tr><th align='left' colspan='" + countFilter + "'>" + form.name_group_team + "</th></tr>";
                                let index_2 = 1;
                                if (form.list_ns) {
                                    form.list_ns.forEach(function (ns) {
                                        htmltable += "<tr>";
                                        htmltable += "<th width='50' style='border-top: 1px solid #000 !important;border-left: 1px solid #000;' align='center'>" + index_2 + "</th>";
                                        data_filter.forEach(function (cl) {
                                            if (cl.column != 'Tiendo' && cl.column != 'Trangthai' && cl.column != 'IsSLA') {
                                                if (ns[cl.column] != null) {
                                                    if (cl.column === "Title" || cl.column === "fulName_nguoitao" || cl.column === "Nguoiduyets") htmltable += "<td style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: left;'>" + ns[cl.column] + "</td>";
                                                    else htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + ns[cl.column] + "</td>";
                                                }
                                                else htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'></td>";
                                            }
                                            else if (cl.column === "Trangthai") {
                                                let trangthai = ''
                                                if (ns[cl.column] === 1) trangthai = "Mới lập";
                                                else if (ns[cl.column] === 2) trangthai = "Đang trình";
                                                else if (ns[cl.column] === 3) trangthai = "Hoàn thành";
                                                htmltable += "<td style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + trangthai + "</td>";
                                            }
                                            else if (cl.column === "Tiendo") {
                                                if (ns[cl.column] != null) {
                                                    htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + ns[cl.column] + "%</td>";
                                                }
                                                else {
                                                    htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'></td>";
                                                }
                                            }
                                            else if (cl.column === "IsSLA") {
                                                if (ns[cl.column] != null) {
                                                    let IsSLA = "";
                                                    if (ns['IsSLAXL']) {
                                                        IsSLA = ns['IsSLAXL'] + "/" + ns[cl.column];
                                                    }
                                                    else {
                                                        IsSLA = ns[cl.column];
                                                    }
                                                    htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + IsSLA + "</td>";
                                                }
                                                else {
                                                    htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'></td>";
                                                }
                                            }
                                        });
                                        htmltable += "</tr>";
                                        index_2 = index_2 + 1;
                                    });
                                }
                                break;
                            default:
                                htmltable += "<tr><th align='left' colspan='" + countFilter + "'>" + form.name_group_form + "</th></tr>";
                                if (form.list_team) {
                                    form.list_team.forEach(function (team) {
                                        htmltable += "<tbody>";
                                        let index_3 = 1;
                                        htmltable += "<tr><th cellpadding='10' style='border: none; border-bottom: 1px solid #000;' align='left' colspan='" + countFilter + "'>" + index_team + ': ' + team.name_group_team + "</th></tr>";
                                        if (team.list_ns) {
                                            team.list_ns.forEach(function (ns) {
                                                htmltable += "<tr>";
                                                htmltable += "<th width='50' style='border-top: 1px solid #000 !important;' align='center'>" + index_3 + "</th>";
                                                data_filter.forEach(function (cl) {
                                                    if (cl.column != 'Tiendo' && cl.column != 'Trangthai' && cl.column != 'IsSLA') {
                                                        if (ns[cl.column] != null) {
                                                            if (cl.column === "Title" || cl.column === "fulName_nguoitao" || cl.column === "Nguoiduyets") htmltable += "<td style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: left;'>" + ns[cl.column] + "</td>";
                                                            else htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + ns[cl.column] + "</td>";
                                                        }
                                                        else htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'></td>";
                                                    }
                                                    else if (cl.column === "Trangthai") {
                                                        let trangthai = ''
                                                        if (ns[cl.column] === 1) trangthai = "Mới lập";
                                                        else if (ns[cl.column] === 2) trangthai = "Đang trình";
                                                        else if (ns[cl.column] === 3) trangthai = "Hoàn thành";
                                                        htmltable += "<td style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + trangthai + "</td>";
                                                    }
                                                    else if (cl.column === "Tiendo") {
                                                        if (ns[cl.column] != null) {
                                                            htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + ns[cl.column] + "%</td>";
                                                        }
                                                        else {
                                                            htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'></td>";
                                                        }
                                                    }
                                                    else if (cl.column === "IsSLA") {
                                                        if (ns[cl.column] != null) {
                                                            let IsSLA = "";
                                                            if (ns['IsSLAXL']) {
                                                                IsSLA = ns['IsSLAXL'] + "/" + ns[cl.column];
                                                            }
                                                            else {
                                                                IsSLA = ns[cl.column];
                                                            }
                                                            htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'>" + IsSLA + "</td>";
                                                        }
                                                        else {
                                                            htmltable += "<td align='left' style='border-top: 1px solid #000 !important;border-left: 1px solid #000 !important;font-size:14px;width:150px;text-align: center;'></td>";
                                                        }
                                                    }
                                                });
                                                htmltable += "</tr>";
                                                index_3 = index_3 + 1;
                                            });
                                        }
                                        htmltable += "</td><tr>";
                                        htmltable += "</tbody>";
                                        index_team = index_team + 1;
                                    });
                                }
                                if ($scope.opition.isTypeGroup === 0 || $scope.opition.isTypeGroup === 1 || $scope.opition.isTypeGroup === 2) {
                                    htmltable += "</tbody>";
                                }
                                index_form_th = index_form_th + 1;
                                break;
                        }
                        
                    });

                    htmltable += "</table></div>";
                    return htmltable;
                }
            }
            return "";
        }
        $scope.ExportExcelDanhsach = function () {
            if ($scope.listData_Filter_Column == null || $scope.listData_Filter_Column.length == 0) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Bạn chưa chọn thông tin cần xuất !'
                });
                return false;
            }
            var htmltable = renderTable();
            var labelexecl = $scope.title_export;
            var name = labelexecl;
            var style = "";
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><meta charset="utf-8"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->' + style + '</head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            var ctx = { worksheet: name || 'Worksheet', table: htmltable.replace(/\u00a0/g, " ") };
            var url = uri + base64(format(template, ctx));
            var a = document.createElement('a');
            document.body.appendChild(a);
            a.href = url;
            a.download = name + '.xls';
            a.click();
            setTimeout(function () { window.URL.revokeObjectURL(url); }, 0);
        };
        $scope.PrintDanhsach = function () {
            if ($scope.listData_Filter_Column == null || $scope.listData_Filter_Column.length == 0) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Bạn chưa chọn thông tin cần xuất !'
                });
                return false;
            }
            swal.showLoading();
            var htmltable = renderTable();
            var popupWin = window.open('', '_blank', 'width=1400,height=400');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + htmltable + '</body></html>');
            popupWin.document.close();
            closeswal();
        };
        //============================================
        $scope.setPageSize = function (p) {
            $scope.opition.currentPage = 1;
            $scope.opition.numPerPage = p;
            $scope.Bind_List_Report_Dexuat(true);
        };
    }])