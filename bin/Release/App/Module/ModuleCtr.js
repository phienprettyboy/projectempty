﻿angular.module('ModuleCtr', [])
    .controller("ModuleCtr", ['$scope', '$rootScope', '$http', '$filter', 'Upload', '$interval', '$state', '$stateParams', '$sce', function ($scope, $rootScope, $http, $filter, Upload, $interval, $state, $stateParams, $sce) {
        //Phan trang
        $scope.trustAsHtml = function (html) {
            return $sce.trustAsHtml(html);
        }
        $scope.View = 1;
        $scope.setView = function (v) {
            $scope.View = v;
        };
        $scope.loadding = false;
        $scope.modules = [];
        modules = [];
        $scope.goModule = function (m) {
            if (m == null) {
                $scope.objModule = null;
                $scope.modules = modules;
            } else {
                $scope.objModule = m;
                $scope.modules = modules.filter(x => x.Parent_ID == m.Module_ID);
                console.log($scope.modules);
            }
        };
        $scope.BindListmodule = function () {
            $http({
                method: "POST",
                url: "Home/callProcNoPar",
                data: {
                    t: $rootScope.login.tk, proc: "Sadmin_Listmodule_All"
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    modules = data[0];
                    $scope.modules = data[0];
                }
            });
        };
        $scope.clearIPLogo = function () {
            $scope.module[n] = null;
            $scope.module["ip" + n] = null;
        };
        $scope.imageUploadIPLogo = function (f, n) {
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 1 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                }
                fi.name = name + fi.name;
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 1MB!'
                });
            }
            $scope.module["ip" + n] = f[0];
        }
        $scope.Addmodule = function (frm) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Module/Update_Module",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    var mo = Object.assign({}, $scope.module);
                    if (mo.iplogoweb) {
                        formData.append('logoweb', mo.iplogoweb, mo.iplogoweb.name);
                    }
                    if (mo.iplogoapp) {
                        formData.append('logoapp', mo.iplogoapp, mo.iplogoapp.name);
                    }
                    delete mo.iplogoweb;
                    delete mo.iplogoapp;
                    formData.append("t", $rootScope.login.tk);
                    formData.append("model", JSON.stringify(mo));
                    return formData;
                }
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: res.data.ms
                    });
                    return false;
                }
                $("#ModalAddmodule").modal("hide");
                showtoastr('Đã cập nhật module thành công!.');
                $scope.BindListmodule();
                //resetForm(frm);
            }, function (response) { // optional
                //resetForm(frm);
                $scope.loadding = false;
            });
        };
        $scope.Update_Trangthaimodule = function (cv, tt) {
            $http({
                method: "POST",
                url: "module/Update_Trangthaimodule",
                data: { t: $rootScope.login.tk, id: cv.module_ID, tt: tt },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                showtoastr('Đã cập nhật trạng thái thành công!.');
                cv.IsActive = tt;
            });
        };
        $scope.openModalAddmodule = function () {
            $scope.dtitle = "Cập nhật Module";
            $scope.module = {thutu: $scope.modules.length + 1, on_off: true, IsBase:true, IsApp:true};
            $("#ModalAddmodule").modal("show");
        };
        $scope.openModalEditmodule = function (p) {
            $scope.dtitle = "Cập nhật Module";
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Sadmin_Getmodule", pas: [
                        { "par": "Module_ID", "va": p.Module_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.module = data[0][0];
                    $("#ModalAddmodule").modal("show");
                }
            });
        };
        
        $scope.Delmodule = function (d) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa module này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "module/Del_module",
                        data: { t: $rootScope.login.tk, id: d.Module_ID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            var i = $scope.modules.findIndex(x => x.Module_ID === d.Module_ID);
                            if (i !== -1) {
                                $scope.modules.splice(i, 1);

                            }
                            showtoastr("Module bạn chọn đã được xóa thành công.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: res.data.ms
                            });
                        }
                    });

                }
            })
        };
        $scope.initOne = function () {
            $rootScope.link = "module";
            $scope.BindListmodule();
            $rootScope.TenDuan = "Danh sách module hệ thống";
        };

        $scope.initOne();
    }]);