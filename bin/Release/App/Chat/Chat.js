﻿// Component topbar
var chatURL = "http://schat.businessportal.vn/";
os.directive('onKeyup', function () {
    return function (scope, elm, attrs) {
        elm.bind("keyup", function () {
            scope.$apply(attrs.onKeyup);
        });
    };
});
os.component('chat', {
    templateUrl: domainUrl + 'App/Chat/Chat.html?v=' + vhtmlcache,
    controller: function ($scope, $rootScope, $http, $stateParams, $state, $filter, Upload, $sce, $timeout ) {
        $scope.noiDungChat={
            Noidung: ""
        };
        var chatCollection = database.ref().child('Chat');
        var first = true;
        this.$onInit = function () {
            if ($rootScope.login) {
                $scope.NewChats();
                var chat = document.getElementById('chat');
                chat.scrollTop = chat.scrollHeight - chat.clientHeight;
                $('#prime').click(function() {
                  toggleFab();
                });
            }
            if ($rootScope.login != null) {
                firebase.database().ref('users').on('value', function (snap) {
                    if (snap.val() != null) {
                        var data = (snap.val())[$rootScope.login.u.congtyID];
                        var ols = [];
                        for (k in data) {
                            let u = { NhanSu_ID:k};
                            let v = data[k];
                            if (v.connections) {
                                u.Online = true;
                                u.lastOnline = new Date();
                                ols.push(u);
                            } else {
                                u.Online = false;
                                u.lastOnline = new Date(v.lastOnline.time);
                            }
                        }
                        $scope.userRealTime = ols;
                        $scope.onlinelen = ols.length;
                        if(!$scope.Chats)$scope.Chats=[];
                        if ($scope.Chats.length > 0) {
                            $scope.Chats.forEach(function (r) {
                                var o = $scope.userRealTime.find(x => x.NhanSu_ID == r.nguoiChat);
                                if (o) {
                                    r.Online = o.Online;
                                    r.lastOnline = o.lastOnline;
                                }
                            });
                        }
                        if (!$scope.$$phase) {
                            $scope.$apply();
                        }
                    }
                });
                //Thêm nội dung chat
                chatCollection.child($rootScope.login.u.NhanSu_ID).on("value", function (doc) {
                    var obj = doc.val();
                    if (obj != null) {
                        if ($scope.Chat != null && $scope.Chat.ChatID == obj.ChatID) {
                            $scope.Chat.Mes.push(obj);
                            if (!$scope.$$phase) {
                                $scope.$apply();
                            }
                            document.getElementById("typing").style.visibility = "hidden";
                            $scope.NewChats();
                            $scope.goBottomChat();
                        } else {
                            $scope.addChat(obj.ChatID);
                            $scope.NewChats();
                        }
                    }
                });
            }
        };
        var timoutPromise = null;
        function setTyping(typ) {
            database.ref('Chat/' + $scope.Chat.ChatID).set({
                "uid": $rootScope.login.u.NhanSu_ID,
                "ten": $rootScope.login.u.fullName
                    .toString()
                    .substring($rootScope.login.u.fullName.toString().lastIndexOf(" ")),
                "typing": typ
            });
        }
        $scope.checkTyping = function () {
            setTyping(true);
            if (timoutPromise) {
                $timeout.cancel(timoutPromise);
            }
        }

        $scope.stoppedTyping = function () {
            timoutPromise = $timeout(function () {
                setTyping(false);
            }, 2000)

        }
        $scope.listenChat = function () {
            chatCollection.child($scope.Chat.ChatID).on("value", function (doc) {
                var obj = doc.val();
                if (!first && obj != null && obj.uid != $rootScope.login.u.NhanSu_ID) {
                    if (obj.typing) {
                        document.getElementById("typing").style.visibility = "visible";
                    } else {
                        document.getElementById("typing").style.visibility = "hidden";
                    }
                   
                }
                first = false;
            });
        }
        $scope.addChat = function (id) {
            if (id == null || id == "") {
                return false;
            }
            $http({
                method: "POST",
                url: chatURL + "/Chat/Chat",
                data: { t: $rootScope.login.tk, id: id },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                var obj = res.data;
                if (obj != null) {
                    obj.ngayLap = parseJsonDate(obj.ngayLap);
                    obj.ngayCapNhat = parseJsonDate(obj.ngayCapNhat);
                    if (obj.message != null) {
                        obj.message.ngayGui = parseJsonDate(obj.message.ngayGui);
                        if (obj.message.ngayGuiRep != null) {
                            obj.message.ngayGuiRep = parseJsonDate(obj.message.ngayGuiRep);
                        }
                    }
                    var idx = $scope.Chats.findIndex(x => x.ChatID == id); 
                    if (idx == -1) {
                        try{
                            $scope.Chats.unshift(obj);
                        }catch(e){
                            
                        }
                    } else {
                        $scope.Chats[idx] = obj;
                    }
                    //if (!$scope.$$phase) {
                    //    $scope.$apply();
                    //}

                }
            });
        }
        $scope.closeChat = function () {
            $scope.Chat = null;
        };
        $scope.SetCountChat = function () {
            if ($scope.Chat != null && $scope.Chat != undefined) {
                //var Chuadoc = $scope.Chat.chuaDoc;
                //$scope.Counts.c26 = parseFloat($scope.Counts.c26) - Chuadoc;
            }

            $http({
                method: "POST",
                url: chatURL + "/Chat/SetCountChat",
                data: { t: $rootScope.login.tk, id: $scope.Chat.ChatID },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                $scope.NewChats();
            });
        }

        function toggleFab() {
            $('#popupChat #prime .prime').toggleClass('fa-close');
            $('#popupChat #prime .prime').toggleClass('is-active');
            $('#popupChat #prime .prime').toggleClass('is-visible');
            $('#popupChat #prime').toggleClass('is-float');
            $('#popupChat>.center').toggleClass('is-visible');
            $('.fab').toggleClass('is-visible');
        }
        $scope.chatUser = function (c) {
            $scope.ReplyUser = {};
            $scope.ShowReplyView = 0;
            $scope.noiDungChat.Noidung = "";
            var ngl = $rootScope.login.u.NhanSu_ID == c.nguoiLap;
            if (ngl) {
                c.u = { anhDaiDien: c.anhnguoiLap, FullName: c.tennguoiLap, tenChucVu: c.tenChucVu, phone: c.phone }
            } else {
                c.u = { anhDaiDien: c.anhnguoiChat, FullName: c.tennguoiChat, tenChucVu: c.tenChucVu, phone: c.phone }
            }
            $scope.Chat = c;
            if ($scope.Chat != null && $scope.Chat != undefined) {
                //var Chuadoc = $scope.Chat.chuaDoc;
                $scope.listenChat();
            }
            $scope.loadMessage(true);
            $scope.Members();
        }
        $scope.goChat = function (u) {
            $http({
                method: "POST",
                url: chatURL + "/Chat/GoChat",
                data: { t: $rootScope.login.tk, congtyID: $rootScope.congtyID, id: u.NhanSu_ID },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                var obj = res.data;
                obj.ngayLap = parseJsonDate(obj.ngayLap);
                obj.ngayCapNhat = parseJsonDate(obj.ngayCapNhat);
                obj.u = u;
                obj.u.FullName = u.fullName;
                obj.p = 1;
                obj.Mes = [];
                $scope.Chat = obj;
                $scope.listenChat();
                $scope.loadMessage(true);
                $scope.Members();
                console.log($scope.Chat);
            });
        }

        $scope.loadMessage = function (load) {
            $http({
                method: "POST",
                url: chatURL + "/Chat/Message",
                data: { t: $rootScope.login.tk, id: $scope.Chat.ChatID, p: $scope.Chat.p },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                var obj = res.data;
                var d1 = new Date();
                var d2 = new Date();
                var sdate;
                angular.forEach(obj, function (o) {
                    o.truedate = false;
                    o.ngayGui = parseJsonDate(o.ngayGui);
                    o.ngayGuiRep = parseJsonDate(o.ngayGuiRep);
                    if (sdate != o.ngay) {
                        o.truedate = true;
                        sdate = o.ngay;
                        o.ngay = parseJsonDate(o.ngay);
                    }
                });
                if (obj.length > 0) {
                    obj[0].truedate = true;
                }
                $scope.Chat.Mes = obj;
                var setcount = $scope.Chats.find(x => x.ChatID == $scope.Chat.ChatID);
                if (setcount != null) {
                    setcount.me.chuaDoc = 0;
                }
                if (load) {
                    $scope.goBottomChat();
                }
            });
        }

        $scope.Members = function () {
            $http({
                method: "POST",
                url: chatURL + "/Chat/Member",
                data: { t: $rootScope.login.tk, id: $scope.Chat.ChatID },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                var obj = res.data;
                angular.forEach(obj, function (o) {
                    o.ngayThamgia = parseJsonDate(o.ngayThamgia);
                    o.ngayThoat = parseJsonDate(o.ngayThoat);
                })
                $scope.Chat.Members = obj;
            });
        }

        $scope.goBottomChat = function () {
            setTimeout(function () {
                var div = document.getElementById("chat");
                div.scrollTop = div.scrollHeight - div.clientHeight + 100;
            }, 200);
            $scope.SetCountChat();
        }
        $scope.NewChats = function () {
            $http({
                method: "POST",
                url: chatURL + "/Chat/NewChats",
                data: { t: $rootScope.login.tk },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                var obj = res.data;
                var chuadoc = 0;
                var countChuaDoc = 0;
                angular.forEach(obj, function (o) {
                    o.ngayLap = parseJsonDate(o.ngayLap);
                    o.ngayCapNhat = parseJsonDate(o.ngayCapNhat);
                    if (o.message != null) {
                        o.message.ngayGui = parseJsonDate(o.message.ngayGui);
                    }
                    if (o.me != null) {
                        chuadoc = parseInt(chuadoc + o.me.chuaDoc);
                    }
                    countChuaDoc += o.countChuaDoc;
                });

                obj.chuadoc = chuadoc;
                obj.countChuaDoc = countChuaDoc;
                if (obj.length > 0 && $scope.userRealTime) {
                    obj.forEach(function (r) {
                        var o = $scope.userRealTime.find(x => x.NhanSu_ID == r.nguoiChat);
                        if (o) {
                            r.Online = o.Online;
                            r.lastOnline = o.lastOnline;
                        }
                    });
                }
                $scope.Chats = obj;
                var IsshowAdmin = obj.findIndex(x => x.nguoiChat === "administrator") === -1;
                $scope.administrator = IsshowAdmin ? { NhanSu_ID: "administrator", fullName: "Smart Office", anhDaiDien:"/logoso.png" } : null;
                if ($rootScope.IDChatMessage != null) {
                    var c = this.Chats.find(x => x.ChatID == $rootScope.IDChatMessage);
                    $rootScope.IDChatMessage = null;
                    $scope.chatUser(c);
                } else if ($rootScope.UChatMessage) {
                    let use = $rootScope.users.find(x => x.NhanSu_ID === $rootScope.UChatMessage);
                    if (use != null) {
                        $scope.goChat(use);
                    }
                }
                //else if (!$scope.Chat && $scope.Chats.length > 0) {
                //    $rootScope.IDChatMessage = null;
                //    //$scope.chatUser($scope.Chats[0]);
                //    if ($('#popupChat #prime .prime').hasClass('fa-close')) {

                //    }
                //}
            });
        }
        $rootScope.bgColor = [
            "#2196f3", "#009688", "#ff9800", "#795548", "#ff5722", "#ff5722"
        ];

        $scope.sendMS = function (loai) {
            if (event.which == 13) {
                if (event.shiftKey || event.altKey || event.ctrlKey) {
                    $scope.noiDungChat.Noidung = $scope.noiDungChat.Noidung + '\n';
                    return false;
                }
            }
            var ms = {
                ChatID: $scope.Chat.ChatID, nguoiGui: $rootScope.login.u.NhanSu_ID, noiDung: $scope.noiDungChat.Noidung, loai: loai, ngayGui: new Date().toUTCString(),
                fullName: $rootScope.login.u.fullName, anhDaiDien: $rootScope.login.u.anhDaiDien,
            };

            //$scope.Chat.me.chuaDoc = 0;
            if ($scope.ReplyUser) {
                ms.ParentID = $scope.ReplyUser.Rep_MessageID || null;
                ms.noiDungRep = $scope.ReplyUser.Rep_NoiDung || null;
                ms.tenNguoiGuiRep = $scope.ReplyUser.Rep_TenNhanSu || null;
                ms.ngayGuiRep = $scope.ReplyUser.Rep_NgayGui || null;
            }
            //chatCollection.child($scope.Chat.ChatID).set(ms);
            angular.forEach($scope.Chat.Members.filter(x => x.nguoiThamGia != $rootScope.login.u.NhanSu_ID), function (m) {
                database.ref('Chat/' + m.nguoiThamGia).set(ms);
            })
            ///


            //
            $scope.Chat.Mes.push(ms);
            $scope.ReplyUser = {};
            $scope.ShowReplyView = 0;
            ///
            $scope.noiDungChat.Noidung = "";
            $scope.goBottomChat();
            $http({
                method: "POST",
                url: chatURL + "/Chat/AddMes",
                data: { t: $rootScope.login.tk, ms: ms, id: $scope.Chat.ChatID },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                $("textarea#noiDungChat").val(null);
                $scope.NewChats();
                // Sau khi chat set lai Reply
                $scope.ReplyUser = {};
                $scope.ShowReplyView = 0;
            });
        }
        $scope.sendMSFile = function (loai) {
            var ms = { ChatID: $scope.Chat.ChatID, nguoiGui: $rootScope.login.u.NhanSu_ID, noiDung: "", loai: loai, ngayGui: new Date().toUTCString(), fullName: $rootScope.login.u.fullName, anhDaiDien: $rootScope.login.u.anhDaiDien };

            $http({
                method: 'POST',
                url: chatURL+"/Chat/AddMesFromDataWeb",
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: function () {
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    formData.append("ms", JSON.stringify(ms));
                    formData.append("id", $scope.Chat.ChatID);
                    if (loai == 1) {
                        if ($('input[name=FileImages]').val() !== "") {
                            formData.append('IsFile', $scope.FileImages);
                        };
                    } else if (loai == 2) {
                        if ($('input[name=FileAttach]').val() !== "") {
                            formData.append('IsFile', $scope.FileAttach);
                        }
                    }
                    return formData;
                }
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error == 0) {
                    var ms = res.data.ms;
                    ms.ngayGui = new Date().toUTCString();
                    ms.anhDaiDien = $rootScope.login.u.anhDaiDien;
                    $scope.Chat.Mes.push(ms);
                    $scope.noiDungChat.Noidung = "";
                    angular.forEach($scope.Chat.Members.filter(x => x.nguoiThamGia != $rootScope.login.u.NhanSu_ID), function (m) {
                        database.ref('Chat/' + m.nguoiThamGia).set(ms);
                    })
                    $scope.goBottomChat();
                    $("textarea#noiDungChat").val(null);
                    $('input[name=FileImages]').val(null);
                    $('input[name=FileAttach]').val(null);
                }
            });
        }

        $scope.RemoveMessage = function (item) {
            //dlg = dialogs.confirm("Xác nhận", 'Bạn có chắc muốn xóa tin nhắn này không?', { windowClass: "apidialog", size: "sm" });
            Swal.fire({
                title: 'Xác nhận',
                text: "Bạn có chắc muốn xóa tin nhắn này không?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: chatURL + "/Chat/DelMs",
                        //headers: headers,
                        data: { t: $rootScope.login.tk, c: $scope.Chat.ChatID, id: item.MessageID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error === 1) {
                            hideloading();
                            Swal.fire({
                                title: 'Thông báo ',
                                text: "Lỗi khi xóa tin nhắn !",
                                icon: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#2196f3',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'OK!',
                                //cancelButtonText: 'Không!',
                            });

                        } else {
                            $scope.loadMessage();
                        }
                    });
                }

            });
        }

        $scope.RemoveChat = function (item) {
            //if (item == null) {
            //    dialogs.error('Thông báo', 'Bạn chưa chọn cuộc hội thoại nào để xóa !', { windowClass: "apidialog", size: "sm" });
            //}
            var obj = { ChatID: item.ChatID };
            Swal.fire({
                title: 'Thông báo!',
                text: "Bạn có chắc chắn muốn xóa cuộc hội thoại này không?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: chatURL + "/Chat/DelChat",
                        //headers: headers,
                        data: { t: $rootScope.login.tk, c: obj },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error === 1) {
                            hideloading();
                            Swal.fire({
                                title: 'Thông báo ',
                                text: "Lỗi khi xóa cuộc hội thoại !",
                                icon: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#2196f3',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'OK!',
                                //cancelButtonText: 'Không!',
                            });

                        } else {
                            if ($scope.Chat != null && $scope.Chat.ChatID == item.ChatID) {
                                $scope.Chat = null;
                            }
                            $scope.NewChats();
                        }
                    });
                }

            });
        }

        $scope.RemoveChatGroup = function (item) {
            //if (item == null) {
            //    dialogs.error('Thông báo', 'Bạn chưa chọn nhóm chat nào để xóa !', { windowClass: "apidialog", size: "sm" });
            //}
            var obj = { ChatID: item.ChatID };
            Swal.fire({
                title: 'Thông báo!',
                text: "Bạn có chắc chắn muốn xóa nhóm Chat này không?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: chatURL + "/Chat/xoaNhom",
                        //headers: headers,
                        data: { t: $rootScope.login.tk, ChatID: item.ChatID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error === 1) {
                            hideloading();
                            Swal.fire({
                                title: 'Thông báo ',
                                text: "Lỗi khi xóa cuộc hội thoại !",
                                icon: 'warning',
                                showCancelButton: false,
                                confirmButtonColor: '#2196f3',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'OK!',
                                //cancelButtonText: 'Không!',
                            });

                        } else {
                            if ($scope.Chat != null && $scope.Chat.ChatID == item.ChatID) {
                                $scope.Chat = null;
                            }
                            $scope.NewChats();
                            //$scope.GroupChat();
                        }
                    });
                }

            });
        }
        $scope.openImage = function () {
            $("#popupChat input.inputimgChat").click();
        }
        $scope.openFile = function () {
            $("#popupChat input.inputfileChat").click();
        }

        $scope.UploadFile = function (file, type) {
            if (type == 1) {
                $scope.FileImages = $('input[name=FileImages]')[0].files[0];
                $scope.sendMSFile(1);
            } else if (type == 2) {
                $scope.FileAttach = $('input[name=FileAttach')[0].files[0];
                $scope.sendMSFile(2);
            }
            type = 0;
        }
    }
});