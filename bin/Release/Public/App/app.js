rootUrl = "localhost:57187";
baseUrl = "http://" + rootUrl + "/";
//rootUrl = "businessportal.vn";
//baseUrl = "http://srequest." + rootUrl + "/";
domainUrl = "http://businessportal.vn/";
Date.prototype.getWeek = function (dowOffset) {
    /*getWeek() was developed by Nick Baicoianu at MeanFreePath: http://www.meanfreepath.com */

    dowOffset = typeof (dowOffset) == 'int' ? dowOffset : 0; //default dowOffset to zero
    var newYear = new Date(this.getFullYear(), 0, 1);
    var day = newYear.getDay() - dowOffset; //the day of week the year begins on
    day = (day >= 0 ? day : day + 7);
    var daynum = Math.floor((this.getTime() - newYear.getTime() -
        (this.getTimezoneOffset() - newYear.getTimezoneOffset()) * 60000) / 86400000) + 1;
    var weeknum;
    //if the year starts before the middle of a week
    if (day < 4) {
        weeknum = Math.floor((daynum + day - 1) / 7) + 1;
        if (weeknum > 52) {
            nYear = new Date(this.getFullYear() + 1, 0, 1);
            nday = nYear.getDay() - dowOffset;
            nday = nday >= 0 ? nday : nday + 7;
            /*if the next year starts before the middle of
              the week, it is week #1 of that year*/
            weeknum = nday < 4 ? 1 : 53;
        }
    }
    else {
        weeknum = Math.floor((daynum + day - 1) / 7);
    }
    return weeknum;
};
var os = angular.module("os", ['dndLists', 'ckeditor','oi.select','angular-nicescroll','ui.utils.masks', 'ui.router', 'oc.lazyLoad', 'angularLazyImg', 'uiSwitch', 'ngtimeago', 'angularTreeview', 'ngFileUpload', 'treeGrid', 'ckeditor', 'ang-drag-drop', 'angular.filter', 'htmlToPdfSave', 'jsTag', 'angucomplete', 'ngSanitize', 'ui.bootstrap', 'ui.bootstrap.datetimepicker', 'csCurrencyInput', 'ui.dateTimeInput', 'ngAnimate', 'smart-table', 'vs-repeat', 'apg.typeaheadDropdown']);
os.config(['$ocLazyLoadProvider', '$stateProvider', '$compileProvider', '$httpProvider', '$urlRouterProvider', '$locationProvider', '$sceDelegateProvider', function ($ocLazyLoadProvider, $stateProvider, $compileProvider, $httpProvider, $urlRouterProvider, $locationProvider, $sceDelegateProvider) {
    $compileProvider.debugInfoEnabled(false);
    $sceDelegateProvider.resourceUrlWhitelist([
        'self',
        domainUrl+'**'
    ]);
    $locationProvider.hashPrefix('');
    $httpProvider.useApplyAsync(1000); //true
    $ocLazyLoadProvider.config({
        'debug': false, // For debugging 'true/false'
        'events': false, // For Event 'true/false'
        'modules': [
            bindModulesFolder("Request", "nhomform/:type", "RequestCtr"),
            bindModulesFolder("Request", "mauform/:type", "RequestCtr"),
            bindModulesFolder("Request", "khaibaoform/:type", "RequestCtr"),
            bindModulesFolder("Request", "nhomteam/:type", "RequestCtr"),
            bindModulesFolder("Request", "khaibaoteam/:type", "RequestCtr"),
            bindModulesFolder("Request", "formsetupteam/:type", "RequestCtr"),
            bindModulesFolder("Request", "formsetupteam/:type/:form/:team", "RequestCtr"),
            bindModulesFolder("Home", "/", "HomeCtr"), 
        ]
    });
    $stateProvider
        .state('nhomform', bindStatePar("nhomform/:type", "Request/Nhomform/Nhomform", "RequestCtr"))
        .state('mauform', bindStatePar("mauform/:type", "Request/Mauform/Mauform", "RequestCtr"))
        .state('khaibaoform', bindStatePar("khaibaoform/:type", "Request/Khaibaoform/Khaibaoform", "RequestCtr"))
        .state('nhomteam', bindStatePar("nhomteam/:type", "Request/Nhomteam/Nhomteam", "RequestCtr"))
        .state('khaibaoteam', bindStatePar("khaibaoteam/:type", "Request/Khaibaoteam/Khaibaoteam", "RequestCtr"))
        .state('formsetupteam', bindStatePar("formsetupteam/:type", "Request/Formsign/formsetupteam", "RequestCtr"))
        .state('formsign', bindStatePar("formsetupteam/:type/:form/:team", "Request/Formsign/formsign", "RequestCtr"))
        .state('/', bindStateParHome("/", "Home/Home", "HomeCtr"))
    $urlRouterProvider.otherwise('/');
}]);
os.run(function ($rootScope, $templateCache) {
    $rootScope.domainUrl = domainUrl;
    var cookie = readCookie("U");
    //if (localStorage.getItem("lo") != cookie && cookie && cookie != "" && cookie != "null") {
    //    localStorage.setItem('lo', cookie);
    //    localStorage.setItem('au', cookie);
    //}
    //else if (!cookie || cookie == "" || cookie == "null") {
    //    deleteAllCookies();
    //    setCookieUserDomain("");
    //    localStorage.removeItem("lo");
    //    localStorage.removeItem("u");
    //}
    if (!localStorage.getItem("lo") && cookie && cookie != "" && cookie != "null") {
        localStorage.setItem('lo', cookie);
        localStorage.setItem('au', cookie);
    } 
    if (localStorage.getItem('au')) {
        $rootScope.objgetLogin = JSON.parse(decr(localStorage.getItem('au')));
    }
    $rootScope.$on('$routeChangeStart', function (event, next, current) {
        if (typeof current !== 'undefined') {
            $templateCache.remove(current.templateUrl);
            $templateCache.remove(turl);
        }
    });
    $rootScope.vcache = vhtmlcache;
    var link = location.href;
    var linkcheck = '/ResetPassowrd/IDKey/';
    if (link.indexOf(linkcheck) > 0) {
        $rootScope.isLogin = true;
    }
    $rootScope.pz = 20;
    if (localStorage.getItem("lo") !== null) {
        $rootScope.login = JSON.parse(decr(localStorage.getItem("lo")));
        $rootScope.isLogin = true;
        if ($rootScope.login && $rootScope.login.u) {
            $rootScope.Onlines = [];
            $rootScope.congtyID = $rootScope.login.u.congtyID;

            $rootScope.logOut = function () {
                deleteAllCookies();
                setCookieUserDomain("");
                localStorage.removeItem("lo");
                localStorage.removeItem("u");
                location.href = baseUrl;
            };
            $rootScope.checkToken = function (res) {
                if (res.data.token === 0) {
                    if (!checkTK) {
                        dlg = dialogs.confirm("Thông báo", 'Token đã hết hạn, vui lòng đăng nhập lại ?', { windowClass: "apidialog", size: "sm" });
                        dlg.result.then(function () {
                            $rootScope.logOut(); checkTK = false;
                        }, function () {
                            $rootScope.logOut(); checkTK = false;
                        });
                    }
                    checkTK = true;
                    hideloading();
                    return false;
                }
                return true;
            };
        }
    }
});
os.controller("MainCtr", ['$scope', '$rootScope', '$state', '$http', '$anchorScroll', function ($scope, $rootScope, $state, $http, $anchorScroll) {
    $scope.rootSearch = function () {
        $scope.$broadcast('Search');
    };
    
    $scope.goMenu = function (key) {
        switch (key) {
            case "nhomform":
                $rootScope.link = "nhomform";
                $state.go('nhomform', { type: 1 });
                break;
            case "mauform":
                $rootScope.link = "mauform";
                $state.go('mauform', { type: 2 });
                break;
            case "khaibaoform":
                $rootScope.link = "khaibaoform";
                $state.go('khaibaoform', { type: 3 });
                break;
            case "nhomteam":
                $rootScope.link = "Nhomteam";
                $state.go('nhomteam', { type: 4 });
                break;
            case "khaibaoteam":
                $rootScope.link = "khaibaoteam";
                $state.go('khaibaoteam', { type: 5 });
                break;
            case "formsetupteam":
                $rootScope.link = "formsetupteam";
                $state.go('formsetupteam', { type: 6 });
        }
    };
    $scope.goMenuHome = function () {
        $rootScope.link = null;
        $state.go('/');
    };
    $scope.openFile = function (name, url) {
        saveAs(baseUrl + url, name);
    };
    $scope.openFileC = function (name, url) {
        saveAs(url, name);
    };
    $scope.BindListModule = function () {
        $http({
            method: "POST",
            url: "Home/CallProc",
            data: {
                t: $rootScope.login.tk, proc: "SOE_List_Module", pas: [
                    { "par": "Users_ID", "va": $rootScope.login.u.NhanSu_ID }
                ]
            },
            contentType: 'application/json; charset=utf-8'
        }).then(function (res) {
            if (!$rootScope.checkToken(res)) return false;
            if (res.data.error !== 1) {
                $rootScope.modules = JSON.parse(res.data.data)[0];   
            }
        });
    };

    $scope.BindListUser = function () {
        $http({
            method: "POST",
            url: "Home/CallProc",
            data: {
                t: $rootScope.login.tk, proc: "BindListUser", pas: [
                    { "par": "Congty_ID", "va": $rootScope.login.u.congtyID }
                ]
            },
            contentType: 'application/json; charset=utf-8'
        }).then(function (res) {
            if (!$rootScope.checkToken(res)) return false;
            if (res.data.error !== 1) {
                var users = JSON.parse(res.data.data);
                if (users[0].length > 0) {
                    if (users[0][0].IDCha == null)
                        $rootScope.congty = users[0];
                    else
                        $rootScope.congty = users[0][0];
                    $rootScope.congtyIDcha = $rootScope.congty.IDCha;
                    localStorage.setItem('congty', JSON.stringify($rootScope.congty));
                }
                if (users[1].length > 0) {
                    $rootScope.childcongtys = users[1];
                }
                if (users[2].length > 0) {
                    $rootScope.roles = users[2];
                    if ($rootScope.congty) {
                        $rootScope.ctyroles = $rootScope.roles.filter(r => r.CongTy_ID === $rootScope.congty[0].Congty_ID);
                    }
                }
                if (users[3].length > 0) {
                    $rootScope.phongbans = users[3];
                }
                if (users[4].length > 0) {
                    $rootScope.users = users[4];
                    //  $rootScope.me = $rootScope.users.find(u => u.User_ID === $rootScope.login.u.User_ID);
                    $rootScope.me = $rootScope.users.find(u => u.NhanSu_ID === $rootScope.login.u.NhanSu_ID);
                }
                $rootScope.phongbans.forEach(function (p) {
                    p.users = $rootScope.users.filter(u => u.phongbans !== null && u.phongbans.indexOf(p.Phongban_ID) !== -1);
                });
                $scope.$broadcast('Contact');
            }
        });
    };

    $scope.BindListFuncCT = function () {
        $http({
            method: "POST",
            url: "Home/CallProc",
            data: {
                t: $rootScope.login.tk, proc: "Scalendar_ListFuncCT", pas: [
                    { "par": "congtyID", "va": $rootScope.login.u.congtyID }
                ]
            },
            contentType: 'application/json; charset=utf-8'
        }).then(function (res) {
            if (!$rootScope.checkToken(res)) return false;
            if (res.data.error !== 1) {

                var data = JSON.parse(res.data.data)[0];
                $rootScope.ListDataFuncCT = data[0]
            }
        });
    };

    $scope.BindListRoleNS = function () {
        $http({
            method: "POST",
            url: "Home/CallProc",
            data: {
                t: $rootScope.login.tk, proc: "Sadmin_ListRoleNS", pas: [
                    { "par": "congtyID", "va": $rootScope.login.u.congtyID },
                    { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID }
                ]
            },
            contentType: 'application/json; charset=utf-8'
        }).then(function (res) {
            if (!$rootScope.checkToken(res)) return false;
            if (res.data.error !== 1) {

                var data = JSON.parse(res.data.data);
                $rootScope.roleModule = data[0];
                $rootScope.roleNS = data[1];
                $rootScope.nsIsAdmin = data[2];
            }
        }).then(function () {
            var rosAdmin = [];
            $rootScope.roleAdmin = false;
            angular.forEach($rootScope.roleModule.filter(x => x.NhanSuRole_ID != null), function (m) {
                if (m.IsFlag == 'AD') {
                    rosAdmin = $rootScope.roleNS.filter(x => x.Role_ID == m.Role_ID && x.Role_ID != null);
                }
            });
            angular.forEach($rootScope.nsIsAdmin, function (r) {
                rosAdmin.push(r);
            })
            if (rosAdmin.length > 0) {
                $rootScope.roleAdmin = true;
            }
        });
    };
    $scope.BindListRoleFC = function () {
        $http({
            method: "POST",
            url: "Home/CallProc",
            data: {
                t: $rootScope.login.tk, proc: "GetRoleFunction", pas: [
                    { "par": "congtyID", "va": $rootScope.login.u.congtyID },
                    { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                    { "par": "Module", "va": 'S06' }
                ]
            },
            contentType: 'application/json; charset=utf-8'
        }).then(function (res) {

            if (!$rootScope.checkToken(res)) return false;
            if (res.data.error !== 1) {
                var data = JSON.parse(res.data.data)[0][0];
                $rootScope.ListDataRoleFunction = data;
            }
        });
    };

    Date.prototype.getWeek = function () {
        var target = new Date(this.valueOf());
        var dayNr = (this.getDay() + 6) % 7;
        target.setDate(target.getDate() - dayNr + 3);
        var firstThursday = target.valueOf();
        target.setMonth(0, 1);
        if (target.getDay() != 4) {
            target.setMonth(0, 1 + ((4 - target.getDay()) + 7) % 7);
        }
        return 1 + Math.ceil((firstThursday - target) / 604800000);
    };

    Date.prototype.getWeek = function (date) {
        //Calcing the starting point
        var now = date ? new Date(date) : new Date();

        // set time to some convenient value
        now.setHours(0, 0, 0, 0);

        // Get the previous Monday
        var monday = new Date(now);
        monday.setDate(monday.getDate() - monday.getDay() + 1);

        // Get next Sunday
        var sunday = new Date(now);
        sunday.setDate(sunday.getDate() - sunday.getDay() + 7);

        // Return array of date objects
        return [monday, sunday];
    }

    var Dates = new Date().getWeek();
    
    function getWeekNumber(d) {
        // Copy date so don't modify original
        d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
        // Set to nearest Thursday: current date + 4 - current day number
        // Make Sunday's day number 7
        d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
        // Get first day of year
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
        // Calculate full weeks to nearest Thursday
        var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
        // Return array of year and week number
        return [d.getUTCFullYear(), weekNo];
    }
    var getDates = function (startDate, endDate) {
        var dates = [],
            currentDate = startDate,
            addDays = function (days) {
                var date = new Date(this.valueOf());
                date.setDate(date.getDate() + days);
                return date;
            };
        while (currentDate <= endDate) {
            dates.push(currentDate);
            currentDate = addDays.call(currentDate, 1);
        }
        return dates;
    };

    function weeksInYear(year) {
        var Tuans = [];
        var max = Math.max(
            moment(new Date(year, 11, 31)).isoWeek()
            , moment(new Date(year, 11, 31 - 7)).isoWeek()
        );
        for (var i = 0; i < max; i++) {
            var d = getDateByWeek(i, year);
            var de = getDateByWeek(i, year);
            var dn = new Date(de.setDate(de.getDate() + 6));
            Tuans.push({ Tuan: "Tuần " + (i + 1), TuNgay: d, DenNgay: dn, hientai: $scope.soTuanTrongNam === i + 1, stt: i + 1, quakhu: i < $scope.soTuanTrongNam - 1 });
        }
        return Tuans;
    }

    function isValidDate(d) {
            return d instanceof Date && !isNaN(d);
        }
    $rootScope.ListCountLHT = function () {
        $scope.NgayDauTuan = Dates[0];
        $scope.NgayCuoiTuan = Dates[1];
        initdate = Dates;
        //khởi tạo result lấy ra số tuần và số năm 
        var result = getWeekNumber($scope.NgayDauTuan);
        $scope.soTuanTrongNam = result[1];
        $scope.soNamCuaTuan = result[0];
        //get thứ 2-7
        $scope.datesarr = getDates(new Date(Dates[0]), new Date(Dates[1]));
        //lấy ra thứ 2 tuần tới
        var getMondayNextWeek = new Date();
        getMondayNextWeek.setDate(getMondayNextWeek.getDate() + (1 + 7 - getMondayNextWeek.getDay()) % 7);
        $scope.getMondayNextWeek = getMondayNextWeek;
        //
        $scope.datesarr = [];
        $scope.Tuans = weeksInYear($scope.getYear);
        $scope.activeIndex = $scope.Tuans.findIndex(x => x.hientai) - 2;

        if ($rootScope.NgayDauTuan != null && $rootScope.NgayCuoiTuan != null) {
            $scope.NgayDauTuan = $rootScope.NgayDauTuan;
            $scope.NgayCuoiTuan = $rootScope.NgayCuoiTuan;

            //if ($rootScope.soTuanTrongNam != null) {
            //    $scope.Tuans.filter(x => x.stt == $rootScope.soTuanTrongNam).forEach(function (r) {
            //        r.active = true;
            //    });
            //    $scope.Thangs.filter(x => x.t == $rootScope.soTuanTrongNam).forEach(function (r) {
            //        r.active = true;
            //    });
            //}
        }

        var bd = $scope.NgayDauTuan;
        var kt = $scope.NgayCuoiTuan;
        if (isValidDate(bd)) {
            bd = bd.toDateString();
        } else {
            bd = new Date().toDateString();
        }
        if (isValidDate(kt)) {
            kt = kt.toDateString();
        } else {
            kt = new Date().toDateString();
        }
        $http({
            method: "POST",
            url: "Home/CallProc",
            data: {
                t: $rootScope.login.tk, proc: "Scalendar_CountLHT", pas: [
                    { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                    { "par": "NgayDauTuan", "va": bd },
                    { "par": "NgayCuoiTuan", "va": kt },
                ]
            },
            contentType: 'application/json; charset=utf-8'
        }).then(function (res) {
            if (!$rootScope.checkToken(res)) return false;
            if (res.data.error !== 1) {
                var data = JSON.parse(res.data.data)[0];
                $rootScope.countLHT = data[0];
            }
        });
    };
    $rootScope.ListCountKH = function () {
        $http({
            method: "POST",
            url: "Home/CallProc",
            data: {
                t: $rootScope.login.tk, proc: "PlanTrip_CountKHCT", pas: [
                    { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID }
                ]
            },
            contentType: 'application/json; charset=utf-8'
        }).then(function (res) {
            if (!$rootScope.checkToken(res)) return false;
            if (res.data.error !== 1) {
                var data = JSON.parse(res.data.data)[0];
                $rootScope.countKHCT = data[0];
            }
        });
    };
    $anchorScroll.yOffset = 0;

    $rootScope.bgColor = [
        "#F8E69A", "#AFDFCF", "#F4B2A3", "#9A97EC", "#CAE2B0", "#8BCFFB", "#CCADD7"
    ];

    if ($rootScope.login) {
        $rootScope.domainUrl = domainUrl;
        $rootScope.TenDuan = $rootScope.login.u.tenCongty;
        $rootScope.fileUrl = "http://sfile.businessportal.vn/";
        $rootScope.href_hd = domainUrl + 'Portals/HDSD_SCalendar_LichCongtac.htm';
        $scope.BindListModule();
        $scope.BindListUser();
        $scope.BindListFuncCT();
        $rootScope.ListCountLHT();
        $rootScope.ListCountKH();
        $scope.BindListRoleNS();
        $scope.BindListRoleFC();
        var tlt = ($rootScope.login.u.fullName + '<br/>') + ($rootScope.login.u.tenChucVu !== null ? ($rootScope.login.u.tenChucVu + '<br/>') : '') + ($rootScope.login.u.tenPhongban !== null ? $rootScope.login.u.tenPhongban : '');
        $rootScope.login.u.tooltip = tlt;

        $rootScope.Cache_Group_FormSign = [];
    }
}]);

os.controller("LoginCtr", ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
    $scope.login = { tenTruyCap: '', matKhau: '', remer: true };
    let u = null;
    try {
        u = localStorage.getItem('u') != null ? JSON.parse(decr(localStorage.getItem('u'))) : null;
    } catch (e) {
        u = null;
    }
    if (u !== null) {
        $scope.login = { Users_ID: u.Users_ID, IsPassword: u.IsPassword, remer: u.remer };
    }
    $scope.err = { errAccount: "", errPass: "" };
    $scope.loadding = false;
    $scope.Login = function () {
        if ($scope.loadding) return false;
        if ($scope.login.tenTruyCap === null || $scope.login.tenTruyCap === undefined || $scope.login.tenTruyCap.trim() === "") {
            $scope.err.errAccount = "* Tên đăng nhập không được để trống!";
            $("input[name='tenTruyCap']").focus();
            Swal.fire({
                type: 'error',
                title: '',
                text: 'Tên đăng nhập không được để trống!'
            });
            return false;
        } else {
            $scope.err.errAccount = "";
        }
        if ($scope.login.matKhau === null || $scope.login.matKhau === undefined || $scope.login.matKhau.trim() === "") {
            $scope.err.errPass = "* Mật khẩu không được để trống!";
            Swal.fire({
                type: 'error',
                title: '',
                text: 'Mật khẩu không được để trống!'
            });
            $("input[name='matKhau']").focus();
            return false;
        } else {
            $scope.err.errPass = "";
        }
        swal.showLoading();
        $scope.loadding = true;
        $.ajax({
            type: "POST",
            url: "/Login/CheckLoginEn",
            data: JSON.stringify({ str: encr(JSON.stringify($scope.login)).toString() }),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                $scope.err.errAccount = "";
                $scope.err.errPass = "";
                if (data === null || data.trim().length===0) {
                    $scope.err.ms = "* Tên đăng nhập hoặc mật khẩu không đúng!";
                    $scope.loadding = false;
                    Swal.fire({
                        type: 'error',
                        title: '',
                        text: 'Tên đăng nhập hoặc mật khẩu không đúng!'
                    });
                } else {
                    $rootScope.isLogin = true;
                    $scope.loadding = false;
                    if ($scope.login.remer) {
                        localStorage.setItem('u', encr(JSON.stringify($scope.login)));
                    }
                    setCookieUserDomain(data);
                    localStorage.setItem('lo', data);
                    localStorage.setItem('au', data);
                    location.href = baseUrl;
                    $scope.loadding = false;
                }
            },
            error: function (result) {
                Swal.fire({
                        type: 'error',
                        title: '',
                        text: 'Tên đăng nhập hoặc mật khẩu không đúng!'
                    });
                $scope.loadding = false;
                $scope.err.ms = "* Tên đăng nhập hoặc mật khẩu không đúng!";
            },
            always: function () {
                $scope.loadding = false;
            }
        });
    };

    $scope.QuenMatKhau = function () {
        $rootScope.mTitleRePass = "Quên mật khẩu";
        $("#ModalRememberPass").modal("show");
    };
    $scope.GuiMailRePass = function () {
        showloading();
        $http({
            method: "POST",
            url: "/Login/SendMailKH",
            headers: {
                'Content-Type': undefined
            },
            transformRequest: function () {
                var formData = new FormData();
                formData.append("NhanSu_ID", $scope.NhanSu_ID);
                formData.append("CongtyID", $rootScope.congtyID);
                formData.append("baseUrl", baseUrl);
                formData.append("userName", $scope.RememPass.userName);
                formData.append("Email", $scope.RememPass.Email);
                return formData;
            }

        }).then(function (res) {
            if (res.data.error === 1) {
                hideloading();
                dialogs.error('Thông báo', res.data.ms, { windowClass: "apidialog", size: "sm" });
            }
            else {
                hideloading();
                $("#ModalRememberPass").modal("hide");
                showtoastr('Đã gửi Email thành công!');
            }
        });
    };
}]);
os.directive('onError', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.on('error', function () {
                element.attr('src', attr.onError);
                $(element).hide();
                $(element.next()).show();
            });
        }
    };
});
os.directive('fancybox', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            // find the inner elements and apply fancybox to all of them at once 
            var apply_fancybox_to = element.find('a.fbelements');
            $(apply_fancybox_to).fancybox({
                fitToView: true,
                autoSize: true,
            });
        }
    };
});
os.directive('jsSelect2', function ($timeout) {
    return {
        link: function (scope, element, attrs) {
            jQuery(element).select2(
            );
            scope.$watch(attrs.ngModel, function () {
                $timeout(function () {
                    element.trigger('change.select2');
                }, 100);
            });

        }
    };
});
os.filter('sumOfValue', function () {
    return function (data, key) {
        if (angular.isUndefined(data) || angular.isUndefined(key))
            return 0;
        var sum = 0;
        angular.forEach(data, function (value) {
            sum = sum + parseInt(value[key]);
        });
        return sum;
    };
});
os.directive('clockPicker', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.clockpicker();
        }
    };
});
os.directive('contenteditable', function () {
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, elm, attr, ngModel) {

            function updateViewValue() {
                ngModel.$setViewValue(this.innerHTML);
            }
            //Binding it to keyup, lly bind it to any other events of interest 
            //like change etc..
            elm.on('keyup', updateViewValue);

            scope.$on('$destroy', function () {
                elm.off('keyup', updateViewValue);
            });

            ngModel.$render = function () {
                elm.html(ngModel.$viewValue);
            };

        }
    };
});
os.directive('myEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === 13) {
                scope.$apply(function () {
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});
os.directive('backButton', function () {
    return {
        restrict: 'A',

        link: function (scope, element, attrs) {
            element.bind('click', goBack);

            function goBack() {
                history.back();
                scope.$apply();
            }
        }
    }
});
os.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                scope.$evalAsync(attr.onFinishRender);
            }
        }
    };
});
os.directive("digitalClock", function ($timeout, dateFilter) {
    return function (scope, element, attrs) {

        element.addClass('text-center clock');

        scope.updateClock = function () {
            $timeout(function () {
                element.text(dateFilter(new Date(), 'hh:mm:ss'));
                scope.updateClock();
            }, 1000);
        };

        scope.updateClock();

    };
});
os.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);
os.directive('ngFiles', ['$parse', function ($parse) {
    function fn_link(scope, element, attrs) {
        var onChange = $parse(attrs.ngFiles);
        element.on('change', function (event) {
            onChange(scope, { $files: event.target.files });
        });
    }
    return {
        link: fn_link
    };
}])
    .directive('modalMovable', ['$document',
        function ($document) {
            return {
                restrict: 'AC',
                link: function (scope, iElement, iAttrs) {
                    var startX = 0,
                        startY = 0,
                        x = 0,
                        y = 0;

                    var dialogWrapper = iElement.parent();

                    dialogWrapper.css({
                        position: 'relative'
                    });

                    iElement.on('mousedown', function (event) {
                        // Prevent default dragging of selected content
                        // event.preventDefault();
                        startX = event.pageX - x;
                        startY = event.pageY - y;
                        $document.on('mousemove', mousemove);
                        $document.on('mouseup', mouseup);
                    });

                    function mousemove(event) {
                        y = event.pageY - startY;
                        x = event.pageX - startX;
                        dialogWrapper.css({
                            top: y + 'px',
                            left: x + 'px'
                        });
                    }

                    function mouseup() {
                        $document.unbind('mousemove', mousemove);
                        $document.unbind('mouseup', mouseup);
                    }
                }
            };
        }
    ]);
os.directive('currencyInput', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, ctrl) {
            return ctrl.$parsers.push(function (inputValue) {
                var inputVal = element.val();
                //clearing left side zeros
                while (inputVal.charAt(0) == '0') {
                    inputVal = inputVal.substr(1);
                }
                inputVal = inputVal.replace(/[^\d.\',']/g, '');
                var point = inputVal.indexOf(".");
                if (point >= 0) {
                    inputVal = inputVal.slice(0, point + 3);
                }
                var decimalSplit = inputVal.split(".");
                var intPart = decimalSplit[0];
                var decPart = decimalSplit[1];
                intPart = intPart.replace(/[^\d]/g, '');
                if (intPart.length > 3) {
                    var intDiv = Math.floor(intPart.length / 3);
                    while (intDiv > 0) {
                        var lastComma = intPart.indexOf(",");
                        if (lastComma < 0) {
                            lastComma = intPart.length;
                        }

                        if (lastComma - 3 > 0) {
                            intPart = intPart.slice(0, lastComma - 3) + "," + intPart.slice(lastComma - 3);
                        }
                        intDiv--;
                    }
                }

                if (decPart === undefined) {
                    decPart = "";
                } else {
                    decPart = "." + decPart;
                }
                var res = intPart + decPart;

                if (res != inputValue) {
                    ctrl.$setViewValue(res);
                    ctrl.$render();
                }
            });

        }
    };
});
os.directive('integer', function () {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function (scope, elem, attr, ngModel) {
            if (!ngModel)
                return;

            function isValid(val) {
                if (val === "")
                    return true;

                var asInt = parseInt(val, 10);
                if (isNaN(asInt) || (asInt.toString() !== val && "0" + asInt.toString() !== val)) {
                    return false;
                }
                var min = parseInt(attr.min);
                if (isNaN(min) && asInt < min) {
                    return false;
                }

                var max = parseInt(attr.max);
                if (isNaN(max) && max < asInt) {
                    return false;
                }

                return true;
            }

            var prev = scope.$eval(attr.ngModel);
            ngModel.$parsers.push(function (val) {
                // short-circuit infinite loop
                if (val === prev)
                    return val;
                if (!isValid(val)) {
                    ngModel.$setViewValue(prev);
                    ngModel.$render();
                    return prev;
                }

                prev = val;
                return val;
            });
        }
    };
});

os.filter('customArray', function ($filter) {
    return function (list, arrayFilter, element) {
        if (arrayFilter) {
            return $filter("filter")(list, function (listItem) {
                return arrayFilter.indexOf(listItem[element]) != -1;
            });
        }
    };
});
os.filter('bytes', function () {
    return function (bytes, precision) {
        if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
        if (typeof precision === 'undefined') precision = 1;
        var units = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB'],
            number = Math.floor(Math.log(bytes) / Math.log(1024));
        return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) + ' ' + units[number];
    };
});
os.directive('validNumber', function () {
    return {
        require: '?ngModel',
        link: function (scope, element, attrs, ngModelCtrl) {
            if (!ngModelCtrl) {
                return;
            }

            ngModelCtrl.$parsers.push(function (val) {
                if (angular.isUndefined(val)) {
                    val = '';
                }

                var clean = val.replace(/[^-0-9\.]/g, '');
                var negativeCheck = clean.split('-');
                var decimalCheck = clean.split('.');
                if (!angular.isUndefined(negativeCheck[1])) {
                    negativeCheck[1] = negativeCheck[1].slice(0, negativeCheck[1].length);
                    clean = negativeCheck[0] + '-' + negativeCheck[1];
                    if (negativeCheck[0].length > 0) {
                        clean = negativeCheck[0];
                    }

                }

                if (!angular.isUndefined(decimalCheck[1])) {
                    decimalCheck[1] = decimalCheck[1].slice(0, 2);
                    clean = decimalCheck[0] + '.' + decimalCheck[1];
                }

                if (val !== clean) {
                    ngModelCtrl.$setViewValue(clean);
                    ngModelCtrl.$render();
                }
                return clean;
            });

            element.bind('keypress', function (event) {
                if (event.keyCode === 32) {
                    event.preventDefault();
                }
            });
        }
    };
});
os.filter('filterMultiple', ['$filter', function ($filter) {
    return function (items, keyObj) {
        var filterObj = {
            data: items,
            filteredData: [],
            applyFilter: function (obj, key) {
                var fData = [];
                if (this.filteredData.length == 0)
                    this.filteredData = this.data;
                if (obj) {
                    var fObj = {};
                    if (!angular.isArray(obj)) {
                        fObj[key] = obj;
                        fData = fData.concat($filter('filter')(this.filteredData, fObj));
                    } else if (angular.isArray(obj)) {
                        if (obj.length > 0) {
                            for (var i = 0; i < obj.length; i++) {
                                if (angular.isDefined(obj[i])) {
                                    fObj[key] = obj[i];
                                    fData = fData.concat($filter('filter')(this.filteredData, fObj));
                                }
                            }

                        }
                    }
                    if (fData.length > 0) {
                        this.filteredData = fData;
                    }
                }
            }
        };
        if (keyObj) {
            angular.forEach(keyObj, function (obj, key) {
                filterObj.applyFilter(obj, key);
            });
        }
        return filterObj.filteredData;
    };
}]);
os.filter('highlight', function ($sce) {
    return function (text, phrase) {
        if (phrase) text = text.replace(new RegExp('(' + phrase + ')', 'gi'),
            '<span class="highlighted">$1</span>');

        return $sce.trustAsHtml(text);
    };
});
os.filter('crop', function () {
    return function (input, limit, respectWordBoundaries, suffix) {
        if (input === null || input === undefined || limit === null || limit === undefined || limit === '') {
            return input;
        }
        if (angular.isUndefined(respectWordBoundaries)) {
            respectWordBoundaries = true;
        }
        if (angular.isUndefined(suffix)) {
            suffix = '...';
        }

        if (input.length <= limit) {
            return input;
        }

        limit = limit - suffix.length;

        var trimmedString = input.substr(0, limit);
        if (respectWordBoundaries) {
            return trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(" "))) + suffix;
        }
        return trimmedString + suffix;
    }
});
os.filter('cut', function () {
    return function (value, wordwise, max, tail) {
        if (!value) return '';

        max = parseInt(max, 10);
        if (!max) return value;
        if (value.length <= max) return value;

        value = value.substr(0, max);
        if (wordwise) {
            var lastspace = value.lastIndexOf(' ');
            if (lastspace !== -1) {
                //Also remove . and , so its gives a cleaner result.
                if (value.charAt(lastspace - 1) === '.' || value.charAt(lastspace - 1) === ',') {
                    lastspace = lastspace - 1;
                }
                value = value.substr(0, lastspace);
            }
        }

        return value + (tail || ' …');
    };
});
// Filter convert url using view file pdf
os.filter('trustAsResourceUrl',
    [
        '$sce', function ($sce) {
            return function (val) {
                return $sce.trustAsResourceUrl(val);
            };
        }
    ]);
//component
//os.component('user', {
//    bindings: {
//        vu: '<',
//        vp: '<',
//        all: '<',
//        ct: '<',//công ty
//        ctch: '<',//công ty cha
//        ctc: '<',//công ty con
//        hTitle: '@',
//        choiceUser: '&',
//        one: '<',
//        objfilter: '<'
//    },
//    templateUrl: baseUrl + '/App/Temp/User.html?v=' + vhtmlcache,
//    controller: function ($rootScope) {
//        debugger;
//        var $ctr = this;
//        this.$onChanges = function (a) {
//            $ctr.uAll = false;
//            $ctr.Search = null;
//            this.reset();
//        };
//        this.uAll = false;
//        this.goFilter = function (m, f) {
//            $ctr.uAll = false;
//            var us = this.vu;
//            if (this.Search) {
//                var s = this.Search.toLowerCase();
//                var sk = change_alias(this.Search.toLowerCase());
//                us = us.filter(u =>
//                    u.fullName.toLowerCase().indexOf(s) !== -1
//                    || (u.enFullName + "").toLowerCase().indexOf(s) !== -1
//                    || u.fullName.toLowerCase().indexOf(sk) !== -1
//                    || (u.enFullName + "").toLowerCase().indexOf(sk) !== -1
//                );
//            }
//            var rs = this.roles.filter(r => r.CongTy_ID === $rootScope.congty.Congty_ID && r.isCheck);
//            if (rs.length > 0) {
//                us = us.filter(u => rs.find(r => u.roles !== null && u.roles.indexOf(r.Role_ID) > -1));
//            }
//            if (m && !f) {
//                if (m.isCheck === true) {
//                    m.isCheck = false;
//                } else {
//                    this.phongbans.filter(p => p.isCheck).forEach(function (r) {
//                        r.isCheck = false;
//                    });
//                    m.isCheck = true;
//                }
//            } else if (m && f) {
//                this.phongbans.filter(p => p.isClick).forEach(function (r) {
//                    r.isClick = false;
//                });
//                m.users.forEach(function (u) {
//                    u.isCheck = m.isCheck;
//                });
//            }
//            var ps = this.phongbans.filter(p => p.isCheck);
//            if (ps.length > 0) {
//                this.phongbansUS = ps;
//            } else {
//                this.phongbansUS = this.phongbans;
//            }
//            this.phongbansUS.forEach(function (p) {
//                p.users = us.filter(u => u.phongbans !== null && u.phongbans.indexOf(p.Phongban_ID) !== -1);
//            });
//        };
        //this.SelectModel = function (m) {
        //    $ctr.uAll = false;
        //    var us = this.vu;
        //    if (this.Search) {
        //        var s = this.Search.toLowerCase();
        //        var sk = change_alias(this.Search.toLowerCase());
        //        us = us.filter(u =>
        //            u.fullName.toLowerCase().indexOf(s) !== -1
        //            || (u.enFullName + "").toLowerCase().indexOf(s) !== -1
        //            || u.fullName.toLowerCase().indexOf(sk) !== -1
        //            || (u.enFullName + "").toLowerCase().indexOf(sk) !== -1
        //        );
        //    }
        //    if (m.isClick === true) {
        //        m.isClick = false;
        //    } else {
        //        this.phongbans.filter(p => p.isClick).forEach(function (r) {
        //            r.isClick = false;
        //        });
        //        m.isClick = true;
        //    }
        //    var rs = this.roles.filter(r => r.CongTy_ID === $rootScope.congty.Congty_ID && r.isCheck);
        //    if (rs.length > 0) {
        //        us = us.filter(u => rs.find(r => u.roles !== null && u.roles.indexOf(r.Role_ID) > -1));
        //    }
        //    var ps = this.phongbans.filter(p => p.isClick);
        //    if (ps.length > 0) {
        //        this.phongbansUS = ps;
        //    } else {
        //        this.phongbansUS = this.phongbans;
        //    }
        //    this.phongbansUS.forEach(function (p) {
        //        p.users = us.filter(u => u.phongbans !== null && u.phongbans.indexOf(p.Phongban_ID) !== -1 && u.congtyID == m.Congty_ID);
        //    });
        //};
//        this.goCtyFilter = function (c) {
//            if (!this.phongbans) return;
//            $ctr.uAll = false;
//            this.phongbans.forEach(function (r) {
//                r.isCheck = false;
//            });
//            this.childcongtys.forEach(function (r) {
//                r.isCheck = false;
//            });
//            $ctr.congty.isCheck = false;
//            c.isCheck = true;
//            var us = this.vu;
//            if (this.Search) {
//                var s = this.Search.toLowerCase();
//                var sk = change_alias(this.Search.toLowerCase());
//                us = us.filter(u =>
//                    u.fullName.toLowerCase().indexOf(s) !== -1
//                    || (u.enFullName + "").toLowerCase().indexOf(s) !== -1
//                    || u.fullName.toLowerCase().indexOf(sk) !== -1
//                    || (u.enFullName + "").toLowerCase().indexOf(sk) !== -1
//                );
//            }
//            var rs = this.roles.filter(r => r.CongTy_ID === $rootScope.congty.Congty_ID && r.isCheck);
//            if (rs.length > 0) {
//                us = us.filter(u => rs.find(r => u.roles !== null && u.roles.indexOf(r.Role_ID) > -1));
//            }
//            this.phongbansUS = this.phongbans.filter(p => p.Congty_ID === c.Congty_ID);
//            this.phongbansUS.forEach(function (p) {
//                p.users = us.filter(u => u.phongbans !== null && u.phongbans.indexOf(p.Phongban_ID) !== -1);
//            });
//        };
//        this.refershModel = function () {
//            $ctr.uAll = false;
//            var pbs = this.vp;
//            var us = this.vu;
//            this.Search = null;
//            if (pbs) {
//                pbs.forEach(function (p) {
//                    p.users = us.filter(u => u.phongbans !== null && u.phongbans.indexOf(p.Phongban_ID) !== -1);
//                });
//                this.phongbans = pbs;
//                this.phongbansUS = pbs;
//            }
//            this.roles.forEach(function (r) {
//                r.isCheck = false;
//            });
//        };
//        this.checkUFilter = function (item) {
//            return item.isCheck;
//        };
//        this.toogleModel = function (m) {
//            $ctr.uAll = false;
//            if (m.close !== true) {
//                m.close = true;
//            } else {
//                m.close = false;
//            }
//        };
//        this.checkAllUST = function () {
//            var check = this.uAll;
//            this.phongbansUS.forEach(function (p) {
//                p.users.forEach(function (u) {
//                    u.isCheck = check;
//                });
//            });
//        };
//        this.ChoiceUser = function () {
//            var us = this.vu.filter(u => u.isCheck);
//            this.reset();
//            this.choiceUser({ us: us });
//        };
//        this.reset = function () {
//            $ctr.uAll = false;
//            $ctr.Search = null;
//            var pbs = $ctr.vp;
//            if (pbs && $ctr.vu) {
//                $ctr.vu.forEach(function (u) {
//                    u.isCheck = false;
//                });
//                if ($rootScope.ctyroles) {
//                    $rootScope.ctyroles.forEach(function (u) {
//                        u.isCheck = false;
//                    });
//                }
//                if ($ctr.vu) {
//                    pbs.forEach(function (p) {
//                        p.isCheck = false;
//                        if ($ctr.vu) {
//                            p.users = $ctr.vu.filter(u => u.phongbans && u.phongbans.indexOf(p.Phongban_ID) !== -1);
//                        }
//                    });
//                    //pbs = pbs.filter(p=>p.users.length>0);
//                } else {
//                    pbs.forEach(function (p) {
//                        p.isCheck = false;
//                    });
//                }
//                $ctr.congty = $rootScope.congty;
//                $ctr.childcongtys = $rootScope.childcongtys;
//                if (this.ct) {
//                    if (!this.ctch) {
//                        if ($rootScope.me.congtyID !== $rootScope.congty.Congty_ID) {
//                            $ctr.congty = $rootScope.childcongtys.find(x => x.Congty_ID === $rootScope.me.congtyID);
//                        }
//                    }
//                    if (!this.ctc) {
//                        $ctr.childcongtys = [];
//                    }
//                }
//                if (!pbs || pbs.length == 0) {
//                    var pbss = $rootScope.phongbans.find(x => x.Phongban_ID === $rootScope.login.u.Phongban_ID);
//                    if (pbss) {
//                        var cty = $rootScope.phongbans.find(x => x.Phongban_ID === pbss.IDCha.split(",")[0]);
//                        pbs = $rootScope.phongbans.filter(x => cty.IDCon.indexOf(x.Phongban_ID) > -1);
//                        pbs.push(cty);
//                    }
//                    $ctr.childcongtys = [];
//                }
//                if (this.all) {
//                    $ctr.congty = $rootScope.childcongtys.find(x => x.Congty_ID === $rootScope.congty.Parent_ID);
//                }
//                if ($ctr.objfilter !== null && $ctr.objfilter !== undefined) {
//                    if ($ctr.objfilter.donvi === true) {//Gửi nội bộ
//                        var cty = $rootScope.childcongtys.find(x => x.Congty_ID === $rootScope.me.congtyID);
//                        if (cty) {
//                            $ctr.congty = cty;
//                        }
//                        $ctr.childcongtys = [];
//                        pbs = pbs.filter(x => x.Congty_ID === $rootScope.me.congtyID);
//                    } else if ($ctr.objfilter.donvi === false) {//Gửi ra ngoài
//                        $ctr.childcongtys = $rootScope.childcongtys.filter(x => x.Congty_ID !== $rootScope.me.congtyID);
//                        pbs = pbs.filter(x => $ctr.childcongtys.filter(a => a.Congty_ID === x.Congty_ID).length > 0);
//                    }
//                }
//                $ctr.phongbans = pbs;
//                $ctr.phongbansUS = pbs;
//                $ctr.roles = $rootScope.ctyroles;
//            }
//        };
//        this.checkU = function (u) {
//            var us = this.vu.filter(m => m.NhanSu_ID !== u.NhanSu_ID);
//            us.forEach(function (n) {
//                n.isCheck = false;
//            });
//        };
//    }
//});
os.component('phong', {
    bindings: {
        vp: '<',
        hTitle: '@',
        choiceUser: '&'
    },
    templateUrl: baseUrl + '/App/Temp/Phong.html?v=' + vhtmlcache,
    controller: function ($scope, $rootScope, dialogs, $http, $stateParams, $state, $filter, Upload) {
        var $ctr = this;
        this.$onInit = function () {
            var pbs = $ctr.vp;
            if (pbs) {
                $ctr.phongbans = pbs;
                $ctr.phongbansUS = pbs;
            }
        };
        var Temp = [];
        function addToArray(array, id, lv) {
            var filter = $filter('filter')(array, { Parent_ID: id }, true);
            filter = $filter('orderBy')(filter, 'thutu');
            if (filter.length > 0) {
                var sp = "";
                for (var i = 0; i < lv; i++) {
                    sp += "----";
                }

                lv++;
                angular.forEach(filter, function (item) {
                    item.lv = lv;
                    item.close = true;
                    item.ids += "," + item.Congty_ID;
                    item.tenmoi = sp + item.tenCongty;
                    Temp.push(item);
                    addToArray(array, item.Congty_ID, lv);
                });
            }
        }
        this.uAll = false;
        this.checkUFilter = function (item) {
            return item.isCheck;
        };
        this.toogleModel = function (m) {
            if (m.close !== true) {
                m.close = true;
            } else {
                m.close = false;
            }
        };
        this.checkAllUST = function () {
            var check = this.uAll;
            this.phongbansUS.forEach(function (p) {
                p.isCheck = check;
            });
        };
        this.ChoiceUser = function () {
            var us = this.vp.filter(u => u.isCheck);
            this.choiceUser({ us: us });
        };
        this.checkU = function (u) {
            var us = this.vp.filter(m => m.Phongban_ID !== u.Phongban_ID);
            us.forEach(function (n) {
                n.isCheck = false;
            });
        };
        this.getCheckPbChild = function (m) {
            var getListPbChild = $ctr.phongbans.filter(x => x.IDCha != null && x.IDCha.indexOf(m.Phongban_ID) > -1);
            if (m.isCheck == true)
                getListPbChild.forEach(function (t) {
                    t.isCheck = true;
                });
            else
                getListPbChild.forEach(function (t) {
                    t.isCheck = false;
                });
        }
        //

        this.changeCheckCty = function (idxID, check) {
            var checkIsCongtyTong = $rootScope.childcongtys.find(m => m.Congty_ID == idxID);
            if (checkIsCongtyTong.Parent_ID == null) {
                var getListPbCtyCha = $ctr.phongbans.filter(n => n.Congty_ID == idxID);
                //
                if (check == true) {
                    getListPbCtyCha.forEach(function (t) {
                        t.isCheck = true;
                    });
                }
                else {
                    getListPbCtyCha.forEach(function (t) {
                        t.isCheck = false;
                    });
                }
            }
            addToArray($rootScope.childcongtys, idxID, 0);
            var listAllCongtyByIDCha = Temp;
            Temp = [];
            if (check == true) {
                var objCongtyHienTai = $rootScope.childcongtys.find(n => n.Congty_ID == idxID);
                angular.forEach(objCongtyHienTai.phongbans, function (u) {
                    u.isCheck = true;
                });
                //
                listAllCongtyByIDCha.forEach(function (t) {
                    t.isCheck = true;
                    angular.forEach(t.phongbans, function (u) {
                        u.isCheck = true;
                    });
                });
            }
            else {
                var objCongtyHienTai = $rootScope.childcongtys.find(n => n.Congty_ID == idxID);
                angular.forEach(objCongtyHienTai.phongbans, function (u) {
                    u.isCheck = false;
                });
                //
                listAllCongtyByIDCha.forEach(function (t) {
                    t.isCheck = false;
                    angular.forEach(t.phongbans, function (u) {
                        u.isCheck = false;
                    });
                });
            }
        }
        //
        //
    }
});
os.component('treephong', {
    bindings: {
        cb: '=',
        vp: '<',
        hTitle: '@',
        choiceUser: '&'
    },
    templateUrl: baseUrl + '/App/Temp/TreePhong.html?v=' + vhtmlcache,
    controller: function ($rootScope) {
        var $ctr = this;
        this.$onInit = function () {
            var pbs = $ctr.vp;
            if (pbs) {
                $ctr.phongbans = pbs;
                $ctr.phongbansUS = pbs;
            }
        };
        this.uAll = false;
        this.checkUFilter = function (item) {
            return item.isCheck;
        };
        this.clickModel = function (m, f) {
            if (!f) {
                this.phongbansUS.filter(p => p.isCheck).forEach(function (p) {
                    p.isCheck = false;
                });
                m.isCheck = !m.isCheck;
            }
            $ctr.ChoiceUser();
        };
        this.toogleModel = function (m, f) {
            if (m.close !== true) {
                m.close = true;
            } else {
                m.close = false;
            }
        };
        this.checkAllUST = function () {
            var check = this.uAll;
            this.phongbansUS.forEach(function (p) {
                p.isCheck = check;
            });
        };
        this.ChoiceUser = function () {
            var us = this.vp.filter(u => u.isCheck);
            this.choiceUser({ us: us });
        };
        this.checkU = function (u) {
            var us = this.vp.filter(m => m.Phongban_ID !== u.Phongban_ID);
            us.forEach(function (n) {
                n.isCheck = false;
            });
        };

    }
});
os.component('treephongstore', {
    bindings: {
        cb: '=',
        vp: '<',
        ct: '<',
        cts: '<',
        hTitle: '@',
        choiceUser: '&'
    },
    templateUrl: baseUrl + '/App/Temp/TreePhongStore.html?v=' + vhtmlcache,
    controller: function ($rootScope) {
        var $ctr = this;
        this.$onInit = function () {
            var pbs = $ctr.vp;
            $ctr.congty = $ctr.ct;
            $ctr.childcongty = $ctr.cts;
            if (pbs) {
                $ctr.phongbans = pbs;
                $ctr.phongbansUS = pbs;
            }
        };
        this.uAll = false;
        this.checkUFilter = function (item) {
            return item.isCheck;
        };
        this.clickModel = function (m, f) {
            if (!f) {
                this.phongbansUS.filter(p => p.isCheck).forEach(function (p) {
                    p.isCheck = false;
                });
                m.isCheck = !m.isCheck;
            }
            $ctr.ChoiceUser();
        };
        this.toogleModel = function (m, f) {
            if (m.close !== true) {
                m.close = true;
            } else {
                m.close = false;
            }
        };
        this.checkAllUST = function () {
            var check = this.uAll;
            this.phongbansUS.forEach(function (p) {
                p.isCheck = check;
            });
        };
        this.ChoiceUser = function () {
            var us = this.vp.filter(u => u.isCheck);
            this.choiceUser({ us: us });
        };
        this.checkU = function (u) {
            var us = this.vp.filter(m => m.Phongban_ID !== u.Phongban_ID);
            us.forEach(function (n) {
                n.isCheck = false;
            });
        };

    }
});
os.component('treephongnocheck', {
    bindings: {
        vp: '<',
        hTitle: '@',
        choiceCongty: '&',
        choicePhong: '&'
    },
    templateUrl: baseUrl + '/App/Temp/TreePhongNoCheck.html?v=' + vhtmlcache,
    controller: function ($filter) {
        var $ctr = this;
        this.$onInit = function () {
            var pbs = $ctr.vp;
            if (pbs) {
                $ctr.phongbans = pbs;
                $ctr.phongbansUS = pbs;
            }
            pbs = this.vp;
        };
        this.uAll = false;
        this.checkUFilter = function (item) {
            return item.isCheck;
        };
        this.so = "thutucha";
        var Temp2 = [];
        this.SortPB = function (f) {
            Temp2 = [];
            if (f) {
                this.so = "thutucha";
            } else {
                this.so = "tenmoi";
            }
            addToArray2(this.phongbans, null, 0, this.so);
            this.phongbans = Temp2;
        };
        function addToArray2(array, id, lv, so) {
            var filter = $filter('filter')(array, { Parent_ID: id }, true);
            filter = $filter('orderBy')(filter, so);
            if (filter.length > 0) {
                var sp = "";
                for (var i = 0; i < lv; i++) {
                    sp += "";
                }
                lv++;
                angular.forEach(filter, function (item) {
                    item.lv = lv;
                    if (!item.ids) {
                        item.ids += "," + item.Phongban_ID;
                        item.tenmoi = sp + item.tenPhongban;
                    }
                    Temp2.push(item);
                    addToArray2(array, item.Phongban_ID, lv, so);
                });
            }
        }
        this.toogleModel = function (m, f) {
            if (f) {
                if (m.close !== true) {
                    m.close = true;
                } else {
                    m.close = false;
                }
            } else {
                this.phongbansUS.filter(p => p.isCheck).forEach(function (p) {
                    p.isCheck = false;
                });
                if (m.close !== true) {
                    m.close = true;
                } else {
                    m.close = false;
                }
                m.isCheck = !m.isCheck;
            }
            $ctr.ChoicePhong();
        };
        this.checkAllUST = function () {
            var check = this.uAll;
            this.phongbansUS.forEach(function (p) {
                p.isCheck = check;
            });
        };
        this.ChoicePhong = function () {
            var us = this.vp.filter(u => u.isCheck);
            this.choicePhong({ us: us });
        };
        this.ChoiceCongty = function (p) {
            if (p.close !== true) {
                p.close = true;
            } else {
                p.close = false;
            }
            this.choiceCongty({ us: p });
        };
        this.checkU = function (u) {
            var us = this.vp.filter(m => m.Phongban_ID !== u.Phongban_ID);
            us.forEach(function (n) {
                n.isCheck = false;
            });
        };
        //var Temp2 = [];
        //$ctr.so = "thutu";
        //$ctr.SortPB = function (f) {
        //    debugger;
        //    if (f) {
        //        $scope.so = "thutu";
        //    } else {
        //        $scope.so = "tenmoi";
        //    }
        //    addToArray2($ctr.phongbans, null, 0);
        //    $ctr.phongbans = Temp2;
        //    Temp2 = [];
        //    var vbs = $rootScope.phongbans;
        //    if (vbs) {
        //        vbs.forEach(function (r) {
        //            r.Count = $scope.phongbans.find(x => x.Phongban_ID === r.Phongban_ID).countNsPb;
        //        });
        //        $scope.vbphongbans = vbs;
        //    }
        //};
        //function addToArray2(array, id, lv) {
        //    debugger;
        //    var filter = $filter('filter')(array, { Parent_ID: id }, true);
        //    filter = $filter('orderBy')(filter, $ctr.so);
        //    if (filter.length > 0) {
        //        var sp = "";
        //        for (var i = 0; i < lv; i++) {
        //            sp += "";
        //        }
        //        lv++;
        //        angular.forEach(filter, function (item) {
        //            item.lv = lv;
        //            item.close = true;
        //            if (!item.ids) {
        //                item.ids += "," + item.Phongban_ID;
        //                item.tenmoi = sp + item.tenPhongban;
        //            }
        //            Temp2.push(item);
        //            addToArray2(array, item.Phongban_ID, lv);
        //        });
        //    }
        //}
    }
});
os.component('treephongusers', {
    bindings: {
        vp: '<',
        hTitle: '@',
        choiceCongty: '&',
        choicePhong: '&'
    },
    templateUrl: baseUrl + '/App/Temp/TreePhongUser.html?v=' + vhtmlcache,
    controller: function ($scope, $rootScope, dialogs, $http, $stateParams, $state, $filter, Upload) {
        var $ctr = this;
        this.$onInit = function () {
            var pbs = $ctr.vp;
            $rootScope.ListDataPbUser = pbs;
            if (pbs) {
                pbs.forEach(function (p) {
                    p.users = $rootScope.users.filter(u => u.phongbans !== null && u.phongbans.indexOf(p.Phongban_ID) !== -1);
                });
                this.phongbans = pbs;
                this.phongbansUS = pbs;
            }
        };
        this.uAll = false;
        this.checkUFilter = function (item) {
            return item.isCheck;
        };
        var Temp = [];
        function addToArray(array, id, lv) {
            var filter = $filter('filter')(array, { Parent_ID: id }, true);
            filter = $filter('orderBy')(filter, 'thutu');
            if (filter.length > 0) {
                var sp = "";
                for (var i = 0; i < lv; i++) {
                    sp += "----";
                }
                lv++;
                angular.forEach(filter, function (item) {
                    item.lv = lv;
                    item.close = true;
                    item.ids += "," + item.Phongban_ID;
                    item.tenmoi = sp + item.tenPhongban;
                    Temp.push(item);
                    addToArray(array, item.Phongban_ID, lv);
                });
            }
        }
        this.toogleModel = function (m, f) {
            if (f) {
                if (m.close !== true) {
                    m.close = true;
                } else {
                    m.close = false;
                }
            } else {
                this.phongbansUS.filter(p => p.isCheck).forEach(function (p) {
                    p.isCheck = false;
                });
                if (m.close !== true) {
                    m.close = true;
                } else {
                    m.close = false;
                }
                m.isCheck = !m.isCheck;
            }
            $ctr.ChoicePhong();
        };
        this.checkAllUST = function () {
            var check = this.uAll;
            this.phongbansUS.forEach(function (p) {
                p.isCheck = check;
            });
        };
        this.ChoicePhong = function () {
            var us = this.vp.filter(u => u.isCheck);
            this.choicePhong({ us: us });
        };
        this.ChoiceCongty = function (p) {
            if (p.close !== true) {
                p.close = true;
            } else {
                p.close = false;
            }
            this.choiceCongty({ us: p });
        };
        this.checkU = function (u) {
            var us = this.vp.filter(m => m.Phongban_ID !== u.Phongban_ID);
            us.forEach(function (n) {
                n.isCheck = false;
            });
        };
        //getListCheck phanquyen
        $scope.arrListUserChoice = [];
        this.getcheckRolePb = function (m) {
            var ListPbUser = $ctr.vp;
            addToArray($ctr.vp, m.Phongban_ID, 0);
            $scope.ListPhongBanChild = Temp;
            Temp = [];
            if (m.isCheckItem == true) {
                //l?y ra list user theo pbID
                $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == m.Phongban_ID);
                $scope.getListUser.users.forEach(function (t) {
                    t.isCheckItem = true;
                    $scope.arrListUserChoice.push(t);
                });
                //get list phongbanCon
                if ($scope.ListPhongBanChild.length > 0) {
                    $scope.ListPhongBanChild.forEach(function (t) {
                        t.isCheckItem = true;
                        //l?y ra list user theo pbID con
                        $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == t.Phongban_ID);
                        $scope.getListUser.users.forEach(function (u) {
                            u.isCheckItem = true;
                            $scope.arrListUserChoice.push(u);
                        });
                    });
                }
            }
            else {
                //l?y ra list user theo pbID
                $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == m.Phongban_ID);
                $scope.getListUser.users.forEach(function (t) {
                    t.isCheckItem = false;
                    $scope.arrListUserChoice.pop(t);
                });
                //get list phongbanCon
                if ($scope.ListPhongBanChild.length > 0) {
                    $scope.ListPhongBanChild.forEach(function (t) {
                        t.isCheckItem = false;
                        //l?y ra list user theo pbID con
                        $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == t.Phongban_ID);
                        $scope.getListUser.users.forEach(function (u) {
                            u.isCheckItem = false;
                            $scope.arrListUserChoice.pop(u);
                        });
                    });
                }
                //
            }
        }
        //check user
        this.getcheckRoleUs = function (m, index) {
            if (m.isCheckItem == true) {
                //chek trùng
                checkTrung = $scope.arrListUserChoice.filter(n => n.NhanSu_ID == m.NhanSu_ID);
                if (checkTrung.length == 0) {
                    //checkTrung.isCheckItem = true;
                    $scope.arrListUserChoice.push(m);
                }
            }
            else {
                checkTrung = $scope.arrListUserChoice.filter(n => n.NhanSu_ID == m.NhanSu_ID);
                if (checkTrung.length > 0) {
                    //$scope.arrListUserChoice.pop(m);
                    $scope.arrListUserChoice.splice(index, 1);
                }
            }
        }

        this.checkChucNangRole = function (m, check) {
            var ListPbUser = $ctr.vp;
            addToArray($ctr.vp, m.Phongban_ID, 0);
            $scope.ListPhongBanChild = Temp;
            Temp = [];

            //check
            if (check == 'IsRead') {
                if (m.IsRead == true) {
                    //l?y ra list user theo pbID
                    $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == m.Phongban_ID);
                    $scope.getListUser.users.forEach(function (t) {
                        t.IsRead = true;
                    });
                    //get list phongbanCon
                    if ($scope.ListPhongBanChild.length > 0) {
                        $scope.ListPhongBanChild.forEach(function (t) {
                            t.IsRead = true;
                            //l?y ra list user theo pbID con
                            $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == t.Phongban_ID);
                            $scope.getListUser.users.forEach(function (u) {
                                u.IsRead = true;
                            });
                        });
                    }
                }
                //
                else {
                    //l?y ra list user theo pbID
                    $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == m.Phongban_ID);
                    $scope.getListUser.users.forEach(function (t) {
                        t.IsRead = false;
                    });
                    //get list phongbanCon
                    if ($scope.ListPhongBanChild.length > 0) {
                        $scope.ListPhongBanChild.forEach(function (t) {
                            t.IsRead = false;
                            //l?y ra list user theo pbID con
                            $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == t.Phongban_ID);
                            $scope.getListUser.users.forEach(function (u) {
                                u.IsRead = false;
                            });
                        });
                    }
                }
            }
            else if (check == 'IsWrite') {
                if (m.IsWrite == true) {
                    //l?y ra list user theo pbID
                    $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == m.Phongban_ID);
                    $scope.getListUser.users.forEach(function (t) {
                        t.IsWrite = true;
                    });
                    //get list phongbanCon
                    if ($scope.ListPhongBanChild.length > 0) {
                        $scope.ListPhongBanChild.forEach(function (t) {
                            t.IsWrite = true;
                            //l?y ra list user theo pbID con
                            $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == t.Phongban_ID);
                            $scope.getListUser.users.forEach(function (u) {
                                u.IsWrite = true;
                            });
                        });
                    }
                }
                //
                else {
                    //l?y ra list user theo pbID
                    $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == m.Phongban_ID);
                    $scope.getListUser.users.forEach(function (t) {
                        t.IsWrite = false;
                    });
                    //get list phongbanCon
                    if ($scope.ListPhongBanChild.length > 0) {
                        $scope.ListPhongBanChild.forEach(function (t) {
                            t.IsWrite = false;
                            //l?y ra list user theo pbID con
                            $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == t.Phongban_ID);
                            $scope.getListUser.users.forEach(function (u) {
                                u.IsWrite = false;
                            });
                        });
                    }
                }
            }
            else {
                if (m.IsFull == true) {
                    //l?y ra list user theo pbID
                    $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == m.Phongban_ID);
                    $scope.getListUser.users.forEach(function (t) {
                        t.IsFull = true;
                    });
                    //get list phongbanCon
                    if ($scope.ListPhongBanChild.length > 0) {
                        $scope.ListPhongBanChild.forEach(function (t) {
                            t.IsFull = true;
                            //l?y ra list user theo pbID con
                            $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == t.Phongban_ID);
                            $scope.getListUser.users.forEach(function (u) {
                                u.IsFull = true;
                            });
                        });
                    }
                }
                //
                else {
                    //l?y ra list user theo pbID
                    $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == m.Phongban_ID);
                    $scope.getListUser.users.forEach(function (t) {
                        t.IsFull = false;
                    });
                    //get list phongbanCon
                    if ($scope.ListPhongBanChild.length > 0) {
                        $scope.ListPhongBanChild.forEach(function (t) {
                            t.IsFull = false;
                            //l?y ra list user theo pbID con
                            $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == t.Phongban_ID);
                            $scope.getListUser.users.forEach(function (u) {
                                u.IsFull = false;
                            });
                        });
                    }
                }
            }
        }

        this.ConfigRoleUser = function (frm) {
            addToArray($rootScope.phongbans, null, 0);
            var listPbcheck = Temp;
            Temp = [];
            $scope.arr = [];
            listPbcheck.forEach(function (t) {
                angular.forEach(t.users, function (value, key) {
                    var check = null;
                    if ($scope.arr.length > 0) {
                        check = $scope.arr.find(n => n.NhanSu_ID == t.users[key].NhanSu_ID);
                    }
                    if ((t.users[key].IsRead || t.users[key].IsWrite || t.users[key].IsFull) && check == null) {
                        $scope.arr.push(t.users[key]);
                    }
                });
            })
            var Urlaction;
            Urlaction = "/KTL_MyFolder/ConfigRoleUser";

            var formData = new FormData();
            formData.append("t", $rootScope.login.tk);
            formData.append("CongtyID", $rootScope.congtyID);
            formData.append("NhanSu_ID", $rootScope.NhanSu_ID);
            formData.append("FolderID", $rootScope.getparentIDFolder);
            //   formData.append("arrListUserChoice", JSON.stringify($scope.arrListUserChoice));
            formData.append("arrListUserChoice", JSON.stringify($scope.arr));

            $http.post(baseUrl + Urlaction, formData, {
                withCredentials: false,
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: angular.identity
            }).then(function (res) {
                if (res.data.error == 1) {
                    hideloading();
                    dialogs.error('Thông báo', res.data.ms, { windowClass: "apidialog", size: "sm" });
                    return false;
                }
                else {
                    $scope.checkLen = 0;
                    $("#ModalPhanQuyen").modal("hide");
                    showtoastr('Đã cập nhật dữ liệu thành công!.');
                }
            });
        };
    }
});
os.directive('pagination', function () {
    return {
        restrict: 'E',
        scope: {
            numPages: '=',
            currentPage: '=',
            onSelectPage: '&'
        },
        templateUrl: 'App/directive/pagination.html',
        replace: true,
        link: function (scope) {
            scope.$watch('numPages', function (value) {
                scope.pages = [];
                for (var i = 1; i <= value; i++) {
                    scope.pages.push(i);
                }
                if (scope.currentPage > value) {
                    scope.selectPage(value);
                }
            });
            scope.noPrevious = function () {
                return scope.currentPage === 1;
            };
            scope.noNext = function () {
                return scope.currentPage === scope.numPages;
            };
            scope.isActive = function (page) {
                return scope.currentPage === page;
            };

            scope.selectPage = function (page) {
                if (!scope.isActive(page)) {
                    scope.currentPage = page;
                    scope.onSelectPage({ page: page });
                }
            };

            scope.selectPrevious = function () {
                if (!scope.noPrevious()) {
                    scope.selectPage(scope.currentPage - 1);
                }
            };
            scope.selectNext = function () {
                if (!scope.noNext()) {
                    scope.selectPage(scope.currentPage + 1);
                }
            };
        }
    };
});
os.component('treephongusersrolemodule', {
    bindings: {
        vp: '<',
        hTitle: '@',
        choiceCongty: '&',
        choicePhong: '&'
    },
    templateUrl: baseUrl + '/App/SetRoleModule/treephongusersRoleModule.html?v=' + vhtmlcache,
    controller: function ($scope, $rootScope, dialogs, $http, $stateParams, $state, $filter, Upload) {
        var $ctr = this;
        this.$onInit = function () {
            // $scope.LoadListTich();
            var pbs = $ctr.vp;
            $rootScope.ListDataPbUser = pbs;
            if (pbs) {
                pbs.forEach(function (p) {
                    p.users = $rootScope.users.filter(u => u.phongbans !== null && u.phongbans.indexOf(p.Phongban_ID) !== -1);
                });
                this.phongbans = pbs;
                this.phongbansUS = pbs;
            }
        };
        this.uAll = false;
        this.checkUFilter = function (item) {
            return item.isCheck;
        };
        var Temp = [];
        function addToArray(array, id, lv) {
            var filter = $filter('filter')(array, { Parent_ID: id }, true);
            filter = $filter('orderBy')(filter, 'thutu');
            if (filter.length > 0) {
                var sp = "";
                for (var i = 0; i < lv; i++) {
                    sp += "----";
                }
                lv++;
                angular.forEach(filter, function (item) {
                    item.lv = lv;
                    item.close = true;
                    item.ids += "," + item.Phongban_ID;
                    item.tenmoi = sp + item.tenPhongban;
                    Temp.push(item);
                    addToArray(array, item.Phongban_ID, lv);
                });
            }
        }
        this.toogleModel = function (m, f) {
            if (f) {
                if (m.close !== true) {
                    m.close = true;
                } else {
                    m.close = false;
                }
            } else {
                this.phongbansUS.filter(p => p.isCheck).forEach(function (p) {
                    p.isCheck = false;
                });
                if (m.close !== true) {
                    m.close = true;
                } else {
                    m.close = false;
                }
                m.isCheck = !m.isCheck;
            }
            $ctr.ChoicePhong();
        };
        this.checkAllUST = function () {
            var check = this.uAll;
            this.phongbansUS.forEach(function (p) {
                p.isCheck = check;
            });
        };
        this.ChoicePhong = function () {
            var us = this.vp.filter(u => u.isCheck);
            this.choicePhong({ us: us });
        };
        this.ChoiceCongty = function (p) {
            if (p.close !== true) {
                p.close = true;
            } else {
                p.close = false;
            }
            this.choiceCongty({ us: p });
        };
        this.checkU = function (u) {
            var us = this.vp.filter(m => m.Phongban_ID !== u.Phongban_ID);
            us.forEach(function (n) {
                n.isCheck = false;
            });
        };

        $scope.LoadListTich = function () {
            var data = [];
            data = [
                { key: 'VB_themDen' },
                { key: 'VB_themDi' },
                { key: 'VB_xoaDen' },
                { key: 'VB_xoaDi' },
                { key: 'VB_xemBCcaNhan' },
                { key: 'VB_xemBCPhong' },
                { key: 'VB_xemBCCty' },
                { key: 'VB_xemBCAll' },
                { key: 'LCT_LapLich' },
                { key: 'LCT_DuyetLich' },
                { key: 'LCT_XemPhongHop' },
                { key: 'LCT_XemLichCaNhan' },
                { key: 'LCT_XemLichPhong' },
                { key: 'LCT_XemLichCongTy' },
                { key: 'LCT_XemLichAll' },
                { key: 'TSC_QuetBarCode' },
                { key: 'TSC_TheoDoiCaNhan' },
                { key: 'TSC_TheoDoiPhong' },
                { key: 'TSC_TheoDoiCongTy' },
                { key: 'TSC_TheoDoiAll' },
                { key: 'DX_XemXe' },
                { key: 'DX_TheoDoiCaNhan' },
                { key: 'DX_TheoDoiPhong' },
                { key: 'DX_TheoDoiCongTy' },
                { key: 'DX_TheoDoiAll' },
                { key: 'VPP_TheoDoiCaNhan' },
                { key: 'VPP_TheoDoiPhong' },
                { key: 'VPP_TheoDoiCongTy' },
                { key: 'VPP_TheoDoiAll' },
                { key: 'DP_TheoDoiCaNhan' },
                { key: 'DP_TheoDoiPhong' },
                { key: 'DP_TheoDoiCongTy' },
                { key: 'DP_TheoDoiAll' }
            ];

            $scope.ListTichFunction = data;
        };
        //getListCheck phanquyen
        $scope.arrListUserChoice = [];
        this.getcheckRolePb = function (m) {
            var ListPbUser = $ctr.vp;
            addToArray($ctr.vp, m.Phongban_ID, 0);
            $scope.ListPhongBanChild = Temp;
            Temp = [];
            if (m.isCheckItem == true) {
                //lấy ra list user theo pbID
                $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == m.Phongban_ID);
                $scope.getListUser.users.forEach(function (t) {
                    t.isCheckItem = true;
                    $scope.arrListUserChoice.push(t);
                });
                //get list phongbanCon
                if ($scope.ListPhongBanChild.length > 0) {
                    $scope.ListPhongBanChild.forEach(function (t) {
                        t.isCheckItem = true;
                        //lấy ra list user theo pbID con
                        $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == t.Phongban_ID);
                        $scope.getListUser.users.forEach(function (u) {
                            u.isCheckItem = true;
                            $scope.arrListUserChoice.push(u);
                        });
                    });
                }
            }
            else {
                //lấy ra list user theo pbID
                $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == m.Phongban_ID);
                $scope.getListUser.users.forEach(function (t) {
                    t.isCheckItem = false;
                    $scope.arrListUserChoice.pop(t);
                });
                //get list phongbanCon
                if ($scope.ListPhongBanChild.length > 0) {
                    $scope.ListPhongBanChild.forEach(function (t) {
                        t.isCheckItem = false;
                        //lấy ra list user theo pbID con
                        $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == t.Phongban_ID);
                        $scope.getListUser.users.forEach(function (u) {
                            u.isCheckItem = false;
                            $scope.arrListUserChoice.pop(u);
                        });
                    });
                }
                //
            }
        }
        //check user
        this.getcheckRoleUs = function (m, index) {
            if (m.isCheckItem == true) {
                //chek trùng
                var checkTrung = $scope.arrListUserChoice.filter(n => n.NhanSu_ID == m.NhanSu_ID);
                if (checkTrung.length == 0) {
                    //checkTrung.isCheckItem = true;
                    $scope.arrListUserChoice.push(m);
                }
            }
            else {
                var checkTrung = $scope.arrListUserChoice.filter(n => n.NhanSu_ID == m.NhanSu_ID);
                if (checkTrung.length > 0) {
                    //$scope.arrListUserChoice.pop(m);
                    $scope.arrListUserChoice.splice(index, 1);
                }
            }
        }

        this.checkChucNangRole = function (m, check) {
            var ListPbUser = $ctr.vp;
            addToArray($ctr.vp, m.Phongban_ID, 0);
            $scope.ListPhongBanChild = Temp;
            Temp = [];

            //check
            if (check == 'IsRead') {
                if (m.IsRead == true) {
                    //lấy ra list user theo pbID
                    $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == m.Phongban_ID);
                    $scope.getListUser.users.forEach(function (t) {
                        t.IsRead = true;
                    });
                    //get list phongbanCon
                    if ($scope.ListPhongBanChild.length > 0) {
                        $scope.ListPhongBanChild.forEach(function (t) {
                            t.IsRead = true;
                            //lấy ra list user theo pbID con
                            $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == t.Phongban_ID);
                            $scope.getListUser.users.forEach(function (u) {
                                u.IsRead = true;
                            });
                        });
                    }
                }
                //
                else {
                    //lấy ra list user theo pbID
                    $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == m.Phongban_ID);
                    $scope.getListUser.users.forEach(function (t) {
                        t.IsRead = false;
                    });
                    //get list phongbanCon
                    if ($scope.ListPhongBanChild.length > 0) {
                        $scope.ListPhongBanChild.forEach(function (t) {
                            t.IsRead = false;
                            //lấy ra list user theo pbID con
                            $scope.getListUser = ListPbUser.find(n => n.Phongban_ID == t.Phongban_ID);
                            $scope.getListUser.users.forEach(function (u) {
                                u.IsRead = false;
                            });
                        });
                    }
                }
            }
        };

        this.ConfigRoleUser = function (frm) {
            addToArray($rootScope.phongbans, null, 0);
            var listPbcheck = Temp;
            Temp = [];
            $scope.arr = [];
            listPbcheck.forEach(function (t) {
                angular.forEach(t.users, function (value, key) {
                    var check = null;
                    if ($scope.arr.length > 0) {
                        check = $scope.arr.find(n => n.NhanSu_ID == t.users[key].NhanSu_ID);
                    }
                    if ((t.users[key].IsRead || t.users[key].IsWrite || t.users[key].IsFull) && check == null) {
                        $scope.arr.push(t.users[key]);
                    }
                });
            })
            var Urlaction;
            Urlaction = "/KTL_MyFolder/ConfigRoleUser";

            var formData = new FormData();
            formData.append("t", $rootScope.login.tk);
            formData.append("CongtyID", $rootScope.congtyID);
            formData.append("NhanSu_ID", $rootScope.NhanSu_ID);
            formData.append("FolderID", $rootScope.getparentIDFolder);
            //   formData.append("arrListUserChoice", JSON.stringify($scope.arrListUserChoice));
            formData.append("arrListUserChoice", JSON.stringify($scope.arr));

            $http.post(baseUrl + Urlaction, formData, {
                withCredentials: false,
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: angular.identity
            }).then(function (res) {
                if (res.data.error == 1) {
                    hideloading();
                    dialogs.error('Thông báo', res.data.ms, { windowClass: "apidialog", size: "sm" });
                    return false;
                }
                else {
                    $scope.checkLen = 0;
                    $("#ModalPhanQuyen").modal("hide");
                    showtoastr('Đã cập nhật dữ liệu thành công!.');
                }
            });
        };
    }
});
os.directive('onFinishRender', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit(attr.onFinishRender);
                });
            }
        }
    }
});
os.filter('selected', [
    '$filter',
    function ($filter) {
        return function (files) {
            return $filter('filter')(files,
                {
                    selected: true
                });
        };
    }
]);
os.directive('owlCarousel', ['$timeout', function ($timeout) {
    return {
        restrict: 'E',
        transclude: false,
        link: function (scope, element) {
            var defaultOptions = {
            };
            scope.initCarousel = function (element) {
                // provide any default options you want

                var customOptions = scope.$eval(jQuery(element).attr('data-options'));
                // combine the two options objects
                for (var key in customOptions) {
                    defaultOptions[key] = customOptions[key];
                }
                // init carousel
                // jQuery(element).owlCarousel(defaultOptions);
            };
            // scope.$on('owlCarouselLoaded', function() {
            $timeout(function () {
                jQuery(element).owlCarousel(defaultOptions)
                scope.initCarousel();
            }, 0, false);
            // });
        }
    };
}]);
os.directive('owlCarouselItem', [function () {
    return {
        restrict: 'A',
        transclude: false,
        link: function (scope, element) {
            // wait for the last item in the ng-repeat then call init
            if (scope.$last) {
                scope.initCarousel(element.parent());
            }
        }
    };
}]);

os.directive('dndList', function () {
    return function (scope, element, attrs) {
        var toUpdate;
        var startIndex = -1;
        scope.$watch(attrs.dndList, function (value) {
            toUpdate = value;
        }, true);
        $(element[0]).sortable({
            items: 'li',
            start: function (event, ui) {
                startIndex = ($(ui.item).index());
            },
            stop: function (event, ui) {
                var newIndex = ($(ui.item).index());
                var toMove = toUpdate[startIndex];
                toUpdate.splice(startIndex, 1);
                toUpdate.splice(newIndex, 0, toMove);
                scope.$apply(scope.model);
            },
            axis: 'y'
        })
    }
});
moment.locale('vi');