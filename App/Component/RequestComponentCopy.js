﻿os.component('requestcomponent', {
    bindings: {
        requestid: '<',
        uid: '<',
        rfFunction:'&'
    },
    templateUrl: domainUrl + 'App/Component/RequestComponent.html?v=' + vhtmlcache,
    controller: function ($scope, $rootScope, $http, $sce, $timeout) {
        var $ctrl = this;
        this.$onChanges = () => {
            if (this.requestid) {
               $scope.RequestTitle = null;
               init();
                $scope.goViewRequest({ RequestMaster_ID: this.requestid });
            }
        };
        $scope.goRFfunction = function () {
            $ctrl.rfFunction();
        };
        //Khai báo mặc định
        $scope.ListRequest = [];
        $scope.ListSort = ListSort;
        $scope.Uutiens = Uutiens;
        $scope.Trangthais = Trangthais;
        //Init Data
        function initTD() {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_Tudien", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.tudien = { Form: data[0], FormTeam: data[1], Team: data[2], FilterTeam: data[2] };
                }
            });
        };
        //Filter
        $scope.FilterTeamForm = function () {
            var fo = $scope.tudien.Form.find(x => x.Form_ID == $scope.modelrequest.Form_ID);
            if (fo.IsUseAll) {
                $scope.tudien.FilterTeam = $scope.tudien.Team
            } else {
                $scope.tudien.FilterTeam = $scope.tudien.Team.filter(x => $scope.tudien.FormTeam.filter(a => a.Form_ID == $scope.modelrequest.Form_ID).length > 0);
            }
            var fo = $scope.tudien.Form.find(x => x.Form_ID == $scope.modelrequest.Form_ID);
            $scope.modelrequest.IsSLA = fo.IsSLA;
            $scope.modelrequest.IsChangeQT = fo.IsChangeQT;
            $scope.modelrequest.IsSkip = fo.IsSkip || false;
            $scope.modelrequest.IsQuytrinhduyet = fo.IsQuytrinhduyet;
        };
        $scope.SelectTeam = function () {
            var fo = $scope.tudien.FormTeam.find(x => x.Form_ID == $scope.modelrequest.Form_ID && x.Team_ID == $scope.modelrequest.Team_ID);
            $scope.modelrequest.IsSLA = fo.IsSLA;
            $scope.modelrequest.IsChangeQT = fo.IsChangeQT;
            $scope.modelrequest.IsSkip = fo.IsSkip || false;
            $scope.modelrequest.IsQuytrinhduyet = fo.IsQuytrinhduyet || 1;
        };
        //File đính kèm
        $scope.removeFilesRequest = function (i, files) {
            files.splice(i, 1);
        };
        var delsFiles = [];
        $scope.DeleteFilesRequest = function (i, files) {
            delsFiles.push(files[i]);
            files.splice(i, 1);
        };
        $scope.openAttachfile = function (ev) {
            $(ev.target).closest("div.form-group").find("input[type='file']").trigger("click");
        };
        $scope.UploadFiles = function (files, f) {
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 100 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    files.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 100MB!'
                });
            }
        };
        //Send Request
        var arrrequest = [];
        function SelectRequest(d) {
            $scope.ListRequest.filter(x => x.Select).forEach(function (r) {
                r.Select = false;
            });
            if (d)
                d.Select = true;
        };
        $scope.OpenThuhoiHuyRequest = function (r, f) {
            $scope.dtitle = (f ? "Thu hồi" : "Hủy") + " phiếu đề xuất.";
            $scope.modelrequest = { IsTH: f, RequestMaster_ID: r.RequestMaster_ID };
            $("#ModelSendRequest").modal("show");
            $scope.IPTailieuFiles = [];
        };
        $scope.OpenSendRequest = function (d, title, issign) {
            if (d) {
                SelectRequest(d);
                arrrequest = [d];
            } else {
                arrrequest = $scope.ListRequest.filter(x => x.Chon);
            }
            if (arrrequest.length == 0) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Bạn chưa chọn phiếu đề xuất nào để gửi đi !'
                });
                return false;
            }
            //Nếu đang View Request
            if ($(".InfoViewRequest").hasClass("show") && $scope.viewRequest.CountSign == 0 && $scope.viewRequest.IsChangeQT) {
                Swal.fire({
                    type: 'warning',
                    icon: 'warning',
                    title: '',
                    text: 'Vui lòng cấu hình người duyệt cho phiếu đề xuất trước khi gửi !'
                });
                return false;
            }
            $scope.dtitle = title + " phiếu đề xuất.";
            $scope.ModalSendUser = null;
            $scope.modelrequest = { IsSign: issign, IsSkip: d.IsSkip };
            if (d.IsSkip) {//Cho phép Skip
                if (issign == -1) {//Từ chối
                    $scope.ArraySendUsers = $scope.UserSigns.filter(x => x.IsClose == false && x.IsSign != 0);
                } else {//Chuyển tiếp
                    $scope.ArraySendUsers = $scope.UserSigns.filter(x => x.IsClose == false && x.IsSign == 0 && x.Trangthai == false);
                }
                if ($scope.ArraySendUsers && $scope.ArraySendUsers.length > 0) {
                    $scope.ArraySendUsers.sort((a, b) => a.STT - b.STT);
                    $scope.ModalSendUser = $scope.ArraySendUsers[0];
                }
            }
            $("#ModelSendRequest").modal("show");
            $scope.IPTailieuFiles = [];
            setdropZone("drop-zonesend");
        };
        $scope.chonUserSend = function (u) {
            $scope.ModalSendUser = u;
        };
        $scope.SendRequest = function () {
            if ($scope.modelrequest.IsTH == true) {//Thu hồi
                $scope.RollBack_Request($scope.modelrequest);
                return false;
            } else if ($scope.modelrequest.IsTH == false) {//Hủy
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Form/SendNext_Request",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    formData.append("NoidungDuyet", $scope.modelrequest.NoidungDuyet || "");
                    if ($scope.modelrequest.IsSign)
                        formData.append("IsSign", $scope.modelrequest.IsSign);
                    formData.append("ids", arrrequest.map(x => x.RequestMaster_ID).join(","));
                    if ($("#ModelSendRequest input[type='file']")[0]) {
                        $.each($("#ModelSendRequest input[type='file']")[0].files, function (i, file) {
                            formData.append('file', file);
                        });
                    }
                    if ($scope.ModalSendUser) {
                        formData.append("RequestSignUser_ID", $scope.ModalSendUser.RequestSignUser_ID);
                    }
                    return formData;
                }
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi gửi đề xuất !'
                    });
                    return false;
                }
                $("#ModelSendRequest").modal("hide");
                showtoastr('Đã gửi phiếu đề xuất thành công!.');
                $("#ModelSendRequest input[type='file']").val("");
                $scope.goRFfunction();
                //Nếu đang View Request
                if ($("#viewRequest .InfoViewRequest").hasClass("show")) {
                    $scope.goViewRequest($scope.viewRequest);
                }
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        //File
        $scope.listFiles = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_File", pas: [
                        { "par": "RequestMaster_ID ", "va": $scope.viewRequest.RequestMaster_ID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.LisFileAttachRQ = data[0];
                }
            });
        };
        $scope.openModalAddFileCV = function (cv) {
            $scope.IPTailieuFiles = [];
            filecv = true;
            $("#ModalFileUpload").modal("show");
        };
        $scope.PutFileUpload = function (f) {
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 100 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Tenfile = fi.name;
                    fi.Dungluong = fi.size;
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    $scope.IPTailieuFiles.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 100MB!'
                });
            }
        };
        function setdropZone(idk) {
            var dropZone = document.getElementById(idk);
            if (dropZone) {
                var startUpload = function (files) {
                    $scope.PutFileUpload(files);
                    //if ($scope.$$phase) {
                    $scope.$apply();
                    //}
                };
                dropZone.ondrop = function (e) {
                    e.preventDefault();
                    this.className = 'upload-drop-zone';
                    startUpload(e.dataTransfer.files);
                };
                dropZone.ondragover = function () {
                    this.className = 'upload-drop-zone drop';
                    return false;
                };
                dropZone.ondragleave = function () {
                    this.className = 'upload-drop-zone';
                    return false;
                };
                dropZone.onclick = function () {
                    $("#" + dropZone.getAttribute("mid") + " input[type='file']").trigger("click");
                };
            }
        };
        $(document).ready(function () {
            setdropZone("drop-zone");
            //setdropZone("drop-zonesend");
        });
        $scope.UploadFileCV = function () {
            if ($scope.IPTailieuFiles.length === 0) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng chọn file để upload !'
                });
                return false;
            }
            $scope.loadding = true;
            //swal.showLoading();
            //Progress File
            var hasfile = $scope.IPTailieuFiles && $scope.IPTailieuFiles.length > 0;
            if (!hasfile) {
                swal.showLoading();
            } else {
                $("#modalfileProgress").modal("show");
                var fileProgress = document.getElementById("fileProgress");
                var desProgress = document.getElementById("podes");
                fileProgress.setAttribute("value", 0);
                fileProgress.setAttribute("max", 0);
                fileProgress.style.display = "block";
            }
            $http({
                method: 'POST',
                url: "Form/UploadFile",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    formData.append("RequestMaster_ID", $scope.viewRequest.RequestMaster_ID || null);
                    $.each($scope.IPTailieuFiles, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                }, uploadEventHandlers: {
                    progress: function (e) {
                        if (hasfile && e.lengthComputable) {
                            fileProgress.setAttribute("value", e.loaded);
                            fileProgress.setAttribute("max", e.total);
                            var po = Math.ceil(e.loaded * 100 / e.total);
                            desProgress.innerHTML = po + "%";
                        }
                    }
                }
            }).then(function (res) {
                $scope.loadding = false;
                if (!hasfile)
                    closeswal();
                $("#modalfileProgress").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {

                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi upload file !'
                    });
                    return false;
                }
                $("#ModalFileUpload").modal("hide");
                $scope.IPTailieuFiles = [];
                showtoastr("Upload File thành công!");
                $scope.listFiles();
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        //Comment
        $scope.listComments = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_ListComment", pas: [
                        { "par": "RequestMaster_ID ", "va": $scope.viewRequest.RequestMaster_ID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    data[0].forEach(function (r) {
                        if (r.files) {
                            r.files = JSON.parse(r.files);
                        }
                    });
                    $scope.Comments = data[0];
                    $scope.Comments.filter(x => x.ParentID != null).forEach(function (r) {
                        r.Comments = $scope.Comments.filter(x => x.ParentID === r.RequestComment_ID);
                        r.ParentComment = $scope.Comments.find(x => r.ParentID === x.RequestComment_ID);
                    });
                }
            });
        };
        $scope.cart = cart;
        $scope.emojis = emojis;
        $scope.resetemojiIndex = function () {
            $scope.emojiIndex = 0;
        }
        $scope.setemojiIndex = function (idx) {
            $scope.emojiIndex = idx;
        }
        $scope.emojiIndex = 0;
        $scope.noiDungChat = {
            Noidung: ''
        };
        $scope.openImageChat = function () {
            $("#ChatCtr input.inputimgChat").click();
        }
        $scope.openFileChat = function (ipn) {
            $("#ChatCtr input." + (ipn || inputfileChat)).click();
        }

        $scope.UploadFileChat = function (f) {
            if (!$scope.FileAttach) $scope.FileAttach = [];
            if ($scope.FileAttach.length + f.length > 5) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 5 file cho dự án!'
                });
                return false;
            }
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 100 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    fi.Tenfile = fi.name;
                    fi.Dungluong = fi.size;
                    $scope.FileAttach.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 100MB!'
                });
            } else {
                if (!ReplyID)
                    $scope.goBottomChat();
            }
        }
        $scope.openFileChatDynamic = function (id) {
            $(id).click();
        }

        $scope.UploadFileChatDynamic = function (f) {
            if (!$scope.FileAttachBC) $scope.FileAttachBC = [];
            if ($scope.FileAttachBC.length + f.length > 10) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 10 file một lần!'
                });
                return false;
            }
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 100 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    fi.Tenfile = fi.name;
                    fi.Dungluong = fi.size;
                    $scope.FileAttachBC.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 100MB!'
                });
            } else {
                if (!ReplyID)
                    $scope.goBottomChat();
            }
        }


        $scope.setEmoji = function (e, type) {
            if (type == false) {
                var noidung = $scope.noiDungChat.Noidung || "";
                var noiDungChat = noidung + e.emoji;
                $scope.noiDungChat.Noidung = noiDungChat;
            } else {
                $scope.noiDungChat.Noidung = e;
            }
            $("#noiDungChat").focus();
        }

        $scope.goBottomChat = function () {
            setTimeout(function () {
                var div = document.getElementById("taskmessagepanel");
                div.scrollTop = div.scrollHeight - div.clientHeight;
            }, 100)
        }
        var ReplyID = null;
        $scope.Reply = function (co) {
            $scope.Comments.filter(x => x.IsReply).forEach(function (r) {
                r.IsReply = false;
            });
            $scope.IsReply = true;
            ReplyID = co.RequestComment_ID;
            co.IsReply = true;
            $scope.tinnhanreply = co;
        };
        $scope.HuyReply = function () {
            var co = $scope.Comments.find(x => x.RequestComment_ID === ReplyID);
            $scope.IsReply = false;
            ReplyID = null;
            $scope.tinnhanreply = null;
            co.IsReply = false;
            $scope.FileAttach = [];
        };
        $scope.EditComment = function (co) {

        };
        $scope.Comments = [];
        $scope.sendMS = function (loai) {
            if ($scope.loadding) {
                return false;
            }
            if (!$scope.noiDungChat.Noidung && $scope.FileAttach.length === 0) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập nội dung comment !'
                });
                return false;
            }
            $scope.loadding = true;
            var ms = { ParentID: ReplyID, RequestMaster_ID: $scope.viewRequest ? $scope.viewRequest.RequestMaster_ID : null, NguoiTao: $rootScope.login.u.NhanSu_ID, Noidung: $scope.noiDungChat.Noidung, NgayTao: new Date(), fullName: $rootScope.login.u.fullName, anhThumb: $rootScope.login.u.anhDaiDien, IsType: 0, IsDelete: false };
            ms.files = $scope.FileAttach;
            $scope.Comments.push(ms);
            $scope.Comments.filter(x => x.ParentID != null).forEach(function (r) {
                r.Comments = $scope.Comments.filter(x => x.ParentID === r.RequestComment_ID);
                r.ParentComment = $scope.Comments.find(x => r.ParentID === x.RequestComment_ID);
            });
            $scope.noiDungChat.Noidung = "";
            $scope.goBottomChat();
            //Progress File
            var hasfile = $scope.FileAttach && $scope.FileAttach.length > 0;
            if (!hasfile) {
                swal.showLoading();
            } else {
                $("#modalfileProgress").modal("show");
                var fileProgress = document.getElementById("fileProgress");
                var desProgress = document.getElementById("podes");
                fileProgress.setAttribute("value", 0);
                fileProgress.setAttribute("max", 0);
                fileProgress.style.display = "block";
            }
            $http({
                method: 'POST',
                url: "Form/Update_Comment",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    var $html = $("#noiDungChatBC");
                    var ids = [];
                    $html.find("uid").each(function (i, el) {
                        ids.push(el.innerText);
                        el.remove();
                    });
                    ms.Noidung = $html.html();
                    formData.append("model", JSON.stringify(ms));
                    if (ids.length > 0) {
                        formData.append("ids", ids.join(","));
                    }
                    $.each($scope.FileAttach, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                },
                uploadEventHandlers: {
                    progress: function (e) {
                        if (hasfile && e.lengthComputable) {
                            fileProgress.setAttribute("value", e.loaded);
                            fileProgress.setAttribute("max", e.total);
                            var po = Math.ceil(e.loaded * 100 / e.total);
                            desProgress.innerHTML = po + "%";
                        }
                    }
                }
            }).then(function (res) {
                $scope.loadding = false;
                if (!hasfile)
                    closeswal();
                $("#modalfileProgress").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi gửi thảo luận !'
                    });
                    return false;
                }
                $scope.Comments[$scope.Comments.length - 1].RequestComment_ID = res.data.RequestComment_ID;
                if ($scope.FileAttach != null && $scope.FileAttach.length > 0) {
                    $scope.listComments();
                }
                $scope.FileAttach = [];
                $('input[name=FileAttachChat]').val("");
                if (ReplyID) $scope.HuyReply();
                $("textarea#noiDungChat").val(null);
                window.parent.postMessage({ Type: 1, Isdel: false }, "*");
            }, function (response) { // optional
                $scope.loadding = false;
            });
        }

        $scope.Del_Comment = function (cm, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa comment này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Del_Comment",
                        data: {
                            t: $rootScope.login.tk, id: cm.RequestComment_ID
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.Comments.splice(i, 1);
                        }
                    });
                }
            })

        };
        $scope.removeFilesComment = function (files, i) {
            files.splice(i, 1);
        };
        $scope.Del_CommentFile = function (files, i, f) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa file này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Del_RequestFile",
                        data: { t: $rootScope.login.tk, ids: [f.FileID] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            files.splice(i, 1);
                            showtoastr("File bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa File không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        // Chọn User
        var IsTypeTD = 0;
        $scope.showusersModal = function (index, f) {
            //2 Thêm người duyệt khi view/0 Thêm người theo dõi 1/ Thêm quản lý khi view/ 3 Thêm quản lý. 4 Cấu hình Duyệt
            $rootScope.phongbans.filter(r => r.isCheck).forEach(function (r) {
                r.isCheck = false;
            });
            $rootScope.users.filter(r => r.isCheck).forEach(function (r) {
                r.isCheck = false;
            });
            $scope.vbphongbans = $rootScope.phongbans;
            $scope.vbusers = $rootScope.users;
            $scope.one = f;
            // set chọn user trong lấy list user config cho quy trình
            IsTypeTD = index;
            $("#usersModal").modal("show");
        };

        $scope.choiceUser = function (users) {
            if (IsTypeTD == 0 || IsTypeTD == 1) {
                users = users.filter(u => $scope.theodois.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                var tds = $scope.theodois.concat(users);
                swal.showLoading();
                $http({
                    method: "POST",
                    url: "Form/Update_Theodoi",
                    data: { t: $rootScope.login.tk, RequestMaster_ID: $scope.viewRequest.RequestMaster_ID, uds: tds.map(a => a["NhanSu_ID"]), IsType: IsTypeTD },
                    contentType: 'application/json; charset=utf-8'
                }).then(function (res) {
                    closeswal();
                    if (!$rootScope.checkToken(res)) return false;
                    if (res.data.error !== 1) {
                        $("#usersModal").modal("hide");
                        $scope.getSignLog($scope.viewRequest);
                    } else {
                        Swal.fire({
                            icon: 'error',
                            type: 'error',
                            title: '',
                            text: 'Thêm người ' + (IsTypeTD == 0 ? "theo dõi" : "quản lý") + ' không thành công, vui lòng thử lại!'
                        });
                    }
                });
            } else {
                $("#usersModal").modal("hide");
                if (IsTypeTD == 3) {

                    $scope.quanlys = $scope.quanlys.concat(users);
                } else if (IsTypeTD == 4) {
                    $scope.duyets = $scope.duyets.concat(users);
                }
            }
        };

        $scope.DelTheodoi = function (u, k) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa người " + (k == 0 ? 'Theo dõi ' : 'Quản lý ') + "này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    swal.showLoading();
                    $http({
                        method: "POST",
                        url: "Form/Del_Theodoi",
                        data: { t: $rootScope.login.tk, RequestMaster_ID: $scope.viewRequest.RequestMaster_ID, NhanSu_ID: u.NhanSu_ID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        closeswal();
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            var idx = $scope.theodois.findIndex(a => a.NhanSu_ID == u.NhanSu_ID);
                            if (idx !== -1) {
                                $scope.theodois.splice(idx, 1);
                            }
                            showtoastr("Xóa người " + (k == 0 ? 'Theo dõi ' : 'Quản lý ') + "thành công");
                        }
                    });
                }
            })
        };

        $scope.Update_NotyTheodoi = function (u) {
            u.IsNoty = !(u.IsNoty || false);
            $http({
                method: "POST",
                url: "Form/Update_NotyTheodoi",
                data: { t: $rootScope.login.tk, RequestMaster_ID: $scope.viewRequest.RequestMaster_ID, NhanSu_ID: u.NhanSu_ID, Noty: u.IsNoty },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    showtoastr("Cập nhật trạng thái nhận thông báo thành công");
                }
            });
        };
        //@Tag người
        function setEndOfContenteditable(contentEditableElement) {
            var range, selection;
            if (document.createRange)//Firefox, Chrome, Opera, Safari, IE 9+
            {
                range = document.createRange();//Create a range (a range is a like the selection but invisible)
                range.selectNodeContents(contentEditableElement);//Select the entire contents of the element with the range
                range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
                selection = window.getSelection();//get the selection object (allows you to change selection)
                selection.removeAllRanges();//remove any selections already made
                selection.addRange(range);//make the range you have just created the visible selection
            }
            else if (document.selection)//IE 8 and lower
            {
                range = document.body.createTextRange();//Create a range (a range is a like the selection but invisible)
                range.moveToElementText(contentEditableElement);//Select the entire contents of the element with the range
                range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
                range.select();//Select the range (make it the visible selection
            }
        }
        $scope.completecm = function ($event) {
            txtstring = $event.target.innerHTML;
            string = $event.target.innerText.trim();
            if (!string.endsWith("@")) return false;
            var arrs = string.split("@");
            string = arrs[arrs.length - 1];
            $scope.viewRequest.Focuscm = true;
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                $("ul.autoUsercm").focus();
                $event.preventDefault();
            }
            string = string.replace("@", "");
            var output = [];
            if (!string) {
                string = "";
            }
            angular.forEach($scope.thanhviens, function (u) {
                if ((u.fullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.enFullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.tenTruyCap || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.phone + '').toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    output.push(u);
                }
            });
            $scope.filterUsercm = output.filter(x => x.NhanSu_ID !== $rootScope.login.u.NhanSu_ID);
        };
        $scope.clickAddUsercm = function (u) {
            $scope.filterUsercm = null;
            var string = $scope.noiDungChat.Noidung;
            string = string.substring(0, string.lastIndexOf("@") - 1) + " <user><uid>" + u.NhanSu_ID + "</uid>@" + u.fullName + "</user>";
            $scope.noiDungChat.Noidung = string + "&nbsp;";
            setTimeout(function () {
                elem = document.getElementById('noiDungChatBC');//This is the element that you want to move the caret to the end of
                setEndOfContenteditable(elem);
            })
        };
        $scope.handleKeyDowncm = function ($event) {
            var objCurrentLi, obj = $('ul.autoUsercm').find('.SelectedLi'), objUl = $('autoUsercm'), code = ($event.keyCode ? $event.keyCode : $event.which);
            if (code === 40) {
                // Down
                $("ul.autoUsercm").focus();
                $event.preventDefault();
                if ((obj.length === 0) || (objUl.find('li:last').hasClass('SelectedLi') === true)) {
                    objCurrentLi = objUl.find('li:first').addClass('SelectedLi');
                }
                else {
                    objCurrentLi = obj.next().addClass('SelectedLi');
                }
                obj.removeClass('SelectedLi');
            }
            else if (code === 38) {
                // Up
                $event.preventDefault();
                if ((obj.length === 0) || (objUl.find('li:first').hasClass('SelectedLi') === true)) {
                    objCurrentLi = objUl.find('li:last').addClass('SelectedLi');
                }
                else {
                    objCurrentLi = obj.prev().addClass('SelectedLi');
                }
                obj.removeClass('SelectedLi');
            }
            else if (code === 13) {
                // Enter
                $event.preventDefault();
                obj = $('ul.autoUser' + i).find('.SelectedLi');
                if (obj) {
                    var u = $scope.filterUser.find(x => x.NhanSu_ID == obj.attr("uid"));
                    $scope.clickAddUsercm(u);
                }
            }
        };
        //@Tag quản lý,quy trình duyệt
        $scope.removeUser = function (us, idx) {
            us.splice(idx, 1);
        };
        $scope.ClearFocus = function () {
            $("#ModalRequestAdd .autoUser").hide();
        }
        $scope.clickAddUser = function (m, us, u, i) {
            $scope.filterUser = null;
            if (!us) {
                us = [];
            }
            if (us.filter(x => x.NhanSu_ID == u.NhanSu_ID).length == 0)
                us.push(u);
            m["searchU" + i] = "";
            setTimeout(function () {
                elem = document.querySelector("input.ipautoUser" + i);//This is the element that you want to move the caret to the end of
                setEndOfContenteditable(elem);
                elem.focus();
                elem.innerText = "";
            })
        };
        $scope.handleKeyDown = function (m, us, i, $event) {
            var objCurrentLi, obj = $('ul.autoUser' + i).find('.SelectedLi'), objUl = $('ul.autoUser' + i), code = ($event.keyCode ? $event.keyCode : $event.which);
            if (code === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
                if ((obj.length === 0) || (objUl.find('li:last').hasClass('SelectedLi') === true)) {
                    objCurrentLi = objUl.find('li:first').addClass('SelectedLi');
                }
                else {
                    objCurrentLi = obj.next().addClass('SelectedLi');
                }
                obj.removeClass('SelectedLi');
            }
            else if (code === 38) {
                // Up
                $event.preventDefault();
                if ((obj.length === 0) || (objUl.find('li:first').hasClass('SelectedLi') === true)) {
                    objCurrentLi = objUl.find('li:last').addClass('SelectedLi');
                }
                else {
                    objCurrentLi = obj.prev().addClass('SelectedLi');
                }
                obj.removeClass('SelectedLi');
            }
            else if (code === 13) {
                // Enter
                $event.preventDefault();
                obj = $('ul.autoUser' + i).find('.SelectedLi');
                if (obj) {
                    var u = $scope.filterUser.find(x => x.NhanSu_ID == obj.attr("uid"));
                    $scope.clickAddUser(m, us, u, i);
                }
            }
        };

        $scope.complete = function (string, m, $event, i) {
            if ($("ul.autoUser" + i).css('display') == 'none')
                $("ul.autoUser" + i).show();
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            if (!string) {
                string = "";
            }
            string = string.replace("@", "");
            if (m) {
                for (var k = 0; k <= 5; k++) {
                    m["Focus" + k] = false;
                }
                m["Focus" + i] = true;
            }
            var output = [];
            if (!string) {
                string = "";
            }
            angular.forEach($rootScope.users, function (u) {
                if ((u.fullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.enFullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.tenTruyCap || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.phone + '').toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    if (!m || m.NhanSu_ID !== u.NhanSu_ID)
                        output.push(u);
                }
            });
            $scope.filterUser = output;
        };
        //Action

        $scope.goViewRequest = function (r) {
            SelectRequest(r);
            $scope.Comments = [];
            $scope.ChartsSigns = [];
            $scope.Logs = [];
            $scope.totalDisplayedLog = 20;
            $scope.LisFileAttachRQ = [];
            $scope.Vanbans = [];
            $scope.Congviecs = [];
            $scope.Lichs = [];
            if (!r.IsFun || !r.IsTao)
                $("#aeditrq").hide();
            $('a[data-target="#viewRequestAA"]').trigger("click");
            $("#viewRequest .InfoViewRequest").addClass('show');
            $("#viewRequest #info-overlay").addClass('show');
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_Get", pas: [
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "RequestMaster_ID ", "va": r.RequestMaster_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $("#viewRequest .InfoViewRequest #loader").hide();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    data[0].forEach(function (r) {
                        r.objTrangthai = TrangthaiViews.find(x => x.id == r.Trangthai);
                        if (r.Thanhviens) {
                            r.Thanhviens = JSON.parse(r.Thanhviens);
                            if (r.Signs) {
                                r.Signs = JSON.parse(r.Signs);
                                var tong = 0;
                                var tc = 0;
                                r.Signs.forEach(function (ro) {
                                    ro.Thanhviens = r.Thanhviens.filter(x => x.RequestSign_ID == ro.RequestSign_ID);
                                    if (ro.IsTypeDuyet == 0) {//Duyệt 1 trong nhiều
                                        ro.USign = ro.Thanhviens.filter(x => x.IsSign != 0);
                                        ro.IsShowTV = ro.Thanhviens.filter(x => x.IsSign != 0 && x.IsType != 4).length == 0;
                                        ro.Thanhviens = ro.Thanhviens.filter(x => x.IsSign == 0);
                                        tong += (ro.IsShowTV ? 0 : 1);
                                        tc += 1;
                                    } else {
                                        tong += ro.Thanhviens.filter(x => x.IsSign != 0 && !x.IsClose).length;
                                        tc += ro.Thanhviens.filter(x => !x.IsClose).length;
                                    }
                                });
                                r.Tiendo = Math.ceil(tong * 100 / tc) || 0;
                                r.color = renderColor(r.Tiendo);
                                r.txtcolor = renderTxtColor(r.Tiendo);
                                r.IsLast = tong + 1 === tc;
                            }
                        }
                    });
                    $scope.viewRequest = data[0][0];
                    if (!$scope.viewRequest.IsFun || !$scope.viewRequest.IsTao)
                        $("#aeditrq").remove();
                    else
                        $("#aeditrq").show();

                    $timeout(function () {
                        $scope.listComments();
                        $scope.listFiles();
                        $scope.listRelated(r);
                        $scope.getSignLog(r);
                        $scope.setViewRequest(r);
                    }, 500);

                }
            });
            if ($scope.RequestMaster_ID != r.RequestMaster_ID)
                $scope.RequestMaster_ID = r.RequestMaster_ID;
        };
        $scope.closeInfoRequest = function () {
            $("#viewRequest .InfoViewRequest").removeClass('show');
            $("#viewRequest #info-overlay").removeClass('show');
            $scope.viewRequest = null;
            $scope.ChartsSigns = [];
            $scope.Logs = [];
            SelectRequest();
        };
        $scope.fullInfoRequest = function () {
            $("#viewRequest .InfoViewRequest").toggleClass("fulliframe");
        };
        $scope.DelRequest = function (u, f) {
            var ids = [];
            if (u) {
                ids.push(u.RequestMaster_ID);
            } else {
                ids = $scope.ListRequest.filter(x => x.Chon).map(a => a["RequestMaster_ID"]);
            }
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa yêu cầu - đề xuất này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Del_Request",
                        data: { t: $rootScope.login.tk, ids: ids },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.Refresh();
                            if (f === 1) {
                                $scope.closeInfoRequest();
                            }
                            showtoastr("Đã xóa yêu cầu - đề xuất thành công!");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.StopRequest = function (u, f) {
            var ids = [];
            if (u) {
                ids.push(u.RequestMaster_ID);
            } else {
                ids = $scope.ListRequest.filter(x => x.Chon).map(a => a["RequestMaster_ID"]);
            }
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn hủy yêu cầu - đề xuất này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Cancel_Request",
                        data: { t: $rootScope.login.tk, ids: ids },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.Refresh();
                            if (f === 1) {
                                $scope.closeInfoRequest();
                            }
                            //Nếu đang View Request
                            if ($(".InfoViewRequest").hasClass("show")) {
                                $scope.goViewRequest($scope.modelrequest);
                            }
                            showtoastr("Đã hủy yêu cầu - đề xuất thành công!");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Hủy không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.RollBack_Request = function (u) {
            var ids = [];
            if (u) {
                ids.push(u.RequestMaster_ID);
            } else {
                ids = $scope.ListRequest.filter(x => x.Chon).map(a => a["RequestMaster_ID"]);
            }
            $http({
                method: "POST",
                url: "Form/RollBack_Request",
                data: { t: $rootScope.login.tk, ids: ids, NoidungDuyet: u.NoidungDuyet, RequestSignUser_ID: $scope.ModalSendUser != null ? $scope.ModalSendUser.RequestSignUser_ID : null },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    $("#ModelSendRequest").modal("hide");
                    $scope.Refresh();
                    //Nếu đang View Request
                    if ($(".InfoViewRequest").hasClass("show")) {
                        $scope.goViewRequest($scope.viewRequest);
                    }
                    showtoastr("Đã thu hồi yêu cầu - đề xuất thành công!");
                } else {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Thu hồi không thành công, vui lòng thử lại!'
                    });
                }
            });
        };
        $scope.BackRequest = function (u, f) {
            var ids = [];
            if (u) {
                ids.push(u.RequestMaster_ID);
            } else {
                ids = $scope.ListRequest.filter(x => x.Chon).map(a => a["RequestMaster_ID"]);
            }
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn bỏ hủy yêu cầu - đề xuất này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Back_Request",
                        data: { t: $rootScope.login.tk, ids: ids },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.Refresh();
                            if (f === 1) {
                                $scope.closeInfoRequest();
                            }
                            //Nếu đang View Request
                            if ($(".InfoViewRequest").hasClass("show")) {
                                $scope.goViewRequest($scope.modelrequest);
                            }
                            showtoastr("Đã bỏ hủy yêu cầu - đề xuất thành công!");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Bỏ hủy không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };

        $scope.AddRequest = function (frm, fc) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Form/Update_RequestMaster",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    if ($scope.modelrequest.Ngaylap) {
                        $scope.modelrequest.Ngaylap = moment($scope.modelrequest.Ngaylap).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                    }
                    formData.append("t", $rootScope.login.tk);
                    formData.append("model", JSON.stringify($scope.modelrequest));
                    if ($scope.quanlys && $scope.quanlys.length > 0)
                        formData.append("quanlys", $scope.quanlys.map(a => a.NhanSu_ID).join(","));
                    if ($scope.duyets && $scope.duyets.filter(x => x.RequestSignUser_ID == null).length > 0)
                        formData.append("duyets", $scope.duyets.map(a => a.NhanSu_ID).join(","));
                    $.each($("#frRequest input[type='file']")[0].files, function (i, file) {
                        formData.append('file', file);
                    });
                    if (delsFiles.length > 0) {
                        formData.append("dels", delsFiles.map(a => a.FileID).join(","));
                    }
                    return formData;
                }
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi lập đề xuất mới !'
                    });
                    return false;
                }
                $scope.modelrequest.Title = "";
                if (!fc)
                    $("#ModalRequestAdd").modal("hide");
                showtoastr('Đã cập nhật phiếu đề xuất thành công!.');
                $("#frRequest input[type='file']").val("");
                $scope.goRFfunction();
                //Nếu đang View Request
                if ($(".InfoViewRequest").hasClass("show")) {
                    $scope.goViewRequest($scope.modelrequest);
                }
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        $scope.trustAsHtml = function (html) {
            if (!html) {
                return "";
            }
            return $sce.trustAsHtml(html.replace(/\n/g, "<br/>"));
        };
        $scope.showChartSign = function (u) {
            $scope.modelChart = u;
            $scope.ChartsSigns = u.Thanhviens;
            $scope.Logs = [];
            $scope.getSignLog(u);
            $("#chartSignModal").modal("show");
        };
        $scope.getSignLog = function (re) {
            $http({
                method: "POST",
                url: "/Home/callProc",
                data: {
                    t: $rootScope.login.tk,
                    proc: "Request_ListLog",
                    pas: [
                        { "par": "RequestMaster_ID", "va": re.RequestMaster_ID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                var data = JSON.parse(res.data.data);
                data[0].forEach(function (r) {
                    r.Thanhviens = data[1].filter(x => x.RequestSign_ID == r.RequestSign_ID);
                    if (r.IsTypeDuyet == 0) {//Duyệt 1 trong nhiều
                        r.USign = r.Thanhviens.filter(x => x.IsSign != 0);
                        r.Thanhviens = r.Thanhviens.filter(x => x.IsSign == 0);
                    }
                    r.SoThanhvien = r.Thanhviens.filter(u => !u.IsClose || u.IsSign != 0).length;
                });
                if (data[0].length == 0 && data[1].length > 0) {
                    var r = { GroupName: "Quy trình động", IsTypeDuyet: re.IsQuytrinhduyet };
                    r.Thanhviens = data[1].filter(x => x.RequestSign_ID == null && x.IsType == 5);
                    if (r.IsTypeDuyet == 0) {//Duyệt 1 trong nhiều
                        r.USign = r.Thanhviens.filter(x => x.IsSign != 0);
                        r.Thanhviens = r.Thanhviens.filter(x => x.IsSign == 0);
                    }
                    r.SoThanhvien = r.Thanhviens.filter(u => !u.IsClose || u.IsSign != 0).length;
                    data[0] = [r];
                }
                $scope.ChartsSigns = data[0];
                $scope.UserSigns = data[1];
                $scope.Logs = data[2];
                $scope.thanhviens = data[3];
                $scope.theodois = data[4];
            },
                function (response) {
                });
        };
        //Load More
        $scope.totalDisplayedLog = 20;
        $scope.loadMoreLog = function () {
            if ($scope.Logs.length > $scope.totalDisplayedLog)
                $scope.totalDisplayedLog += 20;
        };
        $scope.totalDisplayedRelate = 20;
        $scope.loadMoreRelate = function () {
            if ($scope.relates.length > $scope.totalDisplayedRelate)
                $scope.totalDisplayedRelate += 20;
        };
        //Relate
        $scope.setTabVB = function (type) {
            $scope.TabVB = type;
        }
        $scope.setTabCV = function (type) {
            $scope.TabCV = type;
        }
        $scope.setTabLich = function (type) {
            $scope.TabLich = type;
        }
        $scope.changeLoaiVB = function (loai) {
            $scope.mrelate.loai = loai;
            $scope.listRelatelienquan($scope.mrelate);
        };
        $scope.listRelatelienquan = function (rl) {
            swal.showLoading();
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_ListTDRelate", pas: [
                        { "par": "Module_ID ", "va": rl.Module_ID },
                        { "par": "RequestMaster_ID ", "va": rl.RequestMaster_ID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "loai ", "va": rl.loai },
                        { "par": "s ", "va": rl.rlsearch }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    if (rl.Module_ID == "stask") {
                        data.forEach(function (r) {
                            try {
                                if (r.Thanhviens)
                                    r.Thanhviens = JSON.parse(r.Thanhviens);
                            } catch (e) {
                            }
                            try {
                                if (r.Tags)
                                    r.Tags = JSON.parse(r.Tags);
                            } catch (e) {
                            }
                            r.color = renderColor(r.Tiendo);
                            r.txtcolor = renderTxtColor(r.Tiendo);
                            r.NgayHan = Math.abs(r.NgayHan);
                            r.Tao = r.NguoiTao === $rootScope.login.u.NhanSu_ID;
                            if (r.Thanhviens) {
                                r.IsQL = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "2").length > 0;
                                r.IsTH = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "1").length > 0;
                                r.IsTD = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "0").length > 0;
                                r.IsType = r.IsQL ? 2 : (r.IsTH ? 1 : 0);
                            }
                        })
                    }
                    $scope.relates = data;
                }
            });
        };
        $scope.Del_Relate = function (tit, id, list) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa " + tit + " liên quan này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Del_Releate",
                        data: { t: $rootScope.login.tk, ids: [id] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            var idx = list.findIndex(x => x.RequestRelate_ID === id);
                            if (idx !== -1)
                                list.splice(idx, 1);
                            showtoastr(tit + " bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa ' + tit + ' không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.listRelated = function (rl) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_ListRelated", pas: [
                        { "par": "RequestMaster_ID ", "va": rl.RequestMaster_ID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.Vanbans = data[0];
                    //Công việc
                    data[1].forEach(function (r) {
                        try {
                            if (r.Thanhviens)
                                r.Thanhviens = JSON.parse(r.Thanhviens);
                        } catch (e) {
                        }
                        try {
                            if (r.Tags)
                                r.Tags = JSON.parse(r.Tags);
                        } catch (e) {
                        }
                        r.color = renderColor(r.Tiendo);
                        r.txtcolor = renderTxtColor(r.Tiendo);
                        r.NgayHan = Math.abs(r.NgayHan);
                        r.Tao = r.NguoiTao === $rootScope.login.u.NhanSu_ID;
                        if (r.Thanhviens) {
                            r.IsQL = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "2").length > 0;
                            r.IsTH = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "1").length > 0;
                            r.IsTD = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "0").length > 0;
                            r.IsType = r.IsQL ? 2 : (r.IsTH ? 1 : 0);
                        }
                    })
                    $scope.Congviecs = data[1];
                    $scope.Lichs = data[2];
                }
            });
        };
        $scope.openRelate = function (rl, Module_ID, Istype) {
            $scope.totalDisplayedRelate = 20;
            $scope.mrelate = { RequestMaster_ID: rl.RequestMaster_ID, Module_ID: Module_ID, IsType: Istype, rlsearch: "", loai: -1 };
            $("#ModalRelate").modal("show");
            switch (Module_ID) {
                case "sdoc":
                    $scope.dtitle = "Liên kết văn bản";
                    $scope.mrelate.title = "văn bản";
                    break;
                case "stask":
                    $scope.dtitle = "Liên kết công việc";
                    $scope.mrelate.title = "công việc";
                    break;
                case "scalendar":
                    $scope.dtitle = "Liên kết lịch";
                    $scope.mrelate.title = "lịch";
                    break;
            }
            $scope.listRelatelienquan($scope.mrelate);
        };
        $scope.UpdateRelate = function () {
            var rids = $scope.relates.filter(x => x.Chon).map(x => x.Relate_ID);
            if (rids.length === 0) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chưa chọn ' + $scope.mrelate.title + ' liên quan !'
                });
                return false;
            }
            if ($scope.loadding) {
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Form/Update_Releate",
                data: {
                    t: $rootScope.login.tk,
                    id: $scope.mrelate.RequestMaster_ID,
                    Module_ID: $scope.mrelate.Module_ID,
                    IsType: $scope.mrelate.IsType,
                    Ghichu: $scope.mrelate.Ghichu,
                    rids: rids
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                $("#ModalRelate").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm ' + $scope.mrelate.title + ' liên quan !'
                    });
                    return false;
                }
                $scope.listRelated($scope.mrelate);
                showtoastr("Thêm " + $scope.mrelate.title + " liên quan thành công!");
            }, function (res) { // optional
                $scope.loadding = false;
                closeswal();
                $("#ModalRelate").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm ' + $scope.mrelate.title + ' liên quan !'
                    });
                    return false;
                }
            });
        };
        //Open Modal
        $scope.List_TypeDuyet = [
            { value: 0, text: 'Một trong nhiều' },
            { value: 1, text: 'Duyệt tuần tự' },
            { value: 2, text: 'Duyệt đồng thời' }
        ]
        $scope.EditRequest = function (r) {
            $("#btncontinue").hide();
            $scope.dtitle = "Sửa phiếu đề xuất";
            delsFiles = [];
            $scope.FilesAttachList = [];
            $scope.LisFile = [];
            $scope.quanlys = [];
            $scope.duyets = [];
            $("form.ng-dirty").removeClass("ng-dirty");
            $("#ModalRequestAdd").modal("show");
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_Edit", pas: [
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "RequestMaster_ID ", "va": r.RequestMaster_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $(".InfoViewRequest #loader").hide();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.modelrequest = data[0][0];
                    $scope.LisFile = data[1];
                    $scope.quanlys = data[2];
                    $scope.duyets = data[3];
                }
            });
        };
        $scope.OpenModal_Add = function () {
            $scope.dtitle = "Lập phiếu đề xuất";
            $("#btncontinue").show();
            $scope.modelrequest = { Congty_ID: $rootScope.login.u.congtyID, IsQuytrinhduyet: 1, IsEdit: true, Uutien: 0, STT: $scope.ListRequest.length + 1, Trangthai: 0, IsDelete: false, Ngaylap: new Date() };
            if ($scope.tudien.Team.length > 0) {
                $scope.modelrequest.Team_ID = $scope.tudien.Team[0].Team_ID;
            }
            if ($scope.tudien.Form.length > 0) {
                $scope.modelrequest.Form_ID = $scope.tudien.Form[0].Form_ID;
                var fo = $scope.tudien.FormTeam.find(x => x.Form_ID == $scope.modelrequest.Form_ID && x.Team_ID == $scope.modelrequest.Team_ID);
                $scope.modelrequest.IsSLA = fo.IsSLA;
                $scope.modelrequest.IsChangeQT = fo.IsChangeQT;
                $scope.modelrequest.IsSkip = fo.IsSkip || false;
                $scope.modelrequest.IsQuytrinhduyet = fo.IsQuytrinhduyet || 1;
            }
            $scope.FilesAttachList = [];
            delsFiles = [];
            $scope.LisFile = [];
            $scope.quanlys = [];
            $scope.duyets = [];
            $("form.ng-dirty").removeClass("ng-dirty");
            $("#ModalRequestAdd").modal("show");
        };
        $scope.Request_ViewUser = function (r) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_ViewUser", pas: [
                        { "par": "RequestMaster_ID ", "va": r.RequestMaster_ID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $(".InfoViewRequest #loader").hide();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.DataViewUser = data[0];
                }
            });
        };
        $scope.Request_DownloadUser = function (r) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_DownloadUser", pas: [
                        { "par": "FileID ", "va": r.FileID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $(".InfoViewRequest #loader").hide();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.DataFileUser = data[0];
                }
            });
        };
        $scope.setViewRequest = function (r) {
            $http({
                method: "POST",
                url: domainUrl + "/Form/Xem_Request",
                data: { t: $rootScope.login.tk, id: r.RequestMaster_ID },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.Request_ViewUser(r);
            });
        };
        $scope.openViewer = function (f) {
            $scope.Viewer = f;
            $("#ModalViewer").modal("show");
            $("#objectviewpdf").attr("data", null);
            $("#iframeviewpdf").attr("src", null);
            let pdfurl = $rootScope.fileUrl + f.Duongdan;
            $scope.Viewer.pdfurl = pdfurl;
            $scope.DownloadFile(f);
            setTimeout(function () {
                $("#ModalViewer object").attr("data", pdfurl);
                $("#ModalViewer iframe").attr("src", "https://docs.google.com/viewer?url=" + pdfurl + "&embedded=true");
            }, 500);
        }
        $scope.DownloadFile = function (r) {
            $http({
                method: "POST",
                url: domainUrl + "/Form/Download_RequestFile",
                data: { t: $rootScope.login.tk, id: r.FileID },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.Request_DownloadUser(r)
            });
        };
        $scope.openviewUserModal = function () {
            $scope.viewUserTitle = "Danh sách người xem đề xuất";
            $("#viewUserModal").modal("show");
        };
        $scope.clickaccordion = function ($event) {
            angular.element('.panel-collapse').collapse('hide');
        }
        //STask
        $scope.goInfoCV = function (r) {
            $scope.CongviecID = r.CongviecID;
            $scope.Uid = generateUUID();
        };
        $scope.openModalAddCV = function (r) {
            if ($scope.RequestMaster_ID != r.RequestMaster_ID) {
                $scope.RequestMaster_ID = r.RequestMaster_ID;
            }
            $scope.RequestTitle = r.Title;
            $scope.Uid = generateUUID();
        };
       
        ///////////
        function init() {
            initTD();
        };
        //Gọi Jquery Plugin
        //Realtime
        if (messaging) {
            messaging.onMessage(function (payload) {
                console.log(payload);
                if (payload.data.hub) {
                    var hub = JSON.parse(payload.data.hub).Data;
                    if ((hub.loai === 13)) {
                        $scope.goRFfunction();
                        if ($(".InfoViewRequest").hasClass("show") && hub.idKey == $scope.viewRequest.RequestMaster_ID) {
                            $scope.goViewRequest($scope.viewRequest);
                            showtoastrNhacLich("Bạn vừa nhận được một phiếu đề xuất mới!");
                        }
                    }
                }
            });
        }
    }
});
os.component('addtaskcomponent', {
    bindings: {
        requestid: '<',
        uid: '<',
        htitle:'=',
        rfFunction: '&'
    },
    templateUrl: domainUrl + 'App/Component/Task/AddTask.html?v=' + vhtmlcache,
    controller: function ($scope, $rootScope, $http, $sce, $timeout) {
        var $ctrl = this;
        $scope.uutiens = [0, 1, 2, 3];
        $scope.Quyen = { IsBlockQuyen: false };
        $scope.goRFfunction = function () {
            $ctrl.rfFunction();
        };
        this.$onChanges = () => {
            if (this.requestid) {
                var u = $rootScope.users.find(u => u.NhanSu_ID === $rootScope.login.u.NhanSu_ID);
                $scope.nhomcongviec = null;
                $scope.dtitle = "Tạo công việc cho đề xuất : " + this.htitle;
                $scope.modelcongviec = { IsTodo: false, Phongban_ID: $rootScope.login.u.Phongban_ID, Trangthai: 0, NgayBatDau: new Date(), Congty_ID: $rootScope.login.u.congtyID, STT: 1, IsDelete: false, Tiendo: 0, Uutien: false, IsDeadline: true, YeucauReview: true, DuanID: null, NhomCongviecID: null };
                $scope.giaoviecs = [u]; $scope.thuchiens = [u], $scope.theodois = [];
                $scope.FilesAttachTaskList = [];
                $scope.LisFileAttach = [];
                $("form.ng-dirty").removeClass("ng-dirty");
                $("#btncontinue").show();
                $("#ModalCongviecAdd").modal("show");
                initTD();
            }
        };
        $scope.BindListDuan = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListAllDuAnbyUser", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $rootScope.duans = data[0];
                    if ($scope.DuanID && !$scope.opition.TenDuan) {
                        var da = $rootScope.duans.find(x => x.DuanID === $scope.DuanID);
                        if (da)
                            $scope.opition.TenDuan = da.TenDuan;
                    }
                }
            });
        };
        //Công việc
        var objcv, ogvs = [], oths = [], otds = [];
        $scope.openModelEditCongviec = function (r) {
            $scope.dtitle = "Cập nhật công việc";
            $scope.congviec = r;
            $scope.modelcongviec = { ...r };
            $scope.FilesAttachTaskList = [];
            objcv = { ...r };
            if (!$scope.IsViewCV) {
                $scope.giaoviecs = []; $scope.thuchiens = [], $scope.theodois = [];
                $scope.LisFileAttach = [];
                $scope.LisFileAttachCV = [];
                $scope.getCongViec(true);
            }
            $scope.listNhomCV();
            $("form.ng-dirty").removeClass("ng-dirty");
            $("#btncontinue").hide();
            $("#ModalCongviecAdd").modal("show");
        };
        //Valid ngày công việc-----------------------------------------------*/
        $scope.endDateBeforeRender = endDateBeforeRender
        $scope.endDateOnSetTime = endDateOnSetTime
        $scope.startDateBeforeRender = startDateBeforeRender
        $scope.startDateOnSetTime = startDateOnSetTime

        function startDateOnSetTime() {
            $scope.$broadcast('start-date-changed');
        }

        function endDateOnSetTime() {
            $scope.$broadcast('end-date-changed');
        }

        function startDateBeforeRender($dates) {
            if ($scope.modelcongviec) {
                var bd = $scope.duan != null && $scope.duan.NgayBatDau != null ? moment($scope.duan.NgayBatDau) : null;
                var kt = $scope.duan != null && $scope.duan.NgayKetThuc != null ? moment($scope.duan.NgayKetThuc) : null;
                if ($scope.modelcongviec.NgayKetThuc) {
                    var activeDate = moment($scope.modelcongviec.NgayKetThuc);
                    $dates.filter(function (date) {
                        return date.localDateValue() >= activeDate.valueOf() || (kt != null && date.localDateValue() > kt.valueOf()) || (bd != null && date.localDateValue() < bd.valueOf())
                    }).forEach(function (date) {
                        date.selectable = false;
                    })
                } else {
                    $dates.filter(function (date) {
                        return (kt != null && date.localDateValue() > kt.valueOf()) || (bd != null && date.localDateValue() < bd.valueOf())
                    }).forEach(function (date) {
                        date.selectable = false;
                    })
                }
            }
        }

        function endDateBeforeRender($view, $dates) {
            if ($scope.modelcongviec) {
                var bd = $scope.duan != null && $scope.duan.NgayBatDau != null ? moment($scope.duan.NgayBatDau) : null;
                var kt = $scope.duan != null && $scope.duan.NgayKetThuc != null ? moment($scope.duan.NgayKetThuc) : null;
                if ($scope.modelcongviec.NgayBatDau) {
                    var activeDate = moment($scope.modelcongviec.NgayBatDau).subtract(1, $view).add(1, 'minute');
                    $dates.filter(function (date) {
                        return date.localDateValue() <= activeDate.valueOf() || (kt != null && date.localDateValue() > kt.valueOf()) || (bd != null && date.localDateValue() < bd.valueOf())
                    }).forEach(function (date) {
                        date.selectable = false;
                    })
                } else {
                    $dates.filter(function (date) {
                        return (kt != null && date.localDateValue() > kt.valueOf()) || (bd != null && date.localDateValue() < bd.valueOf())
                    }).forEach(function (date) {
                        date.selectable = false;
                    })
                }
            }
        }
        /**/
        function renderMessage() {
            var message = [];
            if (objcv == null) {
                return message;
            }
            if (objcv.CongviecTen != $scope.modelcongviec.CongviecTen) {
                message.push(" - Sửa tên công việc từ <soe>" + objcv.CongviecTen + "</soe> thành <soe>" + $scope.modelcongviec.CongviecTen + "</soe>");
            }
            if (objcv.Mota != $scope.modelcongviec.Mota) {
                message.push(" - Sửa mô tả công việc");
            }
            if (objcv.Muctieu != $scope.modelcongviec.Muctieu) {
                message.push(" - Sửa mục tiêu công việc");
            }
            if (objcv.Ketquadatduoc != $scope.modelcongviec.Ketquadatduoc) {
                message.push(" - Sửa kết quả công việc");
            }
            if (ogvs != null && ogvs != $scope.giaoviecs.map(x => { x.NhanSu_ID, x.fullName })) {
                message.push(" - Thay đổi người giao việc thành <soe>" + ($scope.giaoviecs.map(x => '<soe>' + x.fullName + '</soe>').join(",")) + "</soe>");
            }
            if (oths != null && oths != $scope.thuchiens.map(x => { x.NhanSu_ID, x.fullName })) {
                message.push(" - Thay đổi người thực hiện thành <soe>" + ($scope.thuchiens.map(x => '<soe>' + x.fullName + '</soe>').join(",")) + "</soe>");
            }
            if (otds != null && otds != $scope.theodois.map(x => { x.NhanSu_ID, x.fullName })) {
                message.push(" - Cập nhật thông tin người theo dõi : <soe>" + ($scope.theodois.map(x => '<soe>' + x.fullName + '</soe>').join(",")) + "</soe>");
            }
            return message;
        }
        $scope.AddCongviec = function (frm, fc) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            var message = renderMessage();
            $http({
                method: 'POST',
                url: "Duan/Update_Congviec",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    if ($scope.modelcongviec.NgayBatDau) {
                        $scope.modelcongviec.NgayBatDau = moment($scope.modelcongviec.NgayBatDau).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                    }
                    if ($scope.modelcongviec.NgayKetThuc) {
                        $scope.modelcongviec.NgayKetThuc = moment($scope.modelcongviec.NgayKetThuc).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                    }
                    formData.append("RequestMaster_ID", $ctrl.requestid);
                    formData.append("t", $rootScope.login.tk);
                    formData.append("models", JSON.stringify($scope.modelcongviec));
                    if (message.length > 0)
                        formData.append("message", message.join("<br/>"));
                    formData.append("giaoviecs", JSON.stringify($scope.giaoviecs));
                    formData.append("thuchiens", JSON.stringify($scope.thuchiens));
                    formData.append("theodois", JSON.stringify($scope.theodois));
                    $.each($("#frCongviec input[type='file']")[0].files, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                }
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm công việc mới !'
                    });
                    return false;
                }
                if ($scope.CongviecID) {
                    $scope.getCongViec();
                }
                $scope.modelcongviec.CongviecTen = "";
                if (!fc)
                    $("#ModalCongviecAdd").modal("hide");
                showtoastr('Đã cập nhật công việc thành công!.');
                $("#frCongviec input[type='file']").val("");
                $scope.goRFfunction();
                window.parent.postMessage({ Type: 1, Isdel: false }, "*");
                //resetForm(frm);
            }, function (response) { // optional
                //resetForm(frm);
                $scope.loadding = false;
            });
        };
        //File
        $scope.DeleteFiles = function (i, f) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa file này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_DuanFile",
                        data: { t: $rootScope.login.tk, ids: [f.FileID] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            if ($scope.LisFileAttach) {
                                var idx = $scope.LisFileAttach.findIndex(x => x.FileID === f.FileID);
                                if (idx !== -1)
                                    $scope.LisFileAttach.splice(idx, 1);
                            }
                            if ($scope.LisFileAttachCV) {
                                idx = $scope.LisFileAttachCV.findIndex(x => x.FileID === f.FileID);
                                if (idx !== -1)
                                    $scope.LisFileAttachCV.splice(idx, 1);
                            }
                            showtoastr("File bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa File không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.removeFiles = function (idx) {
            $scope.FilesAttachTaskList.splice(idx, 1);
        };
        //Tag User
        $scope.complete = function (string, m, $event, i) {
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            string = string.replace("@", "");
            var output = [];
            if (!string) {
                string = "";
            }
            if (m) {
                for (var k = 0; k <= 5; k++) {
                    m["Focus" + k] = false;
                }
                m["Focus" + i] = true;
            }
            angular.forEach($rootScope.users, function (u) {
                if ((u.fullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.enFullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.tenTruyCap || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.phone + '').toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    if (!m || m.NhanSu_ID !== u.NhanSu_ID)
                        output.push(u);
                }
            });
            $scope.filterUser = output;
        };
        $scope.handleKeyDown = function (m, $event, f) {
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                //$("ul.autoUser").focus();
                $event.preventDefault();
                if ($scope.focusedIndex !== $scope.filterUser.length - 1) {
                    $scope.focusedIndex++;
                }
            }
            else if (keyCode === 38) {
                // Up
                $event.preventDefault();
                if ($scope.focusedIndex !== 0) {
                    $scope.focusedIndex--;
                }
            }
            else if (keyCode === 13 && $scope.focusedIndex >= 0) {
                // Enter
                $event.preventDefault();
                $scope.clickAddUser(m, $scope.filterUser[$scope.focusedIndex], f);
            }
        };
        $scope.clickAddUser = function (m, us, u, i) {
            $scope.filterUser = null;
            if (!us || i != 2) {
                us = [];
            }
            if (i == "") {
                $scope.giaoviecs = [u];
            } else if (i == "1") {
                $scope.thuchiens = [u];
            } else {
                us.push(u);
            }
            m["searchU" + i] = "";
            $("#ModalCongviecAdd input.ipautoUser" + i + ".true").focus();
        };
        $scope.ClearFocus = function () {
            $("#ModalCongviecAdd .autoUser").hide();
        }
        $scope.removeUser = function (us, idx) {
            if (us[idx].CongviecThamgiaID) {
                $scope.Del_CongviecThamGia(us, idx);
            } else {
                us.splice(idx, 1);

            }
        };
        $scope.Del_CongviecThamGia = function (ds, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa user này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_CongviecThamGia",
                        data: { t: $rootScope.login.tk, ids: [ds[i].CongviecThamgiaID] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.goRFfunction();
                            window.parent.postMessage({ Type: 1, Isdel: false }, "*");
                            ds.splice(i, 1);
                            showtoastr("User bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa User không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.Del_Congviec = function (d, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa công việc này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_Congviec",
                        data: { t: $rootScope.login.tk, id: d.CongviecID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            window.parent.postMessage({ Type: 1, Isdel: true }, "*");
                            $scope.goRFfunction();
                            showtoastr("Bạn đã xóa công việc thành công!");
                            if ($scope.CongviecID != null) {
                                $scope.link = "congviec";
                                $state.go('congviec');
                            }
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa công việc không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.showusersModal = function (f, index) {
            $rootScope.phongbans.filter(r => r.isCheck).forEach(function (r) {
                r.isCheck = false;
            });
            $rootScope.users.filter(r => r.isCheck).forEach(function (r) {
                r.isCheck = false;
            });
            $scope.vbphongbans = $rootScope.phongbans;
            $scope.vbusers = $rootScope.users;
            $scope.one = index != 2;
            // set chọn user trong lấy list user config cho quy trình
            $scope.IndexListUser = index;
            $("#usersTaskModal").modal("show");
        };

        $scope.choiceUser = function (users) {
            if ($scope.IndexListUser === 0) {
                users = users.filter(u => $scope.giaoviecs.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                $scope.giaoviecs = [users[0]];
            } else if ($scope.IndexListUser === 1) {
                users = users.filter(u => $scope.thuchiens.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                $scope.thuchiens = [users[0]];
            } else {
                users = users.filter(u => $scope.theodois.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                $scope.theodois = $scope.theodois.concat(users);
            }
            $("#usersTaskModal").modal("hide");
        };
        //Nhom cong viec
        $scope.changeDA = function () {
            $scope.nhomcongviecs = [];
            $scope.modelcongviec.NhomCongviecID = null;
            $scope.listNhomCV($scope.modelcongviec.DuanID);
        };
        $scope.listNhomCV = function (id) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Duan_ListTudienNhomcongviec", pas: [
                        { "par": "DuanID", "va": id || $scope.DuanID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    var fn = { NhomCongviecID: null, Tennhom: "Chưa phân nhóm" };
                    data[0].push(fn);
                    $scope.nhomcongviecs = data[0];
                }
            });
        };
        $scope.DeleteNhomcongviec = function (cv, i) {
            if (cv.soCV > 0) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Nhóm đã có công việc không thể xóa!'
                });
                return;
            }
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa nhóm công việc này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_NhomCongViec",
                        data: { t: $rootScope.login.tk, id: cv.NhomCongviecID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.nhomcongviecs.splice(i, 1);
                            showtoastr("Nhóm công việc bạn chọn đã được xóa thành công!");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa nhóm không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.viewNhomCongviec = function () {
            $("#ModalNhomcongviecList").modal("show");
        };
        $scope.openModalAddNhomCV = function (ncv) {
            $("form.ModalNhomcongviecAdd.ng-dirty").removeClass("ng-dirty");
            if (!$scope.nhomcongviecs)
                $scope.nhomcongviecs = [];
            if (ncv != null) {
                $scope.nhomcongviec = { ...ncv };
            } else
                $scope.nhomcongviec = { Congty_ID: $rootScope.login.u.congtyID, STT: $scope.nhomcongviecs.length + 1, DuanID: $scope.congviec != null ? $scope.congviec.DuanID : $scope.DuanID, Hienthi: true };
            $("#ModalNhomcongviecAdd").modal("show");
        };
        $scope.AddNhomcongviec = function (frm) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url:"Duan/Update_NhomCongViec",
                data: { t: $rootScope.login.tk, model: $scope.nhomcongviec },
                headers: {
                    contentType: 'application/json; charset=utf-8'
                },
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm nhóm công việc !'
                    });
                    return false;
                }
                $("#ModalNhomcongviecAdd").modal("hide");
                $scope.listNhomCV($scope.congviec != null ? $scope.congviec.DuanID : $scope.DuanID);
                //resetForm(frm);
            }, function (response) { // optional
                //resetForm(frm);
                $scope.loadding = false;
            });
        };
        //UploadFile
        $scope.openAttachfile = function (ev) {
            $(ev.target).closest("div.form-group").find("input[type='file']").trigger("click");
        };
        $scope.UploadFiles = function (f) {
            if (!$scope.LisFileAttach) $scope.LisFileAttach = [];
            if (!$scope.FilesAttachTaskList) $scope.FilesAttachTaskList = [];
            if ($scope.LisFileAttach.length + $scope.FilesAttachTaskList.length + f.length > 10) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 10 file cho dự án!'
                });
                return false;
            }
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 100 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    $scope.FilesAttachTaskList.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 100MB!'
                });
            }
        };
        $scope.initOne = function () {
            $scope.DuanID = null;
            $scope.BindListDuan();
            $scope.listNhomCV();
        };
        function initTD () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Tudien", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.tudien = data;
                    $scope.TrangthaiCV = data[3];
                    $scope.initOne();
                    $scope.CVTTS = $scope.TrangthaiCV.filter(x => x.TrangthaiID < 4 && x.TrangthaiID >= 0);
                }
            });
        };
    }
});
os.component('viewtaskcomponent', {
    bindings: {
        congviecid: '<',
        uid: '<',
        htitle: '=',
        rfFunction: '&'
    },
    templateUrl: domainUrl + 'App/Component/Task/ViewTask.html?v=' + vhtmlcache,
    controller: function ($scope, $rootScope, $http, $sce, $timeout) {
        var $ctrl = this;
        $scope.goRFfunction = function () {
            $ctrl.rfFunction();
        };
        this.$onChanges = () => {
            if (this.congviecid) {
                $scope.congviec = { CongviecID: this.congviecid };
                $scope.initCongviec();
            }
        }; 
        $scope.App_CongviecNotify = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "App_CongviecNotify", pas: [
                        { "par": "CongviecID ", "va": $scope.congviec.CongviecID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.countCV = data[0][0];
                }
            });
        };
        $scope.listNhomCV = function (id) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Duan_ListTudienNhomcongviec", pas: [
                        { "par": "DuanID", "va": id || $scope.DuanID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    var fn = { NhomCongviecID: null, Tennhom: "Chưa phân nhóm" };
                    data[0].push(fn);
                    $scope.nhomcongviecs = data[0];
                }
            });
        };
        //Chart
        var ColorChats = [
            {
                "color": "#04D215"
            },
            {
                "color": "#B0DE09"
            }, {
                "color": "#0D52D1"
            },
            {
                "color": "#8A0CCF"
            },
            {
                "color": "#F8FF01"
            },
            {
                "color": "#FCD202"
            },
            {
                "color": "#FF9E01"
            },
            {
                "color": "#FF6600"
            },

            {
                "color": "#FF0F00"
            }, {
                "color": "#CD0D74"
            }, {
                "color": "#754DEB"
            }, {
                "color": "#FFCCFF"
            }, {
                "color": "#FF99CC"
            }, {
                "color": "#FF6699"
            }, {
                "color": "#FF3366"
            }, {
                "color": "#FF3333"
            }, {
                "color": "#990066"
            }, {
                "color": "#660066"
            }, {
                "color": "#660033"
            }, {
                "color": "#660000"
            }, {
                "color": "#DDDDDD"
            }, {
                "color": "#999999"
            }, {
                "color": "#333333"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }
        ];

        $scope.listCharts = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Congviec_Charts", pas: [
                        { "par": "CongviecID", "va": $scope.CongviecID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    $scope.ThongKeChung = JSON.parse(res.data.data)[0][0];
                }
            });
        };
        $scope.listBaocaoCongviec = function (id, f) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListBaocaoCongViec", pas: [
                        { "par": "CongviecID ", "va": id }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    data.forEach(function (r) {
                        if (r.files)
                            r.files = JSON.parse(r.files);
                        if (r.reviews)
                            r.reviews = JSON.parse(r.reviews);
                        if (r.reviews) {
                            r.reviews.forEach(function (v) {
                                if (v.files) {
                                    v.files = JSON.parse(v.files);
                                }
                                v.color = renderColor(v.Tiendo);
                                if (v.NgayReview)
                                    v.NgayReview = (new Date(v.NgayReview));
                                //v.NgayReview = parseJsonDate(v.NgayReview);
                            })
                        }
                        r.color = renderColor(parseInt(r.Tiendo));
                    });
                    $scope.Baocaos = data;
                    $scope.congviec.IsBC = true;// data.length == 0 || data.filter(x => x.Trangthai == 0).length == 0;
                    if (f) {
                        $scope.goChartLine();
                    }
                }
            });
        };
        $scope.goChartLine = function () {
            var chartData = generatechartData();

            function generatechartData() {
                var chartData = [];
                var firstDate = new Date($scope.congviec.NgayBatDau);
                var start = moment($scope.congviec.NgayBatDau);
                var end = moment($scope.Baocaos[$scope.Baocaos.length - 1].NgayBaocao);
                var day = end.diff(start, "days")
                for (var i = 0; i <= day; i++) {
                    // we create date objects here. In your data, you can have date strings
                    // and then set format of your dates using chart.dataDateFormat property,
                    // however when possible, use date objects, as this will speed up chart rendering.
                    var newDate = new Date(firstDate);
                    newDate.setDate(newDate.getDate() + i);
                    var tiendo = 0;
                    var o = $scope.Baocaos.find(x => compareDate(x.NgayBaocao, newDate))
                    if (o != null) {
                        tiendo = o.Tiendo;
                        if (o.reviews && o.reviews.length > 0 && o.reviews[0].IsType && o.reviews[0].Tiendo && o.reviews[0].Tiendo > 0) {
                            tiendo = o.reviews[0].Tiendo;
                        }
                    } else if (chartData.length > 0) {
                        tiendo = chartData[chartData.length - 1].tiendo;
                    }
                    chartData.push({
                        date: newDate,
                        dateString: moment(newDate).format("DD/MM"),
                        tiendo: tiendo
                    });
                }
                return chartData;
            }


            var chart = AmCharts.makeChart("chartdivcv", {
                "theme": "none",
                "type": "serial",
                "marginRight": 80,
                "autoMarginOffset": 20,
                "marginTop": 20,
                "dataProvider": chartData,
                "valueAxes": [{
                    "id": "v1",
                    "axisAlpha": 0.1
                }],
                "graphs": [{
                    "useNegativeColorIfDown": true,
                    "balloonText": "[[category]]<br><b>value: [[value]]</b>",
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletBorderColor": "#FFFFFF",
                    "hideBulletsCount": 50,
                    "lineThickness": 2,
                    "lineColor": "#fdd400",
                    "negativeLineColor": "#67b7dc",
                    "valueField": "tiendo"
                }],
                "chartScrollbar": {
                    "scrollbarHeight": 5,
                    "backgroundAlpha": 0.1,
                    "backgroundColor": "#868686",
                    "selectedBackgroundColor": "#67b7dc",
                    "selectedBackgroundAlpha": 1
                },
                "chartCursor": {
                    "valueLineEnabled": true,
                    "valueLineBalloonEnabled": true
                },
                "categoryField": "date",
                "categoryAxis": {
                    "parseDates": true,
                    "axisAlpha": 0,
                    "minHorizontalGap": 60
                },
                "export": {
                    "enabled": true
                }
            });

            chart.addListener("dataUpdated", zoomChart);
            //zoomChart();

            function zoomChart() {
                if (chart.zoomToIndexes) {
                    chart.zoomToIndexes(130, chartData.length - 1);
                }
            }
        };
        $scope.setColor = function (co) {
            $scope.ipcolor = co;
        };
        //Hoạt động
        $scope.opitionhd = {
            numPerPage: 20,
            currentPage: 1,
            sort: 2,
            Trangthai: -1,
            tenSort: "STT",
            tenTT: "Tất cả",
            IsType: -1,
            Group: 1
        };
        $scope.setPagelog = function () {
            $scope.opitionhd.currentPage += 1;
            $scope.listDAlog(true);
        };
        $scope.listDAlog = function (f) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListDuAnLog", pas: [
                        { "par": "DuanID", "va": $scope.DuanID },
                        { "par": "CongviecID", "va": $scope.CongviecID != null ? $scope.CongviecID : null },
                        { "par": "NhanSu_ID", "va": $scope.NhanSu_ID || null },
                        { "par": "p", "va": $scope.opitionhd.currentPage },
                        { "par": "pz", "va": $scope.opitionhd.numPerPage },
                        { "par": "IsType", "va": null }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    data[0].forEach(function (r) {
                        r.NgayTao = new Date(r.NgayTao);
                        r.DaysName = weekday[r.NgayTao.getDay()];
                    });
                    if (f) {
                        $scope.logs = $scope.logs.concat(data[0]);
                    } else {
                        $scope.logs = data[0];
                    }
                    if (!$scope.opitionhd.noOfPages) {
                        $scope.opitionhd.noOfPages = Math.ceil(data[1][0].c / $scope.opitionhd.numPerPage);
                    }
                }
            });
        };
        $scope.listCongviecVanban = function (cv) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListCongviecVanban", pas: [
                        { "par": "CongviecID ", "va": cv.CongviecID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    data.forEach(function (r) {
                        if (r.files) {
                            r.files = JSON.parse(r.files);
                        }
                    });
                    $scope.vanbans = data;
                }
            });
        };
        var idtab = null;
        $scope.listSubTask = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListSubTask", pas: [
                        { "par": "CongviecID", "va": $scope.CongviecID != null ? $scope.CongviecID : null },
                        { "par": "NhanSu_ID", "va": $scope.NhanSu_ID || null }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    if (!data) data = [];
                    data.forEach(function (r) {
                        r.Thanhviens = JSON.parse(r.Thanhviens);
                        r.color = renderColor(r.Tiendo);
                        r.txtcolor = renderTxtColor(r.Tiendo);
                        r.NgayHan = Math.abs(r.NgayHan);
                        r.Tao = r.NguoiTao === $rootScope.login.u.NhanSu_ID;
                        if (r.Thanhviens) {
                            r.IsQL = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "2").length > 0;
                            r.IsTH = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "1").length > 0;
                            r.IsTD = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "0").length > 0;
                            r.IsType = r.IsQL ? 2 : (r.IsTH ? 1 : 0);
                        }
                    })
                    $scope.subcongviecs = data;
                }
            });
        };
        $scope.ListTagsCongviec = function (cv) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListTagsKeyDuanCV", pas: [
                        { "par": "DuanID ", "va": cv.DuanID },
                        { "par": "CongviecID ", "va": cv.CongviecID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    $scope.Tagcvs = data;
                }
            });
        };
        $scope.getCongViec = function (f) {
            $scope.LisFileAttachCV = [];
            $scope.checklists = [];
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ChiTietCongViecNhanSu", pas: [
                        { "par": "CongviecID ", "va": $scope.congviec.CongviecID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "Congty_ID ", "va": $rootScope.login.u.congtyID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $("#viewTask .InfoViewRequest #loader").hide();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.giaoviecs = data[1].filter(x => x.IsType === 2);
                    $scope.thuchiens = data[1].filter(x => x.IsType === 1);
                    $scope.theodois = data[1].filter(x => x.IsType === 0);
                    ogvs = $scope.giaoviecs.map(x => { x.NhanSu_ID, x.fullName });
                    oths = $scope.thuchiens.map(x => { x.NhanSu_ID, x.fullName });
                    otds = $scope.theodois.map(x => { x.NhanSu_ID, x.fullName });
                    if (data[0].length > 0) {
                        data[0][0].IsQL = data[1].filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === 2).length > 0;
                        data[0][0].IsTH = data[1].filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === 1).length > 0;
                        data[0][0].IsTD = data[1].filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === 0).length > 0;
                        data[0][0].IsType = data[0][0].IsQL ? 2 : (data[0][0].IsTH ? 1 : 0);
                        data[0][0].Tao = data[0][0].NguoiTao === $rootScope.login.u.NhanSu_ID;
                    }
                    var arr = [];
                    data[1].forEach(function (u) {
                        if (arr.filter(x => x.NhanSu_ID === u.NhanSu_ID).length === 0) {
                            arr.push(u);
                        }
                    });
                    $scope.thanhviens = arr;
                    $scope.LisFileAttachCV = data[2];
                    data[3].forEach(function (r) {
                        r.stisks = JSON.parse(r.stisks);
                        if (r.files)
                            r.files = JSON.parse(r.files);
                    });
                    $scope.Comments = data[3];
                    $scope.Comments.filter(x => x.ParentID != null).forEach(function (r) {
                        r.Comments = $scope.Comments.filter(x => x.ParentID === r.CommentID);
                        r.ParentComment = $scope.Comments.find(x => r.ParentID === x.CommentID);
                    });
                    if (data[0].length > 0) {
                        data[0][0].color = renderColor(data[0][0].Tiendo);
                        $scope.congviec = data[0][0];
                        //if (todos.length === 0) {
                        data[5].forEach(function (r, i) {
                            r.Thuchiens = JSON.parse(r.Thuchiens);
                            if (!r.TenNhomDuan)
                                r.TenNhomDuan = "Chưa phân nhóm";
                            if ($scope.ListTrangThai) {
                                var o = $scope.ListTrangThai.find(x => x.id == r.Trangthai);
                                if (o)
                                    r.TenTT = o.text;
                            }
                            r.color = renderColor(r.tiendo);
                        });
                        todos = data[5];
                        //}
                        data[4].forEach(function (r) {
                            r.tasks = todos.filter(x => x.ChecklistID === r.ChecklistID);
                        });
                        $scope.checklists = data[4];
                        var duid = $scope.congviec.DuanID && $scope.congviec.DuanID != 'null' ? $scope.congviec.DuanID : null;
                        if (data[6].length === 0) {
                            $scope.cauhinhTao = {
                                DuanID: $scope.DuanID,
                                CongviecID: $scope.CongviecID,
                                Congty_ID: $rootScope.login.u.congtyID,
                                CapnhatTT: true,//Cập nhật trạng thái công việc
                                CommentCV: true,//Bình luận công việc
                                IsFullCV: true,
                                IsSubTask: true,
                                IsCopy: true,
                                EditCV: true,//Chỉnh sửa công việc
                                AddQL: true,//Thêm thành viên quản lý
                                AddTH: true,//Thêm thành viên thực hiện
                                AddTD: true,//Thêm thành viên theo dõi
                                AddSetting: true,//Truy cập setting
                                AddThumuc: true,//Thêm thư mục
                                AddTailieu: true,//Thêm tài liệu
                                ViewReport: true,//Truy cập báo cáo
                                ViewTiendo: true,//Xem báo cáo công việc
                                AddTiendo: true,//Cập nhật tiến độ công việc
                                AddReview: true,//Được review công việc
                                AddDanhgia: true,//Được đánh giá công việc
                                DelCV: true,//Xóa công việc
                                IsType: 3,
                                IsTV: true
                            };
                            $scope.cauhinhQL = {
                                DuanID: duid,
                                CongviecID: $scope.congviec.CongviecID,
                                Congty_ID: $rootScope.login.u.congtyID,
                                CapnhatTT: true,//Cập nhật trạng thái công việc
                                CommentCV: true,//Bình luận công việc
                                IsFullCV: true,
                                IsSubTask: true,
                                IsCopy: true,
                                EditCV: true,//Chỉnh sửa công việc
                                AddQL: true,//Thêm thành viên quản lý
                                AddTH: true,//Thêm thành viên thực hiện
                                AddTD: true,//Thêm thành viên theo dõi
                                AddSetting: true,//Truy cập setting
                                AddThumuc: true,//Thêm thư mục
                                AddTailieu: true,//Thêm tài liệu
                                ViewReport: true,//Truy cập báo cáo
                                ViewTiendo: true,//Xem báo cáo công việc
                                AddTiendo: true,//Cập nhật tiến độ công việc
                                AddReview: true,//Được review công việc
                                AddDanhgia: true,//Được đánh giá công việc
                                DelCV: true,//Xóa công việc
                                IsType: 2,
                                IsTV: true
                            };
                            $scope.cauhinhTH = {
                                DuanID: duid,
                                CongviecID: $scope.congviec.CongviecID,
                                Congty_ID: $rootScope.login.u.congtyID,
                                CapnhatTT: false,//Cập nhật trạng thái công việc
                                CommentCV: true,//Bình luận công việc
                                EditCV: true,//Chỉnh sửa công việc
                                IsFullCV: true,
                                IsSubTask: true,
                                IsCopy: true,
                                AddQL: false,//Thêm thành viên quản lý
                                AddTH: true,//Thêm thành viên thực hiện
                                AddTD: true,//Thêm thành viên theo dõi
                                AddSetting: false,//Truy cập setting
                                AddThumuc: true,//Thêm thư mục
                                AddTailieu: true,//Thêm tài liệu
                                ViewReport: true,//Truy cập báo cáo
                                ViewTiendo: true,//Xem báo cáo công việc
                                AddTiendo: true,//Cập nhật tiến độ công việc
                                AddReview: false,//Được review công việc
                                AddDanhgia: true,//Được đánh giá công việc
                                DelCV: false,//Xóa công việc
                                IsType: 1,
                                IsTV: false
                            };
                            $scope.cauhinhTD = {
                                DuanID: duid,
                                CongviecID: $scope.congviec.CongviecID,
                                Congty_ID: $rootScope.login.u.congtyID,
                                CapnhatTT: false,//Cập nhật trạng thái công việc
                                CommentCV: true,//Bình luận công việc
                                IsFullCV: false,
                                IsSubTask: false,
                                IsCopy: true,
                                EditCV: false,//Chỉnh sửa công việc
                                AddQL: false,//Thêm thành viên quản lý
                                AddTH: false,//Thêm thành viên thực hiện
                                AddTD: true,//Thêm thành viên theo dõi
                                AddSetting: false,//Truy cập setting
                                AddThumuc: true,//Thêm thư mục
                                AddTailieu: true,//Thêm tài liệu
                                ViewReport: true,//Truy cập báo cáo
                                ViewTiendo: true,//Xem báo cáo công việc
                                AddTiendo: false,//Cập nhật tiến độ công việc
                                AddReview: false,//Được review công việc
                                AddDanhgia: true,//Được đánh giá công việc
                                DelCV: false,//Xóa công việc
                                IsType: 0,
                                IsTV: false
                            };
                        } else {
                            $scope.cauhinhQL = data[6].find(x => x.IsType === 2);
                            $scope.cauhinhTH = data[6].find(x => x.IsType === 1);
                            $scope.cauhinhTD = data[6].find(x => x.IsType === 0);
                            $scope.cauhinhTao = data[6].find(x => x.IsType === 3);
                            if (!$scope.cauhinhQL.CongviecID) {
                                $scope.cauhinhTao.CongviecID = $scope.congviec.CongviecID;
                                $scope.cauhinhTao.DuanID = duid;
                                $scope.cauhinhQL.CongviecID = $scope.congviec.CongviecID;
                                $scope.cauhinhQL.DuanID = duid;
                                $scope.cauhinhTH.CongviecID = $scope.congviec.CongviecID;
                                $scope.cauhinhTH.DuanID = duid;
                                $scope.cauhinhTD.CongviecID = $scope.congviec.CongviecID;
                                $scope.cauhinhTD.DuanID = duid;
                            }
                        }
                        if (data[7] && data[7].length > 0 && data[7][0].CauhinhCVID != null) {
                            $scope.Quyen = data[7][0];
                        } else if (data[0][0].IsQL || (data[0][0].Tao && !data[0][0].IsTH)) {
                            $scope.Quyen = $scope.cauhinhQL;
                        } else if (data[0][0].IsTH) {
                            $scope.Quyen = $scope.cauhinhTH;
                        } else {
                            $scope.Quyen = $scope.cauhinhTD;
                        }
                        //if (data[0][0].Tao && data[0][0].IsTH && !data[0][0].IsQL) {
                        //    $scope.Quyen.CapnhatTT = false;
                        //}
                        //$scope.Quyen.IsBlockQuyen = data[0][0].Tao && data[0][0].IsTH && !data[0][0].IsQL;
                        $scope.Quyen.IsBlockQuyen = !$scope.Quyen.IsFullCV;//(data[0][0].Tao && data[0][0].IsTH && !data[0][0].IsQL)||!data[0][0].IsQL;
                        $scope.Quyen.IsTV = $scope.Quyen.AddQL && $scope.Quyen.AddTH && $scope.Quyen.AddTD;
                        $scope.Quyen.FunCV = $scope.Quyen.EditCV && $scope.congviec.Trangthai !== 4 && $scope.congviec.Trangthai !== 7;
                        console.log($scope.Quyen);
                        $rootScope.TenDuanCV = $scope.congviec.TenDuan;
                        //$rootScope.TenDuan = $scope.congviec.CongviecTen;
                        $rootScope.did = $scope.congviec.DuanID;
                        var tt = $scope.congviec.Trangthai;
                        $scope.congviec.isclosett = tt == 0 || tt == 2 || tt == 3 || tt == 4 || tt == 7;
                        $scope.congviec.isclose = ($scope.congviec.IsQL != true && $scope.congviec.IsTH != true) || (tt == 0 || tt == 2 || tt == 3 || tt == 4 || tt == 7);
                        $scope.isdelcomment = !$scope.congviec.isclose && ($scope.congviec.IsQL == true || $scope.congviec.IsTH == true);
                        //List Tags
                        $scope.ListTagsCongviec($scope.congviec);
                        $scope.listCongviecVanban($scope.congviec);
                        $scope.listSubTask();
                        if (f) {
                            $scope.modelcongviec = $scope.congviec;
                        }
                        if (idtab) {
                            setTimeout(function () {
                                //$('a[data-target="#' + idtab + '"]').tab('show');
                                $('a[data-target="#' + idtab + '"]').trigger("click");
                                if (idtab == "giahan") {
                                    $scope.goGiahan($scope.congviec);
                                }
                                idtab = null;
                            }, 0)
                        }
                    }
                }
                //
                $timeout(function () {
                    $scope.App_CongviecNotify();
                    $scope.listNhomCV();
                    $scope.listCharts(); 
                }, 500);    
            });
        };
        $scope.initCongviec = function () {
            $("#viewTask .InfoViewRequest").addClass('show');
            $("#infocv-overlay").addClass('show');
            $scope.getCongViec();
        };
        $scope.closeInfoRequest = function () {
            $("#viewTask .InfoViewRequest").removeClass('show');
            $("#infocv-overlay").removeClass('show');
        };
        $scope.fullInfoRequest = function () {
            $("#viewTask .InfoViewRequest").toggleClass("fulliframe");
        };
    }
});