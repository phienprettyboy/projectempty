﻿os.component('user', {
    bindings: {
        vu: '<',
        vp: '<',
        all: '<',
        ct: '<',//công ty
        ctch: '<',//công ty cha
        ctc: '<',//công ty con
        hTitle: '@',
        mTitle: '@',
        choiceUser: '&',
        one: '<',
        objfilter: '<'
    },
    templateUrl: domainUrl + '/App/Temp/User.html?v=' + vhtmlcache,
    controller: function ($rootScope) {
        var $ctr = this;
        this.$onChanges = function (a) {
            $ctr.uAll = false;
            $ctr.Search = null;
            if (!$ctr.mTitle) $ctr.mTitle = "usersModal";
            this.totalDisplayed = 2;
            this.reset();
        };
        this.uAll = false;
        this.goFilter = function (m, f) {
            this.totalDisplayed = 2;
            $ctr.uAll = false;
            var us = this.vu;
            if (this.Search) {
                var s = this.Search.toLowerCase();
                var sk = change_alias(this.Search.toLowerCase());
                
                us = us.filter(u =>
                    u.fullName.toLowerCase().indexOf(s) !== -1
                    || (u.enFullName + "").toLowerCase().indexOf(s) !== -1
                    || u.fullName.toLowerCase().indexOf(sk) !== -1
                    || (u.enFullName + "").toLowerCase().indexOf(sk) !== -1
                    || u.maNhanSu.toLowerCase().indexOf(s) !== -1
                    || u.maNhanSu.toLowerCase().indexOf(sk) !== -1
                    || u.tenTruyCap.toLowerCase().indexOf(s) !== -1
                    || u.tenTruyCap.toLowerCase().indexOf(sk) !== -1
                );
            }
            var rs = this.roles.filter(r => r.CongTy_ID === $rootScope.congty[0].Congty_ID && r.isCheck);
            if (rs.length > 0) {
                us = us.filter(u => rs.find(r => u.roles !== null && u.roles.indexOf(r.Role_ID) > -1));
            }
            if (m && !f) {
                if (m.isCheck === true) {
                    m.isCheck = false;
                } else {
                    this.phongbans.filter(p => p.isCheck).forEach(function (r) {
                        r.isCheck = false;
                    });
                    m.isCheck = true;
                }
            } else if (m && f) {
                this.phongbans.filter(p => p.isClick).forEach(function (r) {
                    r.isClick = false;
                });
                m.users.forEach(function (u) {
                    u.isCheck = m.isCheck;
                });
            }
            var ps = this.phongbans.filter(p => p.isCheck);
            if (ps.length > 0) {
                this.phongbansUS = ps;
            } else {
                this.phongbansUS = this.phongbans;
            }
            this.phongbansUS.forEach(function (p) {
                p.users = us.filter(u => u.phongbans !== null && u.phongbans.indexOf(p.Phongban_ID) !== -1);
                p.IsShow = p.users.length > 0;
            });
            console.log(this.phongbansUS.filter(x => x.IsShow));
        };
        //Load More
        this.totalDisplayed = 2;
        this.loadMore = function () {
            $ctr.totalDisplayed += 2;
        };
        this.SelectModel = function (m) {
            $ctr.uAll = false;
            var us = this.vu;
            if (this.Search) {
                var s = this.Search.toLowerCase();
                var sk = change_alias(this.Search.toLowerCase());
                us = us.filter(u =>
                    u.fullName.toLowerCase().indexOf(s) !== -1
                    || (u.enFullName + "").toLowerCase().indexOf(s) !== -1
                    || u.fullName.toLowerCase().indexOf(sk) !== -1
                    || (u.enFullName + "").toLowerCase().indexOf(sk) !== -1
                );
            }
            if (m.isClick === true) {
                m.isClick = false;
            } else {
                this.phongbans.filter(p => p.isClick).forEach(function (r) {
                    r.isClick = false;
                });
                m.isClick = true;
            }
            var rs = this.roles.filter(r => r.CongTy_ID === $rootScope.congty[0].Congty_ID && r.isCheck);
            if (rs.length > 0) {
                us = us.filter(u => rs.find(r => u.roles !== null && u.roles.indexOf(r.Role_ID) > -1));
            }
            var ps = this.phongbans.filter(p => p.isClick);
            if (ps.length > 0) {
                this.phongbansUS = ps;
            } else {
                this.phongbansUS = this.phongbans;
            }
            this.phongbansUS.forEach(function (p) {
                p.users = us.filter(u => u.phongbans !== null && u.phongbans.indexOf(p.Phongban_ID) !== -1 && u.congtyID == m.Congty_ID);
            });
        };
        this.goCtyFilter = function (c) {
            this.totalDisplayed = 2;
            if (!this.phongbans) return;
            $ctr.uAll = false;
            this.phongbans.forEach(function (r) {
                r.isCheck = false;
            });
            this.childcongtys.forEach(function (r) {
                r.isCheck = false;
            });
            $ctr.congty.isCheck = false;
            c.isCheck = true;
            var us = this.vu;
            if (this.Search) {
                var s = this.Search.toLowerCase();
                var sk = change_alias(this.Search.toLowerCase());
                us = us.filter(u =>
                    u.fullName.toLowerCase().indexOf(s) !== -1
                    || (u.enFullName + "").toLowerCase().indexOf(s) !== -1
                    || u.fullName.toLowerCase().indexOf(sk) !== -1
                    || (u.enFullName + "").toLowerCase().indexOf(sk) !== -1
                );
            }
            var rs = this.roles.filter(r => r.CongTy_ID === $rootScope.congty[0].Congty_ID && r.isCheck);
            if (rs.length > 0) {
                us = us.filter(u => rs.find(r => u.roles !== null && u.roles.indexOf(r.Role_ID) > -1));
            }
            this.phongbansUS = this.phongbans.filter(p => p.Congty_ID === c.Congty_ID);
            this.phongbansUS.forEach(function (p) {
                p.users = us.filter(u => u.phongbans !== null && u.phongbans.indexOf(p.Phongban_ID) !== -1);
                p.IsShow = p.users.length > 0;
            });
        };
        this.refershModel = function () {
            this.totalDisplayed = 2;
            $ctr.uAll = false;
            var pbs = this.vp;
            var us = this.vu;
            this.Search = null;
            if (pbs) {
                pbs.forEach(function (p) {
                    p.users = us.filter(u => u.phongbans !== null && u.phongbans.indexOf(p.Phongban_ID) !== -1);
                    p.IsShow = p.users.length > 0;
                });
                this.phongbans = pbs;
                this.phongbansUS = pbs;
            }
            this.roles.forEach(function (r) {
                r.isCheck = false;
            });
        };
        this.checkUFilter = function (item) {
            return item.isCheck;
        };
        this.toogleModel = function (m) {
            $ctr.uAll = false;
            if (m.close !== true) {
                m.close = true;
            } else {
                m.close = false;
            }
        };
        this.checkAllUST = function () {
            var check = this.uAll;
            this.phongbansUS.forEach(function (p) {
                p.users.forEach(function (u) {
                    u.isCheck = check;
                });
            });
        };
        this.ChoiceUser = function () {
            var us = this.vu.filter(u => u.isCheck);
            this.reset();
            this.choiceUser({ us: us });
        };
        this.reset = function () {
            $ctr.uAll = false;
            $ctr.Search = null;
            var pbs = $ctr.vp;
            if (pbs && $ctr.vu) {
                $ctr.vu.forEach(function (u) {
                    u.isCheck = false;
                });
                if ($rootScope.ctyroles) {
                    $rootScope.ctyroles.forEach(function (u) {
                        u.isCheck = false;
                    });
                }
                if ($ctr.vu) {
                    pbs.forEach(function (p) {
                        p.isCheck = false;
                        if ($ctr.vu) {
                            p.users = $ctr.vu.filter(u => u.phongbans && u.phongbans.indexOf(p.Phongban_ID) !== -1);
                            p.IsShow = p.users && p.users.length > 0;
                        }
                    });
                    //pbs = pbs.filter(p=>p.users.length>0);
                } else {
                    pbs.forEach(function (p) {
                        p.isCheck = false;
                        p.IsShow = p.users && p.users.length > 0;
                    });
                }
                $ctr.congty = $rootScope.congty[0];
                $ctr.childcongtys = $rootScope.childcongtys;
                if (this.ct) {
                    if (!this.ctch) {
                        if ($rootScope.me.congtyID !== $rootScope.congty[0].Congty_ID) {
                            $ctr.congty = $rootScope.childcongtys.find(x => x.Congty_ID === $rootScope.me.congtyID);
                        }
                    }
                    if (!this.ctc) {
                        $ctr.childcongtys = [];
                    }
                }
                if (!pbs || pbs.length === 0) {
                    var pbss = $rootScope.phongbans.find(x => x.Phongban_ID === $rootScope.login.u.Phongban_ID);
                    if (pbss) {
                        var cty = $rootScope.phongbans.find(x => x.Phongban_ID === pbss.IDCha.split(",")[0]);
                        pbs = $rootScope.phongbans.filter(x => cty.IDCon.indexOf(x.Phongban_ID) > -1);
                        pbs.push(cty);
                    }
                    $ctr.childcongtys = [];
                }
                if (this.all) {
                    $ctr.congty = $rootScope.childcongtys.find(x => x.Congty_ID === $rootScope.congty[0].Parent_ID);
                }
                if ($ctr.objfilter !== null && $ctr.objfilter !== undefined) {
                    if ($ctr.objfilter.donvi === true) {//Gửi nội bộ
                        cty = $rootScope.childcongtys.find(x => x.Congty_ID === $rootScope.me.congtyID);
                        if (cty) {
                            $ctr.congty = cty;
                        }
                        $ctr.childcongtys = [];
                        pbs = pbs.filter(x => x.Congty_ID === $rootScope.me.congtyID);
                    } else if ($ctr.objfilter.donvi === false) {//Gửi ra ngoài
                        $ctr.childcongtys = $rootScope.childcongtys.filter(x => x.Congty_ID !== $rootScope.me.congtyID);
                        pbs = pbs.filter(x => $ctr.childcongtys.filter(a => a.Congty_ID === x.Congty_ID).length > 0);
                    }
                }
                $ctr.phongbans = pbs;
                $ctr.phongbansUS = pbs;
                $ctr.roles = $rootScope.ctyroles;
            }
        };
        this.checkU = function (u) {
            var us = this.vu.filter(m => m.NhanSu_ID !== u.NhanSu_ID);
            us.forEach(function (n) {
                n.isCheck = false;
            });
        };
    }
});