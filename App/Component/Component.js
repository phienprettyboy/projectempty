﻿function importScripts(jsFilePath) {
    var js = document.createElement("script");
    js.type = "text/javascript";
    js.src = jsFilePath;
    angular.element(document.body).append(js);
}
//Stask
importScripts(baseUrl + '/App/Component/RequestComponent.js');
importScripts(baseUrl + '/App/Component/Task/AddTaskComponent.js');
importScripts(baseUrl + '/App/Component/Task/ViewTaskComponent.js');
//SDoc
importScripts(baseUrl + '/App/Component/Doc/ViewDocComponent.js');
//SCalendar
importScripts(baseUrl + '/App/Component/Calendar/AddCalendarComponent.js');
importScripts(baseUrl + '/App/Component/Calendar/ViewCalendarComponent.js');
//UserTag
importScripts(baseUrl + '/App/Component/UserTag/UserTagComponent.js');