﻿os.component('viewcalendarcomponent', {
    bindings: {
        calendarid: '<',
        uid: '<',
        isopencalendar: '<',
        htitle: '=',
        rfFunction: '&'
    },
    templateUrl: baseUrl + 'App/Component/Calendar/ViewCalendar.html?v=' + vhtmlcache,
    controller: function ($scope, $rootScope, $http, $sce, $timeout) {
        var $ctrl = this;
        $scope.goRFfunction = function () {
            $ctrl.rfFunction();
        };
        //------------------Khởi tạo-----------------------
        function getDateByWeek(weeks, year) {
            var d = new Date(year, 0, 1);
            var dayNum = d.getDay();
            var requiredDate = --weeks * 7;
            // If 1 Jan is Friday to Sunday, go to next week 
            if (((dayNum != 0) || dayNum > 4)) {
                requiredDate += 7;
            }
            // Add required number of days
            d.setDate(1 - d.getDay() + ++requiredDate);
            return d;
        }
        function getWeekNumber(d) {
            // Copy date so don't modify original
            d = new Date(Date.UTC(d.getFullYear(), d.getMonth(), d.getDate()));
            // Set to nearest Thursday: current date + 4 - current day number
            // Make Sunday's day number 7
            d.setUTCDate(d.getUTCDate() + 4 - (d.getUTCDay() || 7));
            // Get first day of year
            var yearStart = new Date(Date.UTC(d.getUTCFullYear(), 0, 1));
            // Calculate full weeks to nearest Thursday
            var weekNo = Math.ceil((((d - yearStart) / 86400000) + 1) / 7);
            // Return array of year and week number
            return [d.getUTCFullYear(), weekNo];
        }
        function weeksInYear(year) {
            var Tuans = [];
            var TuansLock = [];
            var max = Math.max(
                moment(new Date(year, 11, 31)).isoWeek()
                , moment(new Date(year, 11, 31 - 7)).isoWeek()
            );
            for (var i = 0; i < max; i++) {
                var d = getDateByWeek(i, year);
                var de = getDateByWeek(i, year);
                var dn = new Date(de.setDate(de.getDate() + 6));
                Tuans.push({ Tuan: "Tuần " + (i + 1), TuNgay: d, DenNgay: dn, IsLock: false, IsAuto: false, IsAll: false, hientai: $scope.soTuanTrongNam === i + 1 });
            }
            TuansLock = Tuans;
            //TuansLock.push({ IsAuto: false, ThuKhoa: 7, ThuMo: 7, GioKhoa: new Date().setHours(7), GioMo: new Date().setHours(7), IsAll: true, IsLock: true });
            return Tuans, TuansLock;
        }
        $scope.getYear = new Date().getFullYear();
        //-------------------------------------------------

        this.$onChanges = () => {
            if (this.calendarid && this.isopencalendar) {
                $scope.calendar = { LichhopTuan_ID: this.calendarid };
                $scope.LichhopTuan_ID = this.calendarid;
                $("#viewCalendar .InfoViewRequest").addClass('show');
                $("#infocalendar-overlay").addClass('show');
                $timeout(function () {
                    $('#viewCalendar a[data-target="#ttcalendar"]').trigger("click");
                    $scope.initCalendar();
                    closeswal();
                }, 500);
            }
        };

        $scope.initCalendar = function () {
            $scope.calendars = [];
            $scope.lichphongbans = [];
            $scope.LisFileAttach = [];
            $scope.FilesAttachList = [];
            $scope.initTD();
            //messaging
            if (messaging) {
                messaging.onMessage(function (payload) {
                    if (payload.data.hub) {
                        var hub = JSON.parse(payload.data.hub).Data;
                        if ((hub.loai === 3)) {
                            if (hub.LichhopTuan_ID === $scope.LichhopTuan_ID) {
                                $scope.getCalendar();
                            } else {
                                $scope.RefreshCV();
                            }
                        }
                    }
                });
            }
        };
        $scope.closeInfoRequest = function () {
            $("#viewCalendar .InfoViewRequest").removeClass('show');
            $("#infocalendar-overlay").removeClass('show');
        };
        $scope.fullInfoRequest = function () {
            $("#viewCalendar .InfoViewRequest").toggleClass("fulliframe");
        };
        ////======================== Từ điển =============================
        $rootScope.bgColor = [
            "#F8E69A", "#AFDFCF", "#F4B2A3", "#9A97EC", "#CAE2B0", "#8BCFFB", "#CCADD7"
        ];
        $scope.KieuLap = [
            { id: 0, value: "Không lặp", donvi: "ngày" },
            { id: 1, value: "Lặp ngày", donvi: "ngày" },
            { id: 2, value: "Lặp tuần", donvi: "tuần" },
            { id: 3, value: "Lặp tháng", donvi: "tháng" },
            { id: 4, value: "Lặp năm", donvi: "năm" }
        ];
        var isSend = false;
        ////==============================================================

        ////====================== Function Bydata =====================
        $scope.checkValidDatetimeKetThuc = function (l) {
            $scope.Thongbao = null;
            $scope.Thongbaogio = null;
            $scope.ThongbaoBD = null;
            $scope.ThongbaoBDgio = null;
            var BatDau = new Date($scope.calendar.BatdauNgay);
            if (l == 1) {
                $scope.calendar.KethucNgay = $scope.calendar.BatdauNgay;
            }
            BatDau.setHours(0, 0, 0, 0);
            var TimeCheck = $scope.calendar.gioBatDau ? new Date($scope.calendar.gioBatDau) : null;
            if (!TimeCheck || isNaN(TimeCheck.valueOf())) {
                TimeCheck = $scope.calendar.gioBatDau;
            }
            var Kethuc = new Date($scope.calendar.KethucNgay);
            var TimeCheck2 = $scope.calendar.gioKetThuc ? new Date($scope.calendar.gioKetThuc) : null;
            if (!TimeCheck2 || isNaN(TimeCheck2.valueOf())) {
                TimeCheck2 = $scope.calendar.gioKetThuc;
            }
            Kethuc.setHours(0, 0, 0, 0);
            var g = new Date(BatDau);
            var batdau = g.getDate();

            var h = new Date();
            var gio = moment(h).format("HH:mm");
            if (TimeCheck < gio && g.getDate() == h.getDate() && g.getMonth() == h.getMonth()) {
                $scope.calendar.gioBatDau = null;
                $scope.loai = l;
                $scope.ThongbaoBDgio = "Giờ bắt dầu không được nhỏ hơn giờ hiện tại !";
            }
            else if ((g.getDate() < h.getDate() && g.getMonth() == h.getMonth() && g.getYear() == h.getYear()) || (g.getMonth() < h.getMonth() && g.getYear() == h.getYear()) || (g.getYear() < h.getYear())) {
                $scope.calendar.BatDau = null;
                $scope.loai = l;
                $scope.ThongbaoBD = "Ngày bắt dầu không được nhỏ hơn ngày hiện tại !";
            }
            if (Kethuc < BatDau) {
                $scope.calendar.KethucNgay = null;
                $scope.loai = l;
                $scope.Thongbao = "Ngày kết thúc không được lớn hơn ngày bắt đầu !";
            }

            if (TimeCheck2 <= TimeCheck && Kethuc <= BatDau) {
                $scope.calendar.gioKetThuc = null;

                $scope.loai = l;
                if (l == 2) {
                    $scope.Thongbaogio = "Giờ kết thúc không được nhỏ hơn hoặc bằng giờ bắt đầu !";
                }

            }
            return true;
        };
        function cvTime(time) {
            try {
                var arr = time.split(":");
                return new Date(null, null, null, arr[0], arr[1]);
            } catch (e) {
                return new Date(time);
            }
        }
        $scope.openAttachfile = function (ev) {
            $(ev.target).closest("div.form-group").find("input[type='file']").trigger("click");
        };
        $scope.removeFiles = function (idx) {
            $scope.FilesAttachList.splice(idx, 1);
        };
        $scope.DeleteFiles = function (i, f) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa file này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "/Lichhop/Del_LichHopFile",
                        data: { t: $rootScope.login.tk, ids: [f.LichhopFile_ID] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.LisFileAttach.splice(i, 1);
                            showtoastr("'File bạn chọn đã được xóa thành công!.'");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa File không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.UploadFilesBaoCao = function (f) {

            if ($scope.LisFileAttach.length + f.length > 1) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 1 file cho báo cáo lịch!'
                });
                return false;
            }
            var ms = false;
            $scope.FilesAttachList = [];
            angular.forEach(f, function (fi) {
                if (fi.size > 25 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    fi.NgayTao = new Date();
                    $scope.FilesAttachList.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 25MB!'
                });
            }
        };
        $scope.UploadFiles = function (f) {

            if ($scope.LisFileAttach.length + f.length > 5) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 5 file 1 lần cho lịch!'
                });
                return false;
            }
            var ms = false;
            angular.forEach(f, function (fi) {

                if (fi.size > 25 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    fi.NgayTao = new Date();
                    $scope.FilesAttachList.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 25MB!'
                });
            }
        };
        $scope.OpenFile = function (linkfile) {
            var url = $rootScope.fileUrl + linkfile;
            window.open(url);
        };
        ////==============================================================

        $scope.initTD = function () {
            $scope.initOne();
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "GetRoleFunction", pas: [
                        { "par": "congtyID", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "Module", "va": 'S06' }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0][0];
                    $scope.ListDataRoleFunction = data;
                }
            });
        };

        $scope.initOne = function () {
            $scope.LichhopTuan_ID = null;
            $scope.calendars = [];
            $scope.IsViewCalendar = true;
            $scope.getCalendar();
            $scope.ListFollow();
            $scope.getFunctionCalendar();
            $scope.CalendarTudien();
        };

        ////====================== Get data ===========================
        $scope.CalendarTudien = function () {
            $http({
                method: "POST",
                url: "Home/callProc",
                data: {
                    t: $rootScope.login.tk, proc: "Calendar_Tudien", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                var data = JSON.parse(res.data.data);
                $scope.tudiens = data;
                $scope.BindCongty = data[1][0];
            });
        };
        $scope.getCalendar = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Calendar_GetLichhopTuan_2", pas: [
                        { "par": "id", "va": $scope.calendar.LichhopTuan_ID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    if (data[0] != null) {
                        data[0].forEach(function (r) {
                            if (r.ListNguoiQT) {
                                r.ListNguoiQT = JSON.parse(r.ListNguoiQT);
                            }
                        });
                    }
                    var NSfollow = false;
                    if (data[0][0].ListNguoiQT) {
                        angular.forEach(data[0][0].ListNguoiQT, function (r) {
                            if (r.NhanSu_ID == $rootScope.login.u.NhanSu_ID)
                                NSfollow = true;
                        });
                    }
                    $scope.calendar = data[0][0];
                    $scope.NSfollow = NSfollow;
                    angular.forEach($scope.KieuLap, function (r) {
                        if (r.id == $scope.calendar.Laplich) {
                            $scope.calendar.value = r.value;
                        }
                    });
                    $scope.nguoitao = data[1][0];
                    $scope.chutris = data[2];
                    $scope.thamgias = data[3];
                    $scope.lichphongbans = data[4];
                    $scope.LisFileAttach = data[5];
                    $scope.listngtg = data[6];
                    $scope.Doclich($scope.calendar.LichhopTuan_ID);
                }
            });
        };
        $scope.getFunctionCalendar = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Scalendar_getFuncEdit", pas: [
                        { "par": "LichhopTuan_ID", "va": $scope.calendar.LichhopTuan_ID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0][0];
                    $scope.IsEdit_Calendar = data.IsEdit_Calendar;
                    $scope.IsEdit_Calendar_Count = data.IsEdit_Calendar_Count;
                    $scope.IsEdit_Calendar_nsd = data.IsEdit_Calendar_nsd;
                }
            });
        };
        $scope.ListFollow = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "SCalendar_GetFollow_ByNhanSu", pas: [
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "LichhopTuan_ID", "va": $scope.calendar.LichhopTuan_ID },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    $scope.IsDuyetlich = JSON.parse(res.data.data)[0][0].IsDuyetlich;
                }

            });
        }
        $scope.Doclich = function (LichhopTuan_ID) {
            $http({
                method: 'POST',
                url: "Lichhop/Update_Doclich",
                data: {
                    t: $rootScope.login.tk, LichhopTuan_ID: LichhopTuan_ID
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    $rootScope.BindListSendHub();
                }
            });
        }
        ////===========================================================

        ////====================== Function ===========================
        //----------------Add Lich, chu tri và nguoi tham du-------------------
        $scope.ModalCalendar_LichhopTuan = function (check, t) {
            $scope.GetKhoaLich(undefined, check, t);
            $scope.IsKhoaLich = false;
            $scope.Islaplich = false;
        };
        $scope.Exitlichtrung = function () {
            $scope.lichtrungs = null;
        };
        $scope.GetKhoaLich = function (type, check, t) {
            if ($stateParams.type != 1) {
                //active Địa điểm chọn
                if (t != null) {
                    angular.forEach($scope.LoadDiadiem, function (m) {
                        m.acTiveDiaDiemHop = false;
                    });
                    t.acTiveDiaDiemHop = true;

                    $scope.ChoseActiveDiaDiemID = t.Diadiem_ID;
                }
                else
                    $scope.ChoseActiveDiaDiemID = null;
                $scope.chutris = [];
                $scope.thamgias = [];
                $scope.lichphongbans = [];
                $scope.LisFileAttach = [];
                $scope.FilesAttachList = [];
                $scope.goTagU();
                //
                $("form.ng-dirty").removeClass("ng-dirty");
                if (check === 'add') {
                    $scope.chutris = [];
                    $scope.thamgias = [];
                    $scope.LichhopTuan_ID = null;
                    $scope.MIsDuyet = true;
                    $scope.mTitle = "Thêm mới lịch";
                    $scope.Calendar_LichhopTuan = { LichhopTuan_ID: null, IsSend: false, Congty_ID: $rootScope.login.u.congtyID, BatdauNgay: $scope.getMondayNextWeek, KethucNgay: $scope.getMondayNextWeek, Kieulich: 0, Laplich: 0, Solap: 1, Songuoithamdu: 0 };
                    $("#ModalEditCalendar").modal("show");
                }


                if ($scope.LichhopTuan_ID != null) {
                    $scope.mTitle = "Sửa lịch";

                    if ($scope.checkModalLCT == 1)
                        $("#LichhopCtr  #ModalEditCalendar").modal("show");
                    else
                        $("#ModalEditCalendar").modal("show");
                    //list dữ liệu form
                    $scope.goLich($scope.LichhopTuan_ID);
                }
            }
            else {
                $http({
                    method: "POST",
                    url: "/Lichhop/checkKhoaLich",
                    headers: {
                        'Content-Type': undefined
                    },
                    transformRequest: function () {
                        var formData = new FormData();
                        if ($scope.Calendar_LichhopTuan != null) {
                            var obj = $scope.Calendar_LichhopTuan;
                            obj.Congty_ID = $rootScope.login.u.congtyID;
                            var BatDau = new Date(obj.BatdauNgay);
                            var TimeCheck = cvTime(obj.gioBatDau);
                            if (obj.gioBatDau == null || obj.gioBatDau == '') {
                                obj.Batdau = moment(BatDau).format('YYYY-MM-DDTHH:mm:ssZZ');
                            }
                            else {
                                BatDau = new Date(BatDau.setHours(TimeCheck.getHours(), TimeCheck.getMinutes(), 0, 0));
                                obj.Batdau = moment(BatDau).format('YYYY-MM-DDTHH:mm:ssZZ');
                            }
                            var Kethuc = new Date(obj.KethucNgay);
                            var TimeCheck2 = cvTime(obj.gioKetThuc);
                            if (obj.gioKetThuc == null || obj.gioKetThuc == '') {
                                obj.Kethuc = moment(Kethuc).format('YYYY-MM-DDTHH:mm:ssZZ');
                            }
                            else {
                                Kethuc = new Date(Kethuc.setHours(TimeCheck2.getHours(), TimeCheck2.getMinutes(), 0, 0));
                                obj.Kethuc = moment(Kethuc).format('YYYY-MM-DDTHH:mm:ssZZ');
                            }
                            obj.BatdauNgay = obj.Batdau;
                            obj.KethucNgay = obj.Kethuc;
                        }
                        else
                            obj = null;
                        formData.append("t", $rootScope.login.tk);
                        formData.append("type", type);
                        formData.append("congtyID", $rootScope.login.u.congtyID);
                        formData.append("model", JSON.stringify(obj));
                        return formData;
                    }

                }).then(function (res) {
                    if (res.data.error === 1) {
                        closeswal();
                        Swal.fire({
                            icon: 'error',
                            type: 'error',
                            title: '',
                            text: res.data.ms,
                        });
                        $scope.IsKhoaLich = true;
                        return false;
                    }
                    var KhoaLich = res.data.KhoaLich;
                    if (KhoaLich)
                        $scope.KhoaLich = KhoaLich;
                    else
                        $scope.KhoaLich = false;

                    if (KhoaLich) {
                        Swal.fire({
                            type: 'error',
                            icon: 'error',
                            title: '',
                            text: res.data.sms,
                        });
                        return false;
                    }
                    else {
                        //active Địa điểm chọn
                        if (t != null) {
                            angular.forEach($scope.LoadDiadiem, function (m) {
                                m.acTiveDiaDiemHop = false;
                            });
                            t.acTiveDiaDiemHop = true;

                            $scope.ChoseActiveDiaDiemID = t.Diadiem_ID;
                        }
                        else
                            $scope.ChoseActiveDiaDiemID = null;
                        $scope.chutris = [];
                        $scope.thamgias = [];
                        $scope.lichphongbans = [];
                        $scope.LisFileAttach = [];
                        $scope.FilesAttachList = [];
                        $scope.goTagU();
                        //
                        $("form.ng-dirty").removeClass("ng-dirty");
                        if (check === 'add') {
                            $scope.chutris = [];
                            $scope.thamgias = [];
                            $scope.LichhopTuan_ID = null;
                            $scope.MIsDuyet = true;
                            $scope.mTitle = "Thêm mới lịch";
                            $scope.Calendar_LichhopTuan = { LichhopTuan_ID: null, IsSend: false, Congty_ID: $rootScope.login.u.congtyID, BatdauNgay: $scope.getMondayNextWeek, KethucNgay: $scope.getMondayNextWeek, Kieulich: 0, Laplich: 0, Solap: 1, Songuoithamdu: 0 };
                            $("#ModalEditCalendar").modal("show");
                        }


                        if ($scope.LichhopTuan_ID != null) {
                            $scope.mTitle = "Sửa lịch";

                            if ($scope.checkModalLCT == 1)
                                $("#LichhopCtr  #ModalEditCalendar").modal("show");
                            else
                                $("#ViewModalCalendar_LichhopTuan").modal("show");
                            //list dữ liệu form
                            $scope.goLich($scope.LichhopTuan_ID);
                        }
                    }
                });
            }

        };
        var users = [];
        $scope.goTagU = function (f) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "SCalendar_Listuser", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    users = data[0];
                    $scope.filterUser = data[0];
                }
                if (f) {
                    $scope.Duyetnguoi.Focus = !$scope.Duyetnguoi.Focus;
                }
            });
        };
        $scope.showusersModal = function (f, index) {
            $rootScope.phongbans.filter(r => r.isCheck).forEach(function (r) {
                r.isCheck = false;
            });
            $rootScope.users.filter(r => r.isCheck).forEach(function (r) {
                r.isCheck = false;
            });
            $scope.vbphongbans = $rootScope.phongbans;
            $scope.vbusers = $rootScope.users;
            $scope.one = f;
            // set chọn user trong lấy list user config cho quy trình
            $scope.IndexListUser = index;
            $("#usersModal").modal("show");
        };
        $scope.choiceUser = function (users) {
            if ($scope.IndexListUser === 0) {
                $scope.chutris = [];
                users = users.filter(u => $scope.chutris.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                $scope.chutris = $scope.chutris.concat(users);
                $scope.calendar.searchU0 = null;
            }
            else if ($scope.IndexListUser === 1) {
                users = users.filter(u => $scope.thamgias.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                $scope.thamgias = $scope.thamgias.concat(users);
                $scope.calendar.searchU1 = null;
            }
            else {
                $scope.Duyetnguoi = [];

                users = users.filter(u => $scope.Duyetnguoi.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                $scope.Duyetnguoi = $scope.Duyetnguoi.concat(users);
                //add user
                $http({
                    method: "POST",
                    url: "Home/CallProc",
                    data: {
                        t: $rootScope.login.tk, proc: "GetRoleFunction", pas: [
                            { "par": "congtyID", "va": $rootScope.login.u.congtyID },
                            { "par": "NhanSu_ID", "va": users[0].NhanSu_ID },
                            { "par": "Module", "va": 'S06' }
                        ]
                    },
                    contentType: 'application/json; charset=utf-8'
                }).then(function (res) {
                    if (!$rootScope.checkToken(res)) return false;
                    if (res.data.error !== 1) {
                        var data = JSON.parse(res.data.data)[0][0];
                        $scope.RoleDuyetnguoi = data;
                    }

                }).then(function () {
                    if ($scope.RoleDuyetnguoi.Calendar_Duyetlich) {
                        $scope.Nhansu = users[0];
                        $scope.Duyetnguoi.NhanSu_ID = users[0].NhanSu_ID;
                        $scope.Duyetnguoi.Focus = false;
                    }
                    else {
                        Swal.fire({
                            icon: 'error',
                            type: 'error',
                            title: '',
                            text: "Người bạn chọn không có quyền duyệt."
                        });
                        return false;
                    }
                });
            }
            $("#usersModal").modal("hide");
            var sumsl = 0;
            if ($scope.chutris.length > 0 && $scope.thamgias.length > 0)
                sumsl = $scope.chutris.length + $scope.thamgias.length;
            else if ($scope.chutris.length > 0)
                sumsl = $scope.chutris.length;
            else if ($scope.thamgias.length > 0)
                sumsl = $scope.thamgias.length;

            if ($scope.calendar.Songuoithamdu < sumsl)
                $scope.calendar.Songuoithamdu = sumsl;

            if ($scope.calendar.Songuoithamdu == null)
                $scope.calendar.Songuoithamdu = 0;


        };
        $scope.removeAllUser = function () {
            $scope.thamgias = [];
        };
        //--------------------------------------------------------------

        //---------------------Edit Calendar------------------
        $scope.AddCalendar_LichhopTuan = function (frm, f) {
            swal.showLoading();
            $scope.lichtrungs = [];
            $http({
                method: "POST",
                url: "/Lichhop/checkKhoaLich",
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: function () {
                    var formData = new FormData();
                    if ($scope.calendar != null) {
                        var obj = $scope.calendar;
                        obj.Congty_ID = $rootScope.login.u.congtyID;
                        var BatDau = new Date(obj.BatdauNgay);
                        var TimeCheck = cvTime(obj.gioBatDau);
                        if (obj.gioBatDau == null || obj.gioBatDau == '') {
                            obj.Batdau = moment(BatDau).format('YYYY-MM-DDTHH:mm:ssZZ');
                        }
                        else {
                            BatDau = new Date(BatDau.setHours(TimeCheck.getHours(), TimeCheck.getMinutes(), 0, 0));
                            obj.Batdau = moment(BatDau).format('YYYY-MM-DDTHH:mm:ssZZ');
                        }
                        var Kethuc = new Date(obj.KethucNgay);
                        var TimeCheck2 = cvTime(obj.gioKetThuc);
                        if (obj.gioKetThuc == null || obj.gioKetThuc == '') {
                            obj.Kethuc = moment(Kethuc).format('YYYY-MM-DDTHH:mm:ssZZ');
                        }
                        else {
                            Kethuc = new Date(Kethuc.setHours(TimeCheck2.getHours(), TimeCheck2.getMinutes(), 0, 0));
                            obj.Kethuc = moment(Kethuc).format('YYYY-MM-DDTHH:mm:ssZZ');
                        }
                        obj.BatdauNgay = obj.Batdau;
                        obj.KethucNgay = obj.Kethuc;
                    }
                    else
                        obj = null;
                    formData.append("t", $rootScope.login.tk);
                    formData.append("type", 'IsNotAuto');
                    formData.append("congtyID", $rootScope.login.u.congtyID);
                    formData.append("model", JSON.stringify(obj));
                    return formData;
                }

            }).then(function (res) {
                var KhoaLich = res.data.KhoaLich;
                if (KhoaLich)
                    $scope.KhoaLich = KhoaLich;
                else
                    $scope.KhoaLich = false;

                if (res.data.error === 1) {
                    closeswal();
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: res.data.ms,
                    });
                    $scope.IsKhoaLich = true;
                    return false;
                }
            }).then(function () {
                if ($scope.IsKhoaLich != true) {
                    if (!valid(frm)) {
                        Swal.fire({
                            type: 'error',
                            icon: 'error',
                            title: '',
                            text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                        });
                        return false;
                    }
                    if (!$scope.checkValidDatetimeKetThuc()) {
                        return false;
                    }
                    if (isSend) {
                        return false;
                    }
                    isSend = false;
                    swal.showLoading();
                    $http({
                        method: "POST",
                        url: "/Lichhop/Add_Calendar_LichhopTuan",
                        headers: {
                            'Content-Type': undefined
                        },
                        transformRequest: function () {
                            var formData = new FormData();
                            var obj = $scope.calendar;
                            obj.Congty_ID = $rootScope.login.u.congtyID;
                            obj.Chutri = null;
                            if ($scope.chutris.length > 0)
                                obj.Chutri = $scope.chutris[0].NhanSu_ID;

                            var BatDau = new Date(obj.BatdauNgay);
                            var TimeCheck = cvTime(obj.gioBatDau);
                            if (obj.gioBatDau == null || obj.gioBatDau == '') {
                                obj.Batdau = moment(BatDau).format('YYYY-MM-DDTHH:mm:ssZZ');
                            }
                            else {
                                BatDau = new Date(BatDau.setHours(TimeCheck.getHours(), TimeCheck.getMinutes(), 0, 0));
                                obj.Batdau = moment(BatDau).format('YYYY-MM-DDTHH:mm:ssZZ');
                            }
                            var Kethuc = new Date(obj.KethucNgay);
                            var TimeCheck2 = cvTime(obj.gioKetThuc);
                            if (obj.gioKetThuc == null || obj.gioKetThuc == '') {
                                obj.Kethuc = moment(Kethuc).format('YYYY-MM-DDTHH:mm:ssZZ');
                            }
                            else {
                                Kethuc = new Date(Kethuc.setHours(TimeCheck2.getHours(), TimeCheck2.getMinutes(), 0, 0));
                                obj.Kethuc = moment(Kethuc).format('YYYY-MM-DDTHH:mm:ssZZ');
                            }
                            $scope.checkValidDatetimeKetThuc();
                            obj.BatdauNgay = obj.Batdau;
                            obj.KethucNgay = obj.Kethuc;
                            $scope.objToCheck = obj;
                            formData.append("model", JSON.stringify(obj));
                            formData.append("model_copy", JSON.stringify(obj));
                            var tds = [];
                            $scope.thamgias.forEach(function (u) {
                                tds.push({ "Nguoithamdu_ID": u.NhanSu_ID });
                            });
                            formData.append("thamgias", JSON.stringify(tds));
                            formData.append("phongbans", JSON.stringify($scope.lichphongbans));
                            formData.append("RequestMaster_ID", $ctrl.requestid);
                            formData.append("t", $rootScope.login.tk);
                            formData.append("u", JSON.stringify({ NhanSu_ID: $rootScope.login.u.NhanSu_ID, fullName: $rootScope.login.u.fullName, congtyID: $rootScope.login.u.congtyID }));

                            $.each($scope.FilesAttachList, function (i, file) {
                                formData.append('file', file);
                            });
                            return formData;
                        }

                    }).then(function (res) {

                        var dataLT = res.data.dataLT;
                        $scope.lichtrungs = dataLT;
                        isSend = false;
                        if (res.data.error === 1) {
                            closeswal();
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Có lỗi khi cập nhật !'
                            });
                        }
                        else {
                            if (!f)
                                $("#ModalEditCalendar").modal("hide");
                            else {
                                $scope.lichphongbans = [];
                                $scope.LisFileAttach = [];
                                $scope.FilesAttachList = [];
                                //
                                $("form.ng-dirty").removeClass("ng-dirty");
                                $scope.LichhopTuan_ID = null;
                                $scope.MIsDuyet = true;
                                $scope.calendar.LichhopTuan_ID = null;
                                $scope.calendar.Noidung = null;
                                $scope.calendar.BatdauNgay = $scope.getMondayNextWeek;
                                $scope.calendar.gioBatDau = null;
                                $scope.calendar.KethucNgay = $scope.getMondayNextWeek;
                                $scope.calendar.gioKetThuc = null;
                            }
                            isSend = false;
                            closeswal();
                            $scope.getCalendar();
                            showtoastr('Đã cập nhật dữ liệu thành công!.');
                        }
                    });
                }
            });
            //===================

        };
        $scope.DelCalendar_LichhopTuan = function (rowto) {
            $scope.arr = [];
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa lịch này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $scope.arr.push(rowto.LichhopTuan_ID);
                    $http({
                        method: "POST",
                        url: "/Lichhop/DelLich",
                        data: { t: $rootScope.login.tk, ids: $scope.arr },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.arr = [];
                            $scope.checkLen = 0;
                            $("#viewCalendar .InfoViewRequest").removeClass('show');
                            $("#infocalendar-overlay").removeClass('show');
                            showtoastr("lịch bạn chọn đã được xóa thành công.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: res.data.ms
                            });
                        }
                    });

                }
            })

        };
        //----------------------------------------------------

        //-------------------Show chart-----------------------
        $scope.showChartSign = function (u) {
            $scope.ChartsSigns = u.Signs;
            $scope.Logs = [];
            $scope.getLichLog(u);
            $("#ModalLogCalendar").modal("show");
        };
        $scope.getLichLog = function (r) {
            $http({
                method: "POST",
                url: "/Home/callProc",
                data: {
                    t: $rootScope.login.tk,
                    proc: "Calendar_ListLichLog",
                    pas: [
                        { "par": "LichhopTuan_ID", "va": r.LichhopTuan_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                var data = JSON.parse(res.data.data);
                data[0].forEach(function (r) {
                    if (r.Users) {
                        r.Users = JSON.parse(r.Users);
                    }
                });
                $scope.ChartsSigns = data[0];
                $scope.Logs = data[1];
            },
                function (response) {
                });
        };
        //----------------------------------------------------

        //------------------- Kết luận cuộc họp-----------------
        $scope.ModalCalendar_BaoCao = function () {
            $scope.LisFileAttach = [];
            $scope.FilesAttachList = [];
            var NhanSu_ID = '';
            NhanSu_ID = $rootScope.login.u.NhanSu_ID;
            $scope.BaocaoLich_ID = null;
            $scope.Calendar_BaoCao = { BaocaoLich_ID: null, LichhopTuan_ID: $scope.loglichhop, NguoiBaocao: NhanSu_ID };
        };
        $scope.LogBaoCaoLich = function (LichhopTuan_ID) {
            $scope.ViewKLcuochop(LichhopTuan_ID);
            $scope.Logtitle = "Xem kết luận cuộc họp";
            $scope.loglichhop = LichhopTuan_ID;
            $http({
                method: "POST",
                url: "Home/callProc",
                data: {
                    t: $rootScope.login.tk,
                    proc: "Calendar_GetLogBaoCao",
                    pas: [
                        { "par": "LichhopTuan_ID", "va": LichhopTuan_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                var data = JSON.parse(res.data.data);
                $scope.ListDataBaocaoLich = data[0];
                $("#ModalKetluancuochop").modal("show");
            });
        };
        $scope.ViewKLcuochop = function (id) {
            swal.showLoading();
            $http({
                method: "POST",
                url: "Home/callProc",
                data: {
                    t: $rootScope.login.tk, proc: "SCalendar_ViewKLCH", pas: [
                        { "par": "LichhopTuan_ID", "va": $scope.calendar.LichhopTuan_ID },
                        { "par": "congtyID", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                var data = JSON.parse(res.data.data)[0];
                $scope.IsViewKLCH = data[0].IsViewKLCH;
                closeswal();
            },
                function (response) {
                    closeswal();
                });
        }
        $scope.AddCalendar_BaocaoLich = function (frm) {
            if ($scope.Calendar_BaoCao.Noidung == null || $scope.Calendar_BaoCao.Noidung == '') {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            if ($scope.IsKhoaLich == true) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Thời gian bạn chọn đang khóa !'
                });
                return false;
            }
            if (isSend) {
                return false;
            }
            isSend = false;
            swal.showLoading();
            $http({
                method: "POST",
                url: "/Lichhop/Add_Calendar_BaocaoLich",
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: function () {
                    var formData = new FormData();

                    var obj = $scope.Calendar_BaoCao;
                    obj.NguoiBaocao = $rootScope.login.u.NhanSu_ID;
                    formData.append("model", JSON.stringify(obj));
                    formData.append("t", $rootScope.login.tk);
                    formData.append("congtyID", $rootScope.login.u.congtyID);
                    formData.append("u", JSON.stringify({ NhanSu_ID: $rootScope.login.u.NhanSu_ID, fullName: $rootScope.login.u.fullName, congtyID: $rootScope.login.u.congtyID }));
                    $.each($scope.FilesAttachList, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                }
            }).then(function (res) {
                isSend = false;
                if (res.data.error === 1) {
                    closeswal();
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm báo cáo !'
                    });
                }
                else {
                    closeswal();
                    $scope.LogBaoCaoLich($scope.loglichhop);
                }
            });
        };
        //----------------------------------------------------

        //-------------------Kết quả cuộc họp ------------------
        $scope.ModalEditNguoiThamDu = function (m) {
            $scope.ViewKLcuochop(m.LichhopTuan_ID);
            $scope.getCalendar();
            $scope.TDtitle = "Cập nhật người tham dự";
            $("#ModalKetquacuochop").modal("show");
        };
        $scope.UpdateNguoiThamDu = function (frm) {

            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            if (isSend) {
                return false;
            }
            isSend = false;
            swal.showLoading();
            $http({
                method: "POST",
                url: "/Lichhop/Update_NguoiThamDu",
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: function () {
                    var formData = new FormData();

                    angular.forEach($scope.listngtg, function (r) {
                        r.Nguoicapnhat = $rootScope.login.u.NhanSu_ID;
                    });
                    var obj = $scope.listngtg;
                    formData.append("model", JSON.stringify(obj));
                    formData.append("t", $rootScope.login.tk);
                    formData.append("congtyID", $rootScope.login.u.congtyID);
                    formData.append("LichhopTuan_ID", $scope.listngtg[0].LichhopTuan_ID);
                    return formData;
                }
            }).then(function (res) {
                isSend = false;
                if (res.data.error === 1) {
                    closeswal();
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi cập nhật người tham dự!'
                    });
                }
                else {
                    closeswal();
                    $("#ModalKetquacuochop").modal("hide");
                    showtoastr('Đã cập nhật dữ liệu thành công!.');
                }
            }, function (response) { // optional
                isSend = false;
                closeswal();
            });
        };
        //--------------------------------------------------------

        //-------------------Add nhanh Phòng họp-------------------
        $scope.AddPhongbanNew = function (name) {
            if ($scope.loadding) {
                return false;
            }
            if (!name) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập địa điểm !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $scope.diadiem = { Diadiem_Ten: name, on_off: true, Congty_ID: $rootScope.login.u.congtyID, Nguoitao: $rootScope.login.u.NhanSu_ID, ThuocCongty: false };
            $http({
                method: 'POST',
                url: "Lichhop/Update_diadiem",
                data: {
                    t: $rootScope.login.tk, model: $scope.diadiem
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm địa điểm mới !'
                    });
                    return false;
                }
                $scope.CalendarTudien();
                $scope.checkshow = false;
                showtoastr('Đã cập nhật địa điểm thành công!.');
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        //--------------------------------------------------------

        //--------------Cây user-----------------------
        $scope.completePB = function (string, m, $event) {
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.au2").focus();
                $event.preventDefault();
            }
            string = string.replace("@", "");
            if ($scope.lichphongbans) {
                m.Focus = false;
                m.Focus1 = false;
                m.Focus2 = false;
                $scope.lichphongbans.filter(x => x.Focus2 === true).forEach(function (r) {
                    r.Focus2 = false;
                });
            }
            m.Focus2 = true;
            var output = [];
            if (!string) {
                string = "";
            }
            angular.forEach($rootScope.phongbans, function (u) {
                if ((u.tenPhongban || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.tenkhongdau || "").toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    if (m.Phongban_ID !== u.Phongban_ID)
                        output.push(u);
                }
            });
            $scope.filterPB = output;
        };
        $scope.clickAddPB = function (m, us, u) {
            $scope.filterUser = null;
            if (us.filter(x => x.Phongban_ID == u.Phongban_ID).length === 0)
                us.push(u);
            m.searchU2 = "";
            $("input.au2").focus();
            var i = $scope.filterPB.findIndex(x => x.Phongban_ID == u.Phongban_ID);
            if (i !== -1) {
                $scope.filterPB.splice(i, 1);
            }
        };
        $scope.removePB = function (us, idx) {
            if (us[idx].Phongban) {
                $scope.Del_DuanPhongBan(us, idx);
            } else {
                us.splice(idx, 1);
            }
        };
        $scope.Del_DuanPhongBan = function (ds, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa phòng ban này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_DuanPhongBan",
                        data: { t: $rootScope.login.tk, ids: [ds[i].DuanPhongID] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            ds.splice(i, 1);
                            showtoastr("Phòng ban bạn chọn đã được xóa thành công!");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa Phòng ban không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        //---------------------------------------------

        //-----------------------Trình duyệt-------------------------
        $scope.chonNhom = function (n) {
            $scope.Duyetnhom.nhomD = n;
        };
        $scope.clickAddDuyetnhom = function () {
            $scope.IsRoleDuyet = false;
            var IsBHtrue = false;
            //if (isSend) {
            //    return false;
            //}
            //isSend = true;
            if (!$scope.Duyetnhom.nhomD) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: "Bạn chưa chọn nhóm duyệt."
                });
                return false;
            }
            var folows = [];
            var obj = {};
            if ($scope.LoaiDuyetQuyTrinh == true) {
                if ($scope.Nhomduyets[0].KieuDuyet == true) {
                    angular.forEach($scope.Nhomduyets, function (n) {
                        n.Users.filter(x => x.STT == 1).forEach(function (u) {
                            obj = { LichhopTuan_ID: $scope.calendar.LichhopTuan_ID, Noidung: "", IsType: true, IsDuyet: false, IsView: false, QT_ID: $scope.Nhomduyets[0].QT_ID, NhomDuyetLich_ID: $scope.Duyetnhom.nhomD.NhomDuyetLich_ID, NhanSu_ID: u.NhanSu_ID }; // duyệt nhóm tuần tự có quy trình
                        });
                    });
                }
                else
                    obj = { LichhopTuan_ID: $scope.calendar.LichhopTuan_ID, NhanSu_ID: null, Noidung: "", IsType: true, IsDuyet: false, IsView: false, QT_ID: $scope.Nhomduyets[0].QT_ID, NhomDuyetLich_ID: $scope.Duyetnhom.nhomD.NhomDuyetLich_ID }; //duyệt nhóm 1 nhiều có quy trình
            } else {
                if ($scope.Duyetnhom.nhomD.KieuDuyet == true) {
                    let filter_current = $filter('filter')($scope.Duyetnhom.nhomD.Users, { NhanSu_ID: $rootScope.login.u.NhanSu_ID });
                    let new_list = angular.copy($scope.Duyetnhom.nhomD.Users);
                    if (filter_current != null && filter_current.length > 0) {
                        new_list = new_list.filter(x => parseInt(x.IsStep) === parseInt(filter_current[0].IsStep) + 1);
                        $scope.Duyetnhom.nhomD.Users = new_list;
                    }

                    if ($scope.Duyetnhom.nhomD.Users == null || $scope.Duyetnhom.nhomD.Users.length === 0) {
                        $scope.openModalBanhanhHuy(true);
                        $("#ModalDuyetnhom").modal("hide");
                        IsBHtrue = true;
                    }
                    else if ($scope.Duyetnhom.nhomD.Users[0].NhanSu_ID == $rootScope.login.u.NhanSu_ID) {
                        $scope.Duyetnhom.nhomD.Users.splice(0, 1);
                    }

                    if ($scope.Duyetnhom.nhomD.Users.length > 1) {
                        obj = { LichhopTuan_ID: $scope.calendar.LichhopTuan_ID, Noidung: "", IsType: true, IsDuyet: false, IsView: false, NhomDuyetLich_ID: $scope.Duyetnhom.nhomD.NhomDuyetLich_ID, NhanSu_ID: null }; // duyệt nhóm tuần tự ko có quy trình
                    }
                    else {
                        obj = { LichhopTuan_ID: $scope.calendar.LichhopTuan_ID, Noidung: "", IsType: true, IsDuyet: false, IsView: false, NhomDuyetLich_ID: $scope.Duyetnhom.nhomD.NhomDuyetLich_ID, NhanSu_ID: $scope.Duyetnhom.nhomD.Users[0].NhanSu_ID }; // duyệt nhóm tuần tự ko có quy trình
                    }

                    //let check_step_next = false;
                    //angular.forEach($scope.Duyetnhom.nhomD.Users, function (us) {
                    //    if ($scope.Duyetnhom.nhomD.Users[0].IsStep < us.IsStep) {
                    //        check_step_next = true;
                    //    }
                    //});
                    //if (check_step_next) {

                    //}
                    //else {
                    //    obj = { LichhopTuan_ID: $scope.calendar.LichhopTuan_ID, Noidung: "", IsType: true, IsDuyet: false, IsView: false, NhomDuyetLich_ID: $scope.Duyetnhom.nhomD.NhomDuyetLich_ID, NhanSu_ID: null }; // duyệt nhóm tuần tự ko có quy trình
                    //}
                }
                else {
                    var c = 0;
                    $scope.Duyetnhom.nhomD.Users.filter(x => x.NhanSu_ID == $rootScope.login.u.NhanSu_ID).forEach(function (a) {
                        c++;
                    });
                    if (c > 0) {
                        $scope.openModalBanhanhHuy(true);
                        $("#ModalDuyetnhom").modal("hide");
                        IsBHtrue = true;
                    }
                    else {
                        obj = { LichhopTuan_ID: $scope.calendar.LichhopTuan_ID, NhanSu_ID: null, Noidung: "", IsType: true, IsDuyet: false, IsView: false, NhomDuyetLich_ID: $scope.Duyetnhom.nhomD.NhomDuyetLich_ID }; //duyệt nhóm bình thường
                    }
                }
            }
            folows.push(obj);
            if (!IsBHtrue) {
                $scope.loadding = true;
                swal.showLoading();
                if ($scope.Duyetnhom != null && $scope.Duyetnhom.Noidung == null)
                    $scope.Duyetnhom.Noidung = "";
                $http({
                    method: 'POST',
                    url: "/Lichhop/Update_Follow",
                    data: {
                        t: $rootScope.login.tk, models: folows, u: { NhanSu_ID: $rootScope.login.u.NhanSu_ID, fullName: $rootScope.login.u.fullName, congtyID: $rootScope.login.u.congtyID }, Noidung: $scope.Duyetnhom.Noidung
                    },
                    contentType: 'application/json; charset=utf-8'
                }).then(function (res) {
                    isSend = false;
                    $scope.loadding = false;
                    closeswal();
                    if (!$rootScope.checkToken(res)) return false;
                    if (res.data.error !== 0) {
                        Swal.fire({
                            icon: 'error',
                            type: 'error',
                            title: '',
                            text: 'Có lỗi khi trình duyệt !'
                        });
                        return false;
                    }
                    $("#ModalDuyetnhom").modal("hide");
                    $scope.getCalendar();
                    showtoastr('Đã trình duyệt thành công!.');
                }, function (response) { // optional
                    $scope.loadding = false;
                    isSend = false;
                });
            }
            //else {
            //    $scope.ListCountLHT();
            //    $scope.ListCalendar_LichhopTuan();
            //}
        };
        $scope.openModalDuyetNhom = function (type) {
            var arr = [];
            var td = [];
            td.push($scope.calendar.Nguoitao);
            if (td[0] == $rootScope.login.u.NhanSu_ID) {
                $scope.ttTilte = "Trình duyệt lịch";
            }
            else {
                $scope.ttTilte = "Duyệt lịch";
            }
            arr.push($scope.calendar.LichhopTuan_ID)
            if (arr.length === 0) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: "Bạn chưa chọn lịch để gửi."
                });
                return false;
            }
            $scope.Duyetnhom = { NgayDuyet: new Date() };
            if (type) {
                $scope.ListQuyTrinhDefault();
            }
            else {
                $scope.listNhomDuyets(true);
            }
        };
        //$scope.ListQuyTrinhDefault = function () {
        //    $http({
        //        method: "POST",
        //        url: "Home/CallProc",
        //        data: {
        //            t: $rootScope.login.tk, proc: "Scalendar_GetQT_Default", pas: [
        //                { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
        //            ]
        //        },
        //        contentType: 'application/json; charset=utf-8'
        //    }).then(function (res) {

        //        closeswal();
        //        if (!$rootScope.checkToken(res)) return false;
        //        if (res.data.error === 1) {
        //            Swal.fire({
        //                icon: 'error',
        //                type: 'error',
        //                title: '',
        //                text: "Có lỗi khi lấy dữ liệu quy trình!"
        //            });
        //            return false;
        //        }
        //        var data = JSON.parse(res.data.data)[0];
        //        $scope.DataGetQTDefault = data;
        //    }).then(function () {

        //        if ($scope.DataGetQTDefault == null || $scope.DataGetQTDefault.length === 0) {
        //            Swal.fire({
        //                icon: 'error',
        //                type: 'error',
        //                title: '',
        //                text: "Chưa có quy trình duyệt mặc định."
        //            });
        //            return false;
        //        }
        //        else if ($scope.DataGetQTDefault.length == 1) {
        //            var n = $scope.DataGetQTDefault[0];
        //            $scope.DuyetQuytrinh = { NgayDuyet: new Date() };
        //            $scope.chonQuytrinh(n);
        //            $scope.IsDaGetQuyTrinh = true;
        //            $("#ModalDuyetQuyTrinh").modal("show");
        //        }
        //        else {
        //            $scope.DuyetQuytrinh = { NgayDuyet: new Date() };
        //            $("#ModalDuyetQuyTrinh").modal("show");
        //        }
        //    });
        //};
        $scope.listNhomDuyets = function (df) {
            swal.showLoading();
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Calendar_ListNhomDuyet", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.Nhomduyets = data[0];
                    $scope.Nhomduyets.forEach(function (r) {
                        if (r.Users) {
                            r.Users = JSON.parse(r.Users);
                        }
                    })
                    if (df && $scope.Nhomduyets != null && $scope.Nhomduyets.length == 1) {
                        $scope.IsDaGetQuyTrinh = true;
                        $scope.Duyetnhom.nhomD = $scope.Nhomduyets[0];
                    }
                    $("#ModalDuyetnhom").modal("show");
                }
            });
        };
        //-----------------------------------------------------------

        //------------------------Duyệt lịch--------------------------
        $scope.ModalDuyet = function (f, tt) {
            var lichchecks = [];
            lichchecks.push($scope.calendar);

            if (lichchecks.length > 0 || tt == 2) {
                $scope.DuyetNhieu = { NhanSu_ID: null, IsDuyet: true, IsView: true, NgayDuyet: new Date(), IsType: true }
                $("#ModalDuyetnhieu").modal("show");
            }
            else {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: "Bạn chưa chọn lịch !",
                });
                return false;
            }
        }
        var lenlich = 0;
        $scope.SendLichHopTuan = function (f) {//Goi khi click gui
            swal.showLoading();
            var ArrPhieu = [];
            ArrPhieu.push($scope.calendar.LichhopTuan_ID);

            $http({
                method: "POST",
                url: "Lichhop/getDuyetPhieu",
                data: {
                    t: $rootScope.login.tk,
                    ArrPhieu: ArrPhieu,
                    NhanSu_ID: $rootScope.login.u.NhanSu_ID,
                    Noidung: $scope.DuyetNhieu.Noidung,
                    isBH: f,
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                isSend = false;
                closeswal();
                if (res.data.error === 1) {
                    closeswal();
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi!'
                    });
                }
                else {
                    $scope.getCalendar();
                    $("#ModalDuyetnhieu").modal("hide");
                }
            });
        };
        //------------------------------------------------------------

        //------------------------Trả lại----------------------------
        var isBH = false;
        $scope.openModalBanhanhHuy = function (f, m) {
            isBH = f;
            $scope.isBH = f;
            var arr = [];
            arr.push($scope.calendar.LichhopTuan_ID);
            if (arr.length === 0) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: "Bạn chưa chọn lịch để " + (f ? "ban hành" : "Trả lại") + "."
                });
                return false;
            }
            $scope.Duyetbh = { NgayDuyet: new Date(), IsType: f };
            if (f) {
                $scope.dTitle = "Ban hành lịch";
            }
            else {
                $scope.dTitle = "Trả lại";
            }
            $("#ModalBanhanh").modal("show");
        };
        $scope.AddDuyetBH = function (frm, isBH) {
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            if (isSend) {
                return false;
            }
            isSend = true;
            $scope.loadding = true;
            swal.showLoading();
            var folows = [];
            var o = { LichhopTuan_ID: $scope.calendar.LichhopTuan_ID, NhanSu_ID: $rootScope.login.u.NhanSu_ID, Noidung: $scope.Duyetbh.Noidung, IsType: isBH, IsDuyet: true, IsView: true };
            folows.push(o);

            if ($scope.Duyetbh != null && $scope.Duyetbh.Noidung == null)
                $scope.Duyetbh.Noidung = "";
            $http({
                method: 'POST',
                url: "/Lichhop/Update_Follow",
                data: {
                    t: $rootScope.login.tk, models: folows, u: { NhanSu_ID: $rootScope.login.u.NhanSu_ID, fullName: $rootScope.login.u.fullName, congtyID: $rootScope.login.u.congtyID }, Noidung: $scope.Duyetbh.Noidung, isBH: isBH
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                isSend = false;
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi ' + (isBH ? "ban hành" : "hủy lịch") + ' !'
                    });
                    return false;
                }
                $("#ModalBanhanh").modal("hide");
                $scope.getCalendar();
                showtoastr('Đã ' + (isBH ? "ban hành" : "hủy lịch") + ' thành công!.');
            }, function (response) { // optional
                $scope.loadding = false;
                isSend = false;
            });
        };
        //-----------------------------------------------------------

        //------------------------ Hủy lịch --------------------------
        $scope.HuyLichTuan = function (r) {
            //$("#cvModal").modal("show");

            //$scope.lichhuy = r;

            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn hủy lịch này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: 'POST',
                        url: "/Lichhop/HuyLich",
                        data: {
                            t: $rootScope.login.tk, id: r.LichhopTuan_ID, nid: $rootScope.login.u.NhanSu_ID
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        isSend = false;
                        $scope.loadding = false;
                        closeswal();
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 0) {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Có lỗi khi hủy lịch'
                            });
                            return false;
                        }
                        $scope.getCalendar();
                        showtoastr('Đã hủy lịch thành công!.');
                    }, function (response) { // optional
                        $scope.loadding = false;
                        isSend = false;
                    });
                }
            });
        };
        //------------------------------------------------------------

        ////===========================================================


        ////====================== Modal Function =====================
        $scope.SelectmodelEditViewCT = function () {
            $scope.getCalendar();
            $("#ModalEditCalendar").modal("show");
        }
        ////===========================================================

        //End nhom du an
        $scope.PutFileUpload = function (f) {
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 25 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Tenfile = fi.name;
                    fi.Dungluong = fi.size;
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    $scope.IPTailieuFiles.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 25MB!'
                });
            }
        };
        $(document).ready(function () {
            var dropZone = document.getElementById('#viewTask drop-zone');
            if (dropZone) {
                var startUpload = function (files) {
                    $scope.PutFileUpload(files);
                    //if ($scope.$$phase) {
                    $scope.$apply();
                    //}
                };
                dropZone.ondrop = function (e) {
                    e.preventDefault();
                    this.className = 'upload-drop-zone';
                    startUpload(e.dataTransfer.files);
                };
                dropZone.ondragover = function () {
                    this.className = 'upload-drop-zone drop';
                    return false;
                };
                dropZone.ondragleave = function () {
                    this.className = 'upload-drop-zone';
                    return false;
                };
                dropZone.onclick = function () {
                    $("#ModalFileUpload input[type='file']").trigger("click");
                };
            }
        });
    }
});