﻿os.component('viewdoccomponent', {
    bindings: {
        vanbanmasterid: '<',
        requestid: '<',
        uid: '<',
        isopendoc: '<',
        isopenadddoc: '<',
        htitle: '=',
        rfFunction: '&'
    },
    templateUrl: baseUrl + 'App/Component/Doc/ViewDoc.html?v=' + vhtmlcache,
    controller: function ($scope, $rootScope, $http, $sce, $timeout) {
        var $ctrl = this;
        $scope.goRFfunction = function () {
            $ctrl.rfFunction();
        };
        this.$onChanges = () => {
            if (this.vanbanmasterid && this.isopendoc && !this.isopenadddoc) {
                $scope.ob = { selected: false, checkall: false, errorFile: false, existFile: false, dnTag: false, statusVB: false, searchVB: null, filterVB: null, openCmt: false }
                $("#viewDoc .InfoViewRequest").addClass('show');
                $("#infodoc-overlay").addClass('show');
                $timeout(function () {
                    $scope.initRole();
                    closeswal();
                }, 500);
            } else if (this.requestid && this.isopenadddoc) {
                $scope.BindListTDVB();
            }
        };
        $scope.closeInfoRequest = function () {
            $("#viewDoc .InfoViewRequest").removeClass('show');
            $("#infodoc-overlay").removeClass('show');
        };
        $scope.fullInfoRequest = function () {
            $("#viewDoc .InfoViewRequest").toggleClass("fulliframe");
        };
        $scope.initRole = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "GetRoleFunction", pas: [
                        { "par": "congtyID", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "Module", "va": 'S03' }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    $scope.ListDataRoleFunction = data[0];
                    if ($scope.ListDataRoleFunction)
                    $scope.ob.FuncDongDau = $scope.ListDataRoleFunction['Sdoc_Dongdauvanban'];
                }
                $scope.GetVBNotify();
            }).then(function () {
            });
        };
        $scope.BindListTDVB = function () {
            $scope.td = {}
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Sdoc_ListTDVanban", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                //closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.td.noibanhanh = data[0];

                    $scope.td.nhomvanban = data[1];
                    $scope.td.org_nhomvanban = data[1];

                    $scope.td.phongban = data[2];
                    $scope.td.dokhan = data[3];
                    $scope.td.domat = data[4];
                    $scope.td.linhvuc = data[5];
                    $scope.td.hinhthucgui = data[6];
                    $scope.td.noinhan = data[7];

                    $scope.td.socongvan = data[8];
                    $scope.td.org_socongvan = data[8];

                    $scope.td.chucvu = data[9];
                    $scope.td.nguoiky = data[10];
                    $scope.td.email = data[11];
                    $scope.td.nhomemail = data[12];
                    //
                    $scope.openModalDuthaoVB();
                }
            });
        };
        $scope.AddWatermarkUser = function (d) {
            $http({
                method: "POST",
                url: "Doc/Update_WatermarkUser",
                data: { t: $rootScope.login.tk, id:d.vanBanMasterID },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error === 1) {
                }
                else {
                    if (res.data.tailieuPath_Temp) {
                        d.tempReady = true;
                        d.tailieuPath_Temp = res.data.tailieuPath_Temp;
                    }
                    $scope.ListTailieuByVB(d, true);
                }
            });
        };
        //---------------Xem quy trinh---------------------------
        $scope.showFollow = function (t) {
            $scope.dtitle = "Theo dõi quy trình xử lý văn bản";
            $scope.Follows = [];
            $scope.Logs = [];

            $scope.getVBLog(t);
            $("#ModalQuytrinh").modal("show");
        };
        $scope.getVBLog = function (r) {
            $http({
                method: "POST",
                url: "/Home/callProc",
                data: {
                    t: $rootScope.login.tk,
                    proc: "Sdoc_ListVBLog",
                    pas: [
                        { "par": "vanBanMasterID", "va": r.vanBanMasterID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {

                var data = JSON.parse(res.data.data);
                data[0].forEach(function (r) {
                    if (r.Users) {
                        r.Users = JSON.parse(r.Users);
                    }
                });
                $scope.Follows = data[0];
                if ($scope.Follows.length > 0) {
                    if ($scope.Follows[$scope.Follows.length - 1].vanBanTrangthaiNext_ID === 'tralai') {
                        $scope.Follows[$scope.Follows.length - 1].vanBanTrangthaiNext_ID = 'xulychinh';
                        $scope.Follows[$scope.Follows.length - 1].tenTrangthaiNext = 'Chờ xử lý';
                    }
                }

                $scope.Logs = data[1];
            },
                function (response) {
                });
        };
        $scope.toggleTitleVB = function () {
            $scope.expand = !$scope.expand;
        };
        $scope.setChk = function (u, $event) {
            if ($event) $event.stopPropagation();
            u.selected = !u.selected;
            $scope.ob.isdelete = u.selected;

            $scope.ob.statusVB = u.vanBanTrangthai_ID;
            $scope.ob.FirststatusVB = u.FirstvanBanTrangthai_ID;
            $scope.ob.denDiNoiBo = u.denDiNoiBo;
            $scope.ob.vanBanFollow_ID = u.vanBanFollow_ID;
            $scope.ob.Fldicuoi_vanBanFollow_ID = u.Fldicuoi_vanBanFollow_ID;
            $scope.ob.nguoiGuiHientai = u.nguoiGuiHientai;
            $scope.ob.nguoiTaoVanBanID = u.nguoiTaoVanBanID;
            $scope.ob.Loainhomhientai = u.Loainhomhientai;
            $scope.ob.vanBanMasterID = u.vanBanMasterID;
            $scope.ob.tailieuPath = u.tailieuPath;

            angular.forEach($scope.vanbans, function (r) {
                if (r.vanBanMasterID !== u.vanBanMasterID) r.selected = false;
            })
            var se = $scope.vanbans.find(x => x.selected === true)
            if (se) {
                $scope.ob.selected = true;
            }
            else {
                $scope.ob.selected = false;
            }

            var oe = $scope.vanbans.filter(x => x.selected === true)
            if (oe.length === 1) {
                $scope.ob.oneSelected = true;
            }
            else {
                $scope.ob.oneSelected = false;
            }

            $scope.checkFuncVB(u.vanBanTrangthai_ID, u.denDiNoiBo, u.LoaiNhom, u.nguoiTaoVanBanID, u.FirstvanBanTrangthai_ID);
        }
        $scope.checkFuncVB = function (id, type, typeNhom, nguoitao, firstTrangthai) {
            $scope.curFuncVBs_menu1 = [];
            $scope.curFuncVBs_menu2 = [];
            $scope.curFuncVBs_menu3 = [];
            angular.forEach($scope.ListFuncVB, function (item) {
                if (item.id === '9') {
                    if ($rootScope.login.u.NhanSu_ID === nguoitao && $scope.ob.Fldicuoi_vanBanFollow_ID === null) {
                        item.isNguoitao = true;
                    }
                }
                if (item.id === '11') {
                    if ($scope.ob.statusVB !== 'luutru') {
                        if ($rootScope.login.u.isAdmin && !$rootScope.login.u.IsSupper) {
                            item.allroles = true;
                        }
                        else {
                            if ($scope.rolesNS.find(x => (x.IsFlag === 'VT' || x.IsFlag === 'VTP'))) {
                                item.allroles = true;
                            }
                        }
                    }
                }
                if (item.allroles === true) {
                    switch (item.menu) {
                        case '1':
                            $scope.curFuncVBs_menu1.push(item);
                            break;
                        case '2':
                            $scope.curFuncVBs_menu2.push(item);
                            break;
                        case '3':
                            $scope.curFuncVBs_menu3.push(item);
                            break;
                    }
                }
                else {
                    angular.forEach(item.key, function (r) {
                        if (r.id === id && r.type === type && (!r.firstTrangthai || r.firstTrangthai === firstTrangthai) && (!r.func || r.func.find(x => $scope.ListDataRoleFunction[x] === true)) && (r.typeNhom === typeNhom || typeof r.typeNhom === 'undefined') && !item.isNguoitao) {
                            switch (item.menu) {
                                case '1':
                                    $scope.curFuncVBs_menu1.push(item);
                                    break;
                                case '2':
                                    $scope.curFuncVBs_menu2.push(item);
                                    break;
                                case '3':
                                    $scope.curFuncVBs_menu3.push(item);
                                    break;
                            }
                        }
                    });
                }
            })
        }
        $scope.ListFuncVBGui = [
            {
                menu: '2', icon: 'la la-users', name: 'Phân phát', key: [{ id: 'phanphat' }]
            },
            {
                menu: '1', icon: 'la la-hdd', name: 'Lưu trữ DataStore', key: [], allroles: false
            },
            {
                menu: '1', icon: 'la la-undo-alt', name: 'Thu hồi', key: [], allroles: true, divider: true
            },
        ];
        $scope.trustAsHtml = function (html) {
            return $sce.trustAsHtml(html);
        }
        $scope.trustAsResourceUrl = function (url) {
            return $sce.trustAsResourceUrl("http://docs.google.com/viewer?embedded=true&url=" + url);
        }
        $scope.GetDuyetUr = function (t) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Sdoc_Getduyetur", pas: [
                        { "par": "vanBanMasterID", "va": t.vanBanMasterID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {

                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.dtvb = t;
                    $scope.ListTailieuByVB(t, true);
                    $scope.dtvb.urduyet = data[0];
                    angular.forEach($scope.dtvb.urduyet, function (u) {
                        var tlt = u.fullName + '<br/>' + u.tenCongty + '<br/>' + u.tenChucVu + '<br/>' + u.tenPhongban;
                        u.tooltip = $scope.trustAsHtml(tlt);
                    })
                }
            });
        };
        $scope.checkFuncVBGui = function (id, type, role, listcheck) {
            $scope.curFuncVBGuis_menu1 = [];
            $scope.curFuncVBGuis_menu2 = [];
            angular.forEach(listcheck, function (item) {
                if (item.name === 'Lưu trữ DataStore') {
                    if ($scope.ob.statusVB !== 'luutru') {
                        if ($rootScope.login.u.isAdmin && !$rootScope.login.u.IsSupper) {
                            item.allroles = true;
                        }
                        else {
                            if ($scope.rolesNS && $scope.rolesNS.find(x => (x.IsFlag === 'VT' || x.IsFlag === 'VTP'))) {
                                item.allroles = true;
                            }
                        }
                    }
                }
                if (item.allroles) {
                    switch (item.menu) {
                        case '1':
                            $scope.curFuncVBGuis_menu1.push(item);
                            break;
                        case '2':
                            $scope.curFuncVBGuis_menu2.push(item);
                            break;
                    }
                }
                else {
                    angular.forEach(item.key, function (r) {
                        if (r.id === id && ($scope.ListDataRoleFunction[r.func] === true || !r.func)) {
                            switch (item.menu) {
                                case '1':
                                    $scope.curFuncVBGuis_menu1.push(item);
                                    break;
                                case '2':
                                    $scope.curFuncVBGuis_menu2.push(item);
                                    break;
                            }
                        }
                    });
                }
            })
        }
        $scope.ListTailieuByVB = function (v, st) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Sdoc_ListTailieuVB", pas: [
                        { "par": "vanBanMasterID", "va": v.vanBanMasterID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {

                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.tailieuVBs = data[0].filter(x => !x.IsDT);
                    if (st) {
                        $scope.vbdongdau = data[0].find(x => x.IsDT);
                    }
                }
            });
        }
        $scope.openModalUrFollow = function (u) {
            $rootScope.Urtitle = "Nhân sự xử lý và nhận văn bản";
            $scope.UrFollow = u;

            $("#ModalUserfollow").modal("show");
        }
        $scope.GetVBNotify = function () {
            $scope.vanbans = [];
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Sdoc_GetVanbanNotify", pas: [
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "vanBanMasterID", "va": $ctrl.vanbanmasterid }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    var t = data[0][0];
                    t.IsNguoinhan = data[1][0].IsNguoinhan;
                    t.IsNguoiGui = data[2][0].IsNguoiGui;
                    $scope.vanbans.push(t);
                    t.tempReady = false;
                    $scope.AddWatermarkUser(t);
                    $scope.setChk(t);
                    $scope.checkFuncVBGui(t.vanBanTrangthai_ID, t.denDiNoiBo, $scope.rolesNS, $scope.ListFuncVBGui);
                    if (!t.vanBanXemID) {
                        $scope.UpdateViewVB(t);
                    }
                    if (t.IsView === false) {
                        t.IsView = true;
                        $scope.UpdateViewVBXuly(t);
                    }
                    if (t.vanBanTrangthai_ID !== 'sohoa' && t.vanBanTrangthai_ID !== 'duthao') {
                        $scope.GetDuyetUr(t);
                    }
                    else {
                        $scope.ListTailieuByVB(t, true);
                        $('#viewdocvb').attr('src', 'http://docs.google.com/viewer?url=' + $rootScope.baseUrl + $scope.dtvb.tailieuPath);
                    }
                    $scope.dtvb = t;
                    $scope.ListTailieuByVB(t, true);
                }
            });
        };
        $scope.UpdateViewVBXuly = function (t) {
            if ($scope.loadding) {
                return false;
            }
            if (t === null || typeof t === 'undefined' || t.length === 0) {
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Doc/Update_ViewVBFollow",
                data: {
                    t: $rootScope.login.tk, id: t.vanBanFollow_ID
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {

                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Không cập nhật được, vui lòng kiểm tra lại dữ liệu !'
                    });
                    return false;
                }
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        // Du thao van ban
        $scope.openAttachfile = function (ev) {
            $(ev.target).closest("div.form-group").find("input[type='file']").trigger("click");
        };
        $scope.UploadVBAttach = function (f) {
            //
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 30 * 1024 * 1024) {
                    ms = true;
                } else {
                    //
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    $scope.VBAttach = fi;
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 30MB!'
                });
            }
        };
        $scope.UploadFiles = function (f) {
            if ($scope.ListFilesTemp.length + $scope.ListFiles.length + f.length > 5) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 5 files !'
                });
                return false;
            }
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 30 * 1024 * 1024) {
                    ms = true;
                } else {
                    //fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    $scope.ListFilesTemp.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 30MB!'
                });
            }
        };
        $scope.removeAtchVB = function () {
            $scope.VBAttach = null;
        };
        $scope.deleteAtchVB = function () {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa văn bản đính kèm này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    //
                    $http({
                        method: "POST",
                        url: "Doc/Del_VBAttach",
                        data: { t: $rootScope.login.tk, id: $scope.Vanban.vanBanMasterID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.Vanban.tailieuPath = null;
                            Swal.fire(
                                'Đã xóa!',
                                'File bạn chọn đã được xóa thành công!.',
                                'success'
                            );
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa File không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.deleteAtchFiles = function (f) {
            var ids = [];
            if (f) {
                ids.push($scope.vbdongdau.tailieuID);
            };
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa file đính kèm này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    //
                    $http({
                        method: "POST",
                        url: "Doc/Del_FilesAttach",
                        data: { t: $rootScope.login.tk, ids: ids },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            if (f) {
                                $scope.vbdongdau = null;
                            }
                            Swal.fire(
                                'Đã xóa!',
                                'File bạn chọn đã được xóa thành công!.',
                                'success'
                            );
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa File không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        }
        $scope.removeFilesList = function (list, idx) {
            list.splice(idx, 1);
        };
        $scope.completeRelVBtag = function (string, m, $event, i) {
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            if (!string) {
                string = "";
            }
            //string = string.replace("@", "");
            m["Focus" + i] = true;
            var output = [];
            angular.forEach($scope.nofilterRelvanbans, function (u) {
                if ((u.trichYeuNoiDung || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.soKyHieu || "").toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    output.push(u);
                }
            });
            $scope.relvanbans = output;
            if (m.relvanbans.length > 0) {
                angular.forEach($scope.Vanban.relvanbans, function (item) {
                    var i = $scope.relvanbans.findIndex(x => x.vanBanMasterID === item.vanBanMasterID);
                    if (i !== -1) {
                        $scope.relvanbans.splice(i, 1);
                    }
                });
            }
        };
        $scope.clickAddRelVB = function (model, u) {
            model.relvanbans.push(u);

            var i = $scope.relvanbans.findIndex(x => x.vanBanMasterID === u.vanBanMasterID);
            if (i !== -1) {
                $scope.relvanbans.splice(i, 1);
            }
            $scope.model.Focus1 = false;
            $scope.model.searchV = "";
        };
        $scope.removetag = function (us, us_id, idx) {
            if (us) us.splice(idx, 1);
            if (us_id) us_id.splice(idx, 1);
        };
        $scope.deleteFilesList = function (fi, idx) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa file đính kèm này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    //
                    $http({
                        method: "POST",
                        url: "Doc/Del_FileAttach",
                        data: { t: $rootScope.login.tk, id: fi.tailieuID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.ListFiles.splice(idx, 1);
                            Swal.fire(
                                'Đã xóa!',
                                'File bạn chọn đã được xóa thành công!.',
                                'success'
                            );
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa File không thành công, vui lòng thử lại!'
                            });
                        }
                    });
                }
            })
        };
        $scope.openModalDuthaoVB = function () {
            $scope.BindListRelVB_Duthao();
            $scope.BindListRelativeCty();
            $scope.GetPhongbanTree($rootScope.login.u.congtyID);
            let defaultPhongban = $scope.td.phongban.find(x => x.Phongban_ID = $rootScope.login.u.Phongban_ID);
            $scope.dtitle = "Tạo văn bản cho đề xuất: " + $ctrl.htitle;
            $scope.Vanbanduthao = {
                hanXuly: false, ngayDen: new Date(), NhomvanbanID: $scope.td.nhomvanban[0].NhomvanbanID, Phongban_ID: defaultPhongban ? defaultPhongban.Phongban_ID : $scope.td.phongban[0].Phongban_ID,
                linhVuc: $scope.td.linhvuc[0].linhvucID, sotudong: false, soTrang: 1, phatHanh: false, dangDT: true, IsOK: false, IsQT: false, IsLock: false, IsRecycle: false, Congty_ID: $rootScope.login.u.congtyID,
                nguoiTaoVanBanID: $rootScope.login.u.NhanSu_ID, tailieuPath: null, donVi: $rootScope.login.u.congtyID, denDiNoiBo: 1
            };
            $scope.Vanbanduthao.relvanbans = [];
            $scope.ListVBAttach = [];
            $scope.VBAttach = null;
            $scope.ListFiles = [];
            $scope.ListFilesTemp = [];
            $timeout(function () {
                $("#ModalDuThaoVanban").modal("show");
            }, 100);
        };
        $scope.UpdateViewVB = function (t) {
            if ($scope.loadding) {
                return false;
            }
            if (t === null || typeof t === 'undefined' || t.length === 0) {
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Doc/Update_ViewVB",
                data: {
                    t: $rootScope.login.tk, vbid: t.vanBanMasterID, nsid: $rootScope.login.u.NhanSu_ID
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {

                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Không cập nhật được, vui lòng kiểm tra lại dữ liệu !'
                    });
                    return false;
                }
                if ($rootScope.countVBDens) {
                    $rootScope.countVBDens.vbdxem -= 1;
                }
            }, function (response) { // optional
                $scope.loadding = false;
            });
        }
        $scope.AddDuthaoVB = function (frm) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            if (($scope.Vanbanduthao.tailieuPath === '' || $scope.Vanbanduthao.tailieuPath === null) && ($scope.VBAttach === '' || $scope.VBAttach === null)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Chưa có văn bản đính kèm !'
                });
                return false;
            }
            $scope.loadding = true;
            //swal.showLoading();
            $("#modalfileProgress").modal("show");
            var fileProgress = document.getElementById("fileProgress");
            var desProgress = document.getElementById("podes");
            fileProgress.setAttribute("value", 0);
            fileProgress.setAttribute("max", 0);
            fileProgress.style.display = "block";
            $http({
                method: 'POST',
                url: "Doc/Update_Vanbanduthao",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    formData.append("vanbanduthao", JSON.stringify($scope.Vanbanduthao));
                    formData.append("RequestMaster_ID", $ctrl.requestid);
                    formData.append("phongban_id", $rootScope.login.u.Phongban_ID);
                    if ($scope.Vanbanduthao.relvanbans.length > 0) {
                        formData.append("relvanbans", JSON.stringify($scope.Vanbanduthao.relvanbans));
                    }
                    if ($scope.VBAttach) {
                        formData.append('vbattach', $scope.VBAttach, $scope.VBAttach.name);
                    }
                    $.each($scope.ListFilesTemp, function (i, file) {
                        formData.append('fis', file);
                    });
                    return formData;
                },
                uploadEventHandlers: {
                    progress: function (e) {
                        if (e.lengthComputable) {
                            fileProgress.setAttribute("value", e.loaded);
                            fileProgress.setAttribute("max", e.total);
                            var po = Math.ceil(e.loaded * 100 / e.total);
                            desProgress.innerHTML = po + "%";
                        }
                    }
                }
            }).then(function (res) {
                $scope.loadding = false;
                //closeswal();
                $("#modalfileProgress").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Không cập nhật được, vui lòng kiểm tra lại dữ liệu !'
                    });
                    return false;
                }
                $("#ModalDuThaoVanban").modal("hide");
                $scope.goRFfunction();
                showtoastr('Đã cập nhật văn bản thành công!.');
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        $scope.openModalEditDuthaoVB = function (p) {
            $scope.BindListRelativeCty();
            $scope.VBAttach = null;
            $scope.dtitle = "Dự thảo văn bản";
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Sdoc_GetVanban", pas: [
                        { "par": "vanBanMasterID", "va": p.vanBanMasterID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {

                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.Vanbanduthao = data[0][0];
                    $scope.Vanbanduthao.nguoitheodois = data[4]
                    if ($scope.Vanbanduthao.donVi) $scope.GetPhongbanTree($scope.Vanbanduthao.donVi);
                    $scope.Vanbanduthao.relvanbans = data[3];
                    $scope.ListFiles = data[2];
                    $scope.ListFilesTemp = [];
                    $("#ModalDuThaoVanban").modal("show");
                }
            });
        };
        $scope.BindListRelVB_Duthao = function () {
            if ($scope.loadding) {
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Sdoc_ListRelVB", pas: [
                        { "par": "sohoa", "va": false },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {

                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.relvanbans = data[0];
                    $scope.nofilterRelvanbans = data[0];
                }
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        $scope.BindListRelativeCty = function (f) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Sdoc_ListRelativeCongTy", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.RelCty = data[0];
                    if (!f) {
                        var org_cty = $scope.RelCty.find(x => x.Congty_ID === $rootScope.login.u.congtyID);
                        if (!$rootScope.login.u.SuDungSCVCon) {
                            if (org_cty) {
                                $scope.RelCty = [];
                                $scope.RelCty.push(org_cty);
                            }
                        }
                        if (org_cty.Parent_ID) {
                            $scope.RelCty = [];
                            $scope.RelCty.push(org_cty);
                        }
                    }
                    if (f) {
                        var curCtyIndex = $scope.RelCty.findIndex(x => x.Congty_ID === $rootScope.login.u.congtyID);
                        if (curCtyIndex !== -1) {
                            $scope.Duyetall.Donvis.push($scope.RelCty[curCtyIndex]);
                        }
                        $scope.nofilterRelCty = $scope.RelCty;
                    }
                }
            });
        }
        $scope.GetPhongbanTree = function (congty_ID) {
            //$scope.Vanban.noiBanHanh = r.tenCongty;
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Dequy_PhongBan", pas: [
                        { "par": "Congty_ID", "va": congty_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                //
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.Phongbans = data[0];
                }
            });
        };
        //
    }
});