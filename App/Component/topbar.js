﻿// Component topbar
os.directive('onError1', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            element.on('error', function () {
                element.attr('src', attr.onError1);
            });
        }
    };
});
os.component('topbar', {
    bindings: {
        urlLink: '<'
    },
    templateUrl: domainUrl + 'App/Temp/TopBar.html?v=' + vhtmlcache,
    controller: function ($scope, $rootScope, $http, $stateParams, $state, $filter, Upload, $sce) {
        var $ctr = this;
        this.$onInit = function () {
            if ($rootScope.login) {
                $rootScope.BindListModule();
                $rootScope.BindListSendHub();
                $ctr.CheckUser();
                if((window === window.parent) ? false : true){
                    $('head').append('<link rel="stylesheet" href="'+domainUrl+'/App/Component/iframe.css" type="text/css" />');
                }
            }
        };
        this.goMenuSendHub = function (md) {
            var d=$rootScope.modules.find(x=>location.href.includes(x.IsLink));
            if(md==true && d){
                location.href = domainUrl + "#!/sendhub/"+d.Module_ID;  
            }else{
                location.href = domainUrl + "#!/sendhub";
            }
            
            //;
            //$rootScope.TenDuan = $rootScope.login.u.tenCongty;
            //$state.go('sendhub');
        }
        this.CheckUser = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "OS_CheckUser", pas: [
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                try{
                    if(res!=null && res.data!=null){
                        var users = JSON.parse(res.data.data);
                        if (!users || users[0][0].c == 0) {
                            confirm("Tài khoản của bạn hiện đang không hoạt động, vui lòng liên hệ Admin để kích hoạt lại!");
                            $rootScope.logOut();
                        }
                    }
                }catch(e){
                    
                }
            });
        };
        $rootScope.BindListModule = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "SOE_List_Module", pas: [
                        { "par": "Users_ID", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    $rootScope.modules = JSON.parse(res.data.data)[0];
                }
            });
        };
        function bindType(type){
            switch(type){
                case 6:
                case 12:
                    return "#tiendo";
                break;
                case 11:
                    return "#giahan";
                break;
            }
            return "";
        }
        $rootScope.BindListSendHub = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "SOE_List_SendHub", pas: [
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "p", "va": null },
                        { "par": "pz", "va": null },
                        { "par": "s", "va": null },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $rootScope.notifications = data[0];
                    angular.forEach($rootScope.notifications, function (item) {
                        if (item.loai === 0) {//Văn bản
                            item.href = 'http://sdoc.' + rootUrl + '#!/home/' + item.idKey;
                        }
                        else if (item.loai === 1) {//Chat
                            item.href = 'http://schat.' + rootUrl + '#!/chat/' + item.groupID;
                        }
                        else if (item.loai === 3) {//Lịch
                            item.href = 'http://scalendar.' + rootUrl + '#/'+ item.groupID +'/3/' + item.idKey;
                        }
                        else if (item.loai === 10) {//Dự án
                            item.href = 'http://stask.' + rootUrl + '/project/' + item.DuanID;
                        } else if (item.loai === 11) {//Công việc
                            item.href = 'http://stask.' + rootUrl + '/task/' + item.CongviecID+bindType(item.IsType);
                        }
                        else if (item.loai === 9) {//tintuc
                        item.href = 'http://snews.' + rootUrl + '#!/tintuc/' + item.idKey;
                        }
                        else if (item.loai === 2) {//Thông báo
                            item.href = 'http://snews.' + rootUrl + '#!/thongbao/' + item.idKey;
                        }
                        else if (item.loai === 6) {//Tài sản
                            item.href = 'http://sasset.' + rootUrl + '#!/viewtaisan/' + item.groupID + '/' + item.idKey;
                        }
                        else if (item.loai === 8) {//Văn phòng phẩm
                            item.href = 'http://stationery.' + rootUrl + '#!/viewvpp/' + item.groupID + '/' + item.idKey;
                        }
                        else if (item.loai === 5 && item.tieuDe === 'Lệnh điều xe') {//Lệnh điều xe
                            item.href = 'http://scar.' + rootUrl + '#!/chitietlenh/' + item.idKey;
                        }
                        else if (item.loai === 5 && item.tieuDe === 'Phiếu đặt xe') {//Lệnh điều xe
                            item.href = 'http://scar.' + rootUrl + '#!/chitietphieu/' + item.idKey;
                        }
                        else if (item.loai === 7) {//Đồng phục
                            item.href = 'http://suniform.' + rootUrl + '#!/viewdongphuc/' + item.groupID + '/' + item.idKey;
                        }
                        else if (item.loai === 13) {//Đề xuất
                            item.href = baseUrl+ '#/viewdexuat/' + item.idKey;
                        }
                    })
                    $rootScope.countNotifications = data[1][0].c;
                }
            });
        }
        this.UpdateIsReadSendHub = function (sh) {
            console.log(sh);
            if (sh.doc) {
                if(sh.href) location.href = sh.href;
            }
            else {
                $http({
                    method: "POST",
                    url: domainUrl + "/Home/Update_ReadSendHub",
                    data: { t: $rootScope.login.tk, id: sh.sendHubID },
                    contentType: 'application/json; charset=utf-8'
                }).then(function (res) {
                    if (!$rootScope.checkToken(res)) return false;
                    if (res.data.error !== 1) {
                        $rootScope.BindListSendHub();
                        $rootScope.BindListModule();
                        if (sh.href) location.href = sh.href;
                    }
                });
            }
        }
        this.logOut = function () {
            $http({
                method: "POST",
                url: "Home/delFirebase",
                data: { t: $rootScope.login.tk, Users_ID: $rootScope.login.u.NhanSu_ID, TokenCMID: $rootScope.TokenCMID },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {

                }
            });
            deleteAllCookies();
            setCookieUserDomain("");
            localStorage.removeItem("lo");
            localStorage.removeItem("u");
            location.href = domainUrl;
        };
        this.goMenuAccount = function () {
            location.href = domainUrl + "#!/account";
            //$rootScope.TenDuan = $rootScope.login.u.tenCongty;
            //$state.go('account');
        }
        this.goSwitchAcc = function () {
            location.href = domainUrl + "#!/switchacc";
        }
        this.trustAsHtml = function (html) {
            return $sce.trustAsHtml(html);
        }
        this.goHelper = function (f) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "ListModule", pas: [
                        { "par": "congty_ID", "va": $rootScope.login.u.congtyID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    data = JSON.parse(res.data.data);
                    $scope.lstmodules = data[0];
                    let mo = $scope.lstmodules.find(x => window.location.href.indexOf(x.IsLink) > -1);
                    var module_id = 'S00';
                    if (mo) {
                        module_id = mo.Module_ID;
                    }
                    window.open(
                        'https://shelp.soe.vn/' + "#!/helper/" + module_id,
                        '_blank'
                    );
                }
            });
        };

        $rootScope.bgColor = [
            "#2196f3", "#009688", "#ff9800", "#795548", "#ff5722"
        ];

    }
});