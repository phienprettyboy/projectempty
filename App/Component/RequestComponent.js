﻿os.component('requestcomponent', {
    bindings: {
        requestid: '<',
        uid: '<',
        rfFunction:'&'
    },
    templateUrl: baseUrl + 'App/Component/RequestComponent.html?v=' + vhtmlcache,
    controller: function ($scope, $rootScope, $http, $sce, $timeout) {
        var $ctrl = this;
        this.$onChanges = () => {
            if (this.requestid) {
               $scope.RequestTitle = null;
               init();
                $scope.goViewRequest({ RequestMaster_ID: this.requestid });
            }
        };
        $scope.goRFfunction = function () {
            $ctrl.rfFunction();
        };
        //Khai báo mặc định
        $scope.ListRequest = [];
        $scope.ListSort = ListSort;
        $scope.Uutiens = Uutiens;
        $scope.Trangthais = Trangthais;
        //Init Data
        function initTD() {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_Tudien", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.tudien = { Form: data[0], FormTeam: data[1], Team: data[2], FilterTeam: data[2] };
                }
            });
        };
        //Filter
        $scope.loadFormD = function (Form_ID) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_LoadFormD", pas: [
                        { "par": "Form_ID", "va": Form_ID },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.FormDS = data[0];
                    console.log($scope.FormDS);
                }
            });
        };
        $scope.FilterTeamForm = function () {
            var fo = $scope.tudien.Form.find(x => x.Form_ID == $scope.modelrequest.Form_ID);
            if (fo.IsUseAll) {
                $scope.tudien.FilterTeam = $scope.tudien.Team
            } else {
                $scope.tudien.FilterTeam = $scope.tudien.Team.filter(x => $scope.tudien.FormTeam.filter(a => a.Form_ID == $scope.modelrequest.Form_ID).length > 0);
            }
            var fo = $scope.tudien.Form.find(x => x.Form_ID == $scope.modelrequest.Form_ID);
            $scope.modelrequest.IsSLA = fo.IsSLA;
            $scope.modelrequest.IsChangeQT = fo.IsChangeQT;
            $scope.modelrequest.IsSkip = fo.IsSkip || false;
            $scope.modelrequest.IsQuytrinhduyet = fo.IsQuytrinhduyet;
            //Load Form D
            $scope.loadFormD($scope.modelrequest.Form_ID);
        };
        $scope.SelectTeam = function () {
            var fo = $scope.tudien.FormTeam.find(x => x.Form_ID == $scope.modelrequest.Form_ID && x.Team_ID == $scope.modelrequest.Team_ID);
            $scope.modelrequest.IsSLA = fo.IsSLA;
            $scope.modelrequest.IsChangeQT = fo.IsChangeQT;
            $scope.modelrequest.IsSkip = fo.IsSkip || false;
            $scope.modelrequest.IsQuytrinhduyet = fo.IsQuytrinhduyet || 1;
        };
        //File đính kèm
        $scope.removeFilesRequest = function (i, files) {
            files.splice(i, 1);
        };
        var delsFiles = [];
        $scope.DeleteFilesRequest = function (i, files) {
            delsFiles.push(files[i]);
            files.splice(i, 1);
        };
        $scope.openAttachfile = function (ev) {
            $(ev.target).closest("div.form-group").find("input[type='file']").trigger("click");
        };
        $scope.UploadFiles = function (files, f) {
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 100 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    files.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 100MB!'
                });
            }
        };
        //Send Request
        var arrrequest = [];
        function SelectRequest(d) {
            $scope.ListRequest.filter(x => x.Select).forEach(function (r) {
                r.Select = false;
            });
            if (d)
                d.Select = true;
        };
        $scope.OpenThuhoiHuyRequest = function (r, f) {
            $scope.dtitle = (f ? "Thu hồi" : "Hủy") + " phiếu đề xuất.";
            $scope.modelrequest = { IsTH: f, RequestMaster_ID: r.RequestMaster_ID };
            $("#ModelSendRequest").modal("show");
            $scope.IPTailieuFiles = [];
        };
        $scope.OpenSendRequest = function (d, title, issign) {
            if (d) {
                SelectRequest(d);
                arrrequest = [d];
            } else {
                arrrequest = $scope.ListRequest.filter(x => x.Chon);
            }
            if (arrrequest.length == 0) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Bạn chưa chọn phiếu đề xuất nào để gửi đi !'
                });
                return false;
            }
            //Nếu đang View Request
            if ($(".InfoViewRequest").hasClass("show") && $scope.viewRequest.Thanhviens.length == 0 && $scope.viewRequest.CountSign == 0 && $scope.viewRequest.IsChangeQT) {
                Swal.fire({
                    type: 'warning',
                    icon: 'warning',
                    title: '',
                    text: 'Vui lòng cấu hình người duyệt cho phiếu đề xuất trước khi gửi !'
                });
                return false;
            }
            $scope.dtitle = title + " phiếu đề xuất.";
            $scope.ModalSendUser = null;
            $scope.modelrequest = { IsSign: issign, IsSkip: d.IsSkip };
            if (d.IsSkip) {//Cho phép Skip
                if (issign == -1) {//Từ chối
                    $scope.ArraySendUsers = $scope.UserSigns.filter(x => x.IsClose == false && x.IsSign != 0);
                } else {//Chuyển tiếp
                    $scope.ArraySendUsers = $scope.UserSigns.filter(x => x.IsClose == false && x.IsSign == 0 && x.Trangthai == false);
                }
                if ($scope.ArraySendUsers && $scope.ArraySendUsers.length > 0) {
                    $scope.ArraySendUsers.sort((a, b) => a.STT - b.STT);
                    $scope.ModalSendUser = $scope.ArraySendUsers[0];
                }
            }
            $("#ModelSendRequest").modal("show");
            $scope.IPTailieuFiles = [];
            setdropZone("drop-zonesend");
        };
        $scope.chonUserSend = function (u) {
            $scope.ModalSendUser = u;
        };
        $scope.SendRequest = function () {
            if ($scope.modelrequest.IsTH == true) {//Thu hồi
                $scope.RollBack_Request($scope.modelrequest);
                return false;
            } else if ($scope.modelrequest.IsTH == false) {//Hủy
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Form/SendNext_Request",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    formData.append("NoidungDuyet", $scope.modelrequest.NoidungDuyet || "");
                    if ($scope.modelrequest.IsSign)
                        formData.append("IsSign", $scope.modelrequest.IsSign);
                    formData.append("ids", arrrequest.map(x => x.RequestMaster_ID).join(","));
                    if ($("#ModelSendRequest input[type='file']")[0]) {
                        $.each($("#ModelSendRequest input[type='file']")[0].files, function (i, file) {
                            formData.append('file', file);
                        });
                    }
                    if ($scope.ModalSendUser) {
                        formData.append("RequestSignUser_ID", $scope.ModalSendUser.RequestSignUser_ID);
                    }
                    return formData;
                }
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi gửi đề xuất !'
                    });
                    return false;
                }
                $("#ModelSendRequest").modal("hide");
                showtoastr('Đã gửi phiếu đề xuất thành công!.');
                $("#ModelSendRequest input[type='file']").val("");
                $scope.goRFfunction();
                //Nếu đang View Request
                if ($("#viewRequest .InfoViewRequest").hasClass("show")) {
                    $scope.goViewRequest($scope.viewRequest);
                }
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        //File
        $scope.listFiles = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_File", pas: [
                        { "par": "RequestMaster_ID ", "va": $scope.viewRequest.RequestMaster_ID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.LisFileAttachRQ = data[0];
                }
            });
        };
        $scope.openModalAddFileCV = function (cv) {
            $scope.IPTailieuFiles = [];
            filecv = true;
            $("#ModalFileUpload").modal("show");
        };
        $scope.PutFileUpload = function (f) {
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 100 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Tenfile = fi.name;
                    fi.Dungluong = fi.size;
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    $scope.IPTailieuFiles.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 100MB!'
                });
            }
        };
        function setdropZone(idk) {
            var dropZone = document.getElementById(idk);
            if (dropZone) {
                var startUpload = function (files) {
                    $scope.PutFileUpload(files);
                    //if ($scope.$$phase) {
                    $scope.$apply();
                    //}
                };
                dropZone.ondrop = function (e) {
                    e.preventDefault();
                    this.className = 'upload-drop-zone';
                    startUpload(e.dataTransfer.files);
                };
                dropZone.ondragover = function () {
                    this.className = 'upload-drop-zone drop';
                    return false;
                };
                dropZone.ondragleave = function () {
                    this.className = 'upload-drop-zone';
                    return false;
                };
                dropZone.onclick = function () {
                    $("#" + dropZone.getAttribute("mid") + " input[type='file']").trigger("click");
                };
            }
        };
        $(document).ready(function () {
            setdropZone("drop-zone");
            //setdropZone("drop-zonesend");
        });
        $scope.UploadFileCV = function () {
            if ($scope.IPTailieuFiles.length === 0) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng chọn file để upload !'
                });
                return false;
            }
            $scope.loadding = true;
            //swal.showLoading();
            //Progress File
            var hasfile = $scope.IPTailieuFiles && $scope.IPTailieuFiles.length > 0;
            if (!hasfile) {
                swal.showLoading();
            } else {
                $("#modalfileProgress").modal("show");
                var fileProgress = document.getElementById("fileProgress");
                var desProgress = document.getElementById("podes");
                fileProgress.setAttribute("value", 0);
                fileProgress.setAttribute("max", 0);
                fileProgress.style.display = "block";
            }
            $http({
                method: 'POST',
                url: "Form/UploadFile",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    formData.append("RequestMaster_ID", $scope.viewRequest.RequestMaster_ID || null);
                    $.each($scope.IPTailieuFiles, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                }, uploadEventHandlers: {
                    progress: function (e) {
                        if (hasfile && e.lengthComputable) {
                            fileProgress.setAttribute("value", e.loaded);
                            fileProgress.setAttribute("max", e.total);
                            var po = Math.ceil(e.loaded * 100 / e.total);
                            desProgress.innerHTML = po + "%";
                        }
                    }
                }
            }).then(function (res) {
                $scope.loadding = false;
                if (!hasfile)
                    closeswal();
                $("#modalfileProgress").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {

                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi upload file !'
                    });
                    return false;
                }
                $("#ModalFileUpload").modal("hide");
                $scope.IPTailieuFiles = [];
                showtoastr("Upload File thành công!");
                $scope.listFiles();
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        //Comment
        $scope.first = true;
        $scope.listComments = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_ListComment", pas: [
                        { "par": "RequestMaster_ID ", "va": $scope.viewRequest.RequestMaster_ID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    data[0].forEach(function (r) {
                        if (r.files) {
                            r.files = JSON.parse(r.files);
                        }
                    });
                    if (!$scope.first) {
                        $scope.first = false;
                    }
                    $scope.Comments = data[0];
                    $scope.Comments.filter(x => x.ParentID != null).forEach(function (r) {
                        r.Comments = $scope.Comments.filter(x => x.ParentID === r.RequestComment_ID);
                        r.ParentComment = $scope.Comments.find(x => r.ParentID === x.RequestComment_ID);
                    });
                }
            });
        };
        $scope.cart = cart;
        $scope.emojis = emojis;
        $scope.resetemojiIndex = function () {
            $scope.emojiIndex = 0;
        }
        $scope.setemojiIndex = function (idx) {
            $scope.emojiIndex = idx;
        }
        $scope.emojiIndex = 0;
        $scope.noiDungChat = {
            Noidung: ''
        };
        $scope.openImageChat = function () {
            $("#ChatCtr input.inputimgChat").click();
        }
        $scope.openFileChat = function (ipn) {
            $("#ChatCtr input." + (ipn || inputfileChat)).click();
        }

        $scope.UploadFileChat = function (f) {
            if (!$scope.FileAttach) $scope.FileAttach = [];
            if ($scope.FileAttach.length + f.length > 5) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 5 file cho dự án!'
                });
                return false;
            }
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 100 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    fi.Tenfile = fi.name;
                    fi.Dungluong = fi.size;
                    $scope.FileAttach.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 100MB!'
                });
            } else {
                if (!ReplyID)
                    $scope.goBottomChat();
            }
        }
        $scope.openFileChatDynamic = function (id) {
            $(id).click();
        }

        $scope.UploadFileChatDynamic = function (f) {
            if (!$scope.FileAttachBC) $scope.FileAttachBC = [];
            if ($scope.FileAttachBC.length + f.length > 10) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 10 file một lần!'
                });
                return false;
            }
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 100 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    fi.Tenfile = fi.name;
                    fi.Dungluong = fi.size;
                    $scope.FileAttachBC.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 100MB!'
                });
            } else {
                if (!ReplyID)
                    $scope.goBottomChat();
            }
        }


        $scope.setEmoji = function (e, type) {
            if (type == false) {
                var noidung = $scope.noiDungChat.Noidung || "";
                var noiDungChat = noidung + e.emoji;
                $scope.noiDungChat.Noidung = noiDungChat;
            } else {
                $scope.noiDungChat.Noidung = e;
            }
            $("#noiDungChat").focus();
        }

        $scope.goBottomChat = function () {
            setTimeout(function () {
                var div = document.getElementById("taskmessagepanel");
                div.scrollTop = div.scrollHeight - div.clientHeight;
            }, 100)
        }
        var ReplyID = null;
        $scope.Reply = function (co) {
            $scope.Comments.filter(x => x.IsReply).forEach(function (r) {
                r.IsReply = false;
            });
            $scope.IsReply = true;
            ReplyID = co.RequestComment_ID;
            co.IsReply = true;
            $scope.tinnhanreply = co;
        };
        $scope.HuyReply = function () {
            var co = $scope.Comments.find(x => x.RequestComment_ID === ReplyID);
            $scope.IsReply = false;
            ReplyID = null;
            $scope.tinnhanreply = null;
            co.IsReply = false;
            $scope.FileAttach = [];
        };
        $scope.EditComment = function (co) {

        };
        $scope.Comments = [];
        $scope.sendMS = function (loai) {
            if ($scope.loadding) {
                return false;
            }
            if (!$scope.noiDungChat.Noidung && $scope.FileAttach.length === 0) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập nội dung comment !'
                });
                return false;
            }
            $scope.loadding = true;
            var ms = { ParentID: ReplyID, RequestMaster_ID: $scope.viewRequest ? $scope.viewRequest.RequestMaster_ID : null, NguoiTao: $rootScope.login.u.NhanSu_ID, Noidung: $scope.noiDungChat.Noidung, NgayTao: new Date(), fullName: $rootScope.login.u.fullName, anhThumb: $rootScope.login.u.anhDaiDien, IsType: 0, IsDelete: false };
            ms.files = $scope.FileAttach;
            $scope.Comments.push(ms);
            $scope.Comments.filter(x => x.ParentID != null).forEach(function (r) {
                r.Comments = $scope.Comments.filter(x => x.ParentID === r.RequestComment_ID);
                r.ParentComment = $scope.Comments.find(x => r.ParentID === x.RequestComment_ID);
            });
            $scope.noiDungChat.Noidung = "";
            $scope.goBottomChat();
            //Progress File
            var hasfile = $scope.FileAttach && $scope.FileAttach.length > 0;
            if (!hasfile) {
                swal.showLoading();
            } else {
                $("#modalfileProgress").modal("show");
                var fileProgress = document.getElementById("fileProgress");
                var desProgress = document.getElementById("podes");
                fileProgress.setAttribute("value", 0);
                fileProgress.setAttribute("max", 0);
                fileProgress.style.display = "block";
            }
            $http({
                method: 'POST',
                url: "Form/Update_Comment",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    var $html = $("#noiDungChatBC");
                    var ids = [];
                    $html.find("uid").each(function (i, el) {
                        ids.push(el.innerText);
                        el.remove();
                    });
                    ms.Noidung = $html.html();
                    formData.append("model", JSON.stringify(ms));
                    if (ids.length > 0) {
                        formData.append("ids", ids.join(","));
                    }
                    $.each($scope.FileAttach, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                },
                uploadEventHandlers: {
                    progress: function (e) {
                        if (hasfile && e.lengthComputable) {
                            fileProgress.setAttribute("value", e.loaded);
                            fileProgress.setAttribute("max", e.total);
                            var po = Math.ceil(e.loaded * 100 / e.total);
                            desProgress.innerHTML = po + "%";
                        }
                    }
                }
            }).then(function (res) {
                $scope.loadding = false;
                if (!hasfile)
                    closeswal();
                $("#modalfileProgress").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi gửi thảo luận !'
                    });
                    return false;
                }
                $scope.Comments[$scope.Comments.length - 1].RequestComment_ID = res.data.RequestComment_ID;
                if ($scope.FileAttach != null && $scope.FileAttach.length > 0) {
                    $scope.listComments();
                }
                $scope.FileAttach = [];
                $('input[name=FileAttachChat]').val("");
                if (ReplyID) $scope.HuyReply();
                $("textarea#noiDungChat").val(null);
                window.parent.postMessage({ Type: 1, Isdel: false }, "*");
            }, function (response) { // optional
                $scope.loadding = false;
            });
        }

        $scope.Del_Comment = function (cm, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa comment này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Del_Comment",
                        data: {
                            t: $rootScope.login.tk, id: cm.RequestComment_ID
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.Comments.splice(i, 1);
                        }
                    });
                }
            })

        };
        $scope.removeFilesComment = function (files, i) {
            files.splice(i, 1);
        };
        $scope.Del_CommentFile = function (files, i, f) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa file này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Del_RequestFile",
                        data: { t: $rootScope.login.tk, ids: [f.FileID] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            files.splice(i, 1);
                            showtoastr("File bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa File không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        // Chọn User
        var IsTypeTD = 0;
        $scope.showusersModal = function (index, f) {
            //2 Thêm người duyệt khi view/0 Thêm người theo dõi 1/ Thêm quản lý khi view/ 3 Thêm quản lý. 4 Cấu hình Duyệt
            $rootScope.phongbans.filter(r => r.isCheck).forEach(function (r) {
                r.isCheck = false;
            });
            $rootScope.users.filter(r => r.isCheck).forEach(function (r) {
                r.isCheck = false;
            });
            $scope.vbphongbans = $rootScope.phongbans;
            $scope.vbusers = $rootScope.users;
            $scope.one = f;
            // set chọn user trong lấy list user config cho quy trình
            IsTypeTD = index;
            $("#usersModal").modal("show");
        };

        $scope.choiceUser = function (users) {
            if (IsTypeTD == 0 || IsTypeTD == 1) {
                users = users.filter(u => $scope.theodois.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                var tds = $scope.theodois.concat(users);
                swal.showLoading();
                $http({
                    method: "POST",
                    url: "Form/Update_Theodoi",
                    data: { t: $rootScope.login.tk, RequestMaster_ID: $scope.viewRequest.RequestMaster_ID, uds: tds.map(a => a["NhanSu_ID"]), IsType: IsTypeTD },
                    contentType: 'application/json; charset=utf-8'
                }).then(function (res) {
                    closeswal();
                    if (!$rootScope.checkToken(res)) return false;
                    if (res.data.error !== 1) {
                        $("#usersModal").modal("hide");
                        $scope.getSignLog($scope.viewRequest);
                    } else {
                        Swal.fire({
                            icon: 'error',
                            type: 'error',
                            title: '',
                            text: 'Thêm người ' + (IsTypeTD == 0 ? "theo dõi" : "quản lý") + ' không thành công, vui lòng thử lại!'
                        });
                    }
                });
            } else {
                $("#usersModal").modal("hide");
                if (IsTypeTD == 3) {

                    $scope.quanlys = $scope.quanlys.concat(users);
                } else if (IsTypeTD == 4) {
                    $scope.duyets = $scope.duyets.concat(users);
                }
            }
        };

        $scope.DelTheodoi = function (u, k) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa người " + (k == 0 ? 'Theo dõi ' : 'Quản lý ') + "này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    swal.showLoading();
                    $http({
                        method: "POST",
                        url: "Form/Del_Theodoi",
                        data: { t: $rootScope.login.tk, RequestMaster_ID: $scope.viewRequest.RequestMaster_ID, NhanSu_ID: u.NhanSu_ID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        closeswal();
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            var idx = $scope.theodois.findIndex(a => a.NhanSu_ID == u.NhanSu_ID);
                            if (idx !== -1) {
                                $scope.theodois.splice(idx, 1);
                            }
                            showtoastr("Xóa người " + (k == 0 ? 'Theo dõi ' : 'Quản lý ') + "thành công");
                        }
                    });
                }
            })
        };

        $scope.Update_NotyTheodoi = function (u) {
            u.IsNoty = !(u.IsNoty || false);
            $http({
                method: "POST",
                url: "Form/Update_NotyTheodoi",
                data: { t: $rootScope.login.tk, RequestMaster_ID: $scope.viewRequest.RequestMaster_ID, NhanSu_ID: u.NhanSu_ID, Noty: u.IsNoty },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    showtoastr("Cập nhật trạng thái nhận thông báo thành công");
                }
            });
        };
        //@Tag người
        function setEndOfContenteditable(contentEditableElement) {
            var range, selection;
            if (document.createRange)//Firefox, Chrome, Opera, Safari, IE 9+
            {
                range = document.createRange();//Create a range (a range is a like the selection but invisible)
                range.selectNodeContents(contentEditableElement);//Select the entire contents of the element with the range
                range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
                selection = window.getSelection();//get the selection object (allows you to change selection)
                selection.removeAllRanges();//remove any selections already made
                selection.addRange(range);//make the range you have just created the visible selection
            }
            else if (document.selection)//IE 8 and lower
            {
                range = document.body.createTextRange();//Create a range (a range is a like the selection but invisible)
                range.moveToElementText(contentEditableElement);//Select the entire contents of the element with the range
                range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
                range.select();//Select the range (make it the visible selection
            }
        }
        $scope.completecm = function ($event) {
            txtstring = $event.target.innerHTML;
            string = $event.target.innerText.trim();
            if (!string.endsWith("@")) return false;
            var arrs = string.split("@");
            string = arrs[arrs.length - 1];
            $scope.viewRequest.Focuscm = true;
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                $("ul.autoUsercm").focus();
                $event.preventDefault();
            }
            string = string.replace("@", "");
            var output = [];
            if (!string) {
                string = "";
            }
            angular.forEach($scope.thanhviens, function (u) {
                if ((u.fullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.enFullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.tenTruyCap || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.phone + '').toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    output.push(u);
                }
            });
            $scope.filterUsercm = output.filter(x => x.NhanSu_ID !== $rootScope.login.u.NhanSu_ID);
        };
        $scope.clickAddUsercm = function (u) {
            $scope.filterUsercm = null;
            var string = $scope.noiDungChat.Noidung;
            string = string.substring(0, string.lastIndexOf("@") - 1) + " <user><uid>" + u.NhanSu_ID + "</uid>@" + u.fullName + "</user>";
            $scope.noiDungChat.Noidung = string + "&nbsp;";
            setTimeout(function () {
                elem = document.getElementById('noiDungChatBC');//This is the element that you want to move the caret to the end of
                setEndOfContenteditable(elem);
            })
        };
        $scope.handleKeyDowncm = function ($event) {
            var objCurrentLi, obj = $('ul.autoUsercm').find('.SelectedLi'), objUl = $('autoUsercm'), code = ($event.keyCode ? $event.keyCode : $event.which);
            if (code === 40) {
                // Down
                $("ul.autoUsercm").focus();
                $event.preventDefault();
                if ((obj.length === 0) || (objUl.find('li:last').hasClass('SelectedLi') === true)) {
                    objCurrentLi = objUl.find('li:first').addClass('SelectedLi');
                }
                else {
                    objCurrentLi = obj.next().addClass('SelectedLi');
                }
                obj.removeClass('SelectedLi');
            }
            else if (code === 38) {
                // Up
                $event.preventDefault();
                if ((obj.length === 0) || (objUl.find('li:first').hasClass('SelectedLi') === true)) {
                    objCurrentLi = objUl.find('li:last').addClass('SelectedLi');
                }
                else {
                    objCurrentLi = obj.prev().addClass('SelectedLi');
                }
                obj.removeClass('SelectedLi');
            }
            else if (code === 13) {
                // Enter
                $event.preventDefault();
                obj = $('ul.autoUser' + i).find('.SelectedLi');
                if (obj) {
                    var u = $scope.filterUser.find(x => x.NhanSu_ID == obj.attr("uid"));
                    $scope.clickAddUsercm(u);
                }
            }
        };
        //@Tag quản lý,quy trình duyệt
        $scope.removeUser = function (us, idx) {
            us.splice(idx, 1);
        };
        $scope.ClearFocus = function () {
            $("#ModalRequestAdd .autoUser").hide();
        }
        $scope.clickAddUser = function (m, us, u, i) {
            $scope.filterUser = null;
            if (!us) {
                us = [];
            }
            if (us.filter(x => x.NhanSu_ID == u.NhanSu_ID).length == 0)
                us.push(u);
            m["searchU" + i] = "";
            setTimeout(function () {
                elem = document.querySelector("input.ipautoUser" + i);//This is the element that you want to move the caret to the end of
                setEndOfContenteditable(elem);
                elem.focus();
                elem.innerText = "";
            })
        };
        $scope.handleKeyDown = function (m, us, i, $event) {
            var objCurrentLi, obj = $('ul.autoUser' + i).find('.SelectedLi'), objUl = $('ul.autoUser' + i), code = ($event.keyCode ? $event.keyCode : $event.which);
            if (code === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
                if ((obj.length === 0) || (objUl.find('li:last').hasClass('SelectedLi') === true)) {
                    objCurrentLi = objUl.find('li:first').addClass('SelectedLi');
                }
                else {
                    objCurrentLi = obj.next().addClass('SelectedLi');
                }
                obj.removeClass('SelectedLi');
            }
            else if (code === 38) {
                // Up
                $event.preventDefault();
                if ((obj.length === 0) || (objUl.find('li:first').hasClass('SelectedLi') === true)) {
                    objCurrentLi = objUl.find('li:last').addClass('SelectedLi');
                }
                else {
                    objCurrentLi = obj.prev().addClass('SelectedLi');
                }
                obj.removeClass('SelectedLi');
            }
            else if (code === 13) {
                // Enter
                $event.preventDefault();
                obj = $('ul.autoUser' + i).find('.SelectedLi');
                if (obj) {
                    var u = $scope.filterUser.find(x => x.NhanSu_ID == obj.attr("uid"));
                    $scope.clickAddUser(m, us, u, i);
                }
            }
        };

        $scope.complete = function (string, m, $event, i) {
            if ($("ul.autoUser" + i).css('display') == 'none')
                $("ul.autoUser" + i).show();
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            if (!string) {
                string = "";
            }
            string = string.replace("@", "");
            if (m) {
                for (var k = 0; k <= 5; k++) {
                    m["Focus" + k] = false;
                }
                m["Focus" + i] = true;
            }
            var output = [];
            if (!string) {
                string = "";
            }
            angular.forEach($rootScope.users, function (u) {
                if ((u.fullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.enFullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.tenTruyCap || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.phone + '').toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    if (!m || m.NhanSu_ID !== u.NhanSu_ID)
                        output.push(u);
                }
            });
            $scope.filterUser = output;
        };
        //Action

        $scope.goViewRequest = function (r) {
            SelectRequest(r);
            //
            $scope.FormDS = [];
            $scope.Ftables = [];
            $scope.vanBanMasterID = null;
            $scope.isopenadddoc = false;
            $scope.isopendoc = false;
            $scope.isopen = false;
            $scope.isopentask = false;
            //
            $scope.RQJobs = [];
            $scope.CongviecID = null;
            $scope.Comments = [];
            $scope.ChartsSigns = [];
            $scope.Logs = [];
            $scope.totalDisplayedLog = 20;
            $scope.LisFileAttachRQ = [];
            $scope.Vanbans = [];
            $scope.Congviecs = [];
            $scope.Lichs = [];
            //
            if (!r.IsFun || !r.IsTao)
                $("#aeditrq").hide();
            $('a[data-target="#viewRequestAA"]').trigger("click");
            $("#viewRequest .InfoViewRequest").addClass('show');
            $("#viewRequest #info-overlay").addClass('show');
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_Get", pas: [
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "RequestMaster_ID ", "va": r.RequestMaster_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $("#viewRequest .InfoViewRequest #loader").hide();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    data[0].forEach(function (r) {
                        r.objTrangthai = TrangthaiViews.find(x => x.id == r.Trangthai);
                        if (r.Thanhviens) {
                            r.Thanhviens = JSON.parse(r.Thanhviens);
                            if (r.Signs) {
                                r.Signs = JSON.parse(r.Signs);
                                var tong = 0;
                                var tc = 0;
                                r.Signs.forEach(function (ro) {
                                    ro.Thanhviens = r.Thanhviens.filter(x => x.RequestSign_ID == ro.RequestSign_ID);
                                    if (ro.IsTypeDuyet == 0) {//Duyệt 1 trong nhiều
                                        ro.USign = ro.Thanhviens.filter(x => x.IsSign != 0);
                                        ro.IsShowTV = ro.Thanhviens.filter(x => x.IsSign != 0 && x.IsType != 4).length == 0;
                                        ro.Thanhviens = ro.Thanhviens.filter(x => x.IsSign == 0);
                                       // tong += (ro.IsShowTV ? 0 : 1);
                                        //tc += 1;
                                    } else {
                                       // tong += ro.Thanhviens.filter(x => x.IsSign != 0 && !x.IsClose).length;
                                        //tc += ro.Thanhviens.filter(x => !x.IsClose).length;
                                    }
                                });
                                //r.Tiendo = Math.ceil(tong * 100 / tc) || 0;
                                r.color = renderColor(r.Tiendo);
                                r.txtcolor = renderTxtColor(r.Tiendo);
                                r.IsLast = tong + 1 === tc;
                            }
                        }
                    });
                    $scope.viewRequest = data[0][0];
                    $scope.viewRequest.IsViewXL = r.IsViewXL;
                    //Load FormD
                    if (data[1] != null && data[1].length > 0) {
                        $scope.FormDS = data[1].filter(x => x.STTRow == null);;
                        var fd = data[1].find(x => x.KieuTruong == "radio" && x.IsGiatri == "true");
                        if (fd != null) {
                            $scope.viewRequest.Radio = fd.FormD_ID;
                        }
                        var Ftables = [];
                        data[1].filter(x => x.IsType == 3).forEach(function (r) {
                            Ftables.push([]);
                            var groups = groupBy(data[1].filter(x => x.STTRow != null && x.IsParent_ID == r.FormD_ID), "STTRow");
                            for (var k in groups) {
                                var fr = [];
                                groups[k].forEach(function (rr) {
                                    fr.push({ ...rr });
                                });
                                Ftables[Ftables.length - 1].push(fr);
                            }
                            $scope.Ftables = Ftables;
                        });
                    }
                    if (!$scope.viewRequest.IsFun || !$scope.viewRequest.IsTao)
                        $("#aeditrq").remove();
                    else
                        $("#aeditrq").show();

                    $timeout(function () {
                        $scope.listComments();
                        $scope.listFiles();
                        $scope.listRelated(r);
                        $scope.getSignLog(r);
                        $scope.setViewRequest(r);
                        $scope.listDateline(r);
                        $scope.listJob(r);
                    }, 500);

                }
            });
            if ($scope.RequestMaster_ID != r.RequestMaster_ID)
                $scope.RequestMaster_ID = r.RequestMaster_ID;
        };
        $scope.closeInfoRequest = function () {
            $("#viewRequest .InfoViewRequest").removeClass('show');
            $("#viewRequest #info-overlay").removeClass('show');
            $scope.viewRequest = null;
            $scope.ChartsSigns = [];
            $scope.Logs = [];
            SelectRequest();
        };
        $scope.fullInfoRequest = function () {
            $("#viewRequest .InfoViewRequest").toggleClass("fulliframe");
        };
        $scope.DelRequest = function (u, f) {
            var ids = [];
            if (u) {
                ids.push(u.RequestMaster_ID);
            } else {
                ids = $scope.ListRequest.filter(x => x.Chon).map(a => a["RequestMaster_ID"]);
            }
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa yêu cầu - đề xuất này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Del_Request",
                        data: { t: $rootScope.login.tk, ids: ids },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.Refresh();
                            if (f === 1) {
                                $scope.closeInfoRequest();
                            }
                            showtoastr("Đã xóa yêu cầu - đề xuất thành công!");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.StopRequest = function (v) {
            var ids = [v["RequestMaster_ID"]];
            //if (u) {
            //    ids.push(u.RequestMaster_ID);
            //} else {
            //    ids = $scope.ListRequest.filter(x => x.Chon).map(a => a["RequestMaster_ID"]);
            //}
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn hủy yêu cầu - đề xuất này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Cancel_Request",
                        data: { t: $rootScope.login.tk, ids: ids },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.Refresh();
                            //if (f === 1) {
                            //    $scope.closeInfoRequest();
                            //}
                            //Nếu đang View Request
                            if ($(".InfoViewRequest").hasClass("show")) {
                                $scope.goViewRequest(v);
                            }
                            showtoastr("Đã hủy yêu cầu - đề xuất thành công!");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Hủy không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        }; 
        $scope.RollBack_Request = function (u) {
            var ids = [];
            if (u) {
                ids.push(u.RequestMaster_ID);
            } else {
                ids = $scope.ListRequest.filter(x => x.Chon).map(a => a["RequestMaster_ID"]);
            }
            $http({
                method: "POST",
                url: "Form/RollBack_Request",
                data: { t: $rootScope.login.tk, ids: ids, NoidungDuyet: u.NoidungDuyet, RequestSignUser_ID: $scope.ModalSendUser != null ? $scope.ModalSendUser.RequestSignUser_ID : null },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    $("#ModelSendRequest").modal("hide");
                    $scope.Refresh();
                    //Nếu đang View Request
                    if ($(".InfoViewRequest").hasClass("show")) {
                        $scope.goViewRequest($scope.viewRequest);
                    }
                    showtoastr("Đã thu hồi yêu cầu - đề xuất thành công!");
                } else {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Thu hồi không thành công, vui lòng thử lại!'
                    });
                }
            });
        };
        $scope.BackRequest = function (v) {
            var ids = [v["RequestMaster_ID"]];
            //var ids = [];
            //if (u) {
            //    ids.push(u.RequestMaster_ID);
            //} else {
            //    ids = $scope.ListRequest.filter(x => x.Chon).map(a => a["RequestMaster_ID"]);
            //}
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn bỏ hủy yêu cầu - đề xuất này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Back_Request",
                        data: { t: $rootScope.login.tk, ids: ids },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.Refresh();
                            //if (f === 1) {
                            //    $scope.closeInfoRequest();
                            //}
                            //Nếu đang View Request
                            if ($(".InfoViewRequest").hasClass("show")) {
                                $scope.goViewRequest(v);
                            }
                            showtoastr("Đã bỏ hủy yêu cầu - đề xuất thành công!");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Bỏ hủy không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        }; 

        $scope.AddRequest = function (frm, fc) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Form/Update_RequestMaster",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    debugger;
                    var formData = new FormData();
                    if ($scope.modelrequest.Ngaylap) {
                        $scope.modelrequest.Ngaylap = moment($scope.modelrequest.Ngaylap).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                    }
                    formData.append("t", $rootScope.login.tk);
                    formData.append("model", JSON.stringify($scope.modelrequest));
                    if ($scope.FormDS != null && $scope.FormDS.length > 0) {
                        var cparr = [...$scope.FormDS];
                        cparr.filter(x => x.KieuTruong == "radio").forEach(function (r) {
                            if (r.FormD_ID == $scope.modelrequest.Radio) {
                                r.IsGiatri = true;
                            } else {
                                r.IsGiatri = false;
                            }
                        });
                        cparr.filter(x => x.KieuTruong == "datetime" && x.IsGiatri != null && isValidDate(x.IsGiatri)).forEach(function (r) {
                            r.IsGiatri = moment(r.IsGiatri).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                        });
                        cparr.filter(x => x.KieuTruong == "date" && x.IsGiatri != null && isValidDate(x.IsGiatri)).forEach(function (r) {
                            r.IsGiatri = moment(r.IsGiatri).tz(timezone).format('YYYY-MM-DD');
                        });
                        formData.append("FormDS", JSON.stringify(cparr));
                    }
                    if ($scope.quanlys && $scope.quanlys.length > 0)
                        formData.append("quanlys", $scope.quanlys.map(a => a.NhanSu_ID).join(","));
                    if ($scope.duyets && $scope.duyets.filter(x => x.RequestSignUser_ID == null).length > 0)
                        formData.append("duyets", $scope.duyets.map(a => a.NhanSu_ID).join(","));
                    $.each($("#frRequest input[type='file']")[0].files, function (i, file) {
                        formData.append('file', file);
                    });
                    if (delsFiles.length > 0) {
                        formData.append("dels", delsFiles.map(a => a.FileID).join(","));
                    }
                    return formData;
                }
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi lập đề xuất mới !'
                    });
                    return false;
                }
                $scope.modelrequest.Title = "";
                if (!fc)
                    $("#ModalRequestAdd").modal("hide");
                showtoastr('Đã cập nhật phiếu đề xuất thành công!.');
                $("#frRequest input[type='file']").val("");
                $scope.goRFfunction();
                //Nếu đang View Request
                if ($(".InfoViewRequest").hasClass("show")) {
                    $scope.goViewRequest($scope.modelrequest);
                }
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        $scope.trustAsHtml = function (html) {
            if (!html) {
                return "";
            }
            return $sce.trustAsHtml(html.replace(/\n/g, "<br/>"));
        };
        $scope.showChartSign = function (u) {
            $scope.modelChart = u;
            $scope.ChartsSigns = u.Thanhviens;
            $scope.Logs = [];
            $scope.getSignLog(u);
            $("#chartSignModal").modal("show");
        };
        $scope.getSignLog = function (re) {
            $http({
                method: "POST",
                url: "/Home/callProc",
                data: {
                    t: $rootScope.login.tk,
                    proc: "Request_ListLog",
                    pas: [
                        { "par": "RequestMaster_ID", "va": re.RequestMaster_ID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                var data = JSON.parse(res.data.data);
                data[0].forEach(function (r) {
                    let Thanhviens = data[1].filter(x => x.RequestSign_ID == r.RequestSign_ID);
                    if (r.IsTypeDuyet == 0) {//Duyệt 1 trong nhiều
                        r.USign = Thanhviens.filter(x => x.IsSign != 0);
                        r.IsShowTV = re.Trangthai != 2 && Thanhviens.filter(x => x.IsSign > 0 && x.IsType != 4).length == 0;
                        r.Thanhviens = Thanhviens.filter(x => x.IsSign == 0);
                    }
                    else {
                        r.IsShowTV = true;
                        r.Thanhviens = data[1].filter(x => x.RequestSign_ID == r.RequestSign_ID);
                    }
                    r.SoThanhvien = Thanhviens.filter(u => !u.IsClose || u.IsSign != 0).length;
                });
                if (data[0].length == 0 && data[1].length > 0) {
                    var r = { GroupName: "Quy trình động", IsTypeDuyet: re.IsQuytrinhduyet };
                    let Thanhviens = data[1].filter(x => x.RequestSign_ID == null && x.IsType == 5);
                    if (r.IsTypeDuyet == 0) {//Duyệt 1 trong nhiều
                        r.USign = Thanhviens.filter(x => x.IsSign != 0);
                        r.IsShowTV = re.Trangthai != 2 && Thanhviens.filter(x => x.IsSign > 0 && x.IsType != 4).length == 0;
                        r.Thanhviens = Thanhviens.filter(x => x.IsSign == 0);
                    }
                    else {
                        r.IsShowTV = true;
                        r.Thanhviens = data[1].filter(x => x.RequestSign_ID == null && x.IsType == 5);
                    }
                    r.SoThanhvien = r.Thanhviens.filter(u => !u.IsClose || u.IsSign != 0).length;
                    data[0] = [r];
                }
                $scope.ChartsSigns = data[0];
                $scope.UserSigns = data[1];
                $scope.Logs = data[2];
                $scope.thanhviens = data[3];
                $scope.theodois = data[4];
            },
                function (response) {
                });
        };
        //Load More
        $scope.totalDisplayedLog = 20;
        $scope.loadMoreLog = function () {
            if ($scope.Logs.length > $scope.totalDisplayedLog)
                $scope.totalDisplayedLog += 20;
        };
        $scope.totalDisplayedRelate = 20;
        $scope.loadMoreRelate = function () {
            if ($scope.relates.length > $scope.totalDisplayedRelate)
                $scope.totalDisplayedRelate += 20;
        };
        //Relate
        $scope.setTabVB = function (type) {
            $scope.TabVB = type;
        }
        $scope.setTabCV = function (type) {
            $scope.TabCV = type;
        }
        $scope.setTabLich = function (type) {
            $scope.TabLich = type;
        }
        $scope.changeLoaiVB = function (loai) {
            $scope.mrelate.loai = loai;
            $scope.listRelatelienquan($scope.mrelate);
        };
        $scope.listRelatelienquan = function (rl) {
            swal.showLoading();
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_ListTDRelate", pas: [
                        { "par": "Module_ID ", "va": rl.Module_ID },
                        { "par": "RequestMaster_ID ", "va": rl.RequestMaster_ID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "loai ", "va": rl.loai },
                        { "par": "s ", "va": rl.rlsearch }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    if (rl.Module_ID == "stask") {
                        data.forEach(function (r) {
                            try {
                                if (r.Thanhviens)
                                    r.Thanhviens = JSON.parse(r.Thanhviens);
                            } catch (e) {
                            }
                            try {
                                if (r.Tags)
                                    r.Tags = JSON.parse(r.Tags);
                            } catch (e) {
                            }
                            r.color = renderColor(r.Tiendo);
                            r.txtcolor = renderTxtColor(r.Tiendo);
                            r.NgayHan = Math.abs(r.NgayHan);
                            r.Tao = r.NguoiTao === $rootScope.login.u.NhanSu_ID;
                            if (r.Thanhviens) {
                                r.IsQL = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "2").length > 0;
                                r.IsTH = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "1").length > 0;
                                r.IsTD = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "0").length > 0;
                                r.IsType = r.IsQL ? 2 : (r.IsTH ? 1 : 0);
                            }
                        })
                    }
                    $scope.relates = data;
                }
            });
        };
        $scope.Del_Relate = function (tit, id, list) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa " + tit + " liên quan này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Del_Releate",
                        data: { t: $rootScope.login.tk, ids: [id] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            var idx = list.findIndex(x => x.RequestRelate_ID === id);
                            if (idx !== -1)
                                list.splice(idx, 1);
                            showtoastr(tit + " bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa ' + tit + ' không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.listRelated = function (rl) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_ListRelated", pas: [
                        { "par": "RequestMaster_ID ", "va": rl.RequestMaster_ID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.Vanbans = data[0];
                    //Công việc
                    data[1].forEach(function (r) {
                        try {
                            if (r.Thanhviens)
                                r.Thanhviens = JSON.parse(r.Thanhviens);
                        } catch (e) {
                        }
                        try {
                            if (r.Tags)
                                r.Tags = JSON.parse(r.Tags);
                        } catch (e) {
                        }
                        r.color = renderColor(r.Tiendo);
                        r.txtcolor = renderTxtColor(r.Tiendo);
                        r.NgayHan = Math.abs(r.NgayHan);
                        r.Tao = r.NguoiTao === $rootScope.login.u.NhanSu_ID;
                        if (r.Thanhviens) {
                            r.IsQL = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "2").length > 0;
                            r.IsTH = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "1").length > 0;
                            r.IsTD = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "0").length > 0;
                            r.IsType = r.IsQL ? 2 : (r.IsTH ? 1 : 0);
                        }
                    })
                    $scope.Congviecs = data[1];
                    $scope.Lichs = data[2];
                }
            });
        };
        $scope.openRelate = function (rl, Module_ID, Istype) {
            $scope.totalDisplayedRelate = 20;
            $scope.mrelate = { RequestMaster_ID: rl.RequestMaster_ID, Module_ID: Module_ID, IsType: Istype, rlsearch: "", loai: -1 };
            $("#ModalRelate").modal("show");
            switch (Module_ID) {
                case "sdoc":
                    $scope.dtitle = "Liên kết văn bản";
                    $scope.mrelate.title = "văn bản";
                    break;
                case "stask":
                    $scope.dtitle = "Liên kết công việc";
                    $scope.mrelate.title = "công việc";
                    break;
                case "scalendar":
                    $scope.dtitle = "Liên kết lịch";
                    $scope.mrelate.title = "lịch";
                    break;
            }
            $scope.listRelatelienquan($scope.mrelate);
        };
        $scope.UpdateRelate = function () {
            var rids = $scope.relates.filter(x => x.Chon).map(x => x.Relate_ID);
            if (rids.length === 0) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chưa chọn ' + $scope.mrelate.title + ' liên quan !'
                });
                return false;
            }
            if ($scope.loadding) {
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Form/Update_Releate",
                data: {
                    t: $rootScope.login.tk,
                    id: $scope.mrelate.RequestMaster_ID,
                    Module_ID: $scope.mrelate.Module_ID,
                    IsType: $scope.mrelate.IsType,
                    Ghichu: $scope.mrelate.Ghichu,
                    rids: rids
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                $("#ModalRelate").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm ' + $scope.mrelate.title + ' liên quan !'
                    });
                    return false;
                }
                $scope.listRelated($scope.mrelate);
                showtoastr("Thêm " + $scope.mrelate.title + " liên quan thành công!");
            }, function (res) { // optional
                $scope.loadding = false;
                closeswal();
                $("#ModalRelate").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm ' + $scope.mrelate.title + ' liên quan !'
                    });
                    return false;
                }
            });
        };
        //Open Modal
        $scope.List_TypeDuyet = [
            { value: 0, text: 'Một trong nhiều' },
            { value: 1, text: 'Duyệt tuần tự' },
            { value: 2, text: 'Duyệt đồng thời' }
        ]
        $scope.EditRequest = function (r) {
            $("#btncontinue").hide();
            $scope.dtitle = "Sửa phiếu đề xuất";
            delsFiles = [];
            $scope.FilesAttachList = [];
            $scope.LisFile = [];
            $scope.quanlys = [];
            $scope.duyets = [];
            $("form.ng-dirty").removeClass("ng-dirty");
            $("#ModalRequestAdd").modal("show");
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_Edit", pas: [
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "RequestMaster_ID ", "va": r.RequestMaster_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $(".InfoViewRequest #loader").hide();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.modelrequest = data[0][0];
                    $scope.LisFile = data[1];
                    $scope.quanlys = data[2];
                    $scope.duyets = data[3]; 
                    if (data[5] == null || data[5].length == 0) {
                        //Load Form D
                        $scope.loadFormD($scope.modelrequest.Form_ID);
                    } else {
                        $scope.FormDS = data[5];
                        var fd = data[5].find(x => x.KieuTruong == "radio" && x.IsGiatri == "true");
                        if (fd != null) {
                            $scope.modelrequest.Radio = fd.FormD_ID;
                        }
                    }
                }
            });
        };
        $scope.OpenModal_Add = function () {
            $scope.dtitle = "Lập phiếu đề xuất";
            $("#btncontinue").show();
            $scope.modelrequest = { Congty_ID: $rootScope.login.u.congtyID, IsQuytrinhduyet: 1, IsEdit: true, Uutien: 0, STT: $scope.ListRequest.length + 1, Trangthai: 0, IsDelete: false, Ngaylap: new Date() };
            if ($scope.tudien.Team.length > 0) {
                $scope.modelrequest.Team_ID = $scope.tudien.Team[0].Team_ID;
            }
            if ($scope.tudien.Form.length > 0) {
                $scope.modelrequest.Form_ID = $scope.tudien.Form[0].Form_ID;
                var fo = $scope.tudien.FormTeam.find(x => x.Form_ID == $scope.modelrequest.Form_ID && x.Team_ID == $scope.modelrequest.Team_ID);
                $scope.modelrequest.IsSLA = fo.IsSLA;
                $scope.modelrequest.IsChangeQT = fo.IsChangeQT;
                $scope.modelrequest.IsSkip = fo.IsSkip || false;
                $scope.modelrequest.IsQuytrinhduyet = fo.IsQuytrinhduyet || 1;
                //Load Form D
                $scope.loadFormD($scope.modelrequest.Form_ID);
            }
            $scope.FilesAttachList = [];
            delsFiles = [];
            $scope.LisFile = [];
            $scope.quanlys = [];
            $scope.duyets = [];
            $("form.ng-dirty").removeClass("ng-dirty");
            $("#ModalRequestAdd").modal("show");
        };
        $scope.Request_ViewUser = function (r) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_ViewUser", pas: [
                        { "par": "RequestMaster_ID ", "va": r.RequestMaster_ID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $(".InfoViewRequest #loader").hide();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.DataViewUser = data[0];
                }
            });
        };
        $scope.Request_DownloadUser = function (r) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_DownloadUser", pas: [
                        { "par": "FileID ", "va": r.FileID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $(".InfoViewRequest #loader").hide();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.DataFileUser = data[0];
                }
            });
        };
        $scope.setViewRequest = function (r) {
            $http({
                method: "POST",
                url: "/Form/Xem_Request",
                data: { t: $rootScope.login.tk, id: r.RequestMaster_ID },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.Request_ViewUser(r);
            });
        };
        $scope.openViewer = function (f) {
            $scope.Viewer = f;
            $("#ModalViewer").modal("show");
            $("#objectviewpdf").attr("data", null);
            $("#iframeviewpdf").attr("src", null);
            let pdfurl = $rootScope.fileUrl + f.Duongdan;
            $scope.Viewer.pdfurl = pdfurl;
            $scope.DownloadFile(f);
            setTimeout(function () {
                $("#ModalViewer object").attr("data", pdfurl);
                $("#ModalViewer iframe").attr("src", "https://docs.google.com/viewer?url=" + pdfurl + "&embedded=true");
            }, 500);
        }
        $scope.DownloadFile = function (r) {
            $http({
                method: "POST",
                url: domainUrl + "/Form/Download_RequestFile",
                data: { t: $rootScope.login.tk, id: r.FileID },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.Request_DownloadUser(r)
            });
        };
        $scope.openviewUserModal = function () {
            $scope.viewUserTitle = "Danh sách người xem đề xuất";
            $("#viewUserModal").modal("show");
        };
        $scope.clickaccordion = function ($event) {
            angular.element('.panel-collapse').collapse('hide');
        }
        //Sdoc
        $scope.goInfoDoc = function (r) {
            if ($scope.vanBanMasterID == null) {
                swal.showLoading();
            }
            $scope.vanBanMasterID = r.Relate_ID;
            $scope.Uid = generateUUID();
            $scope.isopenadddoc = false;
            $scope.isopentask = false;
            $scope.isopendoc = true;
            $scope.isopencalendar = false;
            $scope.isopenaddcalendar = false;
        };
        $scope.openAddDoc = function (r) {
            $scope.RequestMaster_ID = r.RequestMaster_ID;
            $scope.vanBanMasterID = r.RequestMaster_ID;
            $scope.RequestTitle = r.Title;
            $scope.Uid = generateUUID();
            $scope.isopenadddoc = true;
            $scope.isopentask = false;
            $scope.isopendoc = true;
            $scope.isopen = false;
            $scope.isopencalendar = false;
            $scope.isopenaddcalendar = false;
        };
        //STask
        $scope.goInfoCV = function (r) {
            if ($scope.CongviecID == null) {
                swal.showLoading();
            }
            $scope.CongviecID = r.CongviecID;
            $scope.Uid = generateUUID();
            $scope.isopen = false;
            $scope.isopendoc = false;
            $scope.isopenadddoc = false;
            $scope.isopentask = true;
            $scope.isopencalendar = false;
            $scope.isopenaddcalendar = false;
        };
        $scope.openModalAddCV = function (r) {
            if ($scope.RequestMaster_ID != r.RequestMaster_ID) {
                $scope.RequestMaster_ID = r.RequestMaster_ID;
            }
            $scope.RequestTitle = r.Title;
            $scope.Uid = generateUUID();
            $scope.isopen = true;
            $scope.isopendoc = false;
            $scope.isopentask = false;
            $scope.isopenadddoc = false;
            $scope.isopencalendar = false;
            $scope.isopenaddcalendar = false;
        };
        //Scalendar
        $scope.goinfocalendar = function (r) {
            if ($scope.calendarID == null) {
                swal.showLoading();
            }
            $scope.calendarID = r.Relate_ID;
            $scope.Uid = generateUUID();
            $scope.isopen = false;
            $scope.isopendoc = false;
            $scope.isopentask = false;
            $scope.isopenadddoc = false;
            $scope.isopencalendar = true;
            $scope.isopenaddcalendar = false;
        };
        $scope.openModal_AddCalendar = function (r) {
            $scope.RequestMaster_ID = r.RequestMaster_ID;
            $scope.calendarID = r.RequestMaster_ID;
            $scope.RequestTitle = r.Title;
            $scope.Uid = generateUUID();
            $scope.isopen = false;
            $scope.isopendoc = false;
            $scope.isopentask = false;
            $scope.isopenadddoc = false;
            $scope.isopencalendar = false;
            $scope.isopenaddcalendar = true;
        };
        ///////////
        //Flow
        var dx;
        function renderGio(ngay) {
            return moment(ngay).tz(timezone).format('HH:mm');
        }
        $scope.openFlow = function (vdx) {
            //showtoastr("Chức năng đang được cập nhật !", 2);
            dx = vdx;
            $scope.JobTasks = [];
            $scope.job = { JobType: 1, RequestMaster_ID: dx.RequestMaster_ID, Trangthai: 0, Congty_ID: $rootScope.login.u.congtyID, IsLock: false, IsTypeJob: 0, thuchien: [], Ngaybatdau: new Date(), Ngayketthuc: new Date(), STT: $scope.RQJobs.length + 1 };
            $("#ModeJobRequest h4.modal-title").text("Thêm nhiệm vụ");
            $("div.frmThemCV").show();
            $("#ModeJobRequest").modal("show");
        };
        $scope.editJob = function (vdx, jo, type, ta) {
            dx = vdx;
            var o = { ...jo };
            //$scope.JobTasks = o.Congviecs;
            $scope.JobTasks = [];
            $scope.JobTasks.forEach(function (r) {
                var th = r.Thanhviens.find(x => x.IsType == 1);
                if (th) {
                    r.thuchien = [{ Nguoithuchien: th.Nguoithuchien, NhanSu_ID: th.NhanSu_ID, anhThumb: th.anhThumb, anhDaiDien: th.anhDaiDien, fullName: th.fullName, ten: th.ten }];
                }
                r.Ngaybatdau = moment(r.NgayBatDau).toDate();
                r.Ngayketthuc = moment(r.NgayKetThuc).toDate();
                r.BDGio = renderGio(r.Ngaybatdau);
                r.KTGio = renderGio(r.Ngayketthuc);
            });
            o.JobType = type;
            o.Ngaybatdau = moment(o.Ngaybatdau).toDate();
            o.Ngayketthuc = moment(o.Ngayketthuc).toDate();
            o.BDGio = renderGio(o.Ngaybatdau);
            o.KTGio = renderGio(o.Ngayketthuc);
            o.thuchien = [{ Nguoithuchien: jo.Nguoithuchien, NhanSu_ID: jo.Nguoithuchien, anhThumb: jo.anhThumb, anhDaiDien: jo.anhDaiDien, fullName: jo.fullName, ten: jo.ten }];
            $scope.job = o;
            $("#ModeJobRequest h4.modal-title").text("Chỉnh sửa nhiệm vụ");
            if (type == 3) {
                $("div.frmThemCV").hide();
                $("#ModeJobRequest h4.modal-title").text("Thêm công việc cho nhiệm vụ");
                $scope.addTask();
            }
            else if (type == 4) {
                $("div.frmThemCV").hide();
                $("#ModeJobRequest h4.modal-title").text("Chỉnh sửa công việc");
                var ota = { ...ta };
                ota.show = "show";
                $scope.JobTasks = [ota];
                $scope.JobTasks.forEach(function (r) {
                    var th = r.Thanhviens.find(x => x.IsType == 1);
                    if (th) {
                        r.thuchien = [{ Nguoithuchien: th.Nguoithuchien, NhanSu_ID: th.NhanSu_ID, anhThumb: th.anhThumb, anhDaiDien: th.anhDaiDien, fullName: th.fullName, ten: th.ten }];
                    }
                    r.Ngaybatdau = moment(r.NgayBatDau).toDate();
                    r.Ngayketthuc = moment(r.NgayKetThuc).toDate();
                    r.BDGio = renderGio(r.Ngaybatdau);
                    r.KTGio = renderGio(r.Ngayketthuc);
                });
            }
            $("#ModeJobRequest").modal("show");
        };
        $scope.addTask = function () {
            var len = 0;
            if ($scope.job.Congviecs) {
                len = $scope.job.Congviecs.length;
            }
            $scope.JobTasks.push({
                RequestJob_ID: $scope.job.RequestJob_ID, YeucauReview: true, show: "show", RequestMaster_ID: dx.RequestMaster_ID, Congty_ID: $rootScope.login.u.congtyID, IsLock: false, IsTypeJob: 0, thuchien: [...$scope.job.thuchien], STT: len + $scope.JobTasks.length + 1, Ngaybatdau: $scope.job.Ngaybatdau, Ngayketthuc: $scope.job.Ngayketthuc
            });
        }
        $scope.checkValidDateJob = function (dx) {
            var batdau = new Date(dx.Ngaybatdau);
            var kethtuc = new Date(dx.Ngayketthuc);
            dx.Thongbaongay = null;
            if (dx.BDGio != null) {
                var arrg = dx.BDGio.split(":");
                batdau.setHours(arrg[0], arrg[1], 0, 0);
            } else {
                batdau.setHours(0, 0, 0, 0);
            }
            if (dx.KTGio != null) {
                var arrg = dx.KTGio.split(":");
                kethtuc.setHours(arrg[0], arrg[1], 0, 0);
            } else {
                kethtuc.setHours(0, 0, 0, 0);
            }
            if (kethtuc <= batdau) {
                dx.Ngayketthuc = null;
                dx.Thongbaongay = "Ngày kết thúc không được nhỏ hơn ngày bắt đầu.";
                return false;
            }
            return true;
        };
        $scope.delTask = function (i) {
            $scope.JobTasks.splice(i, 1);
        };
        $scope.downTask = function (i) {
            var STT = $scope.JobTasks[i].STT;
            $scope.JobTasks[i].STT = $scope.JobTasks[i + 1].STT;
            $scope.JobTasks[i + 1].STT = STT;
        };
        $scope.upTask = function (i) {
            var STT = $scope.JobTasks[i].STT;
            $scope.JobTasks[i].STT = $scope.JobTasks[i - 1].STT;
            $scope.JobTasks[i - 1].STT = STT;
        };
        $scope.viewXLDX = function (dex) {
            dex.IsViewXL = !(dex.IsViewXL || false);
            for (var i = 0; i < $scope.RQJobs.length; i++) {
                $scope.RQJobs[i].isCurrent = null;
            }
            if (dex.IsViewXL === true) {
                $scope.RQJobs = [];
                $scope.listJob(dex);
            }
        };
        var colorTT = [
            { id: 0, text: "Chưa bắt đầu", color: "#aaa" },
            { id: 1, text: "Đang làm", color: "#2196f3" },
            { id: 2, text: "Hoàn thành", color: "#6dd230" },
            { id: 3, text: "Chuyển tiếp", color: "#33c9dc" },
            { id: 4, text: "Tạm dừng", color: "#fe4d97" },
            { id: 5, text: "Không hoàn thành", color: "red" }
        ];
        $scope.setCurrent = function (state) {
            if (state.isCurrent == true) {
                for (var i = 0; i < $scope.RQJobs.length; i++) {
                    $scope.RQJobs[i].isCurrent = null;
                }
            } else {
                for (var i = 0; i < $scope.RQJobs.length; i++) {
                    $scope.RQJobs[i].isCurrent = false;
                }
                state.isCurrent = true;
            }
            $scope.viewRequest.IsViewXL = true;
        };
        $scope.listJob = function (rl) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_JobGet", pas: [
                        { "par": "RequestMaster_ID ", "va": rl.RequestMaster_ID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    data[0].forEach(function (t) {
                        var ot = colorTT.find(x => x.id == t.Trangthai);
                        if (ot) {
                            t.TrangthaiTen = ot.text;
                            t.TrangthaiColor = ot.color;
                        }
                        t.color = renderColor(t.Tiendo);
                        t.txtcolor = renderTxtColor(t.Tiendo);
                        t.Congviecs = data[1].filter(x => x.RequestJob_ID === t.RequestJob_ID);
                        t.Congviecs.forEach(function (r) {
                            try {
                                if (r.Thanhviens)
                                    r.Thanhviens = JSON.parse(r.Thanhviens);
                            } catch (e) {
                            }
                            try {
                                if (r.Tags)
                                    r.Tags = JSON.parse(r.Tags);
                            } catch (e) {
                            }
                            r.color = renderColor(r.Tiendo);
                            r.txtcolor = renderTxtColor(r.Tiendo);
                            r.NgayHan = Math.abs(r.NgayHan);
                            r.Tao = r.NguoiTao === $rootScope.login.u.NhanSu_ID;
                            if (r.Thanhviens) {
                                r.IsQL = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "2").length > 0;
                                r.IsTH = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "1").length > 0;
                                r.IsTD = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "0").length > 0;
                                r.IsType = r.IsQL ? 2 : (r.IsTH ? 1 : 0);
                            }
                        });
                    });
                    $scope.RQJobs = data[0];
                }
            });
        };
        $scope.AddJob = function (frm) {
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            if ($scope.job.thuchien.length == 0) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng chọn người thực hiện nhiệm vụ !'
                });
                $("div.djobthuchien input.ipautoUser").focus();
                return false;
            }
            var otask = [];
            var bre = -1;
            var vngay = false;
            var ojob = { ...$scope.job };
            if (ojob.thuchien && ojob.thuchien.length > 0)
                ojob.Nguoithuchien = ojob.thuchien[0].NhanSu_ID;
            if (ojob.BDGio != null) {
                var arrg = ojob.BDGio.split(":");
                ojob.Ngaybatdau.setHours(arrg[0], arrg[1], 0, 0);
            }
            if (ojob.KTGio != null) {
                var arrg = ojob.KTGio.split(":");
                ojob.Ngayketthuc.setHours(arrg[0], arrg[1], 0, 0);
            }
            ojob.Ngaybatdau = moment(ojob.Ngaybatdau).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
            ojob.Ngayketthuc = moment(ojob.Ngayketthuc).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
            var types = [];
            $scope.JobTasks.forEach(function (r, i) {
                var o = { ...r };
                if (r.thuchien.length == 0) {
                    bre = i;
                    return;
                }
                o.Nguoithuchien = o.thuchien[0].NhanSu_ID;
                if (o.BDGio != null) {
                    var arrg = o.BDGio.split(":");
                    o.Ngaybatdau.setHours(arrg[0], arrg[1], 0, 0);
                }
                if (o.KTGio != null) {
                    var arrg = o.KTGio.split(":");
                    o.Ngayketthuc.setHours(arrg[0], arrg[1], 0, 0);
                }
                if (o.Ngayketthuc <= ojob.Ngaybatdau || o.Ngayketthuc > ojob.Ngayketthuc || o.Ngaybatdau > ojob.Ngayketthuc) {
                    dx.Ngayketthuc = null;
                    dx.Thongbaongay = "Ngày của công việc không được nhỏ hơn hoặc lớn hơn ngày của nhiệm vụ!";
                    vngay = true;
                    return;
                }
                o.NgayBatDau = moment(o.NgayBatDau).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                o.NgayKetThuc = moment(o.NgayKetThuc).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                delete o.thuchien;
                delete o.Ngaybatdau;
                delete o.Ngayketthuc;
                otask.push(o);
                types.push(o.IsTypeJob);
            });
            if (bre !== -1) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng chọn người thực hiện công việc!'
                });
                $("div.row-Task.task" + bre + " input.ipautoUser").focus();
                return false;
            }
            if (vngay) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Ngày của công việc không được nhỏ hơn hoặc lớn hơn ngày của nhiệm vụ!'
                });
                return false;
            }
            if ($scope.loadding) {
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            var url = "Add_Job";
            delete ojob.Congviecs;
            delete ojob.thuchien;
            var body = { t: $rootScope.login.tk, model: ojob, tasks: otask, types: types };
            if (ojob.JobType == 2) {
                url = "Update_Job";
                body = { t: $rootScope.login.tk, model: ojob };
            } else if (ojob.JobType == 3 || ojob.JobType == 4) {
                url = "Update_JobStask";
                var tts = [];
                otask.forEach(function (r) {
                    var tr = { Congty_ID: $rootScope.login.u.congtyID, YeucauReview: r.YeucauReview, CongviecID: r.CongviecID, CongviecTen: r.CongviecTen, NgayBatDau: r.NgayBatDau, NgayKetThuc: r.NgayKetThuc, Mota: r.Mota };
                    var or = { task: tr, jobtask: { RequestJob_ID: r.RequestJob_ID, RequestMaster_ID: r.RequestMaster_ID, RequestStask_ID: r.RequestStask_ID, Trangthai: r.Trangthai || 0, IsTypeJob: r.IsTypeJob }, user: r.Thanhviens || r.thuchien };
                    delete or.task.thuchien;
                    delete or.task.Thanhviens;
                    tts.push(or)
                });
                body = {
                    t: $rootScope.login.tk, tasks: tts
                };
                console.log(body);
            }
            $http({
                method: 'POST',
                url: "Form/" + url,
                data: body,
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Cập nhật nhiệm vụ không thành công, vui lòng kiểm tra lại !'
                    });
                    return false;
                }
                showtoastr("Thêm nhiệm vụ xử lý cho đề xuất thành công!");
                $("#ModeJobRequest").modal("hide");
                $scope.goViewRequest($scope.viewRequest);
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        $scope.DelJob = function (job) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa nhiệm vụ và các công việc của nhiệm vụ này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Del_Job",
                        data: { t: $rootScope.login.tk, id: job.RequestJob_ID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            var idx = $scope.RQJobs.findIndex(x => x.RequestJob_ID === job.RequestJob_ID);
                            if (idx !== -1)
                                $scope.RQJobs.splice(idx, 1);
                            showtoastr("Nhiệm vụ bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.MoveJob = function (idx, f) {
            var id = $scope.RQJobs[idx].RequestJob_ID;
            var dx = !f ? idx + 1 : idx - 1;
            var STT = $scope.RQJobs[idx].STT;
            var tid = $scope.RQJobs[dx].RequestJob_ID;
            $scope.RQJobs[idx].STT = $scope.RQJobs[dx].STT;
            $scope.RQJobs[dx].STT = STT;
            $http({
                method: "POST",
                url: "Form/Move_Job",
                data: { t: $rootScope.login.tk, id: id, tid: tid },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    showtoastr("Đổi thứ tự nhiệm vụ thành công!.");
                } else {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Đổi thứ tự nhiệm vụ không thành công, vui lòng thử lại!'
                    });
                }
            });
        };
        $scope.MoveJobTask = function (job, idx, f) {
            var id = job.Congviecs[idx].RequestStask_ID;
            var dx = !f ? idx + 1 : idx - 1;
            var STT = job.Congviecs[idx].STT;
            var tid = job.Congviecs[dx].RequestStask_ID;
            job.Congviecs[idx].STT = job.Congviecs[dx].STT;
            job.Congviecs[dx].STT = STT;
            $http({
                method: "POST",
                url: "Form/Move_JobTask",
                data: { t: $rootScope.login.tk, id: id, tid: tid },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    showtoastr("Đổi thứ tự công việc thành công!.");
                } else {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Đổi thứ tự công việc không thành công, vui lòng thử lại!'
                    });
                }
            });
        };
        $scope.DelJobTask = function (job, d) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa công việc này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Del_JobStask",
                        data: { t: $rootScope.login.tk, id: d.RequestStask_ID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            var idx = job.Congviecs.findIndex(x => x.RequestStask_ID === d.RequestStask_ID);
                            if (idx !== -1)
                                job.Congviecs.splice(idx, 1);
                            showtoastr("Công việc bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        //Xử lý đề xuất
        $scope.openXLDX = function (dx, tt, tit) {
            $scope.xldx = { RequestMaster_ID: dx.RequestMaster_ID, TrangthaiXL: tt, IsTao: dx.IsTao, DiemDG: 0 };
            $("#ModelXLRequest h4.modal-title").text(tit + " cho đề xuất : " + dx.Title);
            $("#ModelXLRequest").modal("show");
            return false;
        };
        $scope.AddXLDX = function (frm) {
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            if ($scope.loadding) {
                return false;
            }
            if ($scope.xldx.TrangthaiXL == 2 && $scope.xldx.IsTao) {
                $scope.AddDGXLDX(frm);
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Form/UpdateXL_Request",
                data: { t: $rootScope.login.tk, id: $scope.xldx.RequestMaster_ID, noidung: $scope.xldx.NoidungXL, tt: $scope.xldx.TrangthaiXL },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Cập nhật xử lý đề xuất không thành công, vui lòng kiểm tra lại !'
                    });
                    return false;
                }
                showtoastr("Cập nhật xử lý đề xuất thành công!");
                $("#ModelXLRequest").modal("hide");
                $scope.goViewRequest($scope.viewRequest);
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        $scope.AddDGXLDX = function (frm) {
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            if ($scope.loadding) {
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Form/Danhgia_Request",
                data: { t: $rootScope.login.tk, id: $scope.xldx.RequestMaster_ID, noidung: $scope.xldx.NoidungXL, tt: $scope.xldx.TrangthaiXL, point: $scope.xldx.DiemDG },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Cập nhật xử lý đề xuất không thành công, vui lòng kiểm tra lại !'
                    });
                    return false;
                }
                showtoastr("Cập nhật xử lý đề xuất thành công!");
                $("#ModelXLRequest").modal("hide");
                $scope.goViewRequest($scope.viewRequest);
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        //Gia hạn đề xuất
        $scope.checkValidDateline = function (dx) {
            var hientai = new Date();
            var datlinecu = new Date(dx.Datelinecu);
            var datline = new Date(dx.Dateline);
            if ($scope.giahan.DatelineGio != null) {
                var arrg = $scope.giahan.DatelineGio.split(":");
                datline.setHours(arrg[0], arrg[1], 0, 0);
            }
            if (datlinecu > datline) {
                dx.Dateline = null;
                $scope.Thongbaongay = "Hạn xử lý mới không được ít hơn hạn xử lý cũ.";
                return false;
            }
            if (datline < hientai) {
                dx.Dateline = null;
                $scope.Thongbaongay = "Hạn xử lý mới không được nhỏ hơn ngày hiện tại";
                return false;
            }
            return true;
        };
        $scope.openModalDatelineRequest = function (dx) {
            $scope.giahan = { RequestMaster_ID: dx.RequestMaster_ID, Lydo: "", Dateline: new Date(), Datelinecu: dx.Dateline, IsTao: dx.IsTao };
            $("#ModelDatelineRequest h4.modal-title").text("Gia hạn xử lý cho đề xuất : " + dx.Title);
            $("#ModelDatelineRequest").modal("show");
            return false;
        };
        $scope.Edit_Dateline = function (r) {
            r.Datelinecu = $scope.viewRequest.Dateline;
            $scope.giahan = r;
        };
        $scope.Del_Dateline = function (id) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa nội dung gia hạn này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Form/Del_Dateline",
                        data: { t: $rootScope.login.tk, id: id },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            var idx = $scope.datelines.findIndex(x => x.Giahan_ID === id);
                            if (idx !== -1)
                                $scope.datelines.splice(idx, 1);
                            showtoastr("Nội dung gia hạn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.listDateline = function (rl) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_ListDateline", pas: [
                        { "par": "RequestMaster_ID ", "va": rl.RequestMaster_ID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.datelines = data[0];
                }
            });
        };
        $scope.AddDateline = function (frm) {
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            if ($scope.loadding) {
                return false;
            }
            if (!$scope.giahan.Dateline) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng cập nhật hạn xử lý mới !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            var obj = { ...$scope.giahan };
            obj.Dateline = moment(obj.Dateline).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
            $http({
                method: 'POST',
                url: "Form/Update_Dateline",
                data: { t: $rootScope.login.tk, model: obj },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Gửi gia hạn xử lý cho đề xuất không thành công, vui lòng kiểm tra lại !'
                    });
                    return false;
                }
                showtoastr("Gửi gia hạn duyệt cho đề xuất thành công!");
                //$("#ModelDatelineRequest").modal("hide");
                $scope.goViewRequest($scope.viewRequest);
                $scope.giahan = { RequestMaster_ID: dx.RequestMaster_ID, Lydo: "", Dateline: new Date(), Datelinecu: dx.Dateline };
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        ///////////
        function init() {
            initTD();
        };
        //Gọi Jquery Plugin
        //Realtime
        if (messaging) {
            messaging.onMessage(function (payload) {
                console.log(payload);
                if (payload.data.hub) {
                    var hub = JSON.parse(payload.data.hub).Data;
                    if ((hub.loai === 13)) {
                        $scope.goRFfunction();
                        if ($(".InfoViewRequest").hasClass("show") && hub.idKey == $scope.viewRequest.RequestMaster_ID) {
                            $scope.goViewRequest($scope.viewRequest);
                            showtoastrNhacLich("Bạn vừa nhận được một phiếu đề xuất mới!");
                        }
                    }
                }
            });
        }
    }
});
