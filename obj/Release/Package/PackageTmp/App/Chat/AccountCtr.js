﻿angular.module('AccountCtr', ['ngAnimate', 'ui.bootstrap'])
    .controller("AccountCtr", ['$scope', '$rootScope', '$http', '$filter', 'Upload', '$interval', '$state', '$stateParams', '$sce', function ($scope, $rootScope, $http, $filter, Upload, $interval, $state, $stateParams, $sce) {
        $scope.toggleSetting = function () {
            $scope.ob.setting = !$scope.ob.setting;
            $scope.ob.overview = !$scope.ob.overview;
        };
        $scope.togglePass = function () {
            $scope.ob.changepass = !$scope.ob.changepass;
            $scope.ob.overview = !$scope.ob.overview;
        };
        $scope.toggleSwitchAcc = function () {
            $scope.ob.switchacc = !$scope.ob.switchacc;
            $scope.ob.overview = !$scope.ob.overview;
        };
        $scope.initInfoAcc = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "App_initNhanSu", pas: [
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.User = data[0][0];
                }
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        $scope.initPass = function () {
            $scope.Pass = { oldPass: null, matKhau: null, rePass: null, NhanSu_ID: $scope.login.u.NhanSu_ID };
        };
        $scope.initSwitchAcc = function () {
            $scope.Acc = { NhanSu_ID: $rootScope.login.u.NhanSu_ID, Login_ID: null, pass: null };
        };
        $scope.UpdateInfoAcc = function (frm) {

            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Account/Update_NhanSu",
                data: {
                    t: $rootScope.login.tk, model: $scope.User
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;

                var data = res.data;
                if (data === null || data.trim().length === 0) {
                    $scope.loadding = false;
                    Swal.fire({                        icon: 'error',                        type: 'error',                        title: '',                        text: 'Có lỗi khi cập nhật thông tin tài khoản!'                    });
                } else {
                    $scope.loadding = false;
                    setCookieUserDomain(data);
                    localStorage.setItem('lo', data);
                    localStorage.setItem('au', data);
                    $scope.loadding = false;
                    Swal.fire(                        'Thành công!',                        'Tài khoản của bạn đã được thay đổi thông tin thành công!.',                        'success'                    );
                }
            }, function (response) { // optional
                $scope.loadding = false;
            });
        }
        $scope.UpdatePass = function (frm) {

            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            if ($scope.Pass.matKhau !== $scope.Pass.rePass) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Mật khẩu không trùng khớp !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Account/Update_Password",
                data: {
                    t: $rootScope.login.tk, op: encr($scope.Pass.oldPass).toString(), str: encr(JSON.stringify($scope.Pass)).toString(),
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    if (res.data.op === 0) {
                        Swal.fire({
                            icon: 'error',
                            type: 'error',
                            title: '',
                            text: 'Mật khẩu cũ không đúng !'
                        });
                    }
                    else {
                        Swal.fire({
                            icon: 'error',
                            type: 'error',
                            title: '',
                            text: 'Có lỗi khi thay đổi mật khẩu !'
                        });
                    }
                    return false;
                }
                Swal.fire(                    'Thành công!',                    'Mật khẩu đã được thay đổi thành công! Vui lòng đăng nhập lại để tiếp tục!',                    'success'                ).then((result) => {                    $rootScope.logOut();
                });

            }, function (response) { // optional
                $scope.loadding = false;
            });
        }
        $scope.AddAcc = function (frm) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            var curids = $scope.accounts.map(x => x.NhanSu_ID);
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Account/Add_Account",
                data: {
                    t: $rootScope.login.tk, p: encr($scope.Acc.pass).toString(), str: encr(JSON.stringify($scope.Acc)).toString(), nids: curids
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                $scope.initSwitchAcc();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    if (res.data.op === 0) {
                        Swal.fire({
                            icon: 'error',
                            type: 'error',
                            title: '',
                            text: 'Mật khẩu không đúng !'
                        });
                        return false;
                    }
                    else if (res.data.op === 1) {
                        Swal.fire({
                            icon: 'error',
                            type: 'error',
                            title: '',
                            text: 'Tài khoản không tồn tại !'
                        });
                        return false;
                    }
                    else if (res.data.op === 2) {
                        Swal.fire({
                            icon: 'error',
                            type: 'error',
                            title: '',
                            text: 'Tài khoản đã tồn tại trong danh sách !'
                        });
                        return false;
                    }
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm tài khoản !'
                    });
                    return false;
                }
                else {
                    $scope.ob.switchacc = false;
                    $scope.BindListAcc();
                    showtoastr("Thêm tài khoản thành công !");
                }
            });
        };
        $scope.BindListAcc = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "SOE_ListSwitchAcc", pas: [
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.accounts = data[0];
                }
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        $scope.SwitchAcc = function (u) {            Swal.fire({                title: 'Xác nhận?',                text: "Bạn có muốn chuyển sang tài khoản " + u.fullName + " không!",                icon: 'warning',                showCancelButton: true,                confirmButtonColor: '#2196f3',                cancelButtonColor: '#d33',                confirmButtonText: 'Có!',                cancelButtonText: 'Không!',            }).then((result) => {                if (result.value) {                    if ($scope.loadding) {
                        return false;
                    }
                    $scope.loadding = true;
                    swal.showLoading();
                    $http({
                        method: "POST",                        url: "Account/Switch_Account",                        data: {
                            t: $rootScope.login.tk, acc: { Login_ID: u.NhanSu_ID, NhanSu_ID: $rootScope.login.u.NhanSu_ID }                        },                        contentType: 'application/json; charset=utf-8'
                        }).then(function (res) {
                            $scope.loadding = false;
                            closeswal();
                            if (!$rootScope.checkToken(res)) return false;
                            var data = res.data;
                            var obj = JSON.parse(decr(data));
                            if (obj.error !== 0) {
                                $scope.loadding = false;
                                Swal.fire({                                    icon: 'error',                                    type: 'error',                                    title: '',                                    text: data.ms                                });
                            } else {
                                $scope.loadding = false;
                                setCookieUserDomain(data);
                                localStorage.setItem('lo', data);
                                localStorage.setItem('au', data);
                                $scope.loadding = false;
                                Swal.fire(                                    'Thành công!',                                    'Đã đăng nhập tài khoản thành công !',                                    'success'                                );
                            }
                        }, function (response) { // optional
                            $scope.loadding = false;
                        });                }            })        };
        $scope.initOne = function () {
            $scope.ob = { setting: false, changepass: false, overview: true, switchacc: false };
            $rootScope.TenDuan = $rootScope.login.u.tenCongty;
            $scope.initInfoAcc();
            $scope.initPass();
            $scope.initSwitchAcc();
            $scope.BindListAcc();
        }
        $scope.initOne();
    }]);