﻿os.component('usertagcomponent', {
    bindings: {
        hTitle: '@',
        one: '@',
        hide: '@',
        required: '@',
        index:'@',
        users:'=',
        rfFunction: '&',
        usersModal: '&'
    },
    templateUrl: baseUrl + 'App/Component/UserTag/UserTagComponent.html?v=' + vhtmlcache,
    controller: function ($scope, $rootScope, $http, $sce, $timeout) {
        var $ctrl = this;
        $scope.goRFfunction = function () {
            $ctrl.rfFunction();
        };
        this.$onChanges = () => {
        };
        $scope.removeUser = function (idx) {
            $ctrl.users.splice(idx, 1);
        };
        $scope.showusersModal = function () {
            $ctrl.usersModal({ index: $ctrl.index, one:$ctrl.one});
        };
        function setEndOfContenteditable(contentEditableElement) {
            var range, selection;
            if (document.createRange)//Firefox, Chrome, Opera, Safari, IE 9+
            {
                range = document.createRange();//Create a range (a range is a like the selection but invisible)
                range.selectNodeContents(contentEditableElement);//Select the entire contents of the element with the range
                range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
                selection = window.getSelection();//get the selection object (allows you to change selection)
                selection.removeAllRanges();//remove any selections already made
                selection.addRange(range);//make the range you have just created the visible selection
            }
            else if (document.selection)//IE 8 and lower
            {
                range = document.body.createTextRange();//Create a range (a range is a like the selection but invisible)
                range.moveToElementText(contentEditableElement);//Select the entire contents of the element with the range
                range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
                range.select();//Select the range (make it the visible selection
            }
        }
        $scope.clickAddUser = function ($event, u) {
            $scope.Focus = false;
            $scope.filterUser = null;
            if ($ctrl.users.filter(x => x.NhanSu_ID == u.NhanSu_ID).length == 0)
                $ctrl.users.push(u);
            $scope.searchU = "";
            elem = $($event.target).closest('div.usertagcomp').find("input.ipautoUser")[0];//This is the element that you want to move the caret to the end of
            setEndOfContenteditable(elem);
            elem.focus();
            elem.innerText = "";
        };
        $scope.handleKeyDown = function ($event) {
        var comp = $($event.target).closest('div.usertagcomp');
        var ulauto = comp.find("ul.autoUser");
            var objCurrentLi, obj = ulauto.find('.SelectedLi'), objUl = ulauto, code = ($event.keyCode ? $event.keyCode : $event.which);
            if (code === 40) {
                // Down
                ulauto.focus();
                $event.preventDefault();
                if ((obj.length === 0) || (objUl.find('li:last').hasClass('SelectedLi') === true)) {
                    objCurrentLi = objUl.find('li:first').addClass('SelectedLi');
                }
                else {
                    objCurrentLi = obj.next().addClass('SelectedLi');
                }
                obj.removeClass('SelectedLi');
            }
            else if (code === 38) {
                // Up
                $event.preventDefault();
                if ((obj.length === 0) || (objUl.find('li:first').hasClass('SelectedLi') === true)) {
                    objCurrentLi = objUl.find('li:last').addClass('SelectedLi');
                }
                else {
                    objCurrentLi = obj.prev().addClass('SelectedLi');
                }
                obj.removeClass('SelectedLi');
            }
            else if (code === 13) {
                // Enter
                $event.preventDefault();
                obj = ulauto.find('.SelectedLi');
                if (obj) {
                    var u = $scope.filterUser.find(x => x.NhanSu_ID == obj.attr("uid"));
                    $scope.clickAddUser($event,u);
                }
            }
        };
        $scope.complete = function (string, $event) {
            var comp = $($event.target).closest('div.usertagcomp');
            var ulauto = comp.find("ul.autoUser");
            if (ulauto.css('display') == 'none')
                ulauto.show();
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                ulauto.focus();
                $event.preventDefault();
            }
            if (!string) {
                string = "";
            }
            string = string.replace("@", "");
            $scope.Focus = true;
            var output = [];
            if (!string) {
                string = "";
            }
            angular.forEach($rootScope.users, function (u) {
                if ((u.fullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.enFullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.tenTruyCap || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.phone + '').toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                        output.push(u);
                }
            });
            $scope.filterUser = output;
        };
       
    }
});