﻿os.component('addtaskcomponent', {
    bindings: {
        requestid: '<',
        requestjobid: '<',
        isopen:'<',
        uid: '<',
        htitle: '=',
        rfFunction: '&'
    },
    templateUrl: baseUrl + 'App/Component/Task/AddTask.html?v=' + vhtmlcache,
    controller: function ($scope, $rootScope, $http, $sce, $timeout) {
        var $ctrl = this;
        $scope.uutiens = [0, 1, 2, 3];
        $scope.Quyen = { IsBlockQuyen: false };
        $scope.goRFfunction = function () {
            $ctrl.rfFunction();
        };
        this.$onChanges = () => {
            if (this.requestid && this.isopen) {
                var u = $rootScope.users.find(u => u.NhanSu_ID === $rootScope.login.u.NhanSu_ID);
                $scope.nhomcongviec = null;
                $scope.dtitle = "Tạo công việc cho đề xuất : " + this.htitle;
                $scope.modelcongviec = { IsTodo: false, Phongban_ID: $rootScope.login.u.Phongban_ID, Trangthai: 0, NgayBatDau: new Date(), Congty_ID: $rootScope.login.u.congtyID, STT: 1, IsDelete: false, Tiendo: 0, Uutien: false, IsDeadline: true, YeucauReview: true, DuanID: null, NhomCongviecID: null };
                $scope.giaoviecs = [u]; $scope.thuchiens = [u], $scope.theodois = [];
                $scope.FilesAttachTaskList = [];
                $scope.LisFileAttach = [];
                $("form.ng-dirty").removeClass("ng-dirty");
                $("#btncontinue").show();
                $("#ModalCongviecAdd").modal("show");
                initTD();
            }
        };
        $scope.BindListDuan = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListAllDuAnbyUser", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $rootScope.duans = data[0];
                    if ($scope.DuanID && !$scope.opition.TenDuan) {
                        var da = $rootScope.duans.find(x => x.DuanID === $scope.DuanID);
                        if (da)
                            $scope.opition.TenDuan = da.TenDuan;
                    }
                }
            });
        };
        //Công việc
        var objcv, ogvs = [], oths = [], otds = [];
        $scope.openModelEditCongviec = function (r) {
            $scope.dtitle = "Cập nhật công việc";
            $scope.congviec = r;
            $scope.modelcongviec = { ...r };
            $scope.FilesAttachTaskList = [];
            objcv = { ...r };
            if (!$scope.IsViewCV) {
                $scope.giaoviecs = []; $scope.thuchiens = [], $scope.theodois = [];
                $scope.LisFileAttach = [];
                $scope.LisFileAttachCV = [];
                $scope.getCongViec(true);
            }
            $scope.listNhomCV();
            $("form.ng-dirty").removeClass("ng-dirty");
            $("#btncontinue").hide();
            $("#ModalCongviecAdd").modal("show");
        };
        //Valid ngày công việc-----------------------------------------------*/
        $scope.endDateBeforeRender = endDateBeforeRender
        $scope.endDateOnSetTime = endDateOnSetTime
        $scope.startDateBeforeRender = startDateBeforeRender
        $scope.startDateOnSetTime = startDateOnSetTime

        function startDateOnSetTime() {
            $scope.$broadcast('start-date-changed');
        }

        function endDateOnSetTime() {
            $scope.$broadcast('end-date-changed');
        }

        function startDateBeforeRender($dates) {
            if ($scope.modelcongviec) {
                var bd = $scope.duan != null && $scope.duan.NgayBatDau != null ? moment($scope.duan.NgayBatDau) : null;
                var kt = $scope.duan != null && $scope.duan.NgayKetThuc != null ? moment($scope.duan.NgayKetThuc) : null;
                if ($scope.modelcongviec.NgayKetThuc) {
                    var activeDate = moment($scope.modelcongviec.NgayKetThuc);
                    $dates.filter(function (date) {
                        return date.localDateValue() >= activeDate.valueOf() || (kt != null && date.localDateValue() > kt.valueOf()) || (bd != null && date.localDateValue() < bd.valueOf())
                    }).forEach(function (date) {
                        date.selectable = false;
                    })
                } else {
                    $dates.filter(function (date) {
                        return (kt != null && date.localDateValue() > kt.valueOf()) || (bd != null && date.localDateValue() < bd.valueOf())
                    }).forEach(function (date) {
                        date.selectable = false;
                    })
                }
            }
        }

        function endDateBeforeRender($view, $dates) {
            if ($scope.modelcongviec) {
                var bd = $scope.duan != null && $scope.duan.NgayBatDau != null ? moment($scope.duan.NgayBatDau) : null;
                var kt = $scope.duan != null && $scope.duan.NgayKetThuc != null ? moment($scope.duan.NgayKetThuc) : null;
                if ($scope.modelcongviec.NgayBatDau) {
                    var activeDate = moment($scope.modelcongviec.NgayBatDau).subtract(1, $view).add(1, 'minute');
                    $dates.filter(function (date) {
                        return date.localDateValue() <= activeDate.valueOf() || (kt != null && date.localDateValue() > kt.valueOf()) || (bd != null && date.localDateValue() < bd.valueOf())
                    }).forEach(function (date) {
                        date.selectable = false;
                    })
                } else {
                    $dates.filter(function (date) {
                        return (kt != null && date.localDateValue() > kt.valueOf()) || (bd != null && date.localDateValue() < bd.valueOf())
                    }).forEach(function (date) {
                        date.selectable = false;
                    })
                }
            }
        }
        /**/
        function renderMessage() {
            var message = [];
            if (objcv == null) {
                return message;
            }
            if (objcv.CongviecTen != $scope.modelcongviec.CongviecTen) {
                message.push(" - Sửa tên công việc từ <soe>" + objcv.CongviecTen + "</soe> thành <soe>" + $scope.modelcongviec.CongviecTen + "</soe>");
            }
            if (objcv.Mota != $scope.modelcongviec.Mota) {
                message.push(" - Sửa mô tả công việc");
            }
            if (objcv.Muctieu != $scope.modelcongviec.Muctieu) {
                message.push(" - Sửa mục tiêu công việc");
            }
            if (objcv.Ketquadatduoc != $scope.modelcongviec.Ketquadatduoc) {
                message.push(" - Sửa kết quả công việc");
            }
            if (ogvs != null && ogvs != $scope.giaoviecs.map(x => { x.NhanSu_ID, x.fullName })) {
                message.push(" - Thay đổi người giao việc thành <soe>" + ($scope.giaoviecs.map(x => '<soe>' + x.fullName + '</soe>').join(",")) + "</soe>");
            }
            if (oths != null && oths != $scope.thuchiens.map(x => { x.NhanSu_ID, x.fullName })) {
                message.push(" - Thay đổi người thực hiện thành <soe>" + ($scope.thuchiens.map(x => '<soe>' + x.fullName + '</soe>').join(",")) + "</soe>");
            }
            if (otds != null && otds != $scope.theodois.map(x => { x.NhanSu_ID, x.fullName })) {
                message.push(" - Cập nhật thông tin người theo dõi : <soe>" + ($scope.theodois.map(x => '<soe>' + x.fullName + '</soe>').join(",")) + "</soe>");
            }
            return message;
        }
        $scope.AddCongviec = function (frm, fc) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            var message = renderMessage();
            $http({
                method: 'POST',
                url: "Duan/Update_Congviec",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    if ($scope.modelcongviec.NgayBatDau) {
                        $scope.modelcongviec.NgayBatDau = moment($scope.modelcongviec.NgayBatDau).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                    }
                    if ($scope.modelcongviec.NgayKetThuc) {
                        $scope.modelcongviec.NgayKetThuc.setHours(23, 59, 59);
                        $scope.modelcongviec.NgayKetThuc = moment($scope.modelcongviec.NgayKetThuc).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                    }
                    formData.append("RequestMaster_ID", $ctrl.requestid);
                    formData.append("RequestJob_ID", $ctrl.requestjobid);
                    formData.append("t", $rootScope.login.tk);
                    formData.append("models", JSON.stringify($scope.modelcongviec));
                    if (message.length > 0)
                        formData.append("message", message.join("<br/>"));
                    formData.append("giaoviecs", JSON.stringify($scope.giaoviecs));
                    formData.append("thuchiens", JSON.stringify($scope.thuchiens));
                    formData.append("theodois", JSON.stringify($scope.theodois));
                    $.each($("#frCongviec input[type='file']")[0].files, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                }
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm công việc mới !'
                    });
                    return false;
                }
                if ($scope.CongviecID) {
                    $scope.getCongViec();
                }
                $scope.modelcongviec.CongviecTen = "";
                    $("#ModalCongviecAdd").modal("hide");
                showtoastr('Đã cập nhật công việc thành công!.');
                $("#frCongviec input[type='file']").val("");
                $scope.goRFfunction();
                window.parent.postMessage({ Type: 1, Isdel: false }, "*");
                //resetForm(frm);
            }, function (response) { // optional
                //resetForm(frm);
                $scope.loadding = false;
            });
        };
        //File
        $scope.DeleteFiles = function (i, f) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa file này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_DuanFile",
                        data: { t: $rootScope.login.tk, ids: [f.FileID] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            if ($scope.LisFileAttach) {
                                var idx = $scope.LisFileAttach.findIndex(x => x.FileID === f.FileID);
                                if (idx !== -1)
                                    $scope.LisFileAttach.splice(idx, 1);
                            }
                            if ($scope.LisFileAttachCV) {
                                idx = $scope.LisFileAttachCV.findIndex(x => x.FileID === f.FileID);
                                if (idx !== -1)
                                    $scope.LisFileAttachCV.splice(idx, 1);
                            }
                            showtoastr("File bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa File không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.removeFiles = function (idx) {
            $scope.FilesAttachTaskList.splice(idx, 1);
        };
        //Tag User
        $scope.complete = function (string, m, $event, i) {
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            string = string.replace("@", "");
            var output = [];
            if (!string) {
                string = "";
            }
            if (m) {
                for (var k = 0; k <= 5; k++) {
                    m["Focus" + k] = false;
                }
                m["Focus" + i] = true;
            }
            angular.forEach($rootScope.users, function (u) {
                if ((u.fullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.enFullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.tenTruyCap || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.phone + '').toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    if (!m || m.NhanSu_ID !== u.NhanSu_ID)
                        output.push(u);
                }
            });
            $scope.filterUser = output;
        };
        $scope.handleKeyDown = function (m, $event, f) {
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                //$("ul.autoUser").focus();
                $event.preventDefault();
                if ($scope.focusedIndex !== $scope.filterUser.length - 1) {
                    $scope.focusedIndex++;
                }
            }
            else if (keyCode === 38) {
                // Up
                $event.preventDefault();
                if ($scope.focusedIndex !== 0) {
                    $scope.focusedIndex--;
                }
            }
            else if (keyCode === 13 && $scope.focusedIndex >= 0) {
                // Enter
                $event.preventDefault();
                $scope.clickAddUser(m, $scope.filterUser[$scope.focusedIndex], f);
            }
        };
        $scope.clickAddUser = function (m, us, u, i) {
            $scope.filterUser = null;
            if (!us || i != 2) {
                us = [];
            }
            if (i == "") {
                $scope.giaoviecs = [u];
            } else if (i == "1") {
                $scope.thuchiens = [u];
            } else {
                us.push(u);
            }
            m["searchU" + i] = "";
            $("#ModalCongviecAdd input.ipautoUser" + i + ".true").focus();
        };
        $scope.ClearFocus = function () {
            $("#ModalCongviecAdd .autoUser").hide();
        }
        $scope.removeUser = function (us, idx) {
            if (us[idx].CongviecThamgiaID) {
                $scope.Del_CongviecThamGia(us, idx);
            } else {
                us.splice(idx, 1);

            }
        };
        $scope.Del_CongviecThamGia = function (ds, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa user này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_CongviecThamGia",
                        data: { t: $rootScope.login.tk, ids: [ds[i].CongviecThamgiaID] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.goRFfunction();
                            window.parent.postMessage({ Type: 1, Isdel: false }, "*");
                            ds.splice(i, 1);
                            showtoastr("User bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa User không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.Del_Congviec = function (d, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa công việc này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_Congviec",
                        data: { t: $rootScope.login.tk, id: d.CongviecID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            window.parent.postMessage({ Type: 1, Isdel: true }, "*");
                            $scope.goRFfunction();
                            showtoastr("Bạn đã xóa công việc thành công!");
                            if ($scope.CongviecID != null) {
                                $scope.link = "congviec";
                                $state.go('congviec');
                            }
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa công việc không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.showusersModal = function (f, index) {
            $rootScope.phongbans.filter(r => r.isCheck).forEach(function (r) {
                r.isCheck = false;
            });
            $rootScope.users.filter(r => r.isCheck).forEach(function (r) {
                r.isCheck = false;
            });
            $scope.vbphongbans = $rootScope.phongbans;
            $scope.vbusers = $rootScope.users;
            $scope.one = index != 2;
            // set chọn user trong lấy list user config cho quy trình
            $scope.IndexListUser = index;
            $("#usersTaskModal").modal("show");
        };

        $scope.choiceUser = function (users) {
            if ($scope.IndexListUser === 0) {
                users = users.filter(u => $scope.giaoviecs.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                $scope.giaoviecs = [users[0]];
            } else if ($scope.IndexListUser === 1) {
                users = users.filter(u => $scope.thuchiens.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                $scope.thuchiens = [users[0]];
            } else {
                users = users.filter(u => $scope.theodois.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                $scope.theodois = $scope.theodois.concat(users);
            }
            $("#usersTaskModal").modal("hide");
        };
        //Nhom cong viec
        $scope.changeDA = function () {
            $scope.nhomcongviecs = [];
            $scope.modelcongviec.NhomCongviecID = null;
            $scope.listNhomCV($scope.modelcongviec.DuanID);
        };
        $scope.listNhomCV = function (id) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Duan_ListTudienNhomcongviec", pas: [
                        { "par": "DuanID", "va": id || $scope.DuanID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    var fn = { NhomCongviecID: null, Tennhom: "Chưa phân nhóm" };
                    data[0].push(fn);
                    $scope.nhomcongviecs = data[0];
                }
            });
        };
        $scope.DeleteNhomcongviec = function (cv, i) {
            if (cv.soCV > 0) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Nhóm đã có công việc không thể xóa!'
                });
                return;
            }
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa nhóm công việc này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_NhomCongViec",
                        data: { t: $rootScope.login.tk, id: cv.NhomCongviecID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.nhomcongviecs.splice(i, 1);
                            showtoastr("Nhóm công việc bạn chọn đã được xóa thành công!");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa nhóm không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.viewNhomCongviec = function () {
            $("#ModalNhomcongviecList").modal("show");
        };
        $scope.openModalAddNhomCV = function (ncv) {
            $("form.ModalNhomcongviecAdd.ng-dirty").removeClass("ng-dirty");
            if (!$scope.nhomcongviecs)
                $scope.nhomcongviecs = [];
            if (ncv != null) {
                $scope.nhomcongviec = { ...ncv };
            } else
                $scope.nhomcongviec = { Congty_ID: $rootScope.login.u.congtyID, STT: $scope.nhomcongviecs.length + 1, DuanID: $scope.congviec != null ? $scope.congviec.DuanID : $scope.DuanID, Hienthi: true };
            $("#ModalNhomcongviecAdd").modal("show");
        };
        $scope.AddNhomcongviec = function (frm) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Duan/Update_NhomCongViec",
                data: { t: $rootScope.login.tk, model: $scope.nhomcongviec },
                headers: {
                    contentType: 'application/json; charset=utf-8'
                },
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm nhóm công việc !'
                    });
                    return false;
                }
                $("#ModalNhomcongviecAdd").modal("hide");
                $scope.listNhomCV($scope.congviec != null ? $scope.congviec.DuanID : $scope.DuanID);
                //resetForm(frm);
            }, function (response) { // optional
                //resetForm(frm);
                $scope.loadding = false;
            });
        };
        //UploadFile
        $scope.openAttachfile = function (ev) {
            $(ev.target).closest("div.form-group").find("input[type='file']").trigger("click");
        };
        $scope.UploadFiles = function (f) {
            if (!$scope.LisFileAttach) $scope.LisFileAttach = [];
            if (!$scope.FilesAttachTaskList) $scope.FilesAttachTaskList = [];
            if ($scope.LisFileAttach.length + $scope.FilesAttachTaskList.length + f.length > 10) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 10 file cho dự án!'
                });
                return false;
            }
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 100 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    $scope.FilesAttachTaskList.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 100MB!'
                });
            }
        };
        $scope.initOne = function () {
            $scope.DuanID = null;
            $scope.BindListDuan();
            $scope.listNhomCV();
        };
        function initTD() {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Tudien", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.tudien = data;
                    $scope.TrangthaiCV = data[3];
                    $scope.initOne();
                    $scope.CVTTS = $scope.TrangthaiCV.filter(x => x.TrangthaiID < 4 && x.TrangthaiID >= 0);
                }
            });
        };
    }
});
