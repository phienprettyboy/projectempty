﻿os.component('viewtaskcomponent', {
    bindings: {
        congviecid: '<',
        uid: '<',
        isopentask: '<',
        htitle: '=',
        rfFunction: '&'
    },
    templateUrl: baseUrl + 'App/Component/Task/ViewTask.html?v=' + vhtmlcache,
    controller: function ($scope, $rootScope, $http, $sce, $timeout) {
        var $ctrl = this;
        $scope.goRFfunction = function () {
            $ctrl.rfFunction();
        };
        this.$onChanges = () => {
            if (this.congviecid && this.isopentask) {
                $scope.congviec = { CongviecID: this.congviecid };
                $scope.CongviecID = this.congviecid;
                $("#viewTask .InfoViewRequest").addClass('show');
                $("#infocv-overlay").addClass('show');
                $timeout(function () {
                $('#viewTask a[data-target="#ttcv"]').trigger("click");
                    $scope.initCongviec();
                    closeswal();
                }, 500);
            }
        };
        $scope.initCongviec = function () {
            $scope.FilesAttachTaskList = [];
            $scope.giaoviecs = []; $scope.thuchiens = [], $scope.theodois = [];
            $scope.LisFileAttach = [];
            $scope.LisFileAttachCV = [];
            $scope.initTD();
            //messaging
            if (messaging) {
                messaging.onMessage(function (payload) {
                    if (payload.data.hub) {
                        var hub = JSON.parse(payload.data.hub).Data;
                        if ((hub.loai === 10 || hub.loai === 11)) {
                            if (hub.CongviecID === $scope.CongviecID) {
                                $scope.getCongViec();
                                $scope.listDAlog();
                                $scope.listThumuc();
                                $scope.listCharts();
                                if (hub.IsType === 6 || hub.IsType === 12) {
                                    $scope.listBaocaoCongviec($scope.CongviecID);
                                }
                                else if (hub.IsType === 11) {
                                    $scope.goGiahan($scope.congviec);
                                }
                            } else {
                                $scope.RefreshCV();
                            }
                        }
                    }
                });
            }
        };
        $scope.closeInfoRequest = function () {
            $("#viewTask .InfoViewRequest").removeClass('show');
            $("#infocv-overlay").removeClass('show');
        };
        $scope.fullInfoRequest = function () {
            $("#viewTask .InfoViewRequest").toggleClass("fulliframe");
        };
        //Cong viec
        //Phan trang
        $scope.uutiens = [0, 1, 2, 3];
        $scope.opition = {
            numPerPage: 10,
            currentPage: 1,
            sort: 7,
            Trangthai: -1,
            tenSort: "Cập nhật mới nhất",
            tenTT: "Tất cả",
            IsType: 1,
            Group: 1,
            loc: 0,
            sdate: null,
            edate: null,
            loctitle: "Lọc"
        };
        $scope.goTailieu = function (d) {
            $rootScope.link = "tailieu";
            $scope.DuanID = d.DuanID;
            //$rootScope.TenDuan = d.TenDuan
            $state.go('tailieu', { did: d.DuanID, cid: d.CongviecID });
        };
        $scope.goHoatdong = function (d) {
            $rootScope.link = "hoatdong";
            $scope.DuanID = d.DuanID;
            //$rootScope.TenDuan = d.TenDuan
            $state.go('hoatdong', { did: d.DuanID, cid: d.CongviecID });
        };
        $scope.goDuan = function (d) {
            $rootScope.link = "duan";
            $scope.DuanID = d.DuanID;
            //$rootScope.TenDuan = d.TenDuan
            $state.go('vduan', { id: d.DuanID });
        };
        $scope.closeDuan = function () {
            $scope.DuanID = null;
            $state.go('duan');
        };
        $scope.setSort = function (so) {
            $scope.opition.sort = so.id;
            $scope.opition.tenSort = so.text;
            $scope.opition.currentPage = 1;
            $scope.listCongViec();
        };
        $scope.setGroup = function (g) {
            $scope.opition.Group = g;
        };
        $scope.setTT = function (so) {
            if (so) {
                $scope.opition.Trangthai = so.TrangthaiID;
                $scope.opition.tenTT = so.TrangthaiTen;
            } else {
                $scope.opition.Trangthai = -1;
                $scope.opition.tenTT = "Tất cả";
            }
            $scope.opition.currentPage = 1;
            $scope.listCongViec();
        };
        $scope.onSelectPage = function () {
            if ($scope.View === 2 || $scope.View === 3) {
                $scope.opition.IsTBody = false;
                swal.showLoading();
                setTimeout(function () {
                    $(".row.row-content.table-scroll").animate({ scrollTop: 0 }, 500);
                    $scope.$apply(function () {
                        $scope.opition.IsTBody = true;
                    });
                }, 200);
            }
        };
        $scope.setPage = function () {
            //$scope.BindListDuan();

        };
        $scope.serachCV = function () {
            $scope.opition.currentPage = 1;
            $scope.opition.noOfPages = null;
            $scope.listCongViec();
        };
        $scope.RefreshCV = function () {
            $scope.opition.s = "";
            $scope.opition.currentPage = 1;
            $scope.listCongViec();
        };
        $scope.$on('Search', function () {
            $scope.opition.currentPage = 1;
            //$scope.listCongViec();
        });
        $scope.ListTrangThai = [
            { id: -1, text: "Tất cả" },
            { id: 0, text: "Đang lập kế hoạch" },
            { id: 1, text: "Đang thực hiện" },
            { id: 2, text: "Đã hoàn thành" },
            { id: 3, text: "Tạm dừng" },
            { id: 4, text: "Đóng" }
        ];
        $scope.Types = [
            //{ id: -2, text: "Công việc tôi tạo", color:"#6fbf73"},
            { id: 2, text: "Công việc tôi giao", color: "#337ab7" },
            { id: 1, text: "Công việc tôi thực hiện", color: "#5cb85c" },
            { id: 0, text: "Công việc tôi theo dõi", color: "#5bc0de" }
        ];
        $scope.ListSort = [
            { id: 0, text: "A-Z" },
            { id: 1, text: "Z-A" },
            { id: 2, text: "STT thấp nhất" },
            { id: 8, text: "STT cao nhất" },
            { id: 3, text: "Ngày tạo" },
            { id: 4, text: "Ngày tạo mới nhất" },
            { id: 6, text: "Ngày cập nhật" },
            { id: 7, text: "Cập nhật mới nhất" },
            { id: 5, text: "Số ngày quá hạn" }
        ];
        $rootScope.bgColor = [
            "#2196f3", "#009688", "#ff9800", "#795548", "#ff5722"
        ];
        $scope.Colors = [
            "#bbbbbb", "#2a91d6", "#33c9dc", "#51b7ae", "#6fbf73", "#7a87d0", "#ffcd38", "#ff8b4e", "#d87777", "#f17ac7"
        ];
        $scope.setColor = function (co) {
            $scope.duan.Color = co;
        };
        $scope.View = 0;//0 List , 1 Grid
        $scope.loadding = false;
        $scope.duans = [];
        $scope.setView = function (v) {
            $scope.View = v;
            //if (v === 2 || v === 3) {
            //    var cm = new Date().getMonth();
            //    setTimeout(function () {
            //        $(".row.row-content.table-scroll").animate({ scrollLeft: 500 + cm * 29 * 40 }, 500);
            //    }, 100);
            //}
            $scope.opition.IsTBody = false;
            if (v === 2) {
                swal.showLoading();
                $scope.opition.numPerPage = 50;
                $scope.opition.currentPage = 1;
                $scope.opition.noOfPages = Math.ceil($scope.congviecs.length / $scope.opition.numPerPage);
                setTimeout(function () {
                    $(".row.row-content.table-scroll").animate({ scrollTop: 0 }, 500);
                    $scope.$apply(function () {
                        $scope.opition.IsTBody = true;
                    });
                }, 200);

            } else if (v === 3) {
                swal.showLoading();
                $scope.opition.numPerPage = 20;
                $scope.opition.currentPage = 1;
                $scope.opition.noOfPages = Math.ceil($scope.congviecsusers.length / $scope.opition.numPerPage);
                setTimeout(function () {
                    $(".row.row-content.table-scroll").animate({ scrollTop: 0 }, 500);
                    $scope.$apply(function () {
                        $scope.opition.IsTBody = true;
                    });
                }, 200);
            }
        };
        $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
            setTimeout(function () {
                closeswal();
            }, 1000);
        });
        $scope.showusersModal = function (f, index) {
            $rootScope.phongbans.filter(r => r.isCheck).forEach(function (r) {
                r.isCheck = false;
            });
            $rootScope.users.filter(r => r.isCheck).forEach(function (r) {
                r.isCheck = false;
            });
            $scope.vbphongbans = $rootScope.phongbans;
            $scope.vbusers = $rootScope.users;
            $scope.one = index != 2;
            // set chọn user trong lấy list user config cho quy trình
            $scope.IndexListUser = index;
            $("#usersModal").modal("show");
        };

        $scope.choiceUser = function (users) {
            if ($scope.IndexListUser === 0) {
                users = users.filter(u => $scope.giaoviecs.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                $scope.giaoviecs = [users[0]];
            } else if ($scope.IndexListUser === 1) {
                users = users.filter(u => $scope.thuchiens.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                $scope.thuchiens = [users[0]];
            } else {
                users = users.filter(u => $scope.theodois.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                $scope.theodois = $scope.theodois.concat(users);
            }
            $("#usersModal").modal("hide");
        };

        //Todo
        $scope.addTodo = function (cl) {
            cl.tasks.push({ edit: true, IsTodo: true, ChecklistID: cl.ChecklistID, ParentID: cl.CongviecID, Phongban_ID: $rootScope.login.u.Phongban_ID, Trangthai: 0, Congty_ID: $rootScope.login.u.congtyID, STT: cl.tasks.length + 1, IsDelete: false, Tiendo: 0, Uutien: false, IsDeadline: true, YeucauReview: true, DuanID: $scope.DuanID, NhomCongviecID: $scope.congviec.NhomCongviecID });
        };
        $scope.addTodoCL = function (cl) {
            $scope.todo = { edit: true, IsTodo: true, ParentID: $scope.congviec.CongviecID, ChecklistID: cl.ChecklistID, Phongban_ID: $rootScope.login.u.Phongban_ID, Trangthai: 0, Congty_ID: $rootScope.login.u.congtyID, STT: $scope.bcchecklists.length + 1, IsDelete: false, Tiendo: 0, Uutien: false, NgayBatDau: new Date(), IsDeadline: true, YeucauReview: true, DuanID: $scope.congviec.DuanID, NhomCongviecID: $scope.congviec.NhomCongviecID };
            $("#ModalNewsTodo").modal("show");
        };
        $scope.editTodoCL = function (td) {
            $scope.todo = td;
            $("#ModalNewsTodo").modal("show");
        };
        $scope.removeTodo = function (tasks, i) {
            var ta = tasks[i];
            if (!ta.CongviecID) {
                tasks.splice(i, 1);
            } else {
                Swal.fire({
                    title: 'Xác nhận?',
                    text: "Bạn có muốn xóa công việc này không!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#2196f3',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Có!',
                    cancelButtonText: 'Không!',
                }).then((result) => {
                    if (result.value) {
                        tasks.splice(i, 1);
                        $scope.Del_Todo(ta);
                    }
                });
            }
        };
        $scope.Del_Todo = function (d) {
            $http({
                method: "POST",
                url: "Duan/Del_Congviec",
                data: { t: $rootScope.login.tk, id: d.CongviecID },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    window.parent.postMessage({ Type: 1, Isdel: true }, "*");
                    showtoastr("Bạn đã xóa công việc thành công!");
                } else {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Xóa công việc không thành công, vui lòng thử lại!'
                    });
                }
                if ($('#ModalBaocaoCheckList').hasClass('show')) {
                    $scope.goChecklistCV($scope.ocheck);
                    $scope.getCongViec();
                }
            });
        };
        $scope.ocheck = -1;
        $scope.goChecklistCV = function (lo, f) {
            if (f == true) {
                swal.showLoading();
                $scope.ocheck = lo;
            } else {
                $("#ModalBaocaoCheckList").modal("show");
            }
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "App_ListCongviecCheckList", pas: [
                        { "par": "CongviecID ", "va": $scope.CongviecID },
                        { "par": "Type ", "va": lo }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    $scope.bcchecklists = data;
                    $scope.ThongkeCL = {};
                    $scope.ThongkeCL.Tiendo = Math.ceil((data.filter(x => x.IsCheck === true).length) * 100 / data.length) || 0;
                    $scope.ThongkeCL.colortiendo = renderColor($scope.ThongkeCL.Tiendo);
                    $scope.ThongkeCL.bgtiendo = parseInt($scope.ThongkeCL.Tiendo / 10) * 10;
                }
            });
        }

        $scope.editTodo = function (td) {
            td.edit = true;
        };
        $scope.closeTodo = function (td) {
            td.edit = false;
        };

        $scope.openEditTodo = function (ta) {
            $scope.todo = { ...ta };
            $("#ModalDateTodo").modal("show");
        }

        $scope.Update_DateTodo = function () {
            if ($scope.loadding) {
                return false;
            }
            if (!$scope.todo.NgayKetThuc && !$scope.todo.NgayKetThuc && !$scope.todo.Ngaythuchien) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng cập nhật ngày bắt đầu ,ngày xử lý hoặc ngày hoàn thành cho công việc!'
                });
                return false;
            }
            if ($scope.todo.NgayBatDau) {
                $scope.todo.NgayBatDau = moment($scope.todo.NgayBatDau).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
            }
            if ($scope.todo.NgayKetThuc) {
                $scope.todo.NgayKetThuc = moment($scope.todo.NgayKetThuc).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
            }
            if ($scope.todo.Ngaythuchien) {
                $scope.todo.Ngaythuchien = moment($scope.todo.Ngaythuchien).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Duan/Update_Task_DateTodo",
                data: { t: $rootScope.login.tk, model: $scope.todo },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Cập nhật công việc không thành công, vui lòng kiểm tra lại !'
                    });
                    return false;
                }
                showtoastr("Cập nhật công việc thành công!");
                $("#ModalDateTodo").modal("hide");
                $scope.listNhomCV();
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        var lenclcv = 0;
        var bcidx = 0;
        $scope.SendBaoCaoCheckList = function () {
            swal.showLoading();
            var todos = $scope.bcchecklists.filter(x => x.IsChange);
            bcidx = 0;
            lenclcv = todos.length;
            $scope.updateTTCV(todos, 0);
        };

        $scope.updateTTCV = function (todos, k) {
            $http({
                method: "POST",
                url: "Duan/Update_TrangthaiCongviec",
                data: { t: $rootScope.login.tk, id: todos[k].CongviecID, tt: 1, ch: todos[k].IsCheck },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                bcidx += 1;
                if (bcidx < lenclcv) {
                    $scope.updateTTCV(todos, bcidx);
                } else {
                    closeswal();
                    $scope.getCongViec();
                    $scope.goChecklistCV($scope.ocheck);
                }
            });
        };

        $scope.AddCongviecTodo = function (ta, flag) {
            if ($scope.loadding) {
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Duan/Update_Congviec",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    if (ta.NgayBatDau) {
                        ta.NgayBatDau = moment(ta.NgayBatDau).tz(timezone).format('YYYY-MM-DD');
                    }
                    if (ta.NgayKetThuc) {
                        ta.NgayKetThuc = moment(ta.NgayKetThuc).tz(timezone).format('YYYY-MM-DD');
                    }
                    formData.append("t", $rootScope.login.tk);
                    formData.append("todo", true);
                    formData.append("models", JSON.stringify(ta));
                    formData.append("giaoviecs", JSON.stringify($scope.giaoviecs));
                    formData.append("thuchiens", JSON.stringify($scope.thuchiens));
                    formData.append("theodois", JSON.stringify($scope.theodois));
                    $.each($("#frCongviec input[type='file']")[0].files, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                }
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm công việc mới !'
                    });
                    return false;
                }
                ta.CongviecID = res.data.CongviecID;
                $("#CongviecCtr #ModalCongviecAdd").modal("hide");
                showtoastr('Đã cập nhật công việc thành công!.');
                $("#frCongviec input[type='file']").val("");
                $scope.listNhomCV();
                $scope.closeTodo(ta);
                if ($('#ModalNewsTodo').hasClass('show')) {
                    $scope.goChecklistCV($scope.ocheck);
                    if (flag != true) {
                        $("#ModalNewsTodo").modal("hide");
                    } else {
                        $scope.todo = { edit: true, IsTodo: true, ParentID: $scope.congviec.CongviecID, ChecklistID: ta.ChecklistID, Phongban_ID: $rootScope.login.u.Phongban_ID, Trangthai: 0, Congty_ID: $rootScope.login.u.congtyID, STT: $scope.bcchecklists.length + 1, IsDelete: false, Tiendo: 0, Uutien: false, NgayBatDau: new Date(), IsDeadline: true, YeucauReview: true, DuanID: $scope.congviec.DuanID, NhomCongviecID: $scope.congviec.NhomCongviecID };
                    }
                    if ($('#ModalBaocaoCheckList').hasClass('show')) {
                        $scope.getCongViec();
                    }
                }
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        //Gia hạn xử lý
        $scope.setEmojiGH = function (e, type) {
            if (type == false) {
                var noidung = $scope.giahan.Lydo || "";
                var noiDungChat = noidung + e.emoji;
                $scope.giahan.Lydo = noiDungChat;
            } else {
                $scope.giahan.Lydo = e;
            }
            $("#Lydogiahan").focus();
        }
        $scope.setEmojiGiahanTraloi = function (e, type) {
            if (type == false) {
                var noidung = $scope.giahan.Traloi || "";
                var noiDungChat = noidung + e.emoji;
                $scope.giahan.Traloi = noiDungChat;
            } else {
                $scope.giahan.Traloi = e;
            }
            $("#giahantraloi").focus();
        }

        $scope.goGiahan = function (cv) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListCongviecGiahan", pas: [
                        { "par": "CongviecID", "va": cv.CongviecID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    if (!data) data = [];
                    $scope.gianhans = data;
                    $scope.congviec.IsGiahan = ($scope.congviec.IsQH || $scope.congviec.IsTH) && data.filter(x => x.Dongy === null).length == 0;
                    $scope.giahan = {
                        "DuanID": cv["DuanID"],
                        "CongviecID": cv["CongviecID"],
                        "NgayKetThucHienTai": cv["NgayKetThuc"],
                        "HanxulyCu": cv["NgayKetThuc"],
                        "Lydo": "",
                        "Nguoigiahan": $rootScope.login.u.NhanSu_ID,
                        "Ngaygiahan": new Date()
                    };
                }
                closeswal();
            });
        };
        $scope.Del_Giahan = function (u, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa ngày gia hạn này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_Task_GiahanCongviec",
                        data: { t: $rootScope.login.tk, id: u.GiahanID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.gianhans.splice(i, 1);
                            $scope.congviec.IsGiahan = ($scope.congviec.IsQH || $scope.congviec.IsTH) && $scope.gianhans.filter(x => x.Dongy === null).length == 0;
                            showtoastr("Đã xóa ngày gia hạn thành công!");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa ngày gia hạn không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.Update_GiahanCongviec = function (cv) {
            if ($scope.loadding) {
                return false;
            }
            if (!$scope.giahan.HanxulyMoi) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng cập nhật hạn xử lý mới !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Duan/Update_Task_GiahanCongviec",
                data: { t: $rootScope.login.tk, model: $scope.giahan },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Gửi gia hạn công việc không thành công, vui lòng kiểm tra lại !'
                    });
                    return false;
                }
                showtoastr("Gửi gia hạn công việc thành công!");
                $scope.goGiahan($scope.congviec);
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        $scope.openDuyetGaihan = function (bc, f) {
            $("#ModalDuyetGaihan").modal("show");
            $scope.giahan = JSON.parse(JSON.stringify(bc));
            $scope.giahan.Dongy = f;
            $scope.giahan.NgayKetThucHienTai = $scope.congviec.NgayKetThuc;
        };
        $scope.Update_DuyetGiahanCongviec = function () {
            if ($scope.loadding) {
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Duan/Update_Task_DongyGiahanCongviec",
                data: { t: $rootScope.login.tk, model: $scope.giahan },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Duyệt gia hạn công việc không thành công, vui lòng kiểm tra lại !'
                    });
                    return false;
                }
                $("#ModalDuyetGaihan").modal("hide");
                showtoastr("Duyệt gia hạn công việc thành công!");
                $scope.goGiahan($scope.congviec);
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        //Congviec
        var filecv = false;
        $scope.openModalAddFileCV = function (cv) {
            $scope.IPTailieuFiles = [];
            filecv = true;
            $("#ModalFileUpload").modal("show");
        };

        $scope.UploadFileCV = function () {
            if ($scope.IPTailieuFiles.length === 0) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng chọn file để upload !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Duan/UploadFileCV",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    formData.append("DuanID", $scope.DuanID);
                    if ($scope.congviec.CongviecID)
                        formData.append("CongviecID", $scope.congviec.CongviecID || null);
                    $.each($scope.IPTailieuFiles, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                }
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {

                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi upload file !'
                    });
                    return false;
                }
                $("#ModalFileUpload").modal("hide");
                $scope.IPTailieuFiles = [];
                showtoastr("Upload File thành công!");
                $scope.getCongViec();
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };

        $scope.SelectDuan = function (da) {
            if (da) {
                $scope.DuanID = da.DuanID;
                $scope.opition.TenDuan = da.TenDuan;
            } else {
                $scope.DuanID = null;
                $scope.opition.TenDuan = "Tất cả dự án";
            }
            $scope.listCongViec();
        };
        $scope.exportExcel = function (id, name) {
            exportTableToExcel($(id).html(), name);
        };
        $scope.dropCallback = function (item, tt) {
            var o = $scope.TrangthaiCV.find(x => x.TrangthaiID === item.TrangthaiID).congviecs.find(x => x.CongviecID === item.CongviecID);
            //return false;
            $scope.Update_TrangthaiCongviec(o, tt.Id, false);
            return o; // return false to disallow drop
        };
        $scope.trustAsHtml = function (html) {
            return $sce.trustAsHtml(html.replace(/\n/g, "<br/>"));
        };
        $scope.goInfoCV = function (cv) {
            //window.$windowScope = $rootScope;
            //$(".InfoCongviec iframe").attr('src', '');
            //var ti = $(".InfoCongviec").hasClass("show") ? 10 : 500;
            //$(".InfoCongviec").addClass('show');
            //$("#info-overlay").addClass('show');
            //$(".InfoCongviec #loader").show();
            //setTimeout(function () {
            //    var d = new Date();
            //    var n = d.getTime();
            //    $(".InfoCongviec iframe").attr("src", baseUrl + "/task/" + cv.CongviecID + "/" + cv.DuanID + "/1" + "?" + n);
            //    $('.InfoCongviec iframe').on("load", function () {
            //        $(".InfoCongviec #loader").hide();
            //    });
            //}, ti)
            $scope.congviec = { CongviecID: cv.CongviecID };
            $scope.CongviecID = cv.CongviecID;
            $("#viewTask .InfoViewRequest").addClass('show');
            $("#infocv-overlay").addClass('show');
            $timeout(function () {
                $('#viewTask a[data-target="#ttcv"]').trigger("click");
                $scope.initCongviec();
            }, 500);
        };
        $scope.backCV = function () {
            $scope.congviec = { CongviecID: $ctrl.congviecid };
            $scope.CongviecID = $ctrl.congviecid;
            $("#viewTask .InfoViewRequest").addClass('show');
            $("#infocv-overlay").addClass('show');
            $timeout(function () {
                $('#viewTask a[data-target="#ttcv"]').trigger("click");
                $scope.initCongviec();
            }, 500);
        }
        $scope.closeInfoCV = function () {
            $(".InfoCongviec").removeClass('show');
            $("#info-overlay").removeClass('show');
            $(".InfoCongviec iframe").attr('src', '');
        };
        $scope.fullInfoCV = function () {
            $(".InfoCongviec").toggleClass("fulliframe");
            if ($(".InfoCongviec").hasClass("fulliframe")) {
                $(".InfoCongviec iframe").contents().find('div.full-container').css({ "left": "50px" });
            } else {
                $(".InfoCongviec iframe").contents().find('div.full-container').css({ "left": "0px" });
            }
        };
        $scope.filternoParent = function (cv) {
            return cv.ParentID != null && $scope.congviecs.findIndex(x => x.CongviecID == cv.ParentID) === -1;
        };
        $scope.clickCongViec = function (d, $event, tagparent) {
            //$rootScope.link = "vcongviec";
            //$scope.DuanID = d.DuanID;
            ////$rootScope.TenDuan = d.CongviecTen;
            //$state.go('vcongviec', { id: d.CongviecID });
            if ($event) {
                if (tagparent == "ul.ultree") {
                    $(tagparent).find("li.congviecfocus").removeClass("congviecfocus");
                    $($event.target).closest("li").addClass("congviecfocus");
                } else {
                    $(tagparent).find("tr.congviecfocus").removeClass("congviecfocus");
                    $($event.target).closest("tr").addClass("congviecfocus");
                }
            }
            $scope.goInfoCV(d);
        };
        function getDates(startDate, stopDate) {
            var dateArray = [];
            var currentDate = moment(startDate);
            var stopDate = moment(stopDate);
            while (currentDate <= stopDate) {
                var d = moment.utc(currentDate).toDate();
                dateArray.push({ DayN: moment(currentDate).format('DD'), DW: d.getDay(), Day: d, DayName: weekdayS[d.getDay()], Week: d.getWeek(), Month: d.getMonth(), Year: d.getFullYear() })
                currentDate = moment(currentDate).add(1, 'days');
            }
            return dateArray;
        }

        function initDate(data, sdate, edate) {
            //var year = new Date().getFullYear();
            //var sdate = new Date(year, 0, 1);
            //var edate = new Date(year, 11, 31);
            var dat = getDates(sdate, edate);
            var years = [];
            for (var i = sdate.getFullYear(); i <= edate.getFullYear(); i++) {
                for (var j = 0; j < 12; j++) {
                    var Month = { Month: j + 1, Year: i, Dates: [] };
                    Month.Dates = dat.filter(x => x.Month === j && x.Year === i);
                    if (Month.Dates.length > 0)
                        years.push(Month);
                }
            }
            $scope.Grands = years;
            $scope.GrandsDate = dat;
            //
            data.forEach(function (d) {
                var dates = [];// JSON.parse(JSON.stringify(dat));
                var bd = new Date(d.NgayBatDau);
                bd.setHours(0, 0, 0, 0);
                dat.forEach(function (to, i) {
                    var t = { DW: to.DW, TotalDay: parseInt(to.TotalDay), Day: to.Day };
                    if (new Date(t.Day) >= bd && new Date(t.Day) <= new Date(d.NgayKetThuc)) {
                        t.IsCheck = true;
                        t.color = d.color;
                        t.TotalDay = parseInt(d.TotalDay);
                        t.Name = d.CongviecTen;
                        if (i > 0 && dates[i - 1].IsCheck) {
                            t.IsHide = true;
                        }
                    }
                    dates.push(t);
                });
                d.dates = dates;
            });
            //$scope.congviecs = data;
            var users = [];
            data.filter(x => x.Thanhviens).forEach(function (r) {
                r.Thanhviens.forEach(function (u) {
                    var o = users.find(x => x.NhanSu_ID === u.NhanSu_ID);
                    if (!o) {
                        u.congviecids += "," + r.CongviecID;
                        //users.push(JSON.parse(JSON.stringify(u)));
                        users.push(u);
                    } else {
                        o.congviecids += "," + r.CongviecID;
                    }
                });
            });
            users.forEach(function (u) {
                u.congviecs = data.filter(x => u.congviecids.indexOf("," + x.CongviecID) > -1);
                u.congviecs.forEach(function (r) {
                    if (r.Thanhviens) {
                        r[u.NhanSu_ID] = {};
                        r[u.NhanSu_ID].uIsQL = r.Thanhviens.filter(x => x.NhanSu_ID === u.NhanSu_ID && x.IsType === "2").length > 0;
                        r[u.NhanSu_ID].uIsTH = r.Thanhviens.filter(x => x.NhanSu_ID === u.NhanSu_ID && x.IsType === "1").length > 0;
                        r[u.NhanSu_ID].uIsTD = r.Thanhviens.filter(x => x.NhanSu_ID === u.NhanSu_ID && x.IsType === "0").length > 0;
                    }
                });
                u.SoQL = u.congviecs.filter(x => x[u.NhanSu_ID].uIsQL).length;
                u.SoTH = u.congviecs.filter(x => x[u.NhanSu_ID].uIsTH).length;
                u.SoTD = u.congviecs.filter(x => x[u.NhanSu_ID].uIsTD).length;
            });
            $scope.congviecsusers = users;
            //$scope.TrangthaiCV.forEach(function (r) {
            //    if (r === 1) {
            //        r.congviecs = data.filter(x => x.Trangthai === 1 || x.Trangthai === 6);
            //    } else {
            //        r.congviecs = data.filter(x => x.Trangthai === r.TrangthaiID);
            //    }
            //});
        };
        function msConversion(millis) {
            let sec = Math.floor(millis / 1000);
            let hrs = Math.floor(sec / 3600);
            sec -= hrs * 3600;
            let min = Math.floor(sec / 60);
            sec -= min * 60;

            sec = '' + sec;
            sec = ('00' + sec).substring(sec.length);

            if (hrs > 0) {
                min = '' + min;
                min = ('00' + min).substring(min.length);
                return hrs + ":" + min + ":" + sec;
            }
            else {
                return min + ":" + sec;
            }
        }
        $scope.scroolCV = function (cv) {
            var Difference_In_Time = new Date(cv.NgayBatDau).getTime() - new Date($scope.GrandsDate[0].Day).getTime();
            var Difference_In_Days = Difference_In_Time / (1000 * 3600 * 24);
            setTimeout(function () {
                $(".row.row-content.table-scroll").animate({ scrollLeft: Difference_In_Days * 40 - 10 }, 500);
            }, 200);
        };
        var isload = true;
        $scope.FilterCV = function (f) {
            $scope.opition.loc = f;
            var date = new Date();
            switch (f) {
                case -1: //Trong tuần
                    $scope.opition.sdate = null;
                    $scope.opition.edate = null;
                    $scope.opition.loctitle = "Lọc";
                    break;
                case 1: //Trong tuần
                    $scope.opition.sdate = moment().startOf('week').toDate();
                    $scope.opition.edate = moment().endOf('week').toDate();
                    $scope.opition.loctitle = "Trong tuần";
                    break;
                case 2: //Trong tháng
                    $scope.opition.sdate = new Date(date.getFullYear(), date.getMonth(), 1);
                    $scope.opition.edate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
                    $scope.opition.loctitle = "Trong tháng";
                    break;
                case 3: //Trong năm
                    $scope.opition.sdate = new Date(date.getFullYear(), 1, 1);
                    $scope.opition.edate = new Date(date.getFullYear(), 12, 31);
                    $scope.opition.loctitle = "Trong năm";
                    break;
                default:
                    if ($scope.opition.sdate) {
                        if ($scope.opition.edate) {
                            $scope.opition.loctitle = "Từ " + moment($scope.opition.sdate).format('DD/MM/YYYY') + " - " + moment($scope.opition.edate).format('DD/MM/YYYY');
                        } else {
                            $scope.opition.loctitle = "Từ ngày " + moment($scope.opition.sdate).format('DD/MM/YYYY');
                        }
                    } else {
                        if ($scope.opition.edate) {
                            $scope.opition.loctitle = "Đến ngày " + moment($scope.opition.edate).format('DD/MM/YYYY');
                        }
                    }
                    break;
            };
            $("#aloc").trigger("click");
            $scope.listCongViec();
        };
        $scope.setType = function (type) {
            $scope.opition.IsType = type;
            $scope.opition.currentPage = 1;
            $scope.listCongViec();
        }
        $scope.setCty = function (t) {
            $scope.opition.Congty_ID = t != null ? t.Congty_ID : null;
            $scope.opition.currentPage = 1;
            $scope.opition.noOfPages = null;
            $scope.listCongViec();
        };
        $scope.setDA = function (t) {
            $scope.opition.DuanID = t != null ? t.DuanID : null;
            $scope.opition.currentPage = 1;
            $scope.opition.noOfPages = null;
            $scope.listCongViec();
        };
        $scope.openExpandBaocao = function () {
            $("#ModalExpandBaocao").modal("show");
        };
        var arrcongviecs = [];
        $scope.listCongViec = function () {
            $scope.goRFfunction();
            $scope.BindListCountTask();
        };

        $scope.LoadMoreCV = function (t) {
            if (t.p < t.pz) {
                t.p++;
            }
        };

        $scope.xoaDACV = function () {
            $scope.modelcongviec.NhomCongviecID = null;
            $scope.modelcongviec.DuanID = null;
            $scope.nhomcongviecs = [];
            $scope.listNhomCV();
        };

        $scope.changeDA = function () {
            $scope.nhomcongviecs = [];
            $scope.modelcongviec.NhomCongviecID = null;
            $scope.listNhomCV($scope.modelcongviec.DuanID);
        };

        $scope.openModalAddCV = function (nhomcv) {
            var u = $rootScope.users.find(u => u.NhanSu_ID === $rootScope.login.u.NhanSu_ID);
            $scope.nhomcongviec = nhomcv;
            $scope.dtitle = "Tạo công việc " + (nhomcv != null ? " cho nhóm: " + nhomcv.Tennhom : "");
            $scope.modelcongviec = { IsTodo: false, Phongban_ID: $rootScope.login.u.Phongban_ID, Trangthai: 0, NgayBatDau: new Date(), Congty_ID: $rootScope.login.u.congtyID, STT: $scope.tasks.length + 1, IsDelete: false, Tiendo: 0, Uutien: false, IsDeadline: true, YeucauReview: true, DuanID: $scope.DuanID, NhomCongviecID: nhomcv != null ? nhomcv.NhomCongviecID : null };
            $scope.giaoviecs = [u]; $scope.thuchiens = [u], $scope.theodois = [];
            $scope.FilesAttachTaskList = [];
            $scope.LisFileAttach = [];
            $("form.ng-dirty").removeClass("ng-dirty");
            $("#btncontinue").show();
            $("#ModalCongviecAdd").modal("show");
        };
        $scope.openModalAddCVTT = function (tt) {
            if ($scope.Quyen)
                $scope.Quyen.IsBlockQuyen = false;
            else
                $scope.Quyen = { IsBlockQuyen: false };
            if ($rootScope.users) {
                var u = $rootScope.users.find(u => u.NhanSu_ID === $rootScope.login.u.NhanSu_ID);
            } else {
                u = $rootScope.login.u;
            }
            if (tt) {
                $scope.dtitle = "Tạo công việc '" + tt.TrangthaiTen + "'";
            } else {
                $scope.dtitle = "Tạo công việc";
            }
            $scope.modelcongviec = { ParentID: $scope.CongviecID, IsTodo: false, Trangthai: tt ? tt.TrangthaiID : 0, NgayBatDau: new Date(), Phongban_ID: $rootScope.login.u.Phongban_ID, Congty_ID: $rootScope.login.u.congtyID, STT: tt ? tt.congviecs.length + 1 : $scope.congviecs != null ? $scope.congviecs.length + 1 : 1, IsDelete: false, Tiendo: 0, Uutien: false, IsDeadline: true, YeucauReview: true, DuanID: $scope.congviec ? $scope.congviec.DuanID : null };
            $scope.giaoviecs = [u]; $scope.thuchiens = [u], $scope.theodois = [];
            $scope.FilesAttachTaskList = [];
            $scope.LisFileAttach = [];
            $scope.listNhomCV();
            $("form.ng-dirty").removeClass("ng-dirty");
            $("#btncontinue").show();
            $("#ModalCongviecAdd").modal("show");
        };
        $scope.openModalAddCVTTCon = function (d) {
            if ($scope.Quyen)
                $scope.Quyen.IsBlockQuyen = false;
            else
                $scope.Quyen = { IsBlockQuyen: false };
            if ($rootScope.users) {
                var u = $rootScope.users.find(u => u.NhanSu_ID === $rootScope.login.u.NhanSu_ID);
            } else {
                u = $rootScope.login.u;
            }
            $scope.dtitle = "Tạo công việc con"
            $scope.modelcongviec = { ParentID: d.CongviecID, IsTodo: false, Trangthai: 1, NgayBatDau: new Date(), Phongban_ID: $rootScope.login.u.Phongban_ID, Congty_ID: $rootScope.login.u.congtyID, STT: $scope.congviecs.length + 1, IsDelete: false, Tiendo: 0, Uutien: false, IsDeadline: true, YeucauReview: true, DuanID: d.DuanID };
            $scope.giaoviecs = [u]; $scope.thuchiens = [u], $scope.theodois = [];
            $scope.FilesAttachTaskList = [];
            $scope.LisFileAttach = [];
            $scope.listNhomCV();
            $("form.ng-dirty").removeClass("ng-dirty");
            $("#btncontinue").show();
            $("#ModalCongviecAdd").modal("show");
        };
        $scope.onDrop = function (target, source) {
            $scope.DropDragSTTCongViec(source, target);
        };

        $scope.dropValidate = function (target, source) {
            return target !== source;
        };
        $scope.DropDragSTTCongViec = function (fid, tid) {
            $http({
                method: "POST",
                url: "Duan/DropDragSTTCongViec",
                data: { t: $rootScope.login.tk, fid: fid.CongviecID, tid: tid.CongviecID },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                var stt = fid.STT;
                fid.STT = tid.STT;
                tid.STT = stt;
            });
        };

        var objcv, ogvs = [], oths = [], otds = [];
        $scope.openModelEditCongviec = function (r) {
            $scope.dtitle = "Cập nhật công việc";
            $scope.congviec = r;
            $scope.modelcongviec = { ...r };
            $scope.FilesAttachTaskList = [];
            objcv = { ...r };
            if (!$scope.IsViewCV) {
                $scope.giaoviecs = []; $scope.thuchiens = [], $scope.theodois = [];
                $scope.LisFileAttach = [];
                $scope.LisFileAttachCV = [];
                $scope.getCongViec(true);
            }
            $scope.listNhomCV();
            $("form.ng-dirty").removeClass("ng-dirty");
            $("#btncontinue").hide();
            $("#ModalCongviecAdd").modal("show");
        };
        //Valid ngày công việc-----------------------------------------------*/
        $scope.endDateBeforeRender = endDateBeforeRender
        $scope.endDateOnSetTime = endDateOnSetTime
        $scope.startDateBeforeRender = startDateBeforeRender
        $scope.startDateOnSetTime = startDateOnSetTime

        function startDateOnSetTime() {
            $scope.$broadcast('start-date-changed');
        }

        function endDateOnSetTime() {
            $scope.$broadcast('end-date-changed');
        }

        function startDateBeforeRender($dates) {
            if ($scope.modelcongviec) {
                var bd = $scope.duan != null && $scope.duan.NgayBatDau != null ? moment($scope.duan.NgayBatDau) : null;
                var kt = $scope.duan != null && $scope.duan.NgayKetThuc != null ? moment($scope.duan.NgayKetThuc) : null;
                if ($scope.modelcongviec.NgayKetThuc) {
                    var activeDate = moment($scope.modelcongviec.NgayKetThuc);
                    $dates.filter(function (date) {
                        return date.localDateValue() >= activeDate.valueOf() || (kt != null && date.localDateValue() > kt.valueOf()) || (bd != null && date.localDateValue() < bd.valueOf())
                    }).forEach(function (date) {
                        date.selectable = false;
                    })
                } else {
                    $dates.filter(function (date) {
                        return (kt != null && date.localDateValue() > kt.valueOf()) || (bd != null && date.localDateValue() < bd.valueOf())
                    }).forEach(function (date) {
                        date.selectable = false;
                    })
                }
            }
        }

        function endDateBeforeRender($view, $dates) {
            if ($scope.modelcongviec) {
                var bd = $scope.duan != null && $scope.duan.NgayBatDau != null ? moment($scope.duan.NgayBatDau) : null;
                var kt = $scope.duan != null && $scope.duan.NgayKetThuc != null ? moment($scope.duan.NgayKetThuc) : null;
                if ($scope.modelcongviec.NgayBatDau) {
                    var activeDate = moment($scope.modelcongviec.NgayBatDau).subtract(1, $view).add(1, 'minute');
                    $dates.filter(function (date) {
                        return date.localDateValue() <= activeDate.valueOf() || (kt != null && date.localDateValue() > kt.valueOf()) || (bd != null && date.localDateValue() < bd.valueOf())
                    }).forEach(function (date) {
                        date.selectable = false;
                    })
                } else {
                    $dates.filter(function (date) {
                        return (kt != null && date.localDateValue() > kt.valueOf()) || (bd != null && date.localDateValue() < bd.valueOf())
                    }).forEach(function (date) {
                        date.selectable = false;
                    })
                }
            }
        }
        /**/
        function renderMessage() {
            var message = [];
            if (objcv == null) {
                return message;
            }
            if (objcv.CongviecTen != $scope.modelcongviec.CongviecTen) {
                message.push(" - Sửa tên công việc từ <soe>" + objcv.CongviecTen + "</soe> thành <soe>" + $scope.modelcongviec.CongviecTen + "</soe>");
            }
            if (objcv.Mota != $scope.modelcongviec.Mota) {
                message.push(" - Sửa mô tả công việc");
            }
            if (objcv.Muctieu != $scope.modelcongviec.Muctieu) {
                message.push(" - Sửa mục tiêu công việc");
            }
            if (objcv.Ketquadatduoc != $scope.modelcongviec.Ketquadatduoc) {
                message.push(" - Sửa kết quả công việc");
            }
            if (ogvs != null && ogvs != $scope.giaoviecs.map(x => { x.NhanSu_ID, x.fullName })) {
                message.push(" - Thay đổi người giao việc thành <soe>" + ($scope.giaoviecs.map(x => '<soe>' + x.fullName + '</soe>').join(",")) + "</soe>");
            }
            if (oths != null && oths != $scope.thuchiens.map(x => { x.NhanSu_ID, x.fullName })) {
                message.push(" - Thay đổi người thực hiện thành <soe>" + ($scope.thuchiens.map(x => '<soe>' + x.fullName + '</soe>').join(",")) + "</soe>");
            }
            if (otds != null && otds != $scope.theodois.map(x => { x.NhanSu_ID, x.fullName })) {
                message.push(" - Cập nhật thông tin người theo dõi : <soe>" + ($scope.theodois.map(x => '<soe>' + x.fullName + '</soe>').join(",")) + "</soe>");
            }
            return message;
        }
        $scope.AddCongviec = function (frm, fc) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            var message = renderMessage();
            $http({
                method: 'POST',
                url: "Duan/Update_Congviec",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    if ($scope.modelcongviec.NgayBatDau) {
                        $scope.modelcongviec.NgayBatDau = moment($scope.modelcongviec.NgayBatDau).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                    }
                    if ($scope.modelcongviec.NgayKetThuc) {
                        $scope.modelcongviec.NgayKetThuc = moment($scope.modelcongviec.NgayKetThuc).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                    }
                    formData.append("t", $rootScope.login.tk);
                    formData.append("models", JSON.stringify($scope.modelcongviec));
                    if (message.length > 0)
                        formData.append("message", message.join("<br/>"));
                    formData.append("giaoviecs", JSON.stringify($scope.giaoviecs));
                    formData.append("thuchiens", JSON.stringify($scope.thuchiens));
                    formData.append("theodois", JSON.stringify($scope.theodois));
                    $.each($("#frCongviec input[type='file']")[0].files, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                }
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm công việc mới !'
                    });
                    return false;
                }
                if ($scope.CongviecID) {
                    $scope.getCongViec();
                }
                $scope.modelcongviec.CongviecTen = "";
                $("#ModalCongviecAdd").modal("hide");
                showtoastr('Đã cập nhật công việc thành công!.');
                $("#frCongviec input[type='file']").val("");
                $scope.listCongViec();
                window.parent.postMessage({ Type: 1, Isdel: false }, "*");
                //resetForm(frm);
            }, function (response) { // optional
                //resetForm(frm);
                $scope.loadding = false;
            });
        };

        $scope.listComments = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_CongViecListComment", pas: [
                        { "par": "CongviecID ", "va": $scope.congviec.CongviecID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    data[0].forEach(function (r) {
                        if (r.files) {
                            r.files = JSON.parse(r.files);
                        }
                    });
                    $scope.Comments = data[0];
                    $scope.Comments.filter(x => x.ParentID != null).forEach(function (r) {
                        r.Comments = $scope.Comments.filter(x => x.ParentID === r.CommentID);
                        r.ParentComment = $scope.Comments.find(x => r.ParentID === x.CommentID);
                    });
                }
            });
        };

        $scope.App_CongviecNotify = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "App_CongviecNotify", pas: [
                        { "par": "CongviecID ", "va": $scope.congviec.CongviecID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.countCV = data[0][0];
                }
            });
        };

        $scope.listCongviecVanban = function (cv) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListCongviecVanban", pas: [
                        { "par": "CongviecID ", "va": cv.CongviecID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    data.forEach(function (r) {
                        if (r.files) {
                            r.files = JSON.parse(r.files);
                        }
                    });
                    $scope.vanbans = data;
                }
            });
        };
        $scope.viewVB = function (vb, f) {
            if (f == true) {
                window.open(
                    'http://sdoc.' + rootUrl + '#!/home/' + vb.vanBanMasterID,
                    '_blank' // <- This is what makes it open in a new window.
                );
            } else {
                location.href = 'http://sdoc.' + rootUrl + '#!/home/' + vb.vanBanMasterID;
            }
        };
        $scope.openModalAddVanbanCV = function (cv) {
            $("#ModalAddVanbanCV").modal("show");
            $scope.listVanbanlienquan(cv);
        };
        $scope.UpdateVanbanCongviec = function () {
            if ($scope.loadding) {
                return false;
            }
            var vanbans = $scope.tdvanbans.filter(x => x.selected).map(x => x.vanBanMasterID);
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Duan/Update_VanbanCongviecAll",
                data: { t: $rootScope.login.tk, congviecid: $scope.congviec.CongviecID, vanbans: vanbans },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                //closeswal();
                $("#ModalAddVanbanCV").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm văn bản liên quan !'
                    });
                    return false;
                }
                $scope.listCongviecVanban($scope.congviec);
                showtoastr("Thêm văn bản liên quan thành công!");
            }, function (res) { // optional
                $scope.loadding = false;
                closeswal();
                $("#ModalAddVanbanCV").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi gửi Review công việc !'
                    });
                    return false;
                }
            });
        };
        $scope.listVanbanlienquan = function (cv) {
            $scope.vbsearch = "";
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListTDVanban", pas: [
                        { "par": "CongviecID ", "va": cv.CongviecID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    data.forEach(function (r) {
                        if (r.files) {
                            r.files = JSON.parse(r.files);
                        }
                    });
                    $scope.tdvanbans = data;
                }
            });
        };

        $scope.Del_VanbanCongViec = function (files, i, f) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa văn bản liên quan này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_VanbanCongviec",
                        data: { t: $rootScope.login.tk, id: f.CongviecVanbanID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            files.splice(i, 1);
                            showtoastr("Văn bản bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa văn bản không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };

        $scope.getCongViec = function (f) {
            $scope.LisFileAttachCV = [];
            $scope.checklists = [];
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ChiTietCongViecNhanSu", pas: [
                        { "par": "CongviecID ", "va": $scope.congviec.CongviecID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "Congty_ID ", "va": $rootScope.login.u.congtyID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.giaoviecs = data[1].filter(x => x.IsType === 2);
                    $scope.thuchiens = data[1].filter(x => x.IsType === 1);
                    $scope.theodois = data[1].filter(x => x.IsType === 0);
                    ogvs = $scope.giaoviecs.map(x => { x.NhanSu_ID, x.fullName });
                    oths = $scope.thuchiens.map(x => { x.NhanSu_ID, x.fullName });
                    otds = $scope.theodois.map(x => { x.NhanSu_ID, x.fullName });
                    if (data[0].length > 0) {
                        data[0][0].IsQL = data[1].filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === 2).length > 0;
                        data[0][0].IsTH = data[1].filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === 1).length > 0;
                        data[0][0].IsTD = data[1].filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === 0).length > 0;
                        data[0][0].IsType = data[0][0].IsQL ? 2 : (data[0][0].IsTH ? 1 : 0);
                        data[0][0].Tao = data[0][0].NguoiTao === $rootScope.login.u.NhanSu_ID;
                    }
                    var arr = [];
                    data[1].forEach(function (u) {
                        if (arr.filter(x => x.NhanSu_ID === u.NhanSu_ID).length === 0) {
                            arr.push(u);
                        }
                    });
                    $scope.thanhviens = arr;
                    $scope.LisFileAttachCV = data[2];
                    data[3].forEach(function (r) {
                        r.stisks = JSON.parse(r.stisks);
                        if (r.files)
                            r.files = JSON.parse(r.files);
                    });
                    $scope.Comments = data[3];
                    $scope.Comments.filter(x => x.ParentID != null).forEach(function (r) {
                        r.Comments = $scope.Comments.filter(x => x.ParentID === r.CommentID);
                        r.ParentComment = $scope.Comments.find(x => r.ParentID === x.CommentID);
                    });
                    if (data[0].length > 0) {
                        data[0][0].color = renderColor(data[0][0].Tiendo);
                        $scope.congviec = data[0][0];
                        //if (!$scope.DuanID) {
                        //    $scope.DuanID = $scope.congviec.DuanID;
                        //}
                    }
                    //if (todos.length === 0) {
                    data[5].forEach(function (r, i) {
                        r.Thuchiens = JSON.parse(r.Thuchiens);
                        if (!r.TenNhomDuan)
                            r.TenNhomDuan = "Chưa phân nhóm";
                        if ($scope.ListTrangThai) {
                            var o = $scope.ListTrangThai.find(x => x.id == r.Trangthai);
                            if (o)
                                r.TenTT = o.text;
                        }
                        r.color = renderColor(r.tiendo);
                    });
                    todos = data[5];
                    //}
                    data[4].forEach(function (r) {
                        r.tasks = todos.filter(x => x.ChecklistID === r.ChecklistID);
                    });
                    $scope.checklists = data[4];
                    var duid = $scope.congviec.DuanID && $scope.congviec.DuanID != 'null' ? $scope.congviec.DuanID : null;
                    if (data[6].length === 0) {
                        $scope.cauhinhTao = {
                            DuanID: $scope.DuanID,
                            CongviecID: $scope.CongviecID,
                            Congty_ID: $rootScope.login.u.congtyID,
                            CapnhatTT: true,//Cập nhật trạng thái công việc
                            CommentCV: true,//Bình luận công việc
                            IsFullCV: true,
                            IsSubTask: true,
                            IsCopy: true,
                            EditCV: true,//Chỉnh sửa công việc
                            AddQL: true,//Thêm thành viên quản lý
                            AddTH: true,//Thêm thành viên thực hiện
                            AddTD: true,//Thêm thành viên theo dõi
                            AddSetting: true,//Truy cập setting
                            AddThumuc: true,//Thêm thư mục
                            AddTailieu: true,//Thêm tài liệu
                            ViewReport: true,//Truy cập báo cáo
                            ViewTiendo: true,//Xem báo cáo công việc
                            AddTiendo: true,//Cập nhật tiến độ công việc
                            AddReview: true,//Được review công việc
                            AddDanhgia: true,//Được đánh giá công việc
                            DelCV: true,//Xóa công việc
                            IsType: 3,
                            IsTV: true
                        };
                        $scope.cauhinhQL = {
                            DuanID: duid,
                            CongviecID: $scope.congviec.CongviecID,
                            Congty_ID: $rootScope.login.u.congtyID,
                            CapnhatTT: true,//Cập nhật trạng thái công việc
                            CommentCV: true,//Bình luận công việc
                            IsFullCV: true,
                            IsSubTask: true,
                            IsCopy: true,
                            EditCV: true,//Chỉnh sửa công việc
                            AddQL: true,//Thêm thành viên quản lý
                            AddTH: true,//Thêm thành viên thực hiện
                            AddTD: true,//Thêm thành viên theo dõi
                            AddSetting: true,//Truy cập setting
                            AddThumuc: true,//Thêm thư mục
                            AddTailieu: true,//Thêm tài liệu
                            ViewReport: true,//Truy cập báo cáo
                            ViewTiendo: true,//Xem báo cáo công việc
                            AddTiendo: true,//Cập nhật tiến độ công việc
                            AddReview: true,//Được review công việc
                            AddDanhgia: true,//Được đánh giá công việc
                            DelCV: true,//Xóa công việc
                            IsType: 2,
                            IsTV: true
                        };
                        $scope.cauhinhTH = {
                            DuanID: duid,
                            CongviecID: $scope.congviec.CongviecID,
                            Congty_ID: $rootScope.login.u.congtyID,
                            CapnhatTT: false,//Cập nhật trạng thái công việc
                            CommentCV: true,//Bình luận công việc
                            EditCV: true,//Chỉnh sửa công việc
                            IsFullCV: true,
                            IsSubTask: true,
                            IsCopy: true,
                            AddQL: false,//Thêm thành viên quản lý
                            AddTH: true,//Thêm thành viên thực hiện
                            AddTD: true,//Thêm thành viên theo dõi
                            AddSetting: false,//Truy cập setting
                            AddThumuc: true,//Thêm thư mục
                            AddTailieu: true,//Thêm tài liệu
                            ViewReport: true,//Truy cập báo cáo
                            ViewTiendo: true,//Xem báo cáo công việc
                            AddTiendo: true,//Cập nhật tiến độ công việc
                            AddReview: false,//Được review công việc
                            AddDanhgia: true,//Được đánh giá công việc
                            DelCV: false,//Xóa công việc
                            IsType: 1,
                            IsTV: false
                        };
                        $scope.cauhinhTD = {
                            DuanID: duid,
                            CongviecID: $scope.congviec.CongviecID,
                            Congty_ID: $rootScope.login.u.congtyID,
                            CapnhatTT: false,//Cập nhật trạng thái công việc
                            CommentCV: true,//Bình luận công việc
                            IsFullCV: false,
                            IsSubTask: false,
                            IsCopy: true,
                            EditCV: false,//Chỉnh sửa công việc
                            AddQL: false,//Thêm thành viên quản lý
                            AddTH: false,//Thêm thành viên thực hiện
                            AddTD: true,//Thêm thành viên theo dõi
                            AddSetting: false,//Truy cập setting
                            AddThumuc: true,//Thêm thư mục
                            AddTailieu: true,//Thêm tài liệu
                            ViewReport: true,//Truy cập báo cáo
                            ViewTiendo: true,//Xem báo cáo công việc
                            AddTiendo: false,//Cập nhật tiến độ công việc
                            AddReview: false,//Được review công việc
                            AddDanhgia: true,//Được đánh giá công việc
                            DelCV: false,//Xóa công việc
                            IsType: 0,
                            IsTV: false
                        };
                    } else {
                        $scope.cauhinhQL = data[6].find(x => x.IsType === 2);
                        $scope.cauhinhTH = data[6].find(x => x.IsType === 1);
                        $scope.cauhinhTD = data[6].find(x => x.IsType === 0);
                        $scope.cauhinhTao = data[6].find(x => x.IsType === 3);
                        if (!$scope.cauhinhQL.CongviecID) {
                            $scope.cauhinhTao.CongviecID = $scope.congviec.CongviecID;
                            $scope.cauhinhTao.DuanID = duid;
                            $scope.cauhinhQL.CongviecID = $scope.congviec.CongviecID;
                            $scope.cauhinhQL.DuanID = duid;
                            $scope.cauhinhTH.CongviecID = $scope.congviec.CongviecID;
                            $scope.cauhinhTH.DuanID = duid;
                            $scope.cauhinhTD.CongviecID = $scope.congviec.CongviecID;
                            $scope.cauhinhTD.DuanID = duid;
                        }
                    }
                    if (data[7] && data[7].length > 0 && data[7][0].CauhinhCVID != null) {
                        $scope.Quyen = data[7][0];
                    } else if (data[0][0].IsQL || (data[0][0].Tao && !data[0][0].IsTH)) {
                        $scope.Quyen = $scope.cauhinhQL;
                    } else if (data[0][0].IsTH) {
                        $scope.Quyen = $scope.cauhinhTH;
                    } else {
                        $scope.Quyen = $scope.cauhinhTD;
                    }
                    //if (data[0][0].Tao && data[0][0].IsTH && !data[0][0].IsQL) {
                    //    $scope.Quyen.CapnhatTT = false;
                    //}
                    //$scope.Quyen.IsBlockQuyen = data[0][0].Tao && data[0][0].IsTH && !data[0][0].IsQL;
                    $scope.Quyen.IsBlockQuyen = !$scope.Quyen.IsFullCV;//(data[0][0].Tao && data[0][0].IsTH && !data[0][0].IsQL)||!data[0][0].IsQL;
                    $scope.Quyen.IsTV = $scope.Quyen.AddQL && $scope.Quyen.AddTH && $scope.Quyen.AddTD;
                    $scope.Quyen.FunCV = $scope.Quyen.EditCV && $scope.congviec.Trangthai !== 4 && $scope.congviec.Trangthai !== 7;
                    $rootScope.TenDuanCV = $scope.congviec.TenDuan;
                    //$rootScope.TenDuan = $scope.congviec.CongviecTen;
                    $rootScope.did = $scope.congviec.DuanID;
                    var tt = $scope.congviec.Trangthai;
                    $scope.congviec.isclosett = tt == 0 || tt == 2 || tt == 3 || tt == 4 || tt == 7;
                    $scope.congviec.isclose = ($scope.congviec.IsQL != true && $scope.congviec.IsTH != true) || (tt == 0 || tt == 2 || tt == 3 || tt == 4 || tt == 7);
                    $scope.isdelcomment = !$scope.congviec.isclose && ($scope.congviec.IsQL == true || $scope.congviec.IsTH == true);
                    //List Tags
                    if (f) {
                        $scope.modelcongviec = $scope.congviec;
                    }
                    $timeout(function () {
                        $scope.BindListDuan();
                        $scope.listNhomCV();
                        $scope.listCharts()
                        $scope.ListTagsCongviec($scope.congviec);
                        $scope.listCongviecVanban($scope.congviec);
                        $scope.listSubTask();
                        $scope.App_CongviecNotify();
                    }, 500);
                    
                }
            });
        };

        $scope.Del_CongviecThamGia = function (ds, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa user này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_CongviecThamGia",
                        data: { t: $rootScope.login.tk, ids: [ds[i].CongviecThamgiaID] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.listCongViec();
                            window.parent.postMessage({ Type: 1, Isdel: false }, "*");
                            ds.splice(i, 1);
                            showtoastr("User bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa User không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.Del_Congviec = function (d, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa công việc này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_Congviec",
                        data: { t: $rootScope.login.tk, id: d.CongviecID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            window.parent.postMessage({ Type: 1, Isdel: true }, "*");
                            $scope.listCongViec();
                            showtoastr("Bạn đã xóa công việc thành công!");
                            if ($scope.CongviecID != null) {
                                $scope.link = "congviec";
                                $state.go('congviec');
                            }
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa công việc không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.Copy_Congviec = function (d) {
            Swal.fire({
                title: 'Nhập tên mới cho công việc bạn Copy',
                inputValue: "Copy_" + d.CongviecTen,
                input: 'text',
                inputAttributes: {
                    autocapitalize: 'off'
                },
                customClass: {
                    title: 'title-Swal-input',
                },
                showCancelButton: true,
                confirmButtonText: 'OK',
                showLoaderOnConfirm: true,
                preConfirm: (ten) => {
                    $http({
                        method: "POST",
                        url: "Duan/Copy_Congviec",
                        data: { t: $rootScope.login.tk, id: d.CongviecID, ten: ten },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.listCongViec();
                            Swal.fire(
                                '',
                                'Copy công việc thành công!.',
                                'success'
                            );
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Copy không thành công, vui lòng thử lại!'
                            });
                        }
                    });
                },
                allowOutsideClick: () => !Swal.isLoading()
            }).then((result) => {
                if (result.value) {
                    //Swal.fire({
                    //    icon: 'error',
                    //    type: 'error',
                    //    title: '',
                    //    text: 'Copy không thành công, vui lòng thử lại!'
                    //});
                }
            })
        };
        $scope.scrollTo = function (id) {
            var elem = $(id);
            $("#taskmessagepanel").animate({ scrollTop: $("#taskmessagepanel").scrollTop() + elem.offset().top - 150 }, { duration: 'medium', easing: 'swing' });
        };
        $scope.openTiendo = function (d) {
            if (d) $scope.congviec = d;
            $scope.iptiendo = $scope.congviec.Tiendo;
            $scope.ipcolor = renderColor(parseInt($scope.iptiendo));
            $("#ModalTiendo").modal("show");
            //Swal.fire({
            //    title: 'Cập nhật tiến độ cho công việc "' + $scope.congviec.CongviecTen+'"',
            //    icon: 'question',
            //    input: 'range',
            //    inputAttributes: {
            //        min: 8,
            //        max: 120,
            //        step: 1
            //    },
            //    inputValue: 25
            //})
        };
        $scope.setColor = function () {
            $scope.ipcolor = renderColor(parseInt($scope.iptiendo));
        };
        $scope.setColorReview = function () {
            $scope.Review.ipcolor = renderColor(parseInt($scope.Review.IPTiendo));
        };
        $scope.UpdateTiendoCV = function (cv, va) {
            $("#ModalTiendo").modal("hide");
            $http({
                method: "POST",
                url: "Duan/Update_TiendoCongviec",
                data: { t: $rootScope.login.tk, id: cv.CongviecID, vl: va },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                $scope.congviec.Tiendo = va;
                if (va == 100) {
                    $scope.congviec.IsCheck = true;
                } else if (v > 0 && $scope.congviec.Trangthai === 0) {
                    $scope.congviec.Trangthai = 1;
                }
                $scope.congviec.color = renderColor($scope.congviec.Tiendo);
                $scope.congviec.txtcolor = renderTxtColor($scope.congviec.Tiendo);
            });
        };

        $scope.Update_TrangthaiCongviecCheckList = function (cv, tt, ch, f) {//f=true: Check ở công việc
            $("#ModalTiendo").modal("hide");
            if (!ch && tt == null) {
                tt = 1;
            }
            $http({
                method: "POST",
                url: "Duan/Update_TrangthaiCongviec",
                data: { t: $rootScope.login.tk, id: cv.CongviecID, tt: tt, ch: ch },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.congviec.Trangthai = tt;
                cv.Trangthai = tt;
                var o = $scope.tudien[3].find(x => x.TrangthaiID == tt);
                cv.TrangthaiTen = o.TrangthaiTen;
                cv.ttcolor = o.color;
                $scope.congviec.TrangthaiTen = o.TrangthaiTen;
                $scope.congviec.ttcolor = o.color;
            });
        };

        $scope.Update_TrangthaiCongviec = function (cv, tt, ch, f) {//f=true: Check ở công việc
            $("#ModalTiendo").modal("hide");
            if (!ch && tt == null) {
                tt = 1;
            }
            $http({
                method: "POST",
                url: "Duan/Update_TrangthaiCongviec",
                data: { t: $rootScope.login.tk, id: cv.CongviecID, tt: tt, ch: ch },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.congviec.Trangthai = tt;
                cv.Trangthai = tt;
                var o = $scope.tudien[3].find(x => x.TrangthaiID == tt);
                cv.TrangthaiTen = o.TrangthaiTen;
                cv.ttcolor = o.color;
                $scope.congviec.TrangthaiTen = o.TrangthaiTen;
                $scope.congviec.ttcolor = o.color;
                if (!$rootScope.checkToken(res)) return false;
                if (tt == 4) {
                    $scope.congviec.IsCheck = true;
                }
                $scope.listCongViec();
                if ($scope.IsViewCV) $scope.getCongViec();
            });
        };

        $scope.Update_HanxulyCongviec = function (cv, ngay) {//f=true: Check ở công việc
            $http({
                method: "POST",
                url: "Duan/Update_HanxulyCongviec",
                data: { t: $rootScope.login.tk, id: cv.CongviecID, ngay: ngay },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                showtoastr("Cập nhật hạn xử lý thành công!");
            });
        };

        $scope.Update_ThanhvienThamgiaCongviec = function (cv, tgid, tgname, type) {//f=true: Check ở công việc
            $http({
                method: "POST",
                url: "Duan/Update_ThanhvienThamgiaCongviec",
                data: { t: $rootScope.login.tk, id: cv.CongviecID, tgid: tgid, tgname: tgname, type: type },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                showtoastr("Thêm người xử lý thành công!");
            });
        };
        $scope.removeUserobj = function (us, obj) {
            var idx = us.findIndex(x => x.DuanThamgiaID === obj.DuanThamgiaID && x.CongviecThamgiaID === obj.CongviecThamgiaID && x.NhanSu_ID === obj.NhanSu_ID);
            if (us[idx].DuanThamgiaID) {
                $scope.Del_DuanThamGia(us, idx);
            } else if (us[idx].CongviecThamgiaID) {
                $scope.Del_CongviecThamGia(us, idx);
            } else {
                us.splice(idx, 1);

            }
        };
        $scope.Update_ThanhVienCongviec = function () {
            $http({
                method: "POST",
                url: "Duan/Update_ThanhVienCongviec",
                data: { t: $rootScope.login.tk, CongviecID: $scope.congviec.CongviecID, tgs: $scope.DAThanhviens.filter(x => !x.CongviecThamgiaID) },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                showtoastr("Thêm người tham gia thành công!");
            });
        };
        $scope.clickAddUserCongviecTG = function (m, us, u, i) {
            $scope.filterUser = null;
            if (!us) {
                us = [];
            }
            u.IsType = i;
            us.push(u);
            m["searchU" + i] = "";
            $("input.ipautoUser" + i + ".true").focus();
            $scope.Update_ThanhVienCongviec();
            if ($scope.IsViewCV) $scope.getCongViec();
        };
        $scope.getThanhvienCongviec = function (id) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_GetCongviecThamgia", pas: [
                        { "par": "CongviecID ", "va": id }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    data.forEach(function (u) {
                        u.IsDel = $scope.Quyen.AddQL || u.NguoiTao === $rootScope.login.u.NhanSu_ID;
                    });
                    $scope.DAThanhviens = data;
                    $scope.congviec.Thanhviens = data;
                }
            });
        }
        $scope.openQuanlythanhvien = function (cv) {
            $scope.congviec = cv;
            $scope.DAThanhviens = [];
            //$("#ModalThanhviens").modal("show");
            $scope.getThanhvienCongviec(cv.CongviecID);
        };
        //EndCongviec
        //ModalProfile
        $scope.goProfile = function (u) {
            $scope.UserProfile = $rootScope.users.find(x => x.NhanSu_ID === u.NhanSu_ID);
            $("#ModalProfile").modal("show");
        };
        //End ModalProfile
        $scope.openModalAddDA = function () {
            $scope.dtitle = "Tạo dự án mới";
            $scope.duan = { LoaiDuan: 0, Trangthai: 0, Congty_ID: $rootScope.login.u.congtyID, STT: $scope.duans.length + 1, YeucauReview: true };
            $scope.quantris = []; $scope.thamgias = [], $scope.phongbantgs = [];
            $scope.FilesAttachTaskList = [];
            $scope.LisFileAttach = [];
            $("form.ng-dirty").removeClass("ng-dirty");
            $("#ModalAdd").modal("show");
        };
        $scope.openModelEditDA = function (r) {
            $scope.dtitle = "Cập nhật dự án";
            $scope.duan = r;
            $scope.quantris = []; $scope.thamgias = [], $scope.phongbantgs = [];
            $scope.FilesAttachTaskList = [];
            $scope.LisFileAttach = [];
            $scope.getDA();
            $("form.ng-dirty").removeClass("ng-dirty");
            $("#ModalAdd").modal("show");
        };
        $scope.getDA = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_GetDuan", pas: [
                        { "par": "DuanID", "va": $scope.duan.DuanID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.duan = data[0][0];
                    $scope.quantris = data[1];
                    $scope.thamgias = data[2];
                    $scope.phongbantgs = data[3];
                    $scope.LisFileAttach = data[4];
                }
            });
        };
        $scope.DeleteFiles = function (i, f) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa file này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_DuanFile",
                        data: { t: $rootScope.login.tk, ids: [f.FileID] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            if ($scope.LisFileAttach) {
                                var idx = $scope.LisFileAttach.findIndex(x => x.FileID === f.FileID);
                                if (idx !== -1)
                                    $scope.LisFileAttach.splice(idx, 1);
                            }
                            if ($scope.LisFileAttachCV) {
                                idx = $scope.LisFileAttachCV.findIndex(x => x.FileID === f.FileID);
                                if (idx !== -1)
                                    $scope.LisFileAttachCV.splice(idx, 1);
                            }
                            showtoastr("File bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa File không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.Del_DuanPhongBan = function (ds, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa phòng ban này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_DuanPhongBan",
                        data: { t: $rootScope.login.tk, ids: [ds[i].DuanPhongID] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            ds.splice(i, 1);
                            showtoastr("Phòng ban bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa Phòng ban không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };

        $scope.Del_DuanThamGia = function (ds, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa user này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_DuanThamGia",
                        data: { t: $rootScope.login.tk, ids: [ds[i].DuanThamgiaID] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            ds.splice(i, 1);
                            showtoastr("User bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa User không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.DelDuan = function (d, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa dự án này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_Duan",
                        data: { t: $rootScope.login.tk, id: d.DuanID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.duans.splice(i, 1);
                            showtoastr("Dự án bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa dự án không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.AddDuan = function (frm) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Duan/Update_Duan",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    if ($scope.duan.NgayBatDau) {
                        $scope.duan.NgayBatDau = moment($scope.duan.NgayBatDau).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                    }
                    if ($scope.duan.NgayKetThuc) {
                        $scope.duan.NgayKetThuc = moment($scope.duan.NgayKetThuc).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                    }
                    formData.append("t", $rootScope.login.tk);
                    formData.append("duan", JSON.stringify($scope.duan));
                    formData.append("phongbans", JSON.stringify($scope.phongbantgs));
                    formData.append("thamgias", JSON.stringify($scope.thamgias));
                    formData.append("quantris", JSON.stringify($scope.quantris));
                    $.each($("#CongviecCtr input[type='file']")[0].files, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                }
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm dự án mới !'
                    });
                    return false;
                }
                $("#CongviecCtr #ModalAdd").modal("hide");
                showtoastr('Đã cập nhật dự án thành công!.');
                $scope.BindListDuan();
                //resetForm(frm);
            }, function (response) { // optional
                //resetForm(frm);
                $scope.loadding = false;
            });
        };
        //@Tag thanh vien
        $scope.removeUser = function (us, idx) {
            if (us[idx].DuanThamgiaID) {
                $scope.Del_DuanThamGia(us, idx);
            } else if (us[idx].CongviecThamgiaID) {
                $scope.Del_CongviecThamGia(us, idx);
            } else {
                us.splice(idx, 1);

            }
        };
        $scope.removePB = function (us, idx) {
            if (us[idx].DuanPhongID) {
                $scope.Del_DuanPhongBan(us, idx);
            } else {
                us.splice(idx, 1);
            }
        };
        $scope.removeFiles = function (idx) {
            $scope.FilesAttachTaskList.splice(idx, 1);
        };
        $scope.removeFilesList = function (list, idx) {
            list.splice(idx, 1);
        };


        $scope.clickAddUser = function (m, us, u, i) {
            $scope.filterUser = null;
            if (!us || i != 2) {
                us = [];
            }
            if (i == "") {
                $scope.giaoviecs = [u];
            } else if (i == "1") {
                $scope.thuchiens = [u];
            } else {
                us.push(u);
            }
            m["searchU" + i] = "";
            $("input.ipautoUser" + i + ".true").focus();
            if (m.IsTodo) {
                m.Thuchiens = us;
                $scope.Update_ThanhvienThamgiaCongviec(m, u.NhanSu_ID, u.fullName, 1);
            }
        };

        $scope.clickAddPB = function (m, us, u) {
            $scope.filterUser = null;
            us.push(u);
            m.searchU2 = "";
            $("input.au2").focus();
        };
        $scope.completecm = function ($event) {
            txtstring = $event.target.innerHTML;
            string = txtstring.trim();
            if (!string.endsWith("@")) return false;
            var arrs = string.split("@");
            string = arrs[arrs.length - 1];
            $scope.congviec.Focuscm = true;
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                $("ul.autoUsercm").focus();
                $event.preventDefault();
            }
            string = string.replace("@", "");
            var output = [];
            if (!string) {
                string = "";
            }
            angular.forEach($scope.thanhviens, function (u) {
                if ((u.fullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.enFullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.tenTruyCap || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.phone + '').toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    output.push(u);
                }
            });
            $scope.filterUsercm = output.filter(x => x.NhanSu_ID !== $rootScope.login.u.NhanSu_ID);
        };
        $scope.complete = function (string, m, $event, i) {
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            string = string.replace("@", "");
            if ($scope.duans) {
                if (m) {
                    m.Focus = false;
                    m.Focus1 = false;
                    m["Focus" + i] = true;
                }
                $scope.duans.filter(x => x.Focus === true).forEach(function (r) {
                    r.Focus = false;
                    r.Focus1 = false;
                    r.Focus2 = false;
                });
            }
            var output = [];
            if (!string) {
                string = "";
            }
            angular.forEach($rootScope.users, function (u) {
                if ((u.fullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.enFullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.tenTruyCap || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.phone + '').toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    if (!m || m.NhanSu_ID !== u.NhanSu_ID)
                        output.push(u);
                }
            });
            $scope.filterUser = output;
        };
        $scope.handleKeyDowncm = function ($event) {
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                $event.preventDefault();
                if ($scope.focusedIndex !== $scope.filterUsercm.length - 1) {
                    $scope.focusedIndex++;
                }
            }
            else if (keyCode === 38) {
                $event.preventDefault();
                if ($scope.focusedIndex !== 0) {
                    $scope.focusedIndex--;
                }
            }
            else if (keyCode === 13 && $scope.focusedIndex >= 0) {
                $event.preventDefault();
                $scope.clickAddUsercm($scope.filterUsercm[$scope.focusedIndex]);
            }
        };
        function setEndOfContenteditable(contentEditableElement) {
            var range, selection;
            if (document.createRange)//Firefox, Chrome, Opera, Safari, IE 9+
            {
                range = document.createRange();//Create a range (a range is a like the selection but invisible)
                range.selectNodeContents(contentEditableElement);//Select the entire contents of the element with the range
                range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
                selection = window.getSelection();//get the selection object (allows you to change selection)
                selection.removeAllRanges();//remove any selections already made
                selection.addRange(range);//make the range you have just created the visible selection
            }
            else if (document.selection)//IE 8 and lower
            {
                range = document.body.createTextRange();//Create a range (a range is a like the selection but invisible)
                range.moveToElementText(contentEditableElement);//Select the entire contents of the element with the range
                range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
                range.select();//Select the range (make it the visible selection
            }
        }
        $scope.clickAddUsercm = function (u) {
            $scope.filterUsercm = null;
            var string = $scope.noiDungChat.Noidung;
            string = string.substring(0, string.lastIndexOf("@") - 1) + " <user><uid>" + u.NhanSu_ID + "</uid>@" + u.fullName + "</user>";
            $scope.noiDungChat.Noidung = string + "&nbsp;";
            setTimeout(function () {
                elem = document.getElementById('noiDungChatCVComment');//This is the element that you want to move the caret to the end of
                setEndOfContenteditable(elem);
            })
        };
        $scope.handleKeyDown = function (m, $event, f) {
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                //$("ul.autoUser").focus();
                $event.preventDefault();
                if ($scope.focusedIndex !== $scope.filterUser.length - 1) {
                    $scope.focusedIndex++;
                }
            }
            else if (keyCode === 38) {
                // Up
                $event.preventDefault();
                if ($scope.focusedIndex !== 0) {
                    $scope.focusedIndex--;
                }
            }
            else if (keyCode === 13 && $scope.focusedIndex >= 0) {
                // Enter
                $event.preventDefault();
                $scope.clickAddUser(m, $scope.filterUser[$scope.focusedIndex], f);
            }
        };

        $scope.completePB = function (string, m, $event) {
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.au2").focus();
                $event.preventDefault();
            }
            string = string.replace("@", "");
            if ($scope.duans) {
                m.Focus = false;
                m.Focus1 = false;
                m.Focus2 = false;
                $scope.duans.filter(x => x.Focus2 === true).forEach(function (r) {
                    r.Focus2 = false;
                });
            }
            m.Focus2 = true;
            var output = [];
            if (!string) {
                string = "";
            }
            angular.forEach($rootScope.phongbans, function (u) {
                if ((u.tenPhongban || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.tenkhongdau || "").toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    if (m.Phongban_ID !== u.Phongban_ID)
                        output.push(u);
                }
            });
            $scope.filterPB = output;
        };
        $scope.openAttachfile = function (ev) {
            $(ev.target).closest("div.form-group").find("input[type='file']").trigger("click");
        };
        $scope.UploadFiles = function (f) {
            if (!$scope.LisFileAttach) $scope.LisFileAttach = [];
            if (!$scope.FilesAttachTaskList) $scope.FilesAttachTaskList = [];
            if ($scope.LisFileAttach.length + $scope.FilesAttachTaskList.length + f.length > 5) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 5 file cho dự án!'
                });
                return false;
            }
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 25 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    $scope.FilesAttachTaskList.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 25MB!'
                });
            }
        };
        $scope.UploadFilesChung = function (l1, l2, f) {
            if (l1.length + l2.length + f.length > 5) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 5 file cho dự án!'
                });
                return false;
            }
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 25 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    l1.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 25MB!'
                });
            }
        };
        function compareDate(date1, date2) {
            var start = moment(date1);
            var end = moment(date2);
            var day = end.diff(start, "days")
            return day === 0;
        };
        $scope.goChartLine = function () {
            var chartData = generatechartData();

            function generatechartData() {
                var chartData = [];
                var firstDate = new Date($scope.congviec.NgayBatDau);
                var start = moment($scope.congviec.NgayBatDau);
                var end = moment($scope.Baocaos[$scope.Baocaos.length - 1].NgayBaocao);
                var day = end.diff(start, "days")
                for (var i = 0; i <= day; i++) {
                    // we create date objects here. In your data, you can have date strings
                    // and then set format of your dates using chart.dataDateFormat property,
                    // however when possible, use date objects, as this will speed up chart rendering.
                    var newDate = new Date(firstDate);
                    newDate.setDate(newDate.getDate() + i);
                    var tiendo = 0;
                    var o = $scope.Baocaos.find(x => compareDate(x.NgayBaocao, newDate))
                    if (o != null) {
                        tiendo = o.Tiendo;
                        if (o.reviews && o.reviews.length > 0 && o.reviews[0].IsType && o.reviews[0].Tiendo && o.reviews[0].Tiendo > 0) {
                            tiendo = o.reviews[0].Tiendo;
                        }
                    } else if (chartData.length > 0) {
                        tiendo = chartData[chartData.length - 1].tiendo;
                    }
                    chartData.push({
                        date: newDate,
                        dateString: moment(newDate).format("DD/MM"),
                        tiendo: tiendo
                    });
                }
                return chartData;
            }


            var chart = AmCharts.makeChart("chartdivcv", {
                "theme": "none",
                "type": "serial",
                "marginRight": 80,
                "autoMarginOffset": 20,
                "marginTop": 20,
                "dataProvider": chartData,
                "valueAxes": [{
                    "id": "v1",
                    "axisAlpha": 0.1
                }],
                "graphs": [{
                    "useNegativeColorIfDown": true,
                    "balloonText": "[[category]]<br><b>value: [[value]]</b>",
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletBorderColor": "#FFFFFF",
                    "hideBulletsCount": 50,
                    "lineThickness": 2,
                    "lineColor": "#fdd400",
                    "negativeLineColor": "#67b7dc",
                    "valueField": "tiendo"
                }],
                "chartScrollbar": {
                    "scrollbarHeight": 5,
                    "backgroundAlpha": 0.1,
                    "backgroundColor": "#868686",
                    "selectedBackgroundColor": "#67b7dc",
                    "selectedBackgroundAlpha": 1
                },
                "chartCursor": {
                    "valueLineEnabled": true,
                    "valueLineBalloonEnabled": true
                },
                "categoryField": "date",
                "categoryAxis": {
                    "parseDates": true,
                    "axisAlpha": 0,
                    "minHorizontalGap": 60
                },
                "export": {
                    "enabled": true
                }
            });

            chart.addListener("dataUpdated", zoomChart);
            //zoomChart();

            function zoomChart() {
                if (chart.zoomToIndexes) {
                    chart.zoomToIndexes(130, chartData.length - 1);
                }
            }
        };
        $scope.BindListDuan = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListAllDuAnbyUser", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID }//,
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $rootScope.duans = data[0];
                    if ($scope.DuanID && !$scope.opition.TenDuan) {
                        var da = $rootScope.duans.find(x => x.DuanID === $scope.DuanID);
                        if (da)
                            $scope.opition.TenDuan = da.TenDuan;
                        //$rootScope.TenDuan = $scope.opition.TenDuan;
                    }
                }
            });
        };
        $scope.openChart = function () {

        };
        $scope.initTD = function () {
            $scope.initOne();
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Tudien", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.tudien = data;
                    $scope.TrangthaiCV = data[3];
                    $scope.CVTTS = $scope.TrangthaiCV.filter(x => x.TrangthaiID < 4 && x.TrangthaiID >= 0);
                }
            });
        };
        $scope.BindListCountTask = function () {
            $http({
                method: 'POST',
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Count_CongviecCanhan", pas: [
                        { "par": "Congty_ID ", "va": $scope.opition.Congty_ID },
                        { "par": "DuanID ", "va": $scope.opition.DuanID || $scope.DuanID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "Trangthai ", "va": $scope.opition.Trangthai },
                        { "par": "s", "va": $scope.opition.s },
                        { "par": "loc", "va": $scope.opition.loc },
                        { "par": "sdate", "va": $scope.opition.sdate },
                        { "par": "edate", "va": $scope.opition.edate }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    if (data[0].length > 0) {
                        var obj = {
                            "tatca": data[0].find(x => x.FieldValue == -1) == null ? 0 : data[0].find(x => x.FieldValue == -1).c,
                            "thuchien": data[0].find(x => x.FieldValue == 1) == null ? 0 : data[0].find(x => x.FieldValue == 1).c,
                            "giao": data[0].find(x => x.FieldValue == 2) == null ? 0 : data[0].find(x => x.FieldValue == 2).c,
                            "tao": data[0].find(x => x.FieldValue == -2) == null ? 0 : data[0].find(x => x.FieldValue == -2).c,
                            "theodoi": data[0].find(x => x.FieldValue == 0) == null ? 0 : data[0].find(x => x.FieldValue == 0).c,
                        };
                        $scope.countTask = obj;
                    } else {

                        $scope.countTask = null;
                    }
                }
            }, function (response) { // optional
                $scope.loadding = false;
            });
        }
        $scope.initOne = function () {
            $scope.DuanID = null;
            $scope.tasks = [];
            $scope.IsViewCV = true;
            $scope.getCongViec();
        };
        //Cau hinh cong viec
        $scope.CapnhatCauhinh = function () {
            var cauhinhs = [$scope.cauhinhQL, $scope.cauhinhTH, $scope.cauhinhTD, $scope.cauhinhTao];
            $http({
                method: 'POST',
                url: "Duan/CapnhatCauhinh",
                data: { t: $rootScope.login.tk, cauhinhs: cauhinhs },
                headers: {
                    contentType: 'application/json; charset=utf-8'
                },
            }).then(function (res) {
                showtoastr("Cập nhật cấu hình thành công!");
            }, function (response) { // optional
            });
        };
        //Nhom du an
        $scope.openModalAddNhomDA = function () {
            $scope.nhomduan = { Congty_ID: $rootScope.login.u.congtyID, STT: $scope.tudien[0].length + 1 };
            $("form.ModalNhomDuanAdd.ng-dirty").removeClass("ng-dirty");
            $("#CongviecCtr #ModalNhomDuanAdd").modal("show");
        };
        $scope.AddNhomDuan = function (frm) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Duan/Update_NhomDuan",
                data: { t: $rootScope.login.tk, model: $scope.nhomduan },
                headers: {
                    contentType: 'application/json; charset=utf-8'
                },
            }).then(function (res) {
                closeswal();
                $scope.loadding = false;
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm nhóm dự án !'
                    });
                    return false;
                }
                $("#CongviecCtr #ModalNhomDuanAdd").modal("hide");
                $scope.initTD();
                //resetForm(frm);
            }, function (response) { // optional
                //resetForm(frm);
                $scope.loadding = false;
            });
        };
        //CheckList
        $scope.openModalAddChecklist = function (f) {
            $scope.fto = f;
            $scope.checklist = { NguoiTao: $rootScope.login.u.NhanSu_ID, DuanID: $scope.congviec.DuanID, CongviecID: $scope.congviec.CongviecID, Hienthi: true, STT: $scope.checklists.length + 1 };
            $("form.ModalChecklistAdd.ng-dirty").removeClass("ng-dirty");
            $("#CongviecCtr #ModalChecklistAdd").modal("show");
        };
        $scope.editCheckList = function (cl) {
            if (cl.CLSTT) {
                $scope.checklist = $scope.checklists.find(x => x.ChecklistID == cl.ChecklistID);
            } else {
                $scope.checklist = cl;
            }
            $("form.ModalChecklistAdd.ng-dirty").removeClass("ng-dirty");
            $("#CongviecCtr #ModalChecklistAdd").modal("show");
        };
        $scope.AddChecklist = function (frm, flag) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Duan/Update_CheckList",
                data: { t: $rootScope.login.tk, model: $scope.checklist },
                headers: {
                    contentType: 'application/json; charset=utf-8'
                },
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm CheckList !'
                    });
                    return false;
                }
                $scope.getCongViec();
                if ($scope.fto) {
                    $scope.checklist = { NguoiTao: $rootScope.login.u.NhanSu_ID, DuanID: $scope.congviec.DuanID, CongviecID: $scope.congviec.CongviecID, Hienthi: true, STT: $scope.bcchecklists.length + 2 };
                    $scope.goChecklistCV($scope.ocheck);
                }
                if (flag != true) {
                    $("#CongviecCtr #ModalChecklistAdd").modal("hide");
                }
                //resetForm(frm);
            }, function (response) { // optional
                //resetForm(frm);
                $scope.loadding = false;
            });
        };
        $scope.DelChecklist = function (d, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa CheckList này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_CheckList",
                        data: { t: $rootScope.login.tk, id: d.ChecklistID, f: false },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            if ($scope.bcchecklists != null) {
                                var idx = $scope.bcchecklists.findIndex(x => x.ChecklistID == d.ChecklistID)
                                if (idx != -1)
                                    $scope.bcchecklists.splice(idx, 1);
                            }
                            var idx = $scope.checklists.findIndex(x => x.ChecklistID == d.ChecklistID)
                            if (idx != -1)
                                $scope.checklists.splice(idx, 1);
                            //$scope.getCongViec();
                            showtoastr("Xóa CheckList thành công!");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: res.data.ms
                            });
                        }
                    });

                }
            })
        };
        //Chủ đề
        $scope.chudes = [];
        $scope.openModalAddChude = function () {
            $scope.chude = { NguoiTao: $rootScope.login.u.NhanSu_ID, NgayTao: new Date(), STT: $scope.chudes.length + 1, DuanID: $scope.DuanID, CongviecID: $scope.CongviecID };
            $("#ModalChudeAdd form.ng-dirty").removeClass("ng-dirty");
            $("#CongviecCtr #ModalChudeAdd").modal("show");
        };
        $scope.listChude = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Congviec_ListChude", pas: [
                        { "par": "CongviecID", "va": $scope.CongviecID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    data[1].forEach(function (ta) {
                        ta.Thuchiens = JSON.parse(ta.Thuchiens);
                    });
                    data[0].forEach(function (r) {
                        r.tasks = data[1].filter(x => x.ChudeID === r.ChudeID);
                    });
                    var cvs = data[1].filter(x => x.ChudeID === null);
                    if (cvs.length > 0) {
                        var fn = { ChudeID: null, TenChude: "Chưa phân chủ đề", tasks: cvs };
                        data[0].push(fn);
                    }
                    $scope.chudes = data[0];
                }
            });
        };
        $scope.AddChude = function (frm) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Duan/Update_Chude",
                data: { t: $rootScope.login.tk, model: $scope.chude },
                headers: {
                    contentType: 'application/json; charset=utf-8'
                },
            }).then(function (res) {
                closeswal();
                $scope.loadding = false;
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm nhóm công việc !'
                    });
                    return false;
                }
                $("#CongviecCtr #ModalChudeAdd").modal("hide");
                $scope.listChude();
                //resetForm(frm);
            }, function (response) { // optional
                //resetForm(frm);
                $scope.loadding = false;
            });
        };
        //Chart
        var ColorChats = [
            {
                "color": "#04D215"
            },
            {
                "color": "#B0DE09"
            }, {
                "color": "#0D52D1"
            },
            {
                "color": "#8A0CCF"
            },
            {
                "color": "#F8FF01"
            },
            {
                "color": "#FCD202"
            },
            {
                "color": "#FF9E01"
            },
            {
                "color": "#FF6600"
            },

            {
                "color": "#FF0F00"
            }, {
                "color": "#CD0D74"
            }, {
                "color": "#754DEB"
            }, {
                "color": "#FFCCFF"
            }, {
                "color": "#FF99CC"
            }, {
                "color": "#FF6699"
            }, {
                "color": "#FF3366"
            }, {
                "color": "#FF3333"
            }, {
                "color": "#990066"
            }, {
                "color": "#660066"
            }, {
                "color": "#660033"
            }, {
                "color": "#660000"
            }, {
                "color": "#DDDDDD"
            }, {
                "color": "#999999"
            }, {
                "color": "#333333"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }, {
                "color": "#000000"
            }
        ];

        $scope.listCharts = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Congviec_Charts", pas: [
                        { "par": "CongviecID", "va": $scope.CongviecID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    $scope.ThongKeChung = JSON.parse(res.data.data)[0][0];
                }
            });
        };
        $scope.TypeCV = -1;
        $scope.listChartsCVTT = function (type, tt) {
            $scope.TypeCV = type;
            if (!tt) {
                tt = -1;
            }
            $scope.TypeTT = tt;
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Duan_ChartsCV", pas: [
                        { "par": "DuanID", "va": $scope.DuanID },
                        { "par": "IsType", "va": type },
                        { "par": "Trangthai", "va": tt }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var dus = JSON.parse(res.data.data)[0];
                    dus.forEach(function (u, i) {
                        u.color = ColorChats[i].color;
                        u.Name = u.ten;
                    });
                    setChartUser("chartdivU", dus);
                }
            });
        };
        function setChartUser(id, datas) {
            setTimeout(function () {
                AmCharts.makeChart(id, {
                    "theme": "none",
                    "rotate": false,
                    "type": "serial",
                    "startDuration": 2,
                    "dataProvider": datas,
                    "valueAxes": [{
                        "position": "left",
                        "title": "Công việc"
                    }],
                    "graphs": [{
                        "balloonText": "<b>[[fullName]]</b>: <b style='color:#2196f3 '>[[value]] </b> công việc<br/>[[tenChucVu]]<br/><i>[[tenToChuc]]</i>",
                        "fillColorsField": "color",
                        "fillAlphas": 1,
                        "lineAlpha": 0.1,
                        "bulletOffset": 16,
                        "bulletSize": 34,
                        "customBulletField": "anhThumb",
                        "type": "column",
                        "valueField": "SoCV",
                    }],
                    "chartCursor": {
                        "categoryBalloonEnabled": false,
                        "cursorAlpha": 0,
                        "zoomable": false
                    },
                    "categoryField": "Name",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "labelRotation": 90
                    },
                    "export": {
                        "enabled": true
                    }
                });
            }, 100);
        };
        $scope.BindChart = function (type, ti, id, datas) {
            var time = ti || 100;
            if (!id) {
                id = "chartdiv";
            }
            if (!datas) {
                datas = $scope.datas;
            };
            switch (type) {
                case 0:
                    setTimeout(function () {
                        AmCharts.makeChart(id, {
                            "theme": "none",
                            "rotate": false,
                            "type": "serial",
                            "startDuration": 2,
                            "dataProvider": datas,
                            "valueAxes": [{
                                "position": "left",
                                "title": "Công việc"
                            }],
                            "graphs": [{
                                "balloonText": "[[category]]: <b>[[value]]</b>",
                                "fillColorsField": "color", "labelText": "[[value]]",
                                "fillAlphas": 1,
                                "lineAlpha": 0.1,
                                "type": "column",
                                "valueField": "SoCV"
                            }],
                            //"depth3D": 20,
                            //"angle": 30,
                            "chartCursor": {
                                "categoryBalloonEnabled": false,
                                "cursorAlpha": 0,
                                "zoomable": false
                            },
                            "categoryField": "Name",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "labelRotation": 90
                            },
                            "export": {
                                "enabled": true
                            }
                        });
                    }, time);
                    break;
                case 1:
                    setTimeout(function () {
                        AmCharts.makeChart(id, {
                            "type": "pie",
                            "startDuration": 1,
                            "theme": "none",
                            "addClassNames": true,
                            "legend": {
                                "position": "right",
                                "marginRight": 100,
                                "autoMargins": false
                            },
                            "innerRadius": "30%",
                            "colorField": "color",
                            "graphs": [{
                                "balloonText": "[[category]]: <b>[[value]]</b>",
                                "fillColorsField": "color", "labelText": "[[value]]",
                                "fillAlphas": 1,
                                "lineAlpha": 0.1,
                                "type": "column",
                                "valueField": "SoCV"
                            }],
                            //"depth3D": 20,
                            //"angle": 30,
                            "defs": {
                                "filter": [{
                                    "id": "shadow",
                                    "width": "200%",
                                    "height": "200%",
                                    "feOffset": {
                                        "result": "offOut",
                                        "in": "SourceAlpha",
                                        "dx": 0,
                                        "dy": 0
                                    },
                                    "feGaussianBlur": {
                                        "result": "blurOut",
                                        "in": "offOut",
                                        "stdDeviation": 5
                                    },
                                    "feBlend": {
                                        "in": "SourceGraphic",
                                        "in2": "blurOut",
                                        "mode": "normal"
                                    }
                                }]
                            },
                            "dataProvider": datas,
                            "valueField": "SoCV",
                            "titleField": "Name",
                            "export": {
                                "enabled": true
                            }
                        });
                    }, time);
                    break;
                case 2:
                    setTimeout(function () {
                        AmCharts.makeChart(id, {
                            "theme": "none",
                            "rotate": true,
                            "type": "serial",
                            "startDuration": 2,
                            "dataProvider": datas,
                            "valueAxes": [{
                                "position": "left",
                                "title": "Công việc"
                            }],
                            "graphs": [{
                                "balloonText": "[[category]]: <b>[[value]]</b>",
                                "fillColorsField": "color", "labelText": "[[value]]",
                                "fillAlphas": 1,
                                "lineAlpha": 0.1,
                                "type": "column",
                                "valueField": "SoCV"
                            }],
                            //"depth3D": 20,
                            //"angle": 1,
                            "chartCursor": {
                                "categoryBalloonEnabled": false,
                                "cursorAlpha": 0,
                                "zoomable": false
                            },
                            "categoryField": "Name",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "labelRotation": 90
                            },
                            "export": {
                                "enabled": true
                            }
                        });
                    }, time);
                    break;
                case 3:
                    setTimeout(function () {
                        AmCharts.makeChart(id, {
                            "theme": "none",
                            "rotate": true,
                            "type": "radar",
                            "startDuration": 2,
                            "dataProvider": datas,
                            "valueAxes": [{
                                "position": "left",
                                "title": "Công việc"
                            }],
                            "graphs": [{
                                "balloonText": "[[category]]: <b>[[value]]</b>",
                                "fillColorsField": "color", "labelText": "[[value]]",
                                "fillAlphas": 1,
                                "lineAlpha": 0.1,
                                "type": "column",
                                "valueField": "SoCV"
                            }],
                            //"depth3D": 20,
                            //"angle": 1,
                            "chartCursor": {
                                "categoryBalloonEnabled": false,
                                "cursorAlpha": 0,
                                "zoomable": false
                            },
                            "categoryField": "Name",
                            "categoryAxis": {
                                "gridPosition": "start",
                                "labelRotation": 90
                            },
                            "export": {
                                "enabled": true
                            }
                        });
                    }, time);
                    break;
                case 4:
                    setTimeout(function () {
                        AmCharts.makeChart(id, {
                            "type": "radar",
                            "theme": "none",
                            "dataProvider": datas,
                            "valueAxes": [{
                                "gridType": "circles",
                                "minimum": 0,
                                "autoGridCount": false,
                                "axisAlpha": 0.2,
                                "fillAlpha": 0.05,
                                "fillColor": "#FFFFFF",
                                "gridAlpha": 0.08,
                                "guides": [{
                                    "angle": 225,
                                    "fillAlpha": 0.3,
                                    "fillColor": "#0066CC",
                                    "tickLength": 0,
                                    "toAngle": 315,
                                    "toValue": 14,
                                    "value": 0,
                                    "lineAlpha": 0,

                                }, {
                                    "angle": 45,
                                    "fillAlpha": 0.3,
                                    "fillColor": "#CC3333",
                                    "tickLength": 0,
                                    "toAngle": 135,
                                    "toValue": 14,
                                    "value": 0,
                                    "lineAlpha": 0,
                                }],
                                "position": "left"
                            }],
                            "startDuration": 1,
                            "graphs": [{
                                "balloonText": "[[category]]: [[value]]",
                                "bullet": "round",
                                "fillAlphas": 0.3,
                                "valueField": "SoCV"
                            }],
                            "categoryField": "Name",
                            "export": {
                                "enabled": true
                            }
                        });
                    }, time);
                    break;
            }
        };
        //Báo cáo công việc
        $scope.EditBaocao = function (bc) {
            $scope.openTiendo();
        };

        $scope.Del_Baocao = function (cm, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa báo cáo này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_Baocao",
                        data: {
                            t: $rootScope.login.tk, id: cm.BaocaoID
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.Baocaos.splice(i, 1);
                            $scope.congviec.Trangthai = 1;
                        }
                    });
                }
            })

        };
        $scope.listBaocaoCongviec = function (id, f) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListBaocaoCongViec", pas: [
                        { "par": "CongviecID ", "va": id }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    data.forEach(function (r) {
                        if (r.files)
                            r.files = JSON.parse(r.files);
                        if (r.reviews)
                            r.reviews = JSON.parse(r.reviews);
                        if (r.reviews) {
                            r.reviews.forEach(function (v) {
                                if (v.files) {
                                    v.files = JSON.parse(v.files);
                                }
                                v.color = renderColor(v.Tiendo);
                                if (v.NgayReview)
                                    v.NgayReview = (new Date(v.NgayReview));
                                //v.NgayReview = parseJsonDate(v.NgayReview);
                            })
                        }
                        r.color = renderColor(parseInt(r.Tiendo));
                    });
                    $scope.Baocaos = data;
                    $scope.congviec.IsBC = true;// data.length == 0 || data.filter(x => x.Trangthai == 0).length == 0;
                    if (f) {
                        $scope.goChartLine();
                    }
                }
            });
        };
        $scope.changeBCNoidung = function () {
            $scope.Baocaos.forEach(function (r, i) {
                r.STT = i;
            });
            var idx = $scope.Baocaos.indexOf($scope.Baocaos.filter(item => item.IsType == 0).pop())
            if ($scope.Baocao.IsType == 0) {
                var str = "";
                $scope.Baocaos.filter(x => x.IsType == null && x.STT >= $scope.Baocaos[idx].STT).forEach(function (r) {
                    str += r.Noidung + "\n";
                });
                $scope.Baocao.Noidung = str;
            } else if ($scope.Baocao.IsType == 1) {
                var str = "";
                $scope.Baocaos.filter(x => x.IsType == 0).forEach(function (r) {
                    str += r.Noidung + "\n";
                });
                $scope.Baocao.Noidung = str;
            }
        };
        $scope.Baocao = { DuanID: $scope.DuanID, CongviecID: $scope.CongviecID, Trangthai: 0, Tiendo: 0, Noidung: "", NgayBaocao: new Date() };
        $scope.Update_BaocaoCongviec = function () {
            if ($scope.loadding) {
                return false;
            }
            if (!$scope.iptiendo) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng cập nhật tiến độ của công việc !'
                });
                return false;
            }
            if ($scope.DuanID == "null") {
                $scope.DuanID = null;
            }
            $scope.Baocao = { DuanID: $scope.DuanID, CongviecID: $scope.CongviecID, Trangthai: $scope.congviec.IsType === 2 ? 1 : 0, Tiendo: $scope.iptiendo, Noidung: $scope.Baocao.Noidung, IsType: $scope.Baocao.IsType, Khokhan: $scope.Baocao.Khokhan, IsEmail: $scope.Baocao.IsEmail, NgayBaocao: new Date() };
            $scope.loadding = true;
            //swal.showLoading();
            //Progress File
            var hasfile = $scope.FileAttach && $scope.FileAttach.length > 0;
            if (!hasfile) {
                swal.showLoading();
            } else {
                $("#modalfileProgress").modal("show");
                var fileProgress = document.getElementById("fileProgress");
                var desProgress = document.getElementById("podes");
                fileProgress.setAttribute("value", 0);
                fileProgress.setAttribute("max", 0);
                fileProgress.style.display = "block";
            }
            $http({
                method: 'POST',
                url: "Duan/Update_BaocaoCongviec",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    formData.append("models", JSON.stringify($scope.Baocao));
                    $.each($scope.FileAttachBC, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                },
                uploadEventHandlers: {
                    progress: function (e) {
                        if (e.lengthComputable) {
                            fileProgress.setAttribute("value", e.loaded);
                            fileProgress.setAttribute("max", e.total);
                            var po = Math.ceil(e.loaded * 100 / e.total);
                            desProgress.innerHTML = po + "%";
                        }
                    }
                }
            }).then(function (res) {
                $scope.loadding = false;
                $scope.FileAttachBC = [];
                $scope.Baocao = { DuanID: $scope.DuanID, CongviecID: $scope.CongviecID, Trangthai: 0, Tiendo: 0, Noidung: "", IsType: null };
                if (!hasfile)
                    closeswal();
                $("#modalfileProgress").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi gửi báo cáo công việc !'
                    });
                    return false;
                }
                if ($scope.congviec.IsQL) {
                    $scope.congviec.Tiendo = $scope.iptiendo;
                    if ($scope.iptiendo == 100) {
                        $scope.congviec.IsCheck = true;
                    } else if ($scope.iptiendo > 0 && $scope.congviec.Trangthai === 0) {
                        $scope.congviec.Trangthai = 1;
                    } else {
                        $scope.congviec.Trangthai = 5;
                    }
                }
                $scope.congviec.color = renderColor($scope.congviec.Tiendo);
                $scope.congviec.txtcolor = renderTxtColor($scope.congviec.Tiendo);
                showtoastr("Gửi báo cáo thành công!");
                $scope.listBaocaoCongviec($scope.CongviecID);
                $scope.opitionhd.currentPage = 1;
                $scope.listDAlog();
                $scope.iptiendo = 0;
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        //ListTagsKey
        $scope.setColorTag = function (c) {
            $scope.tag.TagTxtColor = c.text;
            $scope.tag.TagColor = c.bgcolor;
        };
        $scope.tagColors = [
            { "text": "#000000", "bgcolor": "#bbbbbb" },
            { "text": "#ffffff", "bgcolor": "#2a91d6" },
            { "text": "#ffffff", "bgcolor": "#33c9dc" },
            { "text": "#ffffff", "bgcolor": "#51b7ae" },
            { "text": "#ffffff", "bgcolor": "#6fbf73" },
            { "text": "#ffffff", "bgcolor": "#7a87d0" },
            { "text": "#ffffff", "bgcolor": "#ffcd38" },
            { "text": "#ffffff", "bgcolor": "#ff8b4e" },
            { "text": "#ffffff", "bgcolor": "#d87777" },
            { "text": "#ffffff", "bgcolor": "#f17ac7" },
            { "text": "#ffffff", "bgcolor": "#ff0000" }
        ];
        $scope.ListTagsKey = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListTagsKey", pas: [
                        { "par": "Congty_ID ", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    $scope.Tags = data;
                    data.forEach(function (r) {
                        r.chon = $scope.Tagcvs ? $scope.Tagcvs.filter(x => x.TaskTagKey_ID === r.TaskTagKey_ID).length > 0 : false;
                    });
                }
            });
        };
        $scope.FindTag = function () {
            //congviecs
            var tags = $scope.Tags.filter(x => x.chon).map(e => e.TaskTagKey_ID);
            if (tags.length > 0) {
                $scope.congviecs = arrcongviecs.filter(x => x.Tags && x.Tags.filter(t => tags.includes(t.TaskTagKey_ID)).length > 0);
            } else {
                $scope.congviecs = arrcongviecs;
            }
            $("#ModalTags").modal("hide");
        };
        $scope.ListTagsCongviec = function (cv) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListTagsKeyDuanCV", pas: [
                        { "par": "DuanID ", "va": cv.DuanID },
                        { "par": "CongviecID ", "va": cv.CongviecID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    $scope.Tagcvs = data;
                }
            });
        };
        $scope.openModalTags = function () {
            $("#ModalTags").modal("show");
            $scope.ListTagsKey();
        };
        $scope.openAddTags = function () {
            $scope.tag = { Congty_ID: $rootScope.login.u.congtyID, IsPublic: true, TagActive: true };
            $("#ModalAddTags").modal("show");
        };
        $scope.Update_TagCV = function () {
            if ($scope.loadding) {
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: "POST",
                url: "Duan/Update_Task_TagKeyDuanCV",
                data: {
                    t: $rootScope.login.tk, tags: $scope.Tags.filter(x => x.chon), idkey: $scope.CongviecID, duanID: $scope.congviec.DuanID, f: false
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    $scope.ListTagsCongviec($scope.congviec);
                    $("#ModalTags").modal("hide");
                }
            });
        }
        $scope.Add_Tag = function () {
            if ($scope.loadding) {
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: "POST",
                url: "Duan/Update_Task_TagKey",
                data: {
                    t: $rootScope.login.tk, model: $scope.tag
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    $scope.ListTagsKey();
                    $("#ModalAddTags").modal("hide");
                }
            });
        }
        //Review công việc
        $scope.listReviewCongviec = function (id) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListReviewCongViec", pas: [
                        { "par": "BaocaoID ", "va": id }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    data.forEach(function (r) {
                        if (r.files)
                            r.files = JSON.parse(r.files);
                    });
                    $scope.Reviews = data;
                }
            });
        };
        $scope.openReview = function (bc, f) {
            $("#ModalReview").modal("show");
            $scope.Baocao = bc;
            $scope.Review = { NgayReview: new Date() };
            if ($scope.Review)
                $scope.Review.IsType = f;
            else
                $scope.Review = { IsType: f, NgayReview: new Date() };
            $scope.Review.IPTiendo = bc.Tiendo;
            $scope.listReviewCongviec($scope.Baocao.BaocaoID);
        };
        $scope.ReviewSetTime = function () {
            if (new Date($scope.Review.NgayReview) < new Date($scope.Baocao.NgayBaocao)) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Ngày đánh giá phải lớn hơn ngày báo cáo!'
                });
                $scope.Review.NgayReview = new Date();
            }
        };
        $scope.Update_ReviewCongviec = function () {
            if ($scope.loadding) {
                return false;
            }
            if ($scope.DuanID == "null") {
                $scope.DuanID = null;
            }
            $scope.Review = { DuanID: $scope.DuanID, BaocaoID: $scope.Baocao.BaocaoID, CongviecID: $scope.CongviecID, NgayReview: $scope.Review.NgayReview, IsType: $scope.Review.IsType, Noidung: $scope.Review.Noidung, Danhgia: $scope.Review.Danhgia, Tiendo: $scope.Baocao.Tiendo !== $scope.Review.IPTiendo ? $scope.Review.IPTiendo : null };
            $scope.loadding = true;
            //swal.showLoading();
            //Progress File
            var hasfile = $scope.FileAttach && $scope.FileAttach.length > 0;
            if (!hasfile) {
                swal.showLoading();
            } else {
                $("#modalfileProgress").modal("show");
                var fileProgress = document.getElementById("fileProgress");
                var desProgress = document.getElementById("podes");
                fileProgress.setAttribute("value", 0);
                fileProgress.setAttribute("max", 0);
                fileProgress.style.display = "block";
            }
            $http({
                method: 'POST',
                url: "Duan/Update_ReviewCongviec",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    var obj = { ...$scope.Review };
                    if (obj.NgayReview) {
                        obj.NgayReview = moment(obj.NgayReview).tz(timezone).format('YYYY-MM-DDTHH:mm:ss');
                    }
                    formData.append("t", $rootScope.login.tk);
                    formData.append("models", JSON.stringify(obj));
                    $.each($scope.FileAttachRV, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                },
                uploadEventHandlers: {
                    progress: function (e) {
                        if (e.lengthComputable && fileProgress != null) {
                            fileProgress.setAttribute("value", e.loaded);
                            fileProgress.setAttribute("max", e.total);
                            var po = Math.ceil(e.loaded * 100 / e.total);
                            desProgress.innerHTML = po + "%";
                        }
                    }
                }
            }).then(function (res) {
                $scope.loadding = false;
                $scope.FileAttachRV = [];
                $scope.Review = { DuanID: $scope.DuanID, BaocaoID: $scope.Baocao.BaocaoID, CongviecID: $scope.CongviecID, IsType: $scope.Review.IsType, Noidung: "", NgayReview: new Date(), Danhgia: 0 };
                if (!hasfile)
                    closeswal();
                $("#modalfileProgress").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi gửi đánh giá công việc !'
                    });
                    return false;
                }
                showtoastr("Gửi đánh giá công việc thành công!");
                $("#ModalReview").modal("hide");
                $scope.listBaocaoCongviec($scope.CongviecID);
                $scope.listReviewCongviec($scope.Baocao.BaocaoID);
                $scope.opitionhd.currentPage = 1;
                $scope.listDAlog();
            }, function (response) { // optional
                $scope.loadding = false;
            });
        };
        //Subtask
        $scope.listSubTask = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListSubTask", pas: [
                        { "par": "CongviecID", "va": $scope.CongviecID != null ? $scope.CongviecID : null },
                        { "par": "NhanSu_ID", "va": $scope.NhanSu_ID || null }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    if (!data) data = [];
                    data.forEach(function (r) {
                        r.Thanhviens = JSON.parse(r.Thanhviens);
                        r.color = renderColor(r.Tiendo);
                        r.txtcolor = renderTxtColor(r.Tiendo);
                        r.NgayHan = Math.abs(r.NgayHan);
                        r.Tao = r.NguoiTao === $rootScope.login.u.NhanSu_ID;
                        if (r.Thanhviens) {
                            r.IsQL = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "2").length > 0;
                            r.IsTH = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "1").length > 0;
                            r.IsTD = r.Thanhviens.filter(x => x.NhanSu_ID === $rootScope.login.u.NhanSu_ID && x.IsType === "0").length > 0;
                            r.IsType = r.IsQL ? 2 : (r.IsTH ? 1 : 0);
                        }
                    })
                    $scope.subcongviecs = data;
                }
            });
        };
        //Hoạt động
        $scope.opitionhd = {
            numPerPage: 20,
            currentPage: 1,
            sort: 2,
            Trangthai: -1,
            tenSort: "STT",
            tenTT: "Tất cả",
            IsType: -1,
            Group: 1
        };
        $scope.setPagelog = function () {
            $scope.opitionhd.currentPage += 1;
            $scope.listDAlog(true);
        };
        $scope.listDAlog = function (f) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListDuAnLog", pas: [
                        { "par": "DuanID", "va": $scope.DuanID },
                        { "par": "CongviecID", "va": $scope.CongviecID != null ? $scope.CongviecID : null },
                        { "par": "NhanSu_ID", "va": $scope.NhanSu_ID || null },
                        { "par": "p", "va": $scope.opitionhd.currentPage },
                        { "par": "pz", "va": $scope.opitionhd.numPerPage },
                        { "par": "IsType", "va": null }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    data[0].forEach(function (r) {
                        r.NgayTao = new Date(r.NgayTao);
                        r.DaysName = weekday[r.NgayTao.getDay()];
                    });
                    if (f) {
                        $scope.logs = $scope.logs.concat(data[0]);
                    } else {
                        $scope.logs = data[0];
                    }
                    if (!$scope.opitionhd.noOfPages) {
                        $scope.opitionhd.noOfPages = Math.ceil(data[1][0].c / $scope.opitionhd.numPerPage);
                    }
                }
            });
        };
        //Thư mục
        $scope.setViewType = function (vt) {
            $scope.ViewFolder = vt;
        };
        $scope.checkTailieu = function (d) {
            $scope.chonTailieus = $scope.tailieus.filter(x => x.IsCheck);
        };
        $scope.checkThumuc = function (d) {
            $scope.chonThumucs = $scope.thumucs.filter(x => x.IsCheck);
        };
        $scope.moveFolder = function (f) {
            f.IsCheck = true;
            moveAll = false;
            $("#folder-right").show();
            $scope.thumuc = f;
            $scope.openRight = 1;
            $scope.FilesFolder = { ThumucID: null }
            $scope.listThumucbyThumuc($scope.FilesFolder);
        };
        $scope.goFolder = function (f) {
            $("#folder-right").hide();
            $scope.thumuc = f;
            $scope.openRight = 0;
        };
        $scope.closeFolder = function () {
            $("#folder-right").hide();
            $scope.openRight = false;
        };
        $scope.openModalAddThumuc = function () {
            $scope.LisFileAttachTL = [];
            $scope.FilesAttachTaskListTL = [];
            $scope.thumuc = { ParentID: $scope.thumuc ? $scope.thumuc.ThumucID : null, NguoiTao: $rootScope.login.u.NhanSu_ID, NgayTao: new Date(), STT: $scope.thumucs.length + 1, DuanID: $scope.DuanID, CongviecID: $scope.CongviecID };
            $("#ModalThumucAdd form.ng-dirty").removeClass("ng-dirty");
            $("#ModalThumucAdd").modal("show");
        };
        $scope.openFolder = function (f) {
            $scope.cvfiles = null;
            if (f) {
                $scope.thumuc = f;
                $scope.ThumucID = f.ThumucID;
                $scope.thumucs = [];
                $scope.tailieus = [];
                $scope.listThumuc();
            } else {
                $scope.thumuc = null;
                $scope.ThumucID = null;
                $scope.thumucs = [];
                $scope.tailieus = [];
                $scope.listThumuc();
            }
        };
        $scope.openganday = function () {
            $scope.thumuc = null;
            $scope.ThumucID = null;
            $scope.thumucs = [];
            $scope.tailieus = [];
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Congviec_ListFileGanday", pas: [
                        { "par": "CongviecID", "va": $scope.CongviecID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.cvfiles = data[0];
                }
            });
        };
        $scope.listThumuc = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Congviec_ListThumuc", pas: [
                        { "par": "CongviecID", "va": $scope.CongviecID },
                        { "par": "ThumucID", "va": $scope.ThumucID || null }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.thumucs = data[0];
                    $scope.tailieus = data[1];
                    $scope.thumuctrees = data[2];
                    if (data[3].length > 0) {
                        $scope.gandays = data[3][0];
                    }
                }
            });
        };
        $scope.listThumucbyThumuc = function (tm) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Congviec_ListThumucGetByThumucID", pas: [
                        { "par": "CongviecID", "va": $scope.CongviecID },
                        { "par": "ThumucID", "va": tm ? tm.ThumucID : null }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    data[0].forEach(function (m) {
                        m.close = false;
                    });
                    tm.thumucs = data[0];
                }
            });
        };
        $scope.toogleModel = function (m) {
            m.close = !m.close;
            if (!m.thumucs || m.thumucs.length == 0)
                $scope.listThumucbyThumuc(m);
        };
        $scope.UpdateThumuc = function (m) {
            if (moveAll) {
                $scope.UpdateThumucTailieu(m);
            } else {
                Swal.fire({
                    title: 'Xác nhận?',
                    text: "Bạn có muốn di chuyển thư mục đã chọn không!",
                    icon: 'info',
                    showCancelButton: true,
                    confirmButtonColor: '#2196f3',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Có!',
                    cancelButtonText: 'Không!',
                }).then((result) => {
                    if (result.value) {
                        $http({
                            method: "POST",
                            url: "Duan/Move_Thumuc",
                            data: { t: $rootScope.login.tk, tid: $scope.thumuc.ThumucID, pid: m.ThumucID, f: true },
                            contentType: 'application/json; charset=utf-8'
                        }).then(function (res) {
                            if (!$rootScope.checkToken(res)) return false;
                            if (res.data.error !== 1) {
                                $scope.listThumuc();
                                $("#folder-right").hide();
                                showtoastr(
                                    'Đã di chuyển thư mục thành công!.',
                                );
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    type: 'error',
                                    title: '',
                                    text: 'Di chuyển thư mục không thành công, vui lòng thử lại!'
                                });
                            }
                        });

                    }
                })
            }
        };
        $scope.ModalThumucRename = function (tm) {
            $scope.thumuc = tm;
            $("#ModalThumucRename").modal("show");
        };
        $scope.AddThumuc = function (frm) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Duan/Update_Thumuc",
                data: { t: $rootScope.login.tk, model: $scope.thumuc },
                headers: {
                    contentType: 'application/json; charset=utf-8'
                },
            }).then(function (res) {
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                $scope.loadding = false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: res.data.ms || 'Có lỗi khi tạo thư mục !'
                    });
                    return false;
                }
                $("#ModalThumucAdd").modal("hide");
                $("#ModalThumucRename").modal("hide");
                $scope.listThumuc();
                //resetForm(frm);
            }, function (response) { // optional
                //resetForm(frm);
                $scope.loadding = false;
            });
        };

        $scope.downloadThumucTailieu = function () {
            //var files = [];
            //if ($scope.chonThumucs) {
            //    $scope.chonThumucs.forEach(function (tm) {
            //        //files.push(tm.Duongdan);
            //        saveAs(baseUrl + tm.Duongdan, tm.Tenfile);
            //    });
            //}
            if ($scope.chonTailieus) {
                $scope.chonTailieus.forEach(function (tm) {
                    //files.push(tm.Duongdan);
                    saveAs(baseUrl + tm.Duongdan, tm.Tenfile);
                });
            }
        };
        var mf = false;
        $scope.moveTailieu = function (f) {
            f.IsCheck = true;
            mf = true;
            moveAll = true;
            $("#folder-right").show();
            $scope.openRight = 1;
            $scope.FilesFolder = { ThumucID: null }
            $scope.listThumucbyThumuc($scope.FilesFolder);
        };
        $scope.moveFolderAll = function (f) {
            moveAll = true;
            mf = f;
            $("#folder-right").show();
            $scope.openRight = 1;
            $scope.FilesFolder = { ThumucID: null }
            $scope.listThumucbyThumuc($scope.FilesFolder);
        };

        $scope.UpdateThumucTailieu = function (m) {
            var ids = [];
            $scope.thumucs.filter(x => x.IsCheck).forEach(function (tm) {
                ids.push(tm.ThumucID);
            });
            var tls = [];
            $scope.tailieus.filter(x => x.IsCheck).forEach(function (tm) {
                tls.push(tm.TailieuID);
            });
            var txt = "di chuyển";
            if (!mf) {
                txt = "copy";
            }
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn " + txt + " mục đã chọn không!",
                icon: 'info',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Move_ThumucTailieu",
                        data: { t: $rootScope.login.tk, ids: ids, tls: tls, pid: m.ThumucID, f: mf },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.listThumuc();
                            $("#folder-right").hide();
                            showtoastr('Đã ' + txt + ' mục đã chọn thành công!.');
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: txt + ' không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };

        $scope.XoaThumucTailieu = function () {
            var ids = [];
            if ($scope.chonThumucs) {
                $scope.chonThumucs.forEach(function (tm) {
                    ids.push(tm.ThumucID);
                });
            }
            var tls = [];
            if ($scope.chonTailieus) {
                $scope.chonTailieus.forEach(function (tm) {
                    tls.push(tm.TailieuID);
                });
            }
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa những mục đã chọn không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_ThumucTailieu",
                        data: {
                            t: $rootScope.login.tk, ids: ids, tls: tls
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            ids.forEach(function (id) {
                                if (res.data.ids.findIndex(x => x === id) === -1) {
                                    var i = $scope.thumucs.findIndex(x => x.ThumucID === id);
                                    if (i != -1) {
                                        $scope.thumucs.splice(i, 1);
                                    }
                                }
                            });

                            tls.forEach(function (id) {
                                if (res.data.ids.findIndex(x => x === id) === -1) {
                                    var i = $scope.tailieus.findIndex(x => x.TailieuID === id);
                                    if (i != -1) {
                                        $scope.tailieus.splice(i, 1);
                                    }
                                }
                            });
                            if (res.data.ids.length > 0) {
                                showtoastr("Một số thư mục không thể xóa. Vui lòng xóa hết file bên trong thư mục trước khi xóa thư mục", 2);
                            }
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: res.data.ms
                            });
                        }
                    });
                }
            })
        };

        $scope.Del_Thumuc = function (cm, i) {
            var ids = [];
            if (cm) {
                ids.push(cm.ThumucID);
            }
            $scope.chonThumucs.forEach(function (tm) {
                ids.push(tm.ThumucID);
            });
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa thư mục này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_Thumuc",
                        data: {
                            t: $rootScope.login.tk, ids: ids
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            ids.forEach(function (id) {
                                if (res.data.ids.findIndex(x => x === id) === -1) {
                                    var i = $scope.thumucs.findIndex(x => x.ThumucID === id);
                                    if (i != -1) {
                                        $scope.thumucs.splice(i, 1);
                                    }
                                }
                            });
                            if (res.data.ids.length > 0) {
                                showtoastr("Một số thư mục không thể xóa. Vui lòng xóa hết file bên trong thư mục trước khi xóa thư mục", 2);
                            }
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: res.data.ms
                            });
                        }
                    });
                }
            })

        };
        //Tài liệu
        $scope.Del_Tailieu = function (cm, i) {
            var ids = [];
            if (cm) {
                ids.push(cm.TailieuID);
            }
            if ($scope.chonThumucs) {
                $scope.chonThumucs.forEach(function (tm) {
                    ids.push(tm.TailieuID);
                });
            }
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa tài liệu này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_Tailieu",
                        data: {
                            t: $rootScope.login.tk, ids: ids
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            ids.forEach(function (id) {
                                if (res.data.ids.findIndex(x => x === id) === -1) {
                                    var i = $scope.tailieus.findIndex(x => x.TailieuID === id);
                                    if (i != -1) {
                                        $scope.tailieus.splice(i, 1);
                                    }
                                }
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: res.data.ms
                            });
                        }
                    });
                }
            })

        };
        $scope.openModelUploadFile = function () {
            filecv = false;
            $scope.IPTailieuFiles = [];
            $("#ModalFileUpload").modal("show");
        };
        $scope.listFileCongViec = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListFileCongViec", pas: [
                        { "par": "CongviecID ", "va": $scope.congviec.CongviecID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.LisFileAttachCV = data[0];
                }
            });
        };
        var len = 0;
        $scope.UploadFileTaiLieu = function () {
            if ($scope.IPTailieuFiles.length === 0) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng chọn file để upload !'
                });
                return false;
            }
            $scope.loadding = true;
            //swal.showLoading();
            $("#modalfileProgress").modal("show");
            fileProgress = document.getElementById("fileProgress");
            desProgress = document.getElementById("podes");
            fileProgress.setAttribute("value", 0);
            fileProgress.setAttribute("max", 0);
            fileProgress.style.display = "block";
            $http({
                method: 'POST',
                url: filecv ? "Duan/UpLoad_CongviecFile" : "Duan/UpLoad_Tailieu",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    formData.append("DuanID", $scope.DuanID || null);
                    if ($scope.ThumucID)
                        formData.append("ThumucID", $scope.ThumucID || null);
                    if ($scope.CongviecID)
                        formData.append("CongviecID", $scope.CongviecID || null);
                    $.each($scope.IPTailieuFiles, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                },
                uploadEventHandlers: {
                    progress: function (e) {
                        if (e.lengthComputable) {
                            fileProgress.setAttribute("value", e.loaded);
                            fileProgress.setAttribute("max", e.total);
                            var po = Math.ceil(e.loaded * 100 / e.total);
                            desProgress.innerHTML = po + "%";
                            document.getElementById("bpo").innerHTML = "Đang tải dữ liệu (" + (len - lenbc) + "/" + len + ")";
                        }
                    }
                }
            }).then(function (res) {
                //closeswal();
                $("#modalfileProgress").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                $scope.loadding = false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi upload file !'
                    });
                    return false;
                }
                $("#ModalFileUpload").modal("hide");
                $scope.IPTailieuFiles = [];
                showtoastr("Upload File thành công!");
                if (filecv)
                    $scope.listFileCongViec();
                else
                    $scope.listThumuc();
            }, function (response) { // optional
                $scope.loadding = false;
                $("#modalfileProgress").modal("hide");
            });
        };
        //Thảo luận
        $scope.ckEditorOptions = {
            toolbar: 'full',
            toolbar_full: [
                {
                    name: 'basicstyles',
                    items: ['Bold', 'Italic', 'Strike', 'Underline']
                },
                {
                    name: 'paragraph',
                    items: ['BulletedList', 'NumberedList', 'Blockquote']
                },
                {
                    name: 'insert',
                    items: ['Table', 'SpecialChar']
                },
                {
                    name: 'forms',
                    items: ['Outdent', 'Indent']
                },
                {
                    name: 'clipboard',
                    items: ['Undo', 'Redo']
                }
            ],
            uiColor: '#FAFAFA',
            height: '250px',
            htmlEncodeOutput: true
        };
        $scope.thaoluans = [];
        $scope.cdthanhviens = [];
        $scope.addTG = function () {
            if ($scope.thaoluan.tgda) {
                $scope.cdthanhviens = $scope.cdthanhviens.concat($scope.thanhviens);
                $scope.cdthanhviens = ([...new Set($scope.cdthanhviens)]);
            }
        };
        $scope.openModalAddThaoluan = function (cd) {
            $scope.LisFileAttachTL = [];
            $scope.FilesAttachTaskListTL = [];
            $scope.thaoluan = { ChudeID: cd != null ? cd.ChudeID : null, NguoiTao: $rootScope.login.u.NhanSu_ID, NgayTao: new Date(), STT: $scope.thaoluans.length + 1, DuanID: $scope.DuanID };
            $("#ModalThaoluanAdd form.ng-dirty").removeClass("ng-dirty");
            $("#CongviecCtr #ModalThaoluanAdd").modal("show");
        };
        $scope.getThaoluan = function () {
            $scope.LisFileAttachTL = [];
            $scope.FilesAttachTaskListTL = [];
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ChiTietThaoluan", pas: [
                        { "par": "ThaoluanID ", "va": $scope.thaoluan.ThaoluanID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.cdthanhviens = data[1];
                    $scope.LisFileAttachTL = data[2];
                    data[3].forEach(function (r) {
                        r.stisks = JSON.parse(r.stisks);
                        if (r.files)
                            r.files = JSON.parse(r.files);
                    });
                    $scope.Comments = data[3];
                    $scope.Comments.filter(x => x.ParentID === null).forEach(function (r) {
                        r.Comments = $scope.Comments.filter(x => x.ParentID === r.CommentID);
                    });
                    $scope.thaoluan = data[0][0];
                }
            });
        };
        $scope.clickThaoluan = function (ta) {
            $scope.thaoluan = ta;
            $scope.getThaoluan();
        };
        $scope.AddThaoluan = function (frm) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Duan/Update_Thaoluan",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    formData.append("models", JSON.stringify($scope.thaoluan));
                    if ($scope.cdthanhviens)
                        formData.append("thanhviens", JSON.stringify($scope.cdthanhviens));
                    if ($("#frThaoluan input[type='file']")[0]) {
                        $.each($scope.FilesAttachTaskListTL, function (i, file) {
                            formData.append('file', file);
                        });
                    }
                    return formData;
                }
            }).then(function (res) {
                closeswal();
                $scope.loadding = false;
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm nhóm công việc !'
                    });
                    return false;
                }
                $("#CongviecCtr #ModalThaoluanAdd").modal("hide");
                $scope.listChude();
                //resetForm(frm);
            }, function (response) { // optional
                //resetForm(frm);
                $scope.loadding = false;
            });
        };
        //Nhom công việc
        var todos = [];
        $scope.listNhomCV = function (id) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_Duan_ListTudienNhomcongviec", pas: [
                        { "par": "DuanID", "va": id || $scope.DuanID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    var fn = { NhomCongviecID: null, Tennhom: "Chưa phân nhóm" };
                    data[0].push(fn);
                    $scope.nhomcongviecs = data[0];
                }
            });
        };
        $scope.DeleteNhomcongviec = function (cv, i) {
            if (cv.soCV > 0) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Nhóm đã có công việc không thể xóa!'
                });
                return;
            }
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa nhóm công việc này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_NhomCongViec",
                        data: { t: $rootScope.login.tk, id: cv.NhomCongviecID },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.nhomcongviecs.splice(i, 1);
                            showtoastr("Nhóm công việc bạn chọn đã được xóa thành công!");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa nhóm không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.viewNhomCongviec = function () {
            $("#ModalNhomcongviecList").modal("show");
        };
        $scope.openModalAddNhomCV = function (ncv) {
            $("form.ModalNhomcongviecAdd.ng-dirty").removeClass("ng-dirty");
            if (!$scope.nhomcongviecs)
                $scope.nhomcongviecs = [];
            if (ncv != null) {
                $scope.nhomcongviec = { ...ncv };
            } else
                $scope.nhomcongviec = { Congty_ID: $rootScope.login.u.congtyID, STT: $scope.nhomcongviecs.length + 1, DuanID: $scope.congviec != null ? $scope.congviec.DuanID : $scope.DuanID, Hienthi: true };
            $("#CongviecCtr #ModalNhomcongviecAdd").modal("show");
        };
        $scope.AddNhomcongviec = function (frm) {
            if ($scope.loadding) {
                return false;
            }
            if (!valid(frm)) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            $scope.loadding = true;
            swal.showLoading();
            $http({
                method: 'POST',
                url: "Duan/Update_NhomCongViec",
                data: { t: $rootScope.login.tk, model: $scope.nhomcongviec },
                headers: {
                    contentType: 'application/json; charset=utf-8'
                },
            }).then(function (res) {
                $scope.loadding = false;
                closeswal();
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm nhóm công việc !'
                    });
                    return false;
                }
                $("#CongviecCtr #ModalNhomcongviecAdd").modal("hide");
                $scope.listNhomCV($scope.congviec != null ? $scope.congviec.DuanID : $scope.DuanID);
                //resetForm(frm);
            }, function (response) { // optional
                //resetForm(frm);
                $scope.loadding = false;
            });
        };
        //Like
        var com;
        var IsType = 0;
        $scope.clickLike = function (co, tt) {
            $("#ModalLike").modal("show");
            com = co;
            IsType = tt;
        };
        $scope.setStick = function (st) {
            $("#ModalLike").modal("hide");
            var sts = { CongviecID: $scope.congviec.CongviecID, DuanID: $scope.congviec.DuanID, NguoiTao: $rootScope.login.u.NhanSu_ID, NgayTao: new Date(), CommentID: com.CommentID, IsType: IsType, Stick_ID: st.Stick_ID };
            $http({
                method: "POST",
                url: "Duan/UpdateStisk",
                data: {
                    t: $rootScope.login.tk, ta: sts
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    $scope.getCongViec();
                    showtoastr('Bản đã gửi Stick thành công!.');
                }
            });
        };

        $scope.getStick = function (cm) {
            $("#ModalStisks").modal("show");
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Task_ListStisk", pas: [
                        { "par": "CommentID", "va": cm.CommentID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    $scope.modalStisks = JSON.parse(res.data.data)[0];
                    $scope.tudien[1].forEach(function (st) {
                        st.Stisks = $scope.modalStisks.filter(x => x.Stick_ID === st.Stick_ID);
                    });
                }
            });
        };
        //Comment
        $scope.cart = cart;
        $scope.emojis = emojis;
        $scope.resetemojiIndex = function () {
            $scope.emojiIndex = 0;
        }
        $scope.setemojiIndex = function (idx) {
            $scope.emojiIndex = idx;
        }
        $scope.emojiIndex = 0;
        $scope.noiDungChat = {
            Noidung: ''
        };
        $scope.openImageChat = function () {
            $("#viewTask input.inputimgChat").click();
        }
        $scope.openFileChat = function (ipn) {
            $("#viewTask input." + (ipn || inputfileChat)).click();
        }

        $scope.UploadFileChat = function (f) {
            if (!$scope.FileAttach) $scope.FileAttach = [];
            if ($scope.FileAttach.length + f.length > 5) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 5 file cho dự án!'
                });
                return false;
            }
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 25 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    fi.Tenfile = fi.name;
                    fi.Dungluong = fi.size;
                    $scope.FileAttach.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 25MB!'
                });
            } else {
                if (!ReplyID)
                    $scope.goBottomChat();
            }
        }
        $scope.openFileChatDynamic = function (id) {
            $(id).click();
        }

        $scope.UploadFileChatDynamic = function (f) {
            if (!$scope.FileAttachBC) $scope.FileAttachBC = [];
            if ($scope.FileAttachBC.length + f.length > 5) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 5 file cho dự án!'
                });
                return false;
            }
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 25 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    fi.Tenfile = fi.name;
                    fi.Dungluong = fi.size;
                    $scope.FileAttachBC.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 25MB!'
                });
            } else {
                if (!ReplyID)
                    $scope.goBottomChat();
            }
        }

        $scope.UploadFileRV = function (f) {
            if (!$scope.FileAttachRV) $scope.FileAttachRV = [];
            if ($scope.FileAttachRV.length + f.length > 5) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 5 file lên hệ thống!'
                });
                return false;
            }
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 25 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    fi.Tenfile = fi.name;
                    fi.Dungluong = fi.size;
                    $scope.FileAttachRV.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 25MB!'
                });
            } else {
                if (!ReplyID)
                    $scope.goBottomChat();
            }
        }


        $scope.setEmoji = function (e, type) {
            if (type == false) {
                var noidung = $scope.noiDungChat.Noidung || "";
                var noiDungChat = noidung + e.emoji;
                $scope.noiDungChat.Noidung = noiDungChat;
            } else {
                $scope.noiDungChat.Noidung = e;
                //$scope.sendMS(4);
            }
            $("#noiDungChat").focus();
        }
        $scope.setEmojiBC = function (e, type) {
            if (type == false) {
                var noidung = $scope.Baocao.Noidung || "";
                var noiDungChat = noidung + e.emoji;
                $scope.Baocao.Noidung = noiDungChat;
            } else {
                $scope.Baocao.Noidung = e;
            }
            $("#noiDungChatCVComment").focus();
        }
        $scope.setEmojiRV = function (e, type) {
            if (type == false) {
                var noidung = $scope.Review.Noidung;
                var noiDungChat = noidung + e.emoji;
                $scope.Review.Noidung = noiDungChat;
            } else {
                $scope.Review.Noidung = e;
            }
            $("#noiDungChatRV").focus();
        }


        $scope.goBottomChat = function () {
            setTimeout(function () {
                var div = document.getElementById("taskmessagepanel");
                div.scrollTop = div.scrollHeight - div.clientHeight;
            }, 100)
        }
        var ReplyID = null;
        $scope.Reply = function (co) {
            $scope.Comments.filter(x => x.IsReply).forEach(function (r) {
                r.IsReply = false;
            });
            $scope.IsReply = true;
            ReplyID = co.CommentID;
            co.IsReply = true;
            $scope.tinnhanreply = co;
        };
        $scope.HuyReply = function () {
            var co = $scope.Comments.find(x => x.CommentID === ReplyID);
            $scope.IsReply = false;
            ReplyID = null;
            $scope.tinnhanreply = null;
            co.IsReply = false;
            $scope.FileAttach = [];
        };
        $scope.Comments = [];
        function bindTypeComment() {
            var ty = 0;
            if ($scope.thaoluan && $scope.thaoluan.ThaoluanID)
                return 1;
            if ($scope.tailieu && $scope.tailieu.TailieuID)
                return 3;
            if ($scope.congviec && $scope.congviec.CongviecID)
                return 4;
            return ty;
        }
        $scope.sendMS = function (loai) {
            if ($scope.loadding) {
                return false;
            }
            if (!$scope.noiDungChat.Noidung && $scope.FileAttach.length === 0) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập nội dung comment !'
                });
                return false;
            }
            $scope.loadding = true;
            //swal.showLoading();
            var ms = { ParentID: ReplyID, CongviecID: $scope.congviec ? $scope.congviec.CongviecID : null, DuanID: $scope.DuanID, ThaoluanID: $scope.thaoluan ? $scope.thaoluan.ThaoluanID : null, TailieuID: $scope.tailieu ? $scope.tailieu.TailieuID : null, NguoiTao: $rootScope.login.u.NhanSu_ID, Noidung: $scope.noiDungChat.Noidung, NgayTao: new Date(), fullName: $rootScope.login.u.fullName, anhThumb: $rootScope.login.u.anhDaiDien, IsType: bindTypeComment(), IsDelete: false };
            ms.files = $scope.FileAttach;
            $scope.Comments.push(ms);
            $scope.Comments.filter(x => x.ParentID != null).forEach(function (r) {
                r.Comments = $scope.Comments.filter(x => x.ParentID === r.CommentID);
                r.ParentComment = $scope.Comments.find(x => r.ParentID === x.CommentID);
            });
            $scope.noiDungChat.Noidung = "";
            $scope.goBottomChat();
            //Progress File
            var hasfile = $scope.FileAttach && $scope.FileAttach.length > 0;
            if (!hasfile) {
                swal.showLoading();
            } else {
                $("#modalfileProgress").modal("show");
                var fileProgress = document.getElementById("fileProgress");
                var desProgress = document.getElementById("podes");
                fileProgress.setAttribute("value", 0);
                fileProgress.setAttribute("max", 0);
                fileProgress.style.display = "block";
            }
            $http({
                method: 'POST',
                url: "Duan/Update_Comment",
                headers: {
                    'Content-Type': undefined,
                },
                transformRequest: function () {
                    debugger;
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    formData.append("Congty_ID", $rootScope.login.u.congtyID);
                    var $html = $("#noiDungChatCVComment");
                    var ids = [];
                    $html.find("uid").each(function (i, el) {
                        ids.push(el.innerText);
                        el.remove();
                    });
                    ms.Noidung = $html.html();
                    formData.append("models", JSON.stringify(ms));
                    if (ids.length > 0) {
                        formData.append("ids", ids.join(","));
                    }
                    $.each($scope.FileAttach, function (i, file) {
                        formData.append('file', file);
                    });
                    return formData;
                },
                uploadEventHandlers: {
                    progress: function (e) {
                        if (hasfile && e.lengthComputable) {
                            fileProgress.setAttribute("value", e.loaded);
                            fileProgress.setAttribute("max", e.total);
                            var po = Math.ceil(e.loaded * 100 / e.total);
                            desProgress.innerHTML = po + "%";
                        }
                    }
                }
            }).then(function (res) {
                $scope.loadding = false;
                if (!hasfile)
                    closeswal();
                if ($scope.ThongKeChung)
                    $scope.ThongKeChung.SoComment += 1;
                $("#modalfileProgress").modal("hide");
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 0) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: 'Có lỗi khi thêm comment mới !'
                    });
                    return false;
                }
                $scope.Comments[$scope.Comments.length - 1].CommentID = res.data.CommentID;
                if ($scope.FileAttach != null && $scope.FileAttach.length > 0) {
                    $scope.listComments();
                }
                $scope.FileAttach = [];
                $('input[name=FileAttachChat]').val("");
                if (ReplyID) $scope.HuyReply();
                $("textarea#noiDungChat").val(null);
                window.parent.postMessage({ Type: 1, Isdel: false }, "*");
            }, function (response) { // optional
                $scope.loadding = false;
            });
        }

        $scope.Del_Comment = function (cm, i) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa comment này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_Comment",
                        data: {
                            t: $rootScope.login.tk, id: cm.CommentID
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.Comments.splice(i, 1);
                            if ($scope.ThongKeChung)
                                $scope.ThongKeChung.SoComment -= 1;
                        }
                    });
                }
            })

        };
        $scope.removeFilesComment = function (files, i) {
            files.splice(i, 1);
        };
        $scope.Del_CommentFile = function (files, i, f) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa file này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "Duan/Del_DuanFile",
                        data: { t: $rootScope.login.tk, ids: [f.FileID] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            files.splice(i, 1);
                            showtoastr("File bạn chọn đã được xóa thành công!.");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa File không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        
        //End nhom du an
        $scope.PutFileUpload = function (f) {
            var ms = false;
            angular.forEach(f, function (fi) {
                if (fi.size > 25 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Tenfile = fi.name;
                    fi.Dungluong = fi.size;
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    $scope.IPTailieuFiles.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 25MB!'
                });
            }
        };
        $(document).ready(function () {
            var dropZone = document.getElementById('#viewTask drop-zone');
            if (dropZone) {
                var startUpload = function (files) {
                    $scope.PutFileUpload(files);
                    //if ($scope.$$phase) {
                    $scope.$apply();
                    //}
                };
                dropZone.ondrop = function (e) {
                    e.preventDefault();
                    this.className = 'upload-drop-zone';
                    startUpload(e.dataTransfer.files);
                };
                dropZone.ondragover = function () {
                    this.className = 'upload-drop-zone drop';
                    return false;
                };
                dropZone.ondragleave = function () {
                    this.className = 'upload-drop-zone';
                    return false;
                };
                dropZone.onclick = function () {
                    $("#ModalFileUpload input[type='file']").trigger("click");
                };
            }
        });
    }
});