﻿angular.module('RequestCtr', [])
    .controller("RequestCtr", ['$scope', '$rootScope', '$http', '$filter', 'Upload', '$interval', '$state', '$stateParams', '$sce', function ($scope, $rootScope, $http, $filter, Upload, $interval, $state, $stateParams, $sce) {
        var Temp = [];
        function addToArray(array, id, lv) {
            var filter = $filter('filter')(array, { Parent_ID: id }, true);
            filter = $filter('orderBy')(filter, 'thutu');
            if (filter.length > 0) {
                var sp = "";
                for (var i = 0; i < lv; i++) {
                    sp += "----- ";
                }
                lv++;
                angular.forEach(filter, function (item) {
                    item.lv = lv;
                    item.close = true;
                    item.ids += "," + item.MauFormD_ID;
                    item.tenmoi = sp + item.TenTruong;
                    Temp.push(item);
                    addToArray(array, item.MauFormD_ID, lv);
                });
            }
        }
        function addToArrayNT(Temp, array, id, lv, od) {
            var filter = $filter('filter')(array, { Nhomcha_ID: id }, true);
            filter = $filter('orderBy')(filter, od);
            if (filter.length > 0) {
                var sp = "";
                for (var i = 0; i < lv; i++) {
                    sp += "---";
                }
                lv++;
                angular.forEach(filter, function (item) {
                    item.lv = lv;
                    item.close = true;
                    if (!item.ids)
                        item.ids += "," + item.NhomTeam_ID;
                    if (!item.tenmoi)
                        item.tenmoi = sp + item.TenNhom;
                    Temp.push(item);
                    addToArrayNT(Temp, array, item.NhomTeam_ID, lv);
                });
            }

        }
        function addToArray_FT(Temp, array, id, lv, od) {
            var filter = $filter('filter')(array, { Parent_ID: id }, true);
            if (filter.length > 0) {
                angular.forEach(filter.filter(x => x.Parent_ID == null || x.lv == 1), function (item) {
                    item.Name = "Tên đề xuất: " + item.Name;
                });
            }
            //filter = $filter('orderBy')(filter, od);
            if (filter.length > 0) {
                var sp = "";
                for (var i = 0; i < lv; i++) {
                    sp += "---Team: ";
                }
                lv++;
                angular.forEach(filter, function (item) {
                    item.lv = lv;
                    item.close = true;
                    if (!item.ids)
                        item.ids += "," + item.Child_ID;
                    if (!item.tenmoi)
                        item.tenmoi = sp + item.Name;
                    Temp.push(item);
                    addToArray_FT(Temp, array, item.Child_ID, lv);
                });
            }

        }
        function addToArray_MFD(Temp, array, id, lv, od) {
            let filter = $filter('filter')(array, { IsParent_ID: id }, true);
            filter = $filter('orderBy')(filter, od);
            if (filter.length > 0) {
                var sp = "";
                for (var i = 0; i < lv; i++) {
                    sp += "---";
                }
                lv++;
                angular.forEach(filter, function (item) {
                    item.lv = lv;
                    item.close = true;
                    if (!item.ids)
                        item.ids += "," + item.MauFormD_ID;
                    if (!item.tenmoi)
                        item.tenmoi = sp + item.TenTruong;
                    //if (id == null) {
                    //    Temp.push(item);
                    //}
                    //item.List_FormChild = $filter('filter')(array, { IsParent_ID: item.MauFormD_ID }, true);
                    Temp.push(item);
                    addToArray_MFD(Temp, array, item.MauFormD_ID, lv);
                });
            }

        }
        function addToArray_FD(Temp, array, id, lv, od) {
            let filter = $filter('filter')(array, { IsParent_ID: id }, true);
            filter = $filter('orderBy')(filter, od);
            if (filter.length > 0) {
                lv++;
                angular.forEach(filter, function (item) {
                    item.lv = lv;
                    item.close = true;
                    if (!item.ids)
                        item.ids += "," + item.FormD_ID;
                    if (!item.tenmoi)
                        item.tenmoi = item.TenTruong;
                    //if (id == null) {
                    //    Temp.push(item);
                    //}
                    //item.List_FormChild = $filter('filter')(array, { IsParent_ID: item.MauFormD_ID }, true);
                    Temp.push(item);
                    addToArray_FD(Temp, array, item.FormD_ID, lv);
                });
            }

        }
        function groupBy(list, props) {
            return list.reduce((a, b) => {
                (a[b[props]] = a[b[props]] || []).push(b);
                return a;
            }, {});
        }

        $scope.trustAsHtml = function (html) {
            return $sce.trustAsHtml(html);
        };
        $scope.filterValue = function ($event) {
            if (isNaN(String.fromCharCode($event.keyCode))) {
                $event.preventDefault();
            }
        };
        $rootScope.bgColor = [
            "#F8E69A", "#AFDFCF", "#F4B2A3", "#9A97EC", "#CAE2B0", "#8BCFFB", "#CCADD7"
        ];

        $scope.opition = {
            search: "",
            search_ns: "",
            search_team: "",
            numPerPage: 50,
            currentPage: 1,
            sort: "STT",
            ob: "asc",
            Status: null,
            tenSort: "",
            tenTT: "Tất cả",
            tenStatus: "Tất cả",
            IsSearchBar: true,
            IsSearchBar_ns: true,
            IsSearchBar_team: true,
            CheckAllG: false,
            CheckAllG_PB: false,
            CheckAllG_T: false,
            n_Congty_ID: $rootScope.login.u.congtyID,
            //fress search
            ViTris: [],
            form_ids: [],
            NoiLamViecs: [],
            TrinhDos: [],
            mltu: null,
            mlden: null,
            HantuyenTu: null,
            HantuyenDen: null,
            //============
            s_ngaylap: null,
            s_vitri: null,
            s_hinhthuc: null,
            Total: 0,
            Pages: [50, 100, 1000, 5000]
        };
        $scope.autoRotate = false;

        //FREE SEARCH =================
        $scope.changeCongty_Advsearch = function () {
            $scope.GetPhongbanTree($scope.opition.Congty_ID);
            $scope.ListChucvu($scope.opition.Congty_ID);
        };
        ///search Vị trí tuyển dụng
        $scope.completeVitri_AdvSearch = function (string, m, $event, i) {
            $scope.opition.Focus1 = false;
            $scope.opition.Focus2 = false;
            $scope.opition.Focus3 = false;
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            if (!string) {
                string = "";
            }
            //string = string.replace("@", "");
            m["Focus" + i] = !m["Focus" + i];
            var output = [];
            angular.forEach($scope.TudienDXTuyenDung[1], function (u) {
                if ((u.TenVitri || "").toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    output.push(u);
                }
            });
            $scope.Vitris_advsearch = output;
            if ($scope.opition.ViTris.length > 0) {
                angular.forEach($scope.opition.ViTris, function (item) {
                    var i = $scope.Vitris_advsearch.findIndex(x => x.VitriTD_ID === item.VitriTD_ID);
                    if (i !== -1) {
                        $scope.Vitris_advsearch.splice(i, 1);
                    }
                });
            }
        };
        ///search Phòng ban
        $scope.completePhongban_AdvSearch = function (string, m, $event, i) {
            $scope.opition.Focus = false;
            $scope.opition.Focus2 = false;
            $scope.opition.Focus3 = false;
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            if (!string) {
                string = "";
            }
            //string = string.replace("@", "");
            m["Focus" + i] = !m["Focus" + i];
            var output = [];
            angular.forEach($scope.TudienDXTuyenDung[2], function (u) {
                if ((u.tenPhongban || "").toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    output.push(u);
                }
            });
            $scope.Phongbans_advsearch = output;
            if ($scope.opition.PhongBans.length > 0) {
                angular.forEach($scope.opition.PhongBans, function (item) {
                    var i = $scope.Phongbans_advsearch.findIndex(x => x.Phongban_ID === item.Phongban_ID);
                    if (i !== -1) {
                        $scope.Phongbans_advsearch.splice(i, 1);
                    }
                });
            }
        };
        ///search Nơi làm việc
        $scope.completeNoilamviec_AdvSearch = function (string, m, $event, i) {
            $scope.opition.Focus = false;
            $scope.opition.Focus1 = false;
            $scope.opition.Focus3 = false;
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            if (!string) {
                string = "";
            }
            //string = string.replace("@", "");
            m["Focus" + i] = !m["Focus" + i];
            var output = [];
            angular.forEach($scope.TudienDXTuyenDung[3], function (u) {
                if ((u.TenDiadanh || "").toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    output.push(u);
                }
            });
            $scope.Noilamviecs_advsearch = output;
            if ($scope.opition.NoiLamViecs.length > 0) {
                angular.forEach($scope.opition.NoiLamViecs, function (item) {
                    var i = $scope.Noilamviecs_advsearch.findIndex(x => x.Diadanh_ID === item.Diadanh_ID);
                    if (i !== -1) {
                        $scope.Noilamviecs_advsearch.splice(i, 1);
                    }
                });
            }
        };
        ///search Nơi làm việc
        $scope.completeTrinhdo_AdvSearch = function (string, m, $event, i) {
            $scope.opition.Focus = false;
            $scope.opition.Focus1 = false;
            $scope.opition.Focus2 = false;
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            if (!string) {
                string = "";
            }
            //string = string.replace("@", "");
            m["Focus" + i] = !m["Focus" + i];
            var output = [];
            angular.forEach($scope.TudienDXTuyenDung[4], function (u) {
                if ((u.TenTrinhdohocvan || "").toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    output.push(u);
                }
            });
            $scope.Trinhdos_advsearch = output;
            if ($scope.opition.TrinhDos.length > 0) {
                angular.forEach($scope.opition.TrinhDos, function (item) {
                    var i = $scope.Trinhdos_advsearch.findIndex(x => x.TrinhdoHV_ID === item.TrinhdoHV_ID);
                    if (i !== -1) {
                        $scope.Trinhdos_advsearch.splice(i, 1);
                    }
                });
            }
        };

        $scope.clickAddVitri_AdvSearch = function (u) {
            $scope.opition.ViTris.push(u);

            var i = $scope.Vitris_advsearch.findIndex(x => x.VitriTD_ID === u.VitriTD_ID);
            if (i !== -1) {
                $scope.Vitris_advsearch.splice(i, 1);
            }
            $scope.opition.Focus = false;
            $scope.opition.searchVitri = "";
        }
        $scope.clickAddPB_AdvSearch = function (u) {
            $scope.opition.PhongBans.push(u);

            var i = $scope.Phongbans_advsearch.findIndex(x => x.Phongban_ID === u.Phongban_ID);
            if (i !== -1) {
                $scope.Phongbans_advsearch.splice(i, 1);
            }
            $scope.opition.Focus1 = false;
            $scope.opition.searchPhongBan = "";
        }
        $scope.clickAddNLV_AdvSearch = function (u) {
            $scope.opition.NoiLamViecs.push(u);

            var i = $scope.Noilamviecs_advsearch.findIndex(x => x.Diadanh_ID === u.Diadanh_ID);
            if (i !== -1) {
                $scope.Noilamviecs_advsearch.splice(i, 1);
            }
            $scope.opition.Focus2 = false;
            $scope.opition.searchNoiLamViec = "";
        }
        $scope.clickAddTD_AdvSearch = function (u) {
            $scope.opition.TrinhDos.push(u);

            var i = $scope.Trinhdos_advsearch.findIndex(x => x.TrinhdoHV_ID === u.TrinhdoHV_ID);
            if (i !== -1) {
                $scope.Trinhdos_advsearch.splice(i, 1);
            }
            $scope.opition.Focus3 = false;
            $scope.opition.searchTrinhDo = "";
        }

        $scope.removetag = function (us, us_id, idx) {
            us.splice(idx, 1);
        };
        //=============================

        //Data set filter, order, page ================
        $scope.ListStatus_Nhomform = [
            { value: true, text: 'Kích hoạt' },
            { value: false, text: 'Khóa' },
        ]
        $scope.ListSex = [
            { value: 0, text: 'Nữ' },
            { value: 1, text: 'Nam' },
            { value: 2, text: 'Không yêu cầu' },
        ]
        $scope.ListExp = [
            { value: 1, text: 'Chưa có kinh nghiệm' },
            { value: 2, text: '1 năm' },
            { value: 3, text: '2 năm' },
            { value: 4, text: '3 năm' },
            { value: 5, text: '4 năm' },
            { value: 6, text: '5 năm' },
            { value: 7, text: 'Trên 5 năm' },
        ]
        $scope.ListSort_Nhomform = [
            { value: 'STT', text: 'Thứ tự' },
            { value: 'Ngaytao', text: 'Ngày tạo' },
            { value: 'TenForm', text: 'Tên nhóm' },
            { value: 'Nguoitao', text: 'Người tạo' },
        ]

        $scope.ListSort_Mauform = [
            { value: 'STT', text: 'Thứ tự' },
            { value: 'Ngaytao', text: 'Ngày tạo' },
            { value: 'Form_Name', text: 'Mãu form' },
            { value: 'Nguoitao', text: 'Người tạo' },
        ]

        $scope.Tudien_LoaiTeam = [
            { value: 0, text: 'Bình thường' },
            { value: 1, text: 'Change quy trình (Cho phép thay đổi quy trình)' },
        ]

        $scope.Tudien_LoaiUser = [
            { value: 0, text: 'Bình thường' },
            { value: 1, text: 'Leader' },
            { value: 2, text: 'Cấp cao nhất (Manager)' },
        ]
        $scope.Tudien_LoaiUser_2 = [
            { value: 0, text: 'Người duyệt' },
            { value: 3, text: 'Người theo dõi' },
        ]

        $scope.List_TypeColumn = [
            { value: 'varchar', text: 'varchar', is_length: true },
            { value: 'nvarchar', text: 'nvarchar', is_length: true },
            { value: 'int', text: 'int', is_length: true },
            { value: 'float', text: 'float', is_length: true },
            { value: 'bit', text: 'bit', is_length: false },
            { value: 'textarea', text: 'textarea', is_length: true },
            { value: 'checkbox', text: 'checkbox', is_length: false },
            { value: 'radio', text: 'radio', is_length: false },
            { value: 'date', text: 'date', is_length: false },
            { value: 'datetime', text: 'datetime', is_length: false },
            { value: 'time', text: 'time', is_length: false },
            { value: 'file', text: 'file', is_length: false },
            { value: 'select', text: 'select', is_length: false },
            { value: 'email', text: 'email', is_length: true },
        ]

        $scope.List_TypeDuyet = [
            //{ value: 0, text: 'Duyệt' },
            { value: 0, text: 'Một trong nhiều' },
            { value: 1, text: 'Duyệt tuần tự' },
            { value: 2, text: 'Duyệt ngẫu nhiên' },
        ]

        $scope.List_TypeGroup = [
            { value: "0", text: 'Test' },
        ]

        $scope.List_TypeGroupDuyet = [
            { value: null, text: "Bình thường" },
            { value: '1', text: "Trưởng bộ phận duyệt"}
        ]

        $scope.listType_Required = [
            { value: true, text: '* Required' },
            { value: false, text: 'Not Required' },
            { value: null, text: 'Not Required' },
        ];

        $scope.List_Data_Class = [
            { value: 'col-md-2', text: 'col-md-2' },
            { value: 'col-md-3', text: 'col-md-3' },
            { value: 'col-md-4', text: 'col-md-4' },
            { value: 'col-md-6', text: 'col-md-6' },
            { value: 'col-md-12', text: 'col-md-12' },
        ];

        $scope.List_Data_Type = [
            { value: 0, text: 'Trường bình thường' },
            { value: 1, text: 'Trường tổng hợp (tính tổng)' },
            { value: 2, text: 'Trường ẩn tên' },
            { value: 3, text: 'Dạng bảng' },
            { value: 4, text: 'Cột trong bảng' },
            { value: 5, text: 'Dòng trong bảng' }
        ];

        $scope.setSort = function (so) {
            if (so)
                $scope.opition.sort = so.value;
            switch ($stateParams.type) {
                case "1":
                    $scope.BindList_Nhomform(true);
                    break;
                case "2":
                    $scope.BindList_Mauform(true);
                    break;
                case "3":
                    $scope.BindList_Khaibaoform(true);
                    break;
                case "4":
                    $scope.BindList_Nhomteam(true);
                    break;
                case "5":
                    $scope.BindList_Khaibaoteam(true);
                    break;
            }
        }
        $scope.setOrderby = function (ob) {
            if (ob)
                $scope.opition.ob = ob.value;
            switch ($stateParams.type) {
                case "1":
                    $scope.BindList_Nhomform(true);
                    break;
                case "2":
                    $scope.BindList_Mauform(true);
                    break;
                case "3":
                    $scope.BindList_Khaibaoform(true);
                    break;
                case "4":
                    $scope.BindList_Nhomteam(true);
                    break;
                case "5":
                    $scope.BindList_Khaibaoteam(true);
                    break;
            }
        }
        $scope.toggleAll = function (rowto, CheckAllG) {
            angular.forEach(rowto, function (value, key) {
                rowto[key].checked = CheckAllG;
            });
            $scope.CheckCheked(rowto);
        };
        $scope.CheckCheked = function (m) {
            var filtereds = [];
            switch ($stateParams.type) {
                case "1":
                    filtereds = $filter('filter')($scope.List_Data_Nhomform, { checked: true }, true);
                    break;
                case "2":
                    filtereds = $filter('filter')($scope.List_Data_Mauform, { checked: true }, true);
                    break;
                case "3":
                    filtereds = $filter('filter')($scope.List_Data_Khaibaoform, { checked: true }, true);
                    break;
                case "4":
                    filtereds = $filter('filter')($scope.List_Data_Nhomteam, { checked: true }, true);
                    break;
                case "5":
                    filtereds = $filter('filter')($scope.List_Data_Khaibaoteam, { checked: true }, true);
                    break;
            }
            $scope.checkLen = filtereds.length;

        };
        $scope.toggleAll_KBF = function (rowto, CheckAllG) {
            angular.forEach(rowto, function (value, key) {
                angular.forEach(rowto[key].list, function (value_kbf, key_kbf) {
                    rowto[key].list[key_kbf].checked = CheckAllG;
                });
            });
            $scope.CheckCheked_KBF(rowto);
        };
        $scope.CheckCheked_KBF = function (m) {
            var filtereds = [];
            switch ($stateParams.type) {
                case "3":
                    angular.forEach($scope.List_Data_Khaibaoform, function (r) {
                        let checked = $filter('filter')(r.list, { checked: true }, true);
                        if (checked != null && checked.length > 0) filtereds.push(checked);
                    })
                    break;
            }
            $scope.checkLen = filtereds.length;
        };
        $scope.setStatusDXTD = function (t) {
            if (t)
                $scope.opition.Status = t.statusdxtd;
            else
                $scope.opition.Status = null;
            $scope.BindList_Nhomform(true);
        };
        $scope.setPageSize = function (p) {
            $scope.opition.currentPage = 1;
            $scope.opition.numPerPage = p;
            $scope.BindList_Nhomform(true);
        };
        $scope.Clear_filter = function () {
            $scope.opition = {
                search: "",
                search_ns: "",
                search_team: "",
                numPerPage: 50,
                currentPage: 1,
                sort: "STT",
                ob: "asc",
                Status: null,
                tenSort: "",
                tenTT: "Tất cả",
                tenStatus: "Tất cả",
                IsSearchBar: true,
                IsSearchBar_ns: true,
                IsSearchBar_team: true,
                CheckAllG: false,
                CheckAllG_PB: false,
                CheckAllG_T: false,
                n_Congty_ID: $rootScope.login.u.congtyID,
                //fress search
                ViTris: [],
                form_ids: [],
                NoiLamViecs: [],
                TrinhDos: [],
                mltu: null,
                mlden: null,
                HantuyenTu: null,
                HantuyenDen: null,
                //============
                Total: 0,
                Pages: [50, 100, 1000, 5000]
            };
        };
        $scope.Clickfilter = function (ngaylap, vitri, hinhthuc) {
            if (ngaylap) {
                $scope.opition.s_ngaylap = ngaylap;
            } else if (vitri) {
                $scope.opition.s_vitri = vitri;
            } else if (hinhthuc) {
                $scope.opition.s_hinhthuc = hinhthuc;
            }
            $scope.BindList_Nhomform(true);
        };
        //=============================================

        //============= Function use Change, Order =======================
        $scope.openAttachfile = function (ev) {
            $(ev.target).closest("div.form-group").find("input[type='file']").trigger("click");
        };
        $scope.removeFiles = function (idx) {
            $scope.FilesAttachList.splice(idx, 1);
        };
        $scope.DeleteFiles = function (i, f) {
            Swal.fire({
                title: 'Xác nhận?',
                text: "Bạn có muốn xóa file này không!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    $http({
                        method: "POST",
                        url: "/Hrm/Del_DXTDFile",
                        data: { t: $rootScope.login.tk, ids: [f.FileSohoa_ID] },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            $scope.LisFileAttach.splice(i, 1);
                            showtoastr("'File bạn chọn đã được xóa thành công!.'");
                        } else {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: 'Xóa File không thành công, vui lòng thử lại!'
                            });
                        }
                    });

                }
            })
        };
        $scope.UploadFiles = function (f) {
            if ($scope.LisFileAttach.length + f.length > 5) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: 'Bạn chỉ được upload tối đa 5 file 1 lần cho HRM!'
                });
                return false;
            }
            var ms = false;
            angular.forEach(f, function (fi) {

                if (fi.size > 25 * 1024 * 1024) {
                    ms = true;
                } else {
                    fi.Dinhdang = fi.name.substring(fi.name.lastIndexOf(".") + 1);
                    fi.NgayTao = new Date();
                    $scope.FilesAttachList.push(fi);
                }
            });
            if (ms) {
                Swal.fire({
                    icon: 'warning',
                    type: 'warning',
                    title: '',
                    text: 'Bạn chỉ được upload file có dung lượng tối đa 25MB!'
                });
            }
        };
        $scope.ChangeSo = function (m) {
            if (m.Soluong != null)
                m.Soluong = formatMoneyVND(m.Soluong);
            if (m.Luongtu != null)
                m.Luongtu = formatMoneyVND(m.Luongtu);
            if (m.Luongden != null)
                m.Luongden = formatMoneyVND(m.Luongden);
        }
        $scope.ChangSearch = function (m) {
            if (m.Luongtu != null)
                m.Luongtu = formatMoneyVND(m.Luongtu);
            if (m.Luongden != null)
                m.Luongden = formatMoneyVND(m.Luongden);
        }
        $scope.ChangeNameForm = function (Khaibaoform, MauForm_ID) {
            if (Khaibaoform.Form_Name == null || Khaibaoform.Form_Name == '') {
                Khaibaoform.Form_Name = $scope.Srequest_Tudien[1].find(x => x.MauForm_ID == MauForm_ID).Form_Name;
            }
        }
        //========================================================

        //============== Funciton Nhóm duyệt Form Sign Add, Edit, Del =================
        $scope.OpenModal_AddKhaibaoForm = function () {
            $scope.IsEditForm = true;
            $scope.List_Data_Teambyform = [];
            $scope.List_Data_Formsign = [];
            $scope.List_Data_Userformsign = [];
            $scope.Khaibaoform = { IsSLA: 1, Congty_ID: $rootScope.login.u.congtyID, Ngaytao: new Date(), IsQuytrinhduyet: 0, STT: $scope.opition.Total + 1, Trangthai: true, }
            $scope.dtitle = "Thêm mới loại đề xuất";
            $("#Modal_Edit_Khaibaoform").modal("show");
        };
        $scope.OpenModal_Add = function () {
            //$scope.LisFileAttach = [];
            //$scope.FilesAttachList = [];
            //$scope.Viewdxtd = true;
            //$scope.nguoiduyets = [];
            switch ($stateParams.type) {
                case "1":
                    $scope.Nhomform = { Congty_ID: $rootScope.login.u.congtyID, Ngaytao: new Date(), STT: $scope.opition.Total + 1, Trangthai: true, }
                    $scope.dtitle = "Thêm mới nhóm đề xuất";
                    $("#Modal_Edit_Nhomform").modal("show");
                    break;
                case "2":
                    $scope.Mauform = { Congty_ID: $rootScope.login.u.congtyID, Ngaytao: new Date(), STT: $scope.opition.Total + 1, Trangthai: true, }
                    $scope.dtitle = "Thêm mới mẫu đề xuất";
                    $("#Modal_Edit_Mauform").modal("show");
                    break;
                case "3":
                    $scope.Khaibaoform = { IsSLA: 1, Congty_ID: $rootScope.login.u.congtyID, Ngaytao: new Date(), STT: $scope.opition.Total + 1, Trangthai: true, }
                    $scope.dtitle = "Thêm mới loại đề xuất";
                    $("#Modal_Edit_Khaibaoform").modal("show");
                    break;
                case "4":
                    $scope.Nhomteam = { Congty_ID: $rootScope.login.u.congtyID, Ngaytao: new Date(), STT: $scope.opition.Total + 1, Trangthai: true, }
                    $scope.dTitle = "Thêm mới nhóm team";
                    $("#Modal_Edit_Nhomteam").modal("show");
                    break;
                case "5":
                    $scope.Khaibaoteam = { Congty_ID: $rootScope.login.u.congtyID, Ngaytao: new Date(), STT: $scope.opition.Total + 1, Trangthai: true, }
                    $scope.dtitle = "Thêm mới team";
                    $("#Modal_Edit_Khaibaoteam").modal("show");
                    break;
                case "6":
                    if ($stateParams.team != null) {
                        $scope.Formsign = { Congty_ID: $rootScope.login.u.congtyID, Ngaytao: new Date(), IsTypeDuyet: 0, STT: $scope.opition.Total + 1, IsActive: true, }
                        $scope.dtitle = "Thêm mới nhóm duyệt";
                        $("#Modal_Edit_Formsign").modal("show");
                    }
                    break;
            }

        }
        $scope.Click_Add = function (frm) {
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            swal.showLoading();
            switch ($stateParams.type) {
                case "1":
                    $scope.Nhomform.Ngaytao = moment($scope.Nhomform.Ngaytao).format('YYYY-MM-DDTHH:mm:ssZZ');
                    $http({
                        method: 'POST',
                        url: "Request/Update_Nhomform",
                        data: {
                            t: $rootScope.login.tk, model: $scope.Nhomform
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        $scope.loadding = false;
                        closeswal();
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 0) {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: res.data.ms
                            });
                            return false;
                        }
                        $("#Modal_Edit_Nhomform").modal("hide");
                        $scope.BindList_Nhomform(true);
                        showtoastr('Đã cập nhật cấu hình thành công!.');
                    }, function (response) { // optional
                        $scope.loadding = false;
                    });
                    break;
                case "2":
                    $scope.Mauform.Ngaytao = moment($scope.Mauform.Ngaytao).format('YYYY-MM-DDTHH:mm:ssZZ');
                    $http({
                        method: 'POST',
                        url: "Request/Update_Mauform",
                        data: {
                            t: $rootScope.login.tk, model: $scope.Mauform
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        $scope.loadding = false;
                        closeswal();
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 0) {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: res.data.ms
                            });
                            return false;
                        }
                        $("#Modal_Edit_Mauform").modal("hide");
                        $scope.BindList_Mauform(true);
                        showtoastr('Đã cập nhật cấu hình thành công!.');
                    }, function (response) { // optional
                        $scope.loadding = false;
                    });
                    break;
                case "3":
                    $scope.Khaibaoform.Ngaytao = moment($scope.Khaibaoform.Ngaytao).format('YYYY-MM-DDTHH:mm:ssZZ');
                    $http({
                        method: 'POST',
                        url: "Request/Update_Khaibaoform",
                        data: {
                            t: $rootScope.login.tk, Congty_ID: $rootScope.login.u.congtyID, model: $scope.Khaibaoform
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        $scope.loadding = false;
                        closeswal();
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 0) {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: res.data.ms
                            });
                            return false;
                        }
                        $("#Modal_Edit_Khaibaoform").modal("hide");
                        $scope.BindList_Khaibaoform(true);
                        showtoastr('Đã cập nhật cấu hình thành công!.');
                    }, function (response) { // optional
                        $scope.loadding = false;
                    });
                    break;
                case "4":
                    $scope.Nhomteam.Ngaytao = moment($scope.Nhomteam.Ngaytao).format('YYYY-MM-DDTHH:mm:ssZZ');
                    $http({
                        method: 'POST',
                        url: "Request/Update_Nhomteam",
                        data: {
                            t: $rootScope.login.tk, model: $scope.Nhomteam
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        $scope.loadding = false;
                        closeswal();
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 0) {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: res.data.ms
                            });
                            return false;
                        }
                        $("#Modal_Edit_Nhomteam").modal("hide");
                        $scope.BindList_Nhomteam(true);
                        showtoastr('Đã cập nhật cấu hình thành công!.');
                    }, function (response) { // optional
                        $scope.loadding = false;
                    });
                    break;
                case "5":
                    $scope.Khaibaoteam.Ngaytao = moment($scope.Khaibaoteam.Ngaytao).format('YYYY-MM-DDTHH:mm:ssZZ');
                    $http({
                        method: 'POST',
                        url: "Request/Update_Khaibaoteam",
                        data: {
                            t: $rootScope.login.tk, model: $scope.Khaibaoteam
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        $scope.loadding = false;
                        closeswal();
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 0) {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: res.data.ms
                            });
                            return false;
                        }
                        $("#Modal_Edit_Khaibaoteam").modal("hide");
                        $scope.BindList_Khaibaoteam(true);
                        showtoastr('Đã cập nhật cấu hình thành công!.');
                    }, function (response) { // optional
                        $scope.loadding = false;
                    });
                    break;
                case "6":
                    if ($stateParams.form || $stateParams.team != null) {
                        $scope.Formsign.Ngaytao = moment($scope.Formsign.Ngaytao).format('YYYY-MM-DDTHH:mm:ssZZ');
                        $http({
                            method: 'POST',
                            url: "Request/Update_Formsign",
                            data: {
                                t: $rootScope.login.tk, Form_ID: $stateParams.form, Team_ID: $stateParams.team, model: $scope.Formsign
                            },
                            contentType: 'application/json; charset=utf-8'
                        }).then(function (res) {
                            $scope.loadding = false;
                            closeswal();
                            if (!$rootScope.checkToken(res)) return false;
                            if (res.data.error !== 0) {
                                Swal.fire({
                                    icon: 'error',
                                    type: 'error',
                                    title: '',
                                    text: res.data.ms
                                });
                                return false;
                            }
                            $("#Modal_Edit_Formsign").modal("hide");
                            $scope.BindList_Formsign(true);
                            showtoastr('Đã cập nhật cấu hình thành công!.');
                        }, function (response) { // optional
                            $scope.loadding = false;
                        });
                    }
                    else {
                        $scope.Khaibaoform.Ngaytao = moment($scope.Khaibaoform.Ngaytao).format('YYYY-MM-DDTHH:mm:ssZZ');

                        let formsign = [];
                        let formsignuser = [];
                        let formsignuserfollow = [];

                        if ($scope.List_Data_Formsign && $scope.List_Data_Formsign.length > 0) {
                            formsign = $scope.List_Data_Formsign;
                            angular.forEach($scope.List_Data_Formsign, function (fs) {
                                if (fs.ListUser) {
                                    fs.ListUser.forEach(function (fsus) {
                                        formsignuser.push(fsus);
                                    });  
                                }
                                //if (fs.ListUserFollow) {
                                //    fs.ListUserFollow.forEach(function (fsusf) {
                                //        formsignuserfollow.push(fsusf);
                                //    });
                                //}
                            });
                        }

                        $http({
                            method: 'POST',
                            url: "Request/Update_Khaibaoform",
                            data: {
                                t: $rootScope.login.tk, Congty_ID: $rootScope.login.u.congtyID, model: $scope.Khaibaoform, formsign: formsign, formsignuser: formsignuser/*, formsignuserfollow: formsignuserfollow*/
                            },
                            contentType: 'application/json; charset=utf-8'
                        }).then(function (res) {
                            $scope.loadding = false;
                            if (!$rootScope.checkToken(res)) return false;
                            if (res.data.error !== 0) {
                                Swal.fire({
                                    icon: 'error',
                                    type: 'error',
                                    title: '',
                                    text: res.data.ms
                                });
                                return false;
                            }
                            $("#Modal_Edit_Khaibaoform").modal("hide");
                            if (res.data.Form_ID && res.data.IsUseAll != true) {
                                if (!valid(frm)) {
                                    Swal.fire({
                                        type: 'error',
                                        icon: 'error',
                                        title: '',
                                        text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                                    });
                                    return false;
                                }
                                Urlaction = "/Request/Update_Formteam";
                                var formData = new FormData();
                                angular.forEach($scope.List_Data_Teambyform, function (r) {
                                    if (r.IsChangeQT == null) r.IsChangeQT = false;
                                    if (r.IsSkip == null) r.IsSkip = false;
                                    if (r.IsNoty == null) r.IsNoty = false;
                                    if (r.IsMail == null) r.IsMail = false;
                                });
                                var obj = $scope.List_Data_Teambyform;
                                formData.append("t", $rootScope.login.tk);
                                formData.append("Form_ID", res.data.Form_ID);
                                formData.append("model", JSON.stringify(obj));
                                $http.post(Urlaction, formData, {
                                    withCredentials: false,
                                    headers: {
                                        'Content-Type': undefined
                                    },
                                    transformRequest: angular.identity
                                }).then(function (res) {
                                    if (res.data.error == 1) {
                                        Swal.fire({
                                            icon: 'error',
                                            type: 'error',
                                            text: res.data.ms
                                        });
                                        return false;
                                    }
                                    $("#Modal_Edit_Tagteam").modal("hide");
                                    $scope.BindList_Setup_Formteam(true);
                                });
                            }
                            else {
                                $scope.BindList_Setup_Formteam(true);
                                showtoastr('Đã cập nhật cấu hình thành công!.');
                            }
                            
                        }, function (response) { // optional
                            $scope.loadding = false;
                            closeswal();
                        });
                    }
                    break;
            }

        };
        $scope.ClickEdit_Nhomform = function (m, f) {
            $scope.LisFileAttach = [];
            $scope.FilesAttachList = [];
            if (f)
                $scope.Viewdxtd = true;
            swal.showLoading();
            switch ($stateParams.type) {
                case "1":
                    $http({
                        method: "POST",
                        url: "Home/CallProc",
                        data: {
                            t: $rootScope.login.tk, proc: "Srequest_Get_Nhomform", pas: [
                                { "par": "NhomForm_ID", "va": m.NhomForm_ID },
                            ]
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error === 1) {
                            Swal.fire({
                                type: 'error',
                                icon: 'error',
                                title: '',
                                text: 'Có lỗi khi xem phiếu !'
                            });
                            return false;
                        }
                        var data = JSON.parse(res.data.data);
                        $scope.Nhomform = data[0][0];
                        $scope.dtitle = "Cập nhật nhóm đề xuất";
                        $("#Modal_Edit_Nhomform").modal("show");
                    }).then(function () {
                        closeswal();
                    });
                    break;
                case "2":
                    $http({
                        method: "POST",
                        url: "Home/CallProc",
                        data: {
                            t: $rootScope.login.tk, proc: "Srequest_Get_Mauform", pas: [
                                { "par": "MauForm_ID", "va": m.MauForm_ID },
                            ]
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error === 1) {
                            Swal.fire({
                                type: 'error',
                                icon: 'error',
                                title: '',
                                text: 'Có lỗi khi xem phiếu !'
                            });
                            return false;
                        }
                        var data = JSON.parse(res.data.data);
                        $scope.Mauform = data[0][0];
                        $scope.dtitle = "Cập nhật mẫu đề xuất";
                        $("#Modal_Edit_Mauform").modal("show");
                    }).then(function () {
                        closeswal();
                    });
                    break;
                case "3":
                    $http({
                        method: "POST",
                        url: "Home/CallProc",
                        data: {
                            t: $rootScope.login.tk, proc: "Srequest_Get_Khaibaoform", pas: [
                                { "par": "Form_ID", "va": m.Form_ID },
                                { "par": "NhanSU_ID", "va": $rootScope.login.u.NhanSu_ID },
                            ]
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error === 1) {
                            Swal.fire({
                                type: 'error',
                                icon: 'error',
                                title: '',
                                text: 'Có lỗi khi xem phiếu !'
                            });
                            return false;
                        }
                        var data = JSON.parse(res.data.data);
                        $scope.Khaibaoform = data[0][0];
                        $scope.dtitle = "Cập nhật mẫu đề xuất";
                        $("#Modal_Edit_Khaibaoform").modal("show");
                    }).then(function () {
                        closeswal();
                    });
                    break;
                case "4":
                    $http({
                        method: "POST",
                        url: "Home/CallProc",
                        data: {
                            t: $rootScope.login.tk, proc: "Srequest_Get_Nhomteam", pas: [
                                { "par": "NhomTeam_ID", "va": m.NhomTeam_ID },
                            ]
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error === 1) {
                            Swal.fire({
                                type: 'error',
                                icon: 'error',
                                title: '',
                                text: 'Có lỗi khi xem phiếu !'
                            });
                            return false;
                        }
                        var data = JSON.parse(res.data.data);
                        $scope.Nhomteam = data[0][0];
                        $scope.dTitle = "Cập nhật nhóm team";
                        $("#Modal_Edit_Nhomteam").modal("show");
                    }).then(function () {
                        closeswal();
                    });
                    break;
                case "5":
                    $http({
                        method: "POST",
                        url: "Home/CallProc",
                        data: {
                            t: $rootScope.login.tk, proc: "Srequest_Get_Khaibaoteam", pas: [
                                { "par": "Team_ID", "va": m.Team_ID },
                                { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                            ]
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error === 1) {
                            Swal.fire({
                                type: 'error',
                                icon: 'error',
                                title: '',
                                text: 'Có lỗi khi xem phiếu !'
                            });
                            return false;
                        }
                        var data = JSON.parse(res.data.data);
                        $scope.Khaibaoteam = data[0][0];
                        $scope.dtitle = "Cập nhật Team";
                        $("#Modal_Edit_Khaibaoteam").modal("show");
                    }).then(function () {
                        closeswal();
                    });
                    break;
                case "6":
                    if (($stateParams.form != null && $stateParams.form != "") || ($stateParams.team != null && $stateParams.team != "")) {
                        $http({
                            method: "POST",
                            url: "Home/CallProc",
                            data: {
                                t: $rootScope.login.tk, proc: "Srequest_Get_Formsign", pas: [
                                    { "par": "FormSign_ID", "va": m.FormSign_ID },
                                    { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                                ]
                            },
                            contentType: 'application/json; charset=utf-8'
                        }).then(function (res) {
                            if (!$rootScope.checkToken(res)) return false;
                            if (res.data.error === 1) {
                                Swal.fire({
                                    type: 'error',
                                    icon: 'error',
                                    title: '',
                                    text: 'Có lỗi khi xem phiếu !'
                                });
                                return false;
                            }
                            var data = JSON.parse(res.data.data);
                            $scope.Formsign = data[0][0];
                            $scope.dtitle = "Cập nhật nhóm duyệt";
                            $("#Modal_Edit_Formsign").modal("show");
                        }).then(function () {
                            closeswal();
                        });
                    }
                    else {
                        $scope.IsEditForm = true;
                        $scope.List_Data_Formsign = [];
                        $scope.List_Data_Teambyform = [];
                        $http({
                            method: "POST",
                            url: "Home/CallProc",
                            data: {
                                t: $rootScope.login.tk, proc: "Srequest_Get_Khaibaoform", pas: [
                                    { "par": "Form_ID", "va": m.Child_ID },
                                    { "par": "NhanSU_ID", "va": $rootScope.login.u.NhanSu_ID },
                                ]
                            },
                            contentType: 'application/json; charset=utf-8'
                        }).then(function (res) {
                            if (!$rootScope.checkToken(res)) return false;
                            if (res.data.error === 1) {
                                Swal.fire({
                                    type: 'error',
                                    icon: 'error',
                                    title: '',
                                    text: 'Có lỗi khi xem phiếu !'
                                });
                                return false;
                            }
                            var data = JSON.parse(res.data.data);
                            $scope.Khaibaoform = data[0][0];
                            angular.forEach(data[1], function (value, p) {
                                data[1][p].IsOpen = true;
                                if (data[1][p].ListUser) {
                                    data[1][p].ListUser = JSON.parse(data[1][p].ListUser);
                                    data[1][p].ListUser.forEach(function (valueus, us) {
                                        data[1][p].ListUser[us].IsSLA = parseFloat(data[1][p].ListUser[us].IsSLA);
                                        data[1][p].ListUser[us].IsType = parseInt(data[1][p].ListUser[us].IsType);
                                        data[1][p].ListUser[us].STT = parseInt(data[1][p].ListUser[us].STT);

                                        data[1][p].ListUser[us].IsDown = false;
                                        if (us !== -1 && us < data[1][p].ListUser.length - 1) {
                                            data[1][p].ListUser[us].IsDown = true;
                                        }
                                        data[1][p].ListUser[us].IsUp = false;
                                        if (us > 0) {
                                            data[1][p].ListUser[us].IsUp = true;
                                        }
                                    });
                                }
                                //if (data[1][p].ListUserFollow) {
                                //    data[1][p].ListUserFollow = JSON.parse(data[1][p].ListUserFollow);
                                //}

                                data[1][p].IsDown = false;
                                if (p !== -1 && p < data[1].length - 1) {
                                    data[1][p].IsDown = true;
                                }
                                data[1][p].IsUp = false;
                                if (p > 0) {
                                    data[1][p].IsUp = true;
                                }
                            });
                            $scope.List_Data_Formsign = data[1];
                            $scope.List_Data_Teambyform = data[2];
                            $scope.dtitle = "Cập nhật loại đề xuất";
                            $("#Modal_Edit_Khaibaoform").modal("show");
                        }).then(function () {
                            closeswal();
                        });
                    }
                    break;
            }

        };
        $scope.ClickDel_Multi = function (rowto, type) {
            Swal.fire({
                title: 'Xác nhận?',
                text: type == 0 ? "Bạn có muốn xóa nhóm này không?" : "Bạn có muốn xóa các nhóm đề xuất đã chọn không?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#2196f3',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Có!',
                cancelButtonText: 'Không!',
            }).then((result) => {
                if (result.value) {
                    if ($scope.loadding) {
                        return false;
                    }
                    $scope.loadding = true;
                    switch ($stateParams.type) {
                        case "1":
                            var arr = [];
                            if (type == 0) {
                                arr.push(rowto.NhomForm_ID);
                            }
                            else {
                                angular.forEach(rowto, function (value, key) {

                                    if (rowto[key].checked) {
                                        arr.push(rowto[key].NhomForm_ID);
                                    }
                                });
                            }
                            $http({
                                method: "POST",
                                url: "Request/Del_Multi_Nhomform",
                                data: { t: $rootScope.login.tk, ids: arr },
                                contentType: 'application/json; charset=utf-8'
                            }).then(function (res) {
                                $scope.loadding = false;
                                if (!$rootScope.checkToken(res)) return false;
                                if (res.data.error !== 1) {
                                    $scope.BindList_Nhomform(true);
                                    showtoastr("Xóa thành công.");
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        type: 'error',
                                        title: '',
                                        text: res.data.ms
                                    });
                                }
                            });
                            break;
                        case "2":
                            var arr = [];
                            if (type == 0) {
                                arr.push(rowto.MauForm_ID);
                            }
                            else {
                                angular.forEach(rowto, function (value, key) {

                                    if (rowto[key].checked) {
                                        arr.push(rowto[key].MauForm_ID);
                                    }
                                });
                            }
                            $http({
                                method: "POST",
                                url: "Request/Del_Multi_Mauform",
                                data: { t: $rootScope.login.tk, ids: arr },
                                contentType: 'application/json; charset=utf-8'
                            }).then(function (res) {
                                $scope.loadding = false;
                                if (!$rootScope.checkToken(res)) return false;
                                if (res.data.error !== 1) {
                                    $scope.BindList_Mauform(true);
                                    showtoastr("Xóa thành công.");
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        type: 'error',
                                        title: '',
                                        text: res.data.ms
                                    });
                                }
                            });
                            break;
                        case "3":
                            var arr = [];
                            if (type == 0) {
                                arr.push(rowto.Form_ID);
                            }
                            else {
                                angular.forEach(rowto, function (value, key) {
                                    angular.forEach(rowto[key].list, function (values, keys) {
                                        if (rowto[key].list[keys].checked) {
                                            arr.push(rowto[key].list[keys].Form_ID);
                                        }
                                    });
                                });
                            }
                            $http({
                                method: "POST",
                                url: "Request/Del_Multi_Khaibaoform",
                                data: { t: $rootScope.login.tk, ids: arr },
                                contentType: 'application/json; charset=utf-8'
                            }).then(function (res) {
                                $scope.loadding = false;
                                if (!$rootScope.checkToken(res)) return false;
                                if (res.data.error !== 1) {
                                    $scope.BindList_Khaibaoform(true);
                                    showtoastr("Xóa thành công.");
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        type: 'error',
                                        title: '',
                                        text: res.data.ms
                                    });
                                }
                            });
                            break;
                        case "4":
                            var arr = [];
                            if (type == 0) {
                                arr.push(rowto.NhomTeam_ID);
                            }
                            else {
                                angular.forEach(rowto, function (value, key) {

                                    if (rowto[key].checked) {
                                        arr.push(rowto[key].NhomTeam_ID);
                                    }
                                });
                            }
                            $http({
                                method: "POST",
                                url: "Request/Del_Multi_Nhomteam",
                                data: { t: $rootScope.login.tk, ids: arr },
                                contentType: 'application/json; charset=utf-8'
                            }).then(function (res) {
                                $scope.loadding = false;
                                if (!$rootScope.checkToken(res)) return false;
                                if (res.data.error !== 1) {
                                    $scope.BindList_Nhomteam(true);
                                    showtoastr("Xóa thành công.");
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        type: 'error',
                                        title: '',
                                        text: res.data.ms
                                    });
                                }
                            });
                            break;
                        case "5":
                            var arr = [];
                            if (type == 0) {
                                arr.push(rowto.Team_ID);
                            }
                            else {
                                angular.forEach(rowto, function (value, key) {

                                    if (rowto[key].checked) {
                                        arr.push(rowto[key].Team_ID);
                                    }
                                });
                            }
                            $http({
                                method: "POST",
                                url: "Request/Del_Multi_Khaibaoteam",
                                data: { t: $rootScope.login.tk, ids: arr },
                                contentType: 'application/json; charset=utf-8'
                            }).then(function (res) {
                                $scope.loadding = false;
                                if (!$rootScope.checkToken(res)) return false;
                                if (res.data.error !== 1) {
                                    $scope.List_Data_Userteam = [];
                                    $scope.GetTuDien();
                                    $scope.BindList_Khaibaoteam(true);
                                    showtoastr("Xóa thành công.");
                                } else {
                                    Swal.fire({
                                        icon: 'error',
                                        type: 'error',
                                        title: '',
                                        text: res.data.ms
                                    });
                                }
                            });
                            break;
                        case "6":
                            if ($stateParams.form != null || $stateParams.team != null) {
                                var arr = [];
                                if (type == 0) {
                                    arr.push(rowto.FormSign_ID);
                                }
                                else {
                                    angular.forEach(rowto, function (value, key) {

                                        if (rowto[key].checked) {
                                            arr.push(rowto[key].FormSign_ID);
                                        }
                                    });
                                }
                                $http({
                                    method: "POST",
                                    url: "Request/Del_Multi_Formsign",
                                    data: { t: $rootScope.login.tk, ids: arr },
                                    contentType: 'application/json; charset=utf-8'
                                }).then(function (res) {
                                    $scope.loadding = false;
                                    if (!$rootScope.checkToken(res)) return false;
                                    if (res.data.error !== 1) {
                                        $scope.List_Data_Userteam = [];
                                        $scope.GetTuDien();
                                        $scope.BindList_Formsign(true);
                                        showtoastr("Xóa thành công.");
                                    } else {
                                        Swal.fire({
                                            icon: 'error',
                                            type: 'error',
                                            title: '',
                                            text: res.data.ms
                                        });
                                    }
                                });
                            }
                            else {
                                var arr = [];
                                if (type == 0) {
                                    arr.push(rowto.Child_ID);
                                }
                                else {
                                    angular.forEach(rowto, function (value, key) {
                                        if (rowto[key].checked) {
                                            arr.push(rowto[key].Child_ID);
                                        }
                                    });
                                    //angular.forEach(rowto, function (value, key) {
                                    //    angular.forEach(rowto[key].list, function (values, keys) {
                                    //        if (rowto[key].list[keys].checked) {
                                    //            arr.push(rowto[key].list[keys].Form_ID);
                                    //        }
                                    //    });
                                    //});
                                }
                                $http({
                                    method: "POST",
                                    url: "Request/Del_Multi_Khaibaoform",
                                    data: { t: $rootScope.login.tk, ids: arr },
                                    contentType: 'application/json; charset=utf-8'
                                }).then(function (res) {
                                    $scope.loadding = false;
                                    if (!$rootScope.checkToken(res)) return false;
                                    if (res.data.error !== 1) {
                                        $scope.BindList_Setup_Formteam(true);
                                        showtoastr("Xóa thành công.");
                                    } else {
                                        Swal.fire({
                                            icon: 'error',
                                            type: 'error',
                                            title: '',
                                            text: res.data.ms
                                        });
                                    }
                                });
                            }
                            break;
                    }
                }
            })
        }
        $scope.OpenFile = function (linkfile) {
            var url = $rootScope.fileUrl + linkfile;
            window.open(url);
        };
        $scope.OpenModal_Add_FlashForm = function () {
            angular.forEach($scope.Srequest_Tudien[1], function (r) {
                r.checked = false;
            });
            $scope.ititle = "Khai báo nhanh loại đề xuất";
            $("#Modal_Add_FlashForm").modal("show");
        };

        $scope.Click_Add_NhomduyetForm = function () {
            $scope.List_Data_Formsign.push({ IsTypeDuyet: $scope.Khaibaoform.IsQuytrinhduyet, IsActive: true, ListUser: [], STT: $scope.List_Data_Formsign.length + 1 });
            $scope.RintimeQT();
        };
        $scope.Remove_FormSign = function (arr, rowto, idx) {
            arr.splice(idx, 1);
        }
        $scope.Remove_FormSignUser = function (d, status) {
            if (status) {
                d.ListUser = [];
            }
            else {
                d.ListUserFollow = [];
            }
        };
        //==============================================

        //=============== Function không dùng chung ==============
        $scope.ClickAdd_GroupParent = function (p) {
            $scope.dTitle = "Thêm mới nhóm team";
            $scope.Nhomteam = { Congty_ID: $rootScope.login.u.congtyID, STT: $scope.List_Data_Nhomteam.length + 1, Nhomcha_ID: p ? p.NhomTeam_ID : null, Trangthai: true, };
            $("#Modal_Edit_Nhomteam").modal("show");
        };

        $scope.OpenModal_ImportTeam = function () {
            $scope.ititle = "Modal Import phòng ban";
            $("#Modal_Import_Team").modal("show");
        };

        $scope.toggleAll_PB = function (rowto, CheckAllG_PB) {
            angular.forEach(rowto, function (value, key) {
                rowto[key].checked = CheckAllG_PB;
            });
            $scope.CheckCheked_PB(rowto);
        };
        $scope.CheckCheked_PB = function (m) {
            var filtereds = $filter('filter')($scope.Srequest_Tudien[3], { checked: true }, true);
            //$scope.checkLen_PB = filtereds.length;

        };
        $scope.Click_Import_team = function () {
            swal.showLoading();
            if ($scope.loadding) {
                return false;
            }
            $scope.loadding = true;
            let Phongbans = '';
            if ($scope.Srequest_Tudien[3] != null && $scope.Srequest_Tudien[3].length > 0) {
                angular.forEach($scope.Srequest_Tudien[3].filter(x => x.checked), function (r) {
                    Phongbans += r.Phongban_ID + ',';
                });
            }
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_TeamCopyfromPB", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "Phongbans", "va": Phongbans },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error === 1) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: "Có lỗi khi import dữ liệu team",
                    });
                    return false;
                }
                $scope.loadding = false;
                $scope.BindList_Khaibaoteam(true);
                showtoastr('Import thành công!');
                $("#Modal_Import_Team").modal("hide");
            }).then(function () {
                closeswal();
            });
        };
        $scope.OpenChild = function (d) {
            d.IsOpen = !(d.IsOpen || false); //parent open
            $scope.List_Data_Setup_Formteam.filter(x => x.Parent_ID === d.Child_ID).forEach(function (r) {
                r.IsOpen = !(r.IsOpen || false); //child open
            });

            let arrCache = [];
            for (let i = 0; i < $scope.List_Data_Setup_Formteam.length; i++) {
                let obj = {
                    "Child_ID": $scope.List_Data_Setup_Formteam[i].Child_ID,
                    "Parent_ID": $scope.List_Data_Setup_Formteam[i].Parent_ID,
                    "IsOpen": $scope.List_Data_Setup_Formteam[i].IsOpen,
                };
                arrCache.push(obj);
            }
            $rootScope.Cache_Group_FormSign = arrCache;
        };
        $scope.OpenChild_Group = function (u) {
            u.IsOpen = !(u.IsOpen || false); //parent open
            angular.forEach($scope.List_Data_Khaibaoform.filter(x => x.MauForm_ID === u.MauForm_ID), function (p) {
                angular.forEach(p.list, function (c) {
                    c.IsOpen = !(c.IsOpen || false); //child open
                });
            });
        };
        $scope.ChangeFilter = function (search) {
           $scope.filtersearch = change_alias_filter(search);
        };
        //======================================================

        //==================== Setup Form mẫu D ==============================
        $scope.OpenModal_SetupMauFormD = function (m) {
            switch ($stateParams.type) {
                case '2':
                    $scope.getMauform = m;
                    $scope.List_Data_SetupFormD = [];
                    swal.showLoading();
                    $http({
                        method: "POST",
                        url: "Home/CallProc",
                        data: {
                            t: $rootScope.login.tk, proc: "Srequest_Get_FormD", pas: [
                                { "par": "MauForm_ID", "va": m.MauForm_ID },
                            ]
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            var data = JSON.parse(res.data.data);
                            let check_count = 0;
                            angular.forEach(data[0], function (r) {
                                if (r.IsLength === true) {
                                    check_count++;
                                }

                                if ($scope.List_TypeColumn.find(x => x.value === r.KieuTruong).is_length === true) {
                                    r.Islength_row = true;
                                }
                                else {
                                    r.Islength_row = false;
                                }
                            });
                            if (check_count > 0) {
                                $scope.Is_length = true;
                            }
                            else {
                                $scope.Is_length = false;
                            }
                            var Temp = [];
                            addToArray_MFD(Temp, data[0], null, 0, 'thutu');
                            angular.forEach(Temp, function (value, key) {
                                let arr_by_lv = [];
                                angular.forEach(Temp.filter(x => (x.lv == Temp[key].lv && Temp[key].IsParent_ID == null) || (x.lv == Temp[key].lv && Temp[key].IsParent_ID != null && (x.index_ID === Temp[key].IsParent_ID || x.IsParent_ID === Temp[key].IsParent_ID))), function (value, key) {
                                    arr_by_lv.push(value);
                                });
                                let idx = -1;
                                angular.forEach(arr_by_lv, function (value, key_child) {
                                    if ((arr_by_lv[key_child].MauFormD_ID != null && arr_by_lv[key_child].MauFormD_ID === Temp[key].MauFormD_ID) || (arr_by_lv[key_child].index_ID != null && arr_by_lv[key_child].index_ID === Temp[key].index_ID) || ((arr_by_lv[key_child].STT != null) && arr_by_lv[key_child].STT === Temp[key].STT)) {
                                        idx = key_child;
                                    }
                                });
                                Temp[key].IsDown = false;
                                if (idx !== -1 && idx < arr_by_lv.length - 1) {
                                    Temp[key].IsDown = true;
                                }
                                Temp[key].IsUp = false;
                                if (idx > 0) {
                                    Temp[key].IsUp = true;
                                }
                            });
                            $scope.List_Data_SetupFormD = Temp;
                            $scope.DataChangeParent = angular.copy(Temp);
                            $scope.ttitle = "Thiết lập form " + m.Form_Name;
                            $("#Modal_SetupFormD").modal("show");
                        }
                    }).then(function () {
                        closeswal();
                    });
                    break;
                case '6':
                    $scope.getform = m;
                    $scope.List_Data_SetupFormKyD = [];
                    swal.showLoading();
                    $http({
                        method: "POST",
                        url: "Home/CallProc",
                        data: {
                            t: $rootScope.login.tk, proc: "Srequest_Get_FormKyD", pas: [
                                { "par": "Form_ID", "va": m.Child_ID },
                            ]
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error !== 1) {
                            var data = JSON.parse(res.data.data);
                            let check_count = 0;
                            angular.forEach(data[0], function (r) {
                                if (r.IsLength != null) {
                                    check_count++;
                                }
                                var o = $scope.List_TypeColumn.find(x => x.value === r.KieuTruong);
                                if (o && o.is_length === true) {
                                    r.Islength_row = true;
                                }
                                else {
                                    r.Islength_row = false;
                                }
                            });
                            if (check_count > 0) {
                                $scope.Is_length = true;
                            }
                            else {
                                $scope.Is_length = false;
                            }
                            var Temp = [];
                            addToArray_FD(Temp, data[0], null, 0, 'thutu');
                            angular.forEach(Temp, function (value, key) {
                                let arr_by_lv = [];
                                angular.forEach(Temp.filter(x => (x.lv == Temp[key].lv && Temp[key].IsParent_ID == null) || (x.lv == Temp[key].lv && Temp[key].IsParent_ID != null && (x.index_ID === Temp[key].IsParent_ID || x.IsParent_ID === Temp[key].IsParent_ID))), function (value, key) {
                                    arr_by_lv.push(value);
                                });
                                let idx = -1;
                                angular.forEach(arr_by_lv, function (value, key_child) {
                                    if ((arr_by_lv[key_child].FormD_ID != null && arr_by_lv[key_child].FormD_ID === Temp[key].FormD_ID) || (arr_by_lv[key_child].index_ID != null && arr_by_lv[key_child].index_ID === Temp[key].index_ID) || ((arr_by_lv[key_child].STT != null) && arr_by_lv[key_child].STT === Temp[key].STT)) {
                                        idx = key_child;
                                    }
                                });
                                Temp[key].IsDown = false;
                                if (idx !== -1 && idx < arr_by_lv.length - 1) {
                                    Temp[key].IsDown = true;
                                }
                                Temp[key].IsUp = false;
                                if (idx > 0) {
                                    Temp[key].IsUp = true;
                                }
                            });
                            $scope.List_Data_SetupFormKyD = Temp;
                            $scope.DataChangeParent = angular.copy(Temp);
                            $scope.ttitle = "Thiết lập " + m.Name;
                            $("#Modal_SetupFormKyD").modal("show");
                        }
                    }).then(function () {
                        closeswal();
                    });
                    break;
            }
        };
        $scope.Add_Column_MauFormD = function () {
            switch ($stateParams.type) {
                case '2':
                    $scope.List_Data_SetupFormD.push({ lv: 1, IsEdit: true, MauFormD_ID: null, MauForm_ID: $scope.getMauform.MauForm_ID, IsRequired: false, IsLabel: 0, IsParent_ID: null, IsType: 0, index_ID: $scope.List_Data_SetupFormD.length + 1, STT: $scope.List_Data_SetupFormD.length + 1 });
                    break;
                case '6':
                    $scope.List_Data_SetupFormKyD.push({ lv: 1, IsEdit: true, FormD_ID: null, Form_ID: $scope.getform.Form_ID, IsRequired: false, IsLabel: 0, IsParent_ID: null, IsType: 0, index_ID: $scope.List_Data_SetupFormKyD.length + 1, STT: $scope.List_Data_SetupFormKyD.length + 1 });
                    break;
            }
        };
        $scope.ChangeLength = function (arr, m, idx) {
            var o = $scope.List_TypeColumn.find(x => x.value === m.KieuTruong);
            if (o && o.is_length === true) {
                m.Islength_row = true;
            }
            else {
                m.Islength_row = false;
            }
            let check_count = 0;
            angular.forEach(arr, function (r) {
                if (r.Islength_row === true) {
                    check_count++;
                }
            });
            if (check_count > 0) {
                $scope.Is_length = true;
            }
            else {
                $scope.Is_length = false;
            }
        };
        $scope.AddChild_MauFormD = function (arr, m, idx) {
            if (m.IDCon == null) { m.IDCon = ""; }
            
            switch ($stateParams.type) {
                case '2':
                    if ($rootScope.roles_Root) {
                        let obj = { lv: m.lv + 1, IsEdit: true, MauFormD_ID: null, MauForm_ID: $scope.getMauform.MauForm_ID, IsRequired: false, IsLabel: 0, IsParent_ID: m.MauFormD_ID != null ? m.MauFormD_ID : m.index_ID != null ? m.index_ID : m.STT, IsType: 0, index_ID: $scope.List_Data_SetupFormD.length + 1, STT: $scope.List_Data_SetupFormD.length + 1 };
                        let count_child = 1;
                        angular.forEach(arr, function (r) {
                            if (r.IsParent_ID === m.MauFormD_ID || r.IsParent_ID === m.index_ID) {
                                count_child++;
                            }
                        });
                        arr.splice(idx + count_child, 0, obj);
                        angular.forEach(arr, function (value, key) {
                            let arr_by_lv = [];
                            angular.forEach(arr.filter(x => (x.lv == arr[key].lv && arr[key].IsParent_ID == null) || (x.lv == arr[key].lv && arr[key].IsParent_ID != null && (x.index_ID === arr[key].IsParent_ID || x.IsParent_ID === arr[key].IsParent_ID))), function (value, key) {
                                arr_by_lv.push(value);
                            });
                            let idx = -1;
                            angular.forEach(arr_by_lv, function (value, key_child) {
                                if ((arr_by_lv[key_child].MauFormD_ID != null && arr_by_lv[key_child].MauFormD_ID === arr[key].MauFormD_ID) || (arr_by_lv[key_child].index_ID != null && arr_by_lv[key_child].index_ID === arr[key].index_ID) || ((arr_by_lv[key_child].STT != null) && arr_by_lv[key_child].STT === arr[key].STT)) {
                                    idx = key_child;
                                }
                            });
                            arr[key].IsDown = false;
                            if (idx !== -1 && idx < arr_by_lv.length - 1) {
                                arr[key].IsDown = true;
                            }
                            arr[key].IsUp = false;
                            if (idx > 0) {
                                arr[key].IsUp = true;
                            }
                        });

                        // Add child_ID dont save
                        let MauFormD_ID_New = $scope.List_Data_SetupFormD.length;
                        let Parent_ID_new = null;
                        if (!Parent_ID_new) {
                            Parent_ID_new = m.MauFormD_ID != null ? m.MauFormD_ID : m.index_ID != null ? m.index_ID : m.STT;
                        }
                        let lv_parent = m.lv;
                        let list_parent = [];
                        do {
                            list_parent = arr.find(x => ((x.MauFormD_ID != null && x.MauFormD_ID === Parent_ID_new) || x.STT == Parent_ID_new) && x.lv == lv_parent);
                            if (list_parent != null) {
                                list_parent.IDCon += MauFormD_ID_New + ',';
                                Parent_ID_new = list_parent.IsParent_ID;
                            }
                            lv_parent--;
                        } while (list_parent != null);
                    }
                    break;
                case '6':
                    if ($rootScope.roles_Root || $rootScope.roles_Admin) {
                        let obj = { lv: m.lv + 1, IsEdit: true, FormD_ID: null, FormD_ID: $scope.getform.FormD_ID, IsRequired: false, IsLabel: 0, IsParent_ID: m.FormD_ID != null ? m.FormD_ID : m.index_ID != null ? m.index_ID : m.STT, IsType: 0, index_ID: $scope.List_Data_SetupFormKyD.length + 1, STT: $scope.List_Data_SetupFormKyD.length + 1 };
                        let count_child = 1;
                        angular.forEach(arr, function (r) {
                            if (r.IsParent_ID === m.FormD_ID || r.IsParent_ID === m.index_ID) {
                                count_child++;
                            }
                        });
                        arr.splice(idx + count_child, 0, obj);
                        angular.forEach(arr, function (value, key) {
                            let arr_by_lv = [];
                            angular.forEach(arr.filter(x => (x.lv == arr[key].lv && arr[key].IsParent_ID == null) || (x.lv == arr[key].lv && arr[key].IsParent_ID != null && (x.index_ID === arr[key].IsParent_ID || x.IsParent_ID === arr[key].IsParent_ID))), function (value, key) {
                                arr_by_lv.push(value);
                            });
                            let idx = -1;
                            angular.forEach(arr_by_lv, function (value, key_child) {
                                if ((arr_by_lv[key_child].FormD_ID != null && arr_by_lv[key_child].FormD_ID === arr[key].FormD_ID) || (arr_by_lv[key_child].index_ID != null && arr_by_lv[key_child].index_ID === arr[key].index_ID) || ((arr_by_lv[key_child].STT != null) && arr_by_lv[key_child].STT === arr[key].STT)) {
                                    idx = key_child;
                                }
                            });
                            arr[key].IsDown = false;
                            if (idx !== -1 && idx < arr_by_lv.length - 1) {
                                arr[key].IsDown = true;
                            }
                            arr[key].IsUp = false;
                            if (idx > 0) {
                                arr[key].IsUp = true;
                            }
                        });

                        // Add child_ID dont save
                        let FormD_ID_New = $scope.List_Data_SetupFormKyD.length;
                        let Parent_ID_new = null;
                        if (!Parent_ID_new) {
                            Parent_ID_new = m.FormD_ID != null ? m.FormD_ID : m.index_ID != null ? m.index_ID : m.STT;
                        }
                        let lv_parent = m.lv;
                        let list_parent = [];
                        do {
                            list_parent = arr.find(x => ((x.FormD_ID != null && x.FormD_ID === Parent_ID_new) || x.STT == Parent_ID_new) && x.lv == lv_parent);
                            if (list_parent != null) {
                                list_parent.IDCon += FormD_ID_New + ',';            
                                Parent_ID_new = list_parent.IsParent_ID;
                            }
                            lv_parent--;
                        } while (list_parent != null);
                    }
                    break;
            }
        };
        $scope.Edit_MauFormD = function (m) {
            switch ($stateParams.type) {
                case '2':
                    if ($rootScope.roles_Root) {
                        m.IsEdit = !m.IsEdit;
                    }
                    break;
                case '6':
                    if ($rootScope.roles_Root || $rootScope.roles_Admin) {
                        m.IsEdit = !m.IsEdit;
                    }
                    break;
            }
        };
        $scope.Remove_MauFormD = function (arr, d, idx) {
            switch ($stateParams.type) {
                case '2':
                    if ($rootScope.roles_Root) {
                        if (!d.MauFormD_ID) {
                            let child_id = [];
                            if (d.IDCon) {
                                child_id = d.IDCon.split(',');
                            }
                            if (child_id.length > 0) {
                                angular.forEach(child_id, function (id) {
                                    angular.forEach(arr, function (value, key) {
                                        if (arr[key].STT === parseInt(id)) {
                                            arr.splice(key, 1);
                                        }
                                    });
                                });
                            }
                            arr.splice(idx, 1);
                        }
                        else {
                            Swal.fire({
                                title: 'Xác nhận?',
                                text: "Bạn có muốn xóa trường này và các dữ liệu liên quan không!",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#2196f3',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Có!',
                                cancelButtonText: 'Không!',
                            }).then((result) => {
                                if (result.value) {
                                    let child_id = [];
                                    angular.forEach(arr, function (value, key) {
                                        if (key === idx) {
                                            child_id = arr[key].IDCon.split(',');
                                        }
                                    });
                                    if (child_id.length > 0) {
                                        angular.forEach(child_id, function (id) {
                                            angular.forEach(arr, function (value, key) {
                                                if (arr[key].MauFormD_ID === id) {
                                                    arr.splice(key, 1);
                                                }
                                            });
                                        });
                                    }
                                    arr.splice(idx, 1);
                                    $scope.Save_MauFormD();
                                }
                            });
                        }
                    }
                    break;
                case '6':
                    if ($rootScope.roles_Root || $rootScope.roles_Admin) {
                        if (!d.FormD_ID) {
                            let child_id = [];
                            if (d.IDCon) {
                                child_id = d.IDCon.split(',');
                            }
                            if (child_id.length > 0) {
                                angular.forEach(child_id, function (id) {
                                    angular.forEach(arr, function (value, key) {
                                        if (arr[key].STT === parseInt(id)) {
                                            arr.splice(key, 1);
                                        }
                                    });
                                });
                            }
                            arr.splice(idx, 1);
                        }
                        else {
                            Swal.fire({
                                title: 'Xác nhận?',
                                text: "Bạn có muốn xóa trường này và các dữ liệu liên quan không!",
                                icon: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#2196f3',
                                cancelButtonColor: '#d33',
                                confirmButtonText: 'Có!',
                                cancelButtonText: 'Không!',
                            }).then((result) => {
                                if (result.value) {
                                    let child_id = [];
                                    angular.forEach(arr, function (value, key) {
                                        if (key === idx) {
                                            child_id = arr[key].IDCon.split(',');
                                        }
                                    });
                                    if (child_id.length > 0) {
                                        angular.forEach(child_id, function (id) {
                                            angular.forEach(arr, function (value, key) {
                                                if (arr[key].FormD_ID === id || arr[key].IsParent_ID === d.FormD_ID) {
                                                    arr.splice(key, 1);
                                                }
                                            });
                                        });
                                    }
                                    arr.splice(idx, 1);
                                    $scope.Save_MauFormD();
                                }
                            });
                        }
                    }
                    break;
            }
            
            
        };
        $scope.MoveDown = function (arr, m, idx_cr) {
            switch ($stateParams.type) {
                case '2':
                    if ($rootScope.roles_Root) {
                        let arr_by_lv = [];
                        angular.forEach(arr.filter(x => (x.lv == m.lv && m.IsParent_ID == null) || (x.lv == m.lv && m.IsParent_ID != null && (x.index_ID === m.IsParent_ID || x.IsParent_ID === m.IsParent_ID))), function (value, key) {
                            arr_by_lv.push(value);
                        });
                        let idx = -1;
                        angular.forEach(arr_by_lv, function (value, key) {
                            if ((arr_by_lv[key].MauFormD_ID != null && arr_by_lv[key].MauFormD_ID === m.MauFormD_ID) || (arr_by_lv[key].index_ID != null && arr_by_lv[key].index_ID === m.index_ID) || (arr_by_lv[key].MauFormD_ID == null && arr_by_lv[key].index_ID == null && arr_by_lv[key].STT === m.STT)) {
                                idx = key;
                            }
                        });
                        if (idx !== -1 && idx < arr_by_lv.length - 1) {
                            m.Count_Child = m.Count_Child != null ? m.Count_Child : 0;
                            let count_down = m.Count_Child + 1;
                            let el = arr[idx_cr];
                            arr[idx_cr] = arr[idx_cr + count_down];
                            arr[idx_cr + count_down] = el;

                            $scope.Save_MauFormD();
                        }                    }
                    break;
                case '6':
                    if ($rootScope.roles_Root || $rootScope.roles_Admin) {
                        let arr_by_lv = [];
                        angular.forEach(arr.filter(x => (x.lv == m.lv && m.IsParent_ID == null) || (x.lv == m.lv && m.IsParent_ID != null && (x.index_ID === m.IsParent_ID || x.IsParent_ID === m.IsParent_ID))), function (value, key) {
                            arr_by_lv.push(value);
                        });
                        let idx = -1;
                        angular.forEach(arr_by_lv, function (value, key) {
                            if ((arr_by_lv[key].FormD_ID != null && arr_by_lv[key].FormD_ID === m.FormD_ID) || (arr_by_lv[key].index_ID != null && arr_by_lv[key].index_ID === m.index_ID) || (arr_by_lv[key].FormD_ID == null && arr_by_lv[key].index_ID == null && arr_by_lv[key].STT === m.STT)) {
                                idx = key;
                            }
                        });
                        if (idx !== -1 && idx < arr_by_lv.length - 1) {
                            m.Count_Child = m.Count_Child != null ? m.Count_Child : 0;
                            let count_down = m.Count_Child + 1;
                            let el = arr[idx_cr];
                            arr[idx_cr] = arr[idx_cr + count_down];
                            arr[idx_cr + count_down] = el;

                            $scope.Save_MauFormD();
                        }
                    }
                    break;
            }
            
        };
        $scope.MoveUp = function (arr, m, idx_cr) {
            switch ($stateParams.type) {
                case '2':
                    if ($rootScope.roles_Root) {
                        let arr_by_lv = [];
                        angular.forEach(arr.filter(x => (x.lv == m.lv && m.IsParent_ID == null) || (x.lv == m.lv && m.IsParent_ID != null && (x.index_ID === m.IsParent_ID || x.IsParent_ID === m.IsParent_ID))), function (value, key) {
                            arr_by_lv.push(value);
                        });
                        let idx = -1;
                        angular.forEach(arr_by_lv, function (value, key) {
                            if (arr_by_lv[key].MauFormD_ID === m.MauFormD_ID || (arr_by_lv[key].index_ID != null && arr_by_lv[key].index_ID === m.index_ID) || (arr_by_lv[key].MauFormD_ID == null && arr_by_lv[key].index_ID == null && arr_by_lv[key].STT === m.STT)) {
                                idx = key;
                            }
                        });

                        if (idx > 0) {
                            arr_by_lv[idx - 1].Count_Child = arr_by_lv[idx - 1].Count_Child != null ? arr_by_lv[idx - 1].Count_Child : 0;
                            let count_up = arr_by_lv[idx - 1].Count_Child + 1;
                            let el = arr[idx_cr];
                            arr[idx_cr] = arr[idx_cr - count_up];
                            arr[idx_cr - count_up] = el;

                            $scope.Save_MauFormD();
                        }
                    }
                    break;
                case '6':
                    if ($rootScope.roles_Root || $rootScope.roles_Admin) {
                        let arr_by_lv = [];
                        angular.forEach(arr.filter(x => (x.lv == m.lv && m.IsParent_ID == null) || (x.lv == m.lv && m.IsParent_ID != null && (x.index_ID === m.IsParent_ID || x.IsParent_ID === m.IsParent_ID))), function (value, key) {
                            arr_by_lv.push(value);
                        });
                        let idx = -1;
                        angular.forEach(arr_by_lv, function (value, key) {
                            if (arr_by_lv[key].FormD_ID === m.FormD_ID || (arr_by_lv[key].index_ID != null && arr_by_lv[key].index_ID === m.index_ID) || (arr_by_lv[key].FormD_ID == null && arr_by_lv[key].index_ID == null && arr_by_lv[key].STT === m.STT)) {
                                idx = key;
                            }
                        });

                        if (idx > 0) {
                            arr_by_lv[idx - 1].Count_Child = arr_by_lv[idx - 1].Count_Child != null ? arr_by_lv[idx - 1].Count_Child : 0;
                            let count_up = arr_by_lv[idx - 1].Count_Child + 1;
                            let el = arr[idx_cr];
                            arr[idx_cr] = arr[idx_cr - count_up];
                            arr[idx_cr - count_up] = el;

                            $scope.Save_MauFormD();
                        }
                    }
                    break;
            }
        };
        $scope.Toogle_MauFormD = function (m) {
            if (m.IsEdit) {
                m.IsRequired = !m.IsRequired;
            }
        };
        $scope.ChangeParent = function (arr, m, idx) {
            switch ($stateParams.type) {
                case '2':
                    let checkd_mf = false;
                    if (m.IsParent_ID === m.MauFormD_ID) {
                        checkd_mf = true;
                    }
                    angular.forEach(arr.filter(x => x.MauFormD_ID === m.IsParent_ID), function (r) {
                        if (m.MauFormD_ID != null && m.MauFormD_ID === r.IsParent_ID) {
                            checkd_mf = true;
                        }
                    });
                    if (checkd_mf) {
                        Swal.fire({
                            type: 'error',
                            icon: 'error',
                            title: '',
                            text: 'Vui lòng chọn trường cha khác !'
                        });
                        m.IsParent_ID = null;
                        return false;
                    }
                    break;
                case '6':
                    let checkd_f = false;
                    if (m.IsParent_ID === m.FormD_ID) {
                        checkd_f = true;
                    }
                    angular.forEach(arr.filter(x => x.FormD_ID === m.IsParent_ID), function (r) {
                        if (m.FormD_ID != null && m.FormD_ID === r.IsParent_ID) {
                            checkd_f = true;
                        }
                    });
                    if (checkd_f) {
                        Swal.fire({
                            type: 'error',
                            icon: 'error',
                            title: '',
                            text: 'Vui lòng chọn trường cha khác !'
                        });
                        m.IsParent_ID = null;
                        return false;
                    }
                    break;
            }
            
            $scope.Save_MauFormD();
        };
        $scope.Click_Update_MauFormD = function (frm) {
            swal.showLoading();
            switch ($stateParams.type) {
                case '2':
                    //if (!valid(frm)) {
                    //    Swal.fire({
                    //        type: 'error',
                    //        icon: 'error',
                    //        title: '',
                    //        text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                    //    });
                    //    return false;
                    //}
                    Urlaction = "/Request/Update_MauFormD";
                    var formData = new FormData();
                    var obj = $scope.List_Data_SetupFormD;
                    formData.append("t", $rootScope.login.tk);
                    formData.append("MauForm_ID", $scope.getMauform.MauForm_ID);
                    formData.append("modelD", JSON.stringify(obj));
                    $http.post(Urlaction, formData, {
                        withCredentials: false,
                        headers: {
                            'Content-Type': undefined
                        },
                        transformRequest: angular.identity
                    }).then(function (res) {
                        closeswal();
                        if (res.data.error == 1) {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                text: res.data.ms
                            });
                            return false;
                        }
                        $scope.BindList_Mauform(true);
                        $("#Modal_SetupFormD").modal("hide");
                        showtoastr('Đã cập nhật dữ liệu thành công!.');
                    });
                    break;
                case '6':
                    //if (!valid(frm)) {
                    //    Swal.fire({
                    //        type: 'error',
                    //        icon: 'error',
                    //        title: '',
                    //        text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                    //    });
                    //    return false;
                    //}
                    Urlaction = "/Request/Update_FormD";
                    var formData = new FormData();
                    var obj = $scope.List_Data_SetupFormKyD;
                    formData.append("t", $rootScope.login.tk);
                    formData.append("Form_ID", $scope.getform.Child_ID);
                    formData.append("modelD", JSON.stringify(obj));
                    $http.post(Urlaction, formData, {
                        withCredentials: false,
                        headers: {
                            'Content-Type': undefined
                        },
                        transformRequest: angular.identity
                    }).then(function (res) {
                        closeswal();
                        if (res.data.error == 1) {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                text: res.data.ms
                            });
                            return false;
                        }
                        $scope.BindList_Setup_Formteam(true);
                        $("#Modal_SetupFormKyD").modal("hide");
                        showtoastr('Đã cập nhật dữ liệu thành công!.');
                    });
                    break;
            }
        };
        $scope.Save_MauFormD = function () {
            swal.showLoading();
            switch ($stateParams.type) {
                case '2':
                    let obj1 = $scope.List_Data_SetupFormD;
                    //let error1 = false;
                    //angular.forEach(obj1, function (r) {
                    //    if (r.TenTruong == null || r.KieuTruong == null || (r.KieuTruong != null && (r.KieuTruong === 'varchar' || r.KieuTruong === 'nvarchar' || r.KieuTruong === 'int' || r.KieuTruong === 'float' || r.KieuTruong === 'textarea') && r.IsLength == null)) {
                    //        error1 = true;
                    //    }
                    //});

                    //if (error1) {
                    //    Swal.fire({
                    //        type: 'error',
                    //        icon: 'error',
                    //        title: '',
                    //        text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                    //    });
                    //    return false;
                    //}
                    Urlaction = "/Request/Update_MauFormD";
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    formData.append("MauForm_ID", $scope.getMauform.MauForm_ID);
                    formData.append("modelD", JSON.stringify(obj1));
                    $http.post(Urlaction, formData, {
                        withCredentials: false,
                        headers: {
                            'Content-Type': undefined
                        },
                        transformRequest: angular.identity
                    }).then(function (res) {
                        closeswal();
                        if (res.data.error == 1) {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                text: res.data.ms
                            });
                            return false;
                        }
                        $scope.OpenModal_SetupMauFormD($scope.getMauform);
                        showtoastr('Đã cập nhật dữ liệu thành công!.');
                    });
                    break;
                case '6':
                    let obj2 = $scope.List_Data_SetupFormKyD;
                    //let error2 = false;
                    //angular.forEach(obj2, function (r) {
                    //    if (r.TenTruong == null || r.KieuTruong == null || (r.KieuTruong != null && (r.KieuTruong === 'varchar' || r.KieuTruong === 'nvarchar' || r.KieuTruong === 'int' || r.KieuTruong === 'float' || r.KieuTruong === 'textarea') && r.IsLength == null)) {
                    //        error2 = true;
                    //    }
                    //});

                    //if (error2) {
                    //    Swal.fire({
                    //        type: 'error',
                    //        icon: 'error',
                    //        title: '',
                    //        text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                    //    });
                    //    return false;
                    //}
                    Urlaction = "/Request/Update_FormD";
                    var formData = new FormData();
                    formData.append("t", $rootScope.login.tk);
                    formData.append("Form_ID", $scope.getform.Child_ID);
                    formData.append("modelD", JSON.stringify(obj2));
                    $http.post(Urlaction, formData, {
                        withCredentials: false,
                        headers: {
                            'Content-Type': undefined
                        },
                        transformRequest: angular.identity
                    }).then(function (res) {
                        closeswal();
                        if (res.data.error == 1) {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                text: res.data.ms
                            });
                            return false;
                        }
                        $scope.OpenModal_SetupMauFormD($scope.getform);
                        showtoastr('Đã cập nhật dữ liệu thành công!.');
                    });
                    break;
            }
            
        };
        //============================================================

        //==================== region flash form ==========================

        $scope.toggleAll_F = function (rowto, CheckAllG_F) {
            angular.forEach(rowto, function (value, key) {
                rowto[key].checked = CheckAllG_F;
            });
            $scope.CheckCheked_F(rowto);
        };
        $scope.CheckCheked_F = function (m) {
            var filtereds = $filter('filter')($scope.Srequest_Tudien[1], { checked: true }, true);
            //$scope.checkLen_PB = filtereds.length;
        };
        $scope.Click_Add_FlashForm = function () {
            swal.showLoading();
            if ($scope.loadding) {
                return false;
            }
            $scope.loadding = true;
            let MauForms = '';
            if ($scope.Srequest_Tudien[1] != null && $scope.Srequest_Tudien[1].length > 0) {
                angular.forEach($scope.Srequest_Tudien[1].filter(x => x.checked), function (r) {
                    MauForms += r.MauForm_ID + ',';
                });
            }
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_Add_FlashForm", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "MauForms", "va": MauForms },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error === 1) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: "Có lỗi khi thêm mới loại đề xuất",
                    });
                    return false;
                }
                $scope.loadding = false;
                $scope.BindList_Setup_Formteam(true);
                showtoastr('cập nhật dữ liệu thành công!');
                $("#Modal_Add_FlashForm").modal("hide");
            }).then(function () {
                closeswal();
            });
        };
        $scope.OpenChild = function (d) {
            d.IsOpen = !(d.IsOpen || false); //parent open
            $scope.List_Data_Setup_Formteam.filter(x => x.Parent_ID === d.Child_ID).forEach(function (r) {
                r.IsOpen = !(r.IsOpen || false); //child open
            });

            let arrCache = [];
            for (let i = 0; i < $scope.List_Data_Setup_Formteam.length; i++) {
                let obj = {
                    "Child_ID": $scope.List_Data_Setup_Formteam[i].Child_ID,
                    "Parent_ID": $scope.List_Data_Setup_Formteam[i].Parent_ID,
                    "IsOpen": $scope.List_Data_Setup_Formteam[i].IsOpen,
                };
                arrCache.push(obj);
            }
            $rootScope.Cache_Group_FormSign = arrCache;
        };
        //======================================================

        ////==================== region tag team ==========================
        $scope.OpenModal_Edit_Tagteam = function (m) {
            $scope.IsEditForm = false;
            $scope.GetForm = m;
            $scope.Get_Teamform_byForm(m);           
        };
        $scope.Get_Teamform_byForm = function (m, type) {
            if (type === 2) {
                $scope.ttitle = "Team sử dụng cho đề xuất";
                $("#Modal_Edit_Tagteam").modal("show");
            }
            else {
                swal.showLoading();
                $http({
                    method: "POST",
                    url: "Home/CallProc",
                    data: {
                        t: $rootScope.login.tk, proc: "Srequest_Get_TeamByfrom", pas: [
                            { "par": "Form_ID", "va": m.Form_ID || m.Child_ID },
                            { "par": "NhanSu_ID", "va": $scope.opition.NhanSu_ID },
                        ]
                    },
                    contentType: 'application/json; charset=utf-8'
                }).then(function (res) {
                    if (!$rootScope.checkToken(res)) return false;
                    if (res.data.error !== 1) {
                        var data = JSON.parse(res.data.data);
                        $scope.List_Data_Teambyform = data[0];
                        $scope.Get_Form = data[1][0];
                    }
                    $scope.ttitle = "Team sử dụng cho đề xuất";
                    $("#Modal_Edit_Tagteam").modal("show");
                }).then(function () {
                    closeswal();
                });
            }
            
        };
        $scope.Click_Edit_Tagteam = function () {
            $scope.Get_Teamform_byCT();
        };
        $scope.Get_Teamform_byCT = function (f) {
            swal.showLoading();
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_Get_Teamfrom", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "n_Congty_ID", "va": $scope.opition.n_Congty_ID },
                        { "par": "p", "va": $scope.opition.currentPage },
                        { "par": "pz", "va": $scope.opition.numPerPage },
                        { "par": "Status", "va": $scope.opition.Status },
                        { "par": "s", "va": $scope.opition.search_team },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.List_Data_Teamform = data[0];
                    if ($scope.List_Data_Teambyform.length > 0) {
                        $scope.List_Data_Teamform = $scope.List_Data_Teamform.filter(a => $scope.List_Data_Teambyform.filter(k => k.Team_ID === a.Team_ID).length == 0);
                    }
                    if (f || !$scope.opition.noOfPages) {
                        $scope.opition.Total = data[1][0].c;
                        $scope.opition.noOfPages = Math.ceil(data[1][0].c / $scope.opition.numPerPage);
                        $scope.opition.CheckAllG_T = false;
                        $scope.checkLen = 0;
                    }
                }
                $scope.tstitle = "Chọn Team sử dụng";
                $("#Modal_Select_Tagteam").modal("show");
            }).then(function () {
                closeswal();
            });
        };
        $scope.toggleAll_T = function (rowto, CheckAllG_T) {
            angular.forEach(rowto, function (value, key) {
                rowto[key].checked = CheckAllG_T;
            });
            $scope.CheckCheked_PB(rowto);
        };
        $scope.CheckCheked_T = function (m) {
            var filtereds = $filter('filter')($scope.List_Data_Teamform, { checked: true }, true);
            //$scope.checkLen_PB = filtereds.length;

        };
        $scope.Click_Push_Teamforform = function (item) {
            angular.forEach(item, function (value, key) {
                if (item[key].checked) {
                    let IsSLA = null
                    if ($scope.Get_Form && $scope.Get_Form.IsSLA) {
                        IsSLA = $scope.Get_Form.IsSLA;
                    }
                    else if ($scope.Khaibaoform && $scope.Khaibaoform.IsSLA) {
                        IsSLA = $scope.Khaibaoform.IsSLA;
                    }
                    var obj_ts = { "Team_ID": item[key].Team_ID, "Team_Name": item[key].Team_Name, "Trangthai": item[key].Trangthai, IsSLA: IsSLA, IsChangeQT: item[key].Isloai == 1 ? true : false, IsSkip: false, IsNoty: true, IsMail: true, };
                    checkCount = $scope.List_Data_Teambyform.filter(n => n.Team_ID == item[key].Team_ID);
                    if (checkCount.length == 0) {
                        $scope.List_Data_Teambyform.push(obj_ts);
                    }
                }
            });
            $("#Modal_Select_Tagteam").modal("hide");
            $scope.CheckAllG_T = false;
        }
        $scope.Remove_Teamform = function (team, t_id, idx) {
            team.splice(idx, 1);
        };
        $scope.Click_Edit_Teamform = function (frm) {
            swal.showLoading();
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            Urlaction = "/Request/Update_Formteam";
            var formData = new FormData();
            angular.forEach($scope.List_Data_Teambyform, function (r) {
                if (r.IsChangeQT == null) r.IsChangeQT = false;
                if (r.IsSkip == null) r.IsSkip = false;
                if (r.IsNoty == null) r.IsNoty = false;
                if (r.IsMail == null) r.IsMail = false;
            });
            var obj = $scope.List_Data_Teambyform;
            formData.append("t", $rootScope.login.tk);
            switch ($stateParams.type) {
                case "3":
                    formData.append("Form_ID", $scope.GetForm.Form_ID);
                    break;
                case "6":
                    formData.append("Form_ID", $scope.GetForm.Child_ID);
                    break;
            }
            
            formData.append("model", JSON.stringify(obj));
            $http.post(Urlaction, formData, {
                withCredentials: false,
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: angular.identity
            }).then(function (res) {
                closeswal();
                if (res.data.error == 1) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        text: res.data.ms
                    });
                    return false;
                }
                switch ($stateParams.type) {
                    case "3":
                        $scope.BindList_Khaibaoform(true);
                        break;
                    case "6":
                        $scope.BindList_Setup_Formteam(true);
                        break;
                }
                $("#Modal_Edit_Tagteam").modal("hide");
                showtoastr('Đã cập nhật dữ liệu thành công!.');
            });
        };
        ////===================== end region tag team =====================


        //// =====================Region Steup Formteam =======================
        $scope.completeForms_AdvSearch = function (string, m, $event, i) {
            $scope.opition.Focus = false;
            $scope.opition.Focus2 = false;
            $scope.opition.Focus3 = false;
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            if (!string) {
                string = "";
            }
            //string = string.replace("@", "");
            m["Focus" + i] = !m["Focus" + i];
            var output = [];
            angular.forEach($scope.Srequest_Tudien[4], function (u) {
                if ((u.Form_Name || "").toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    output.push(u);
                }
            });
            $scope.Forms_advsearch = output;
            if ($scope.opition.form_ids.length > 0) {
                angular.forEach($scope.opition.form_ids, function (item) {
                    var i = $scope.Forms_advsearch.findIndex(x => x.Form_ID === item.Form_ID);
                    if (i !== -1) {
                        $scope.Forms_advsearch.splice(i, 1);
                    }
                });
            }
        };
        $scope.clickAddF_AdvSearch = function (u) {
            $scope.opition.form_ids.push(u);
            var i = $scope.Forms_advsearch.findIndex(x => x.Form_ID === u.Form_ID);
            if (i !== -1) {
                $scope.Forms_advsearch.splice(i, 1);
            }
            $scope.opition.Focus1 = false;
            $scope.opition.searchForm = "";
        }
        $scope.Clear_SelectForm = function () {
            $scope.opition.form_ids = [];
        };
        $scope.goFormSign = function (m) {
            $rootScope.link = "formsign";
            if (m.Parent_ID) {
                $state.go('formsign', { type: $stateParams.type, form: m.Parent_ID, team: m.Child_ID });
            }
            else {
                $state.go('formsign', { type: $stateParams.type, form: m.Child_ID, team: null });
            }
        };
        $scope.Update_Trangthai = function (cv, tt) {
            swal.showLoading();
            let id = null;
            switch ($stateParams.type) {
                case '1':
                    id = cv.NhomForm_ID;
                    break;
                case '2':
                    id = cv.MauForm_ID;
                    break;
                case '3':
                    id = cv.Form_ID;
                    break;
                case '4':
                    id = cv.NhomTeam_ID;
                    break;
                case '5':
                    id = cv.Team_ID;
                    break;
                case '6':
                    if (($stateParams.form != null && $stateParams.form != "") || ($stateParams.team != null && $stateParams.team != "")) {
                        id = cv.FormSign_ID;
                    }
                    else {
                        id = cv.Child_ID;
                    }
                    break;
            }
            $http({
                method: "POST",
                url: "Request/Update_Trangthai",
                data: { t: $rootScope.login.tk, id: id, tt: tt, type: $stateParams.type, form: $stateParams.form, team: $stateParams.team },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                showtoastr('Đã cập nhật trạng thái thành công!.');
                cv.Trangthai = tt;
                cv.IsActive = tt;
            }).then(function () {
                closeswal();
            });
        };
        //// ==================================================================
        //======================================================
        $scope.SLA_Toida = function (arr) {
            let total_sla = 0;
            angular.forEach(arr, function (r) {
                if (r.IsSLA) {
                    total_sla += parseFloat(r.IsSLA);
                }
            });
            if (total_sla > $scope.SLA_Max) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: "Tổng số giờ xử lý nhỏ hơn " + $scope.SLA_Max + " số giờ xử lý tối đa của loại đề xuất !",
                });
                return false;
            }
        };
        $scope.MoveDownQT = function (arr, m, idx_cr) {
            if (idx_cr !== -1 && idx_cr < arr.length - 1) {
                let el = arr[idx_cr];
                arr[idx_cr] = arr[idx_cr + 1];
                arr[idx_cr + 1] = el;
            }
            $scope.RintimeQT();
        };
        $scope.MoveUpQT = function (arr, m, idx_cr) {
            if (idx_cr > 0) {
                let el = arr[idx_cr];
                arr[idx_cr] = arr[idx_cr - 1];
                arr[idx_cr - 1] = el;
            }
            $scope.RintimeQT();
        };
        $scope.RintimeQT = function () {
            if ($scope.List_Data_Formsign && $scope.List_Data_Formsign.length > 0) {
                angular.forEach($scope.List_Data_Formsign, function (valuefs, p) {
                    $scope.List_Data_Formsign[p].IsOpen = true;
                    $scope.List_Data_Formsign[p].ListUser.forEach(function (valueus, us) {
                        $scope.List_Data_Formsign[p].ListUser[us].IsDown = false;
                        if (us !== -1 && us < $scope.List_Data_Formsign[p].ListUser.length - 1) {
                            $scope.List_Data_Formsign[p].ListUser[us].IsDown = true;
                        }
                        $scope.List_Data_Formsign[p].ListUser[us].IsUp = false;
                        if (us > 0) {
                            $scope.List_Data_Formsign[p].ListUser[us].IsUp = true;
                        }
                    });
                    $scope.List_Data_Formsign[p].IsDown = false;
                    if (p !== -1 && p < $scope.List_Data_Formsign.length - 1) {
                        $scope.List_Data_Formsign[p].IsDown = true;
                    }
                    $scope.List_Data_Formsign[p].IsUp = false;
                    if (p > 0) {
                        $scope.List_Data_Formsign[p].IsUp = true;
                    }
                });
            }
        };
        //===================================

        //bind list proc ===============================
        $scope.GetTuDien = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_Get_Tudien", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    var Temp = [];
                    addToArrayNT(Temp, data[2], null, 0, 'STT');
                    data[2] = Temp;
                    $scope.Srequest_Tudien = data;
                }
            });
        };
        $scope.BindList_Nhomform = function (f) {
            if (f) {
                $("#RequestCtr .row-content").animate({ scrollTop: 0 }, 400);
            }
            $scope.Viewdxtd = false;
            swal.showLoading();
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_List_Nhomform", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "n_Congty_ID", "va": $scope.opition.n_Congty_ID },
                        { "par": "p", "va": $scope.opition.currentPage },
                        { "par": "pz", "va": $scope.opition.numPerPage },
                        { "par": "Status", "va": $scope.opition.Status },
                        { "par": "sort", "va": $scope.opition.sort },
                        { "par": "ob", "va": $scope.opition.ob },
                        { "par": "s", "va": $scope.opition.search },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.List_Data_Nhomform = data[0];
                    if (f || !$scope.opition.noOfPages) {
                        $scope.opition.Total = data[1][0].c;
                        $scope.opition.noOfPages = Math.ceil(data[1][0].c / $scope.opition.numPerPage);
                        $scope.opition.CheckAllG = false;
                        $scope.checkLen = 0;
                    }
                }
            }).then(function () {
                closeswal();
            });
        };
        $scope.BindList_Mauform = function (f) {
            if (f) {
                $("#RequestCtr .row-content").animate({ scrollTop: 0 }, 400);
            }
            $scope.Viewdxtd = false;
            swal.showLoading();
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_List_Mauform", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "n_Congty_ID", "va": $scope.opition.n_Congty_ID },
                        { "par": "p", "va": $scope.opition.currentPage },
                        { "par": "pz", "va": $scope.opition.numPerPage },
                        { "par": "Status", "va": $scope.opition.Status },
                        { "par": "sort", "va": $scope.opition.sort },
                        { "par": "ob", "va": $scope.opition.ob },
                        { "par": "s", "va": $scope.opition.search },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.Open_IsSetup_FormD = false;
                    angular.forEach(data[0], function (r) {
                        if (r.IsSetup_FormD || r.IsDynamic) {
                            $scope.Open_IsSetup_FormD = true;
                        }
                    });
                    $scope.List_Data_Mauform = data[0];
                    if (f || !$scope.opition.noOfPages) {
                        $scope.opition.Total = data[1][0].c;
                        $scope.opition.noOfPages = Math.ceil(data[1][0].c / $scope.opition.numPerPage);
                        $scope.opition.CheckAllG = false;
                        $scope.checkLen = 0;
                    }
                }
            }).then(function () {
                closeswal();
            });
        };
        $scope.BindList_Khaibaoform = function (f) {
            if (f) {
                $("#RequestCtr .row-content").animate({ scrollTop: 0 }, 400);
            }
            $scope.Viewdxtd = false;
            swal.showLoading();
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_List_Khaibaoform", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "n_Congty_ID", "va": $scope.opition.n_Congty_ID },
                        { "par": "p", "va": $scope.opition.currentPage },
                        { "par": "pz", "va": $scope.opition.numPerPage },
                        { "par": "Status", "va": $scope.opition.Status },
                        { "par": "sort", "va": $scope.opition.sort },
                        { "par": "ob", "va": $scope.opition.ob },
                        { "par": "s", "va": $scope.opition.search },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);

                    data[0] = groupBy(data[0], "MauForm_ID");
                    var arr = [];
                    for (let k in data[0]) {
                        arr.push({ MauForm_ID: k, Group_Name: data[0][k][0].MauForm_Name, list: data[0][k] });
                    }
                    data[0] = arr;
                    $scope.List_Data_Khaibaoform = data[0];
                    if (f || !$scope.opition.noOfPages) {
                        $scope.opition.Total = data[1][0].c;
                        $scope.opition.noOfPages = Math.ceil(data[1][0].c / $scope.opition.numPerPage);
                        $scope.opition.CheckAllG = false;
                        $scope.checkLen = 0;
                    }
                }
            }).then(function () {
                closeswal();
            });
        };
        $scope.BindList_Nhomteam = function (f) {
            if (f) {
                $("#RequestCtr .row-content").animate({ scrollTop: 0 }, 400);
            }
            $scope.Viewdxtd = false;
            swal.showLoading();
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_List_Nhomteam", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "n_Congty_ID", "va": $scope.opition.n_Congty_ID },
                        { "par": "p", "va": $scope.opition.currentPage },
                        { "par": "pz", "va": $scope.opition.numPerPage },
                        { "par": "Status", "va": $scope.opition.Status },
                        { "par": "sort", "va": $scope.opition.sort },
                        { "par": "ob", "va": $scope.opition.ob },
                        { "par": "s", "va": $scope.opition.search },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    var Temp = [];
                    var AllTemp = [];
                    addToArrayNT(Temp, data[0], null, 0, 'STT');
                    $scope.List_Data_Nhomteam = Temp;
                    addToArrayNT(AllTemp, data[1], null, 0, 'STT');
                    $scope.All_List_Data_Nhomteam = AllTemp;
                    //$scope.treephongbans = getTreeForDueToAnalysis(Temp, "NhomTeam_ID", "Nhomcha_ID");
                    if ($scope.List_Data_Nhomteam != null) {
                        $scope.checkCount = $scope.List_Data_Nhomteam.length;
                    }
                    $scope.opition.Total = $scope.List_Data_Nhomteam.length;
                    //if (f || !$scope.opition.noOfPages) {
                    //    $scope.opition.Total = data[1][0].c;
                    //    $scope.opition.noOfPages = Math.ceil(data[1][0].c / $scope.opition.numPerPage);
                    //    $scope.opition.CheckAllG = false;
                    //    $scope.checkLen = 0;
                    //}
                }
            }).then(function () {
                closeswal();
            });
        };
        $scope.BindList_Khaibaoteam = function (f) {
            if (f) {
                $("#RequestCtr .row-content").animate({ scrollTop: 0 }, 400);
            }
            $scope.Viewdxtd = false;
            swal.showLoading();
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_List_Khaibaoteam", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "n_Congty_ID", "va": $scope.opition.n_Congty_ID },
                        { "par": "p", "va": $scope.opition.currentPage },
                        { "par": "pz", "va": $scope.opition.numPerPage },
                        { "par": "Status", "va": $scope.opition.Status },
                        { "par": "sort", "va": $scope.opition.sort },
                        { "par": "ob", "va": $scope.opition.ob },
                        { "par": "s", "va": $scope.opition.search },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    angular.forEach(data[0], function (r) {
                        r.ListUser = JSON.parse(r.ListUser);
                    });
                    $scope.List_Data_Khaibaoteam = data[0];
                    if (f || !$scope.opition.noOfPages) {
                        $scope.opition.Total = data[1][0].c;
                        $scope.opition.noOfPages = Math.ceil(data[1][0].c / $scope.opition.numPerPage);
                        $scope.opition.CheckAllG = false;
                        $scope.checkLen = 0;
                    }
                }
            }).then(function () {
                closeswal();
            });
        };
        $scope.BindList_Setup_Formteam = function (f) {
            if (f) {
                $("#RequestCtr .row-content").animate({ scrollTop: 0 }, 400);
            }
            $scope.Viewdxtd = false;
            swal.showLoading();
            let form_ids = '';
            if ($scope.opition.form_ids.length > 0) {
                angular.forEach($scope.opition.form_ids, function (r) {
                    form_ids += r.Form_ID + ',';
                })
                form_ids = form_ids.slice(0, -1);
            }
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_List_Setup_Formteam_nonelever", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID },
                        { "par": "n_Congty_ID", "va": $scope.opition.n_Congty_ID },
                        { "par": "form_ids", "va": form_ids },
                        { "par": "p", "va": $scope.opition.currentPage },
                        { "par": "pz", "va": $scope.opition.numPerPage },
                        { "par": "Status", "va": $scope.opition.Status },
                        { "par": "sort", "va": $scope.opition.sort },
                        { "par": "ob", "va": $scope.opition.ob },
                        { "par": "s", "va": $scope.opition.search },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    angular.forEach(data[0], function (f) {
                        f.Count_IsSetup = 0;
                        if (f.List_TeamByForm) {
                            f.List_TeamByForm = JSON.parse(f.List_TeamByForm);
                            f.List_TeamByForm.forEach(function (t) {
                                f.Count_IsSetup += parseInt(t.IsSetup_Group);
                                t.List_FormSign = JSON.parse(t.List_FormSign);
                                t.List_FormSign.forEach(function (u) {
                                    u.Count_FormSignUser = parseInt(u.Count_FormSignUser);
                                    if (u.List_FormSignUser) {
                                        u.List_FormSignUser = JSON.parse(u.List_FormSignUser);
                                    }
                                })
                            });
                        }
                        if (f.List_FormSign) {
                            f.List_FormSign = JSON.parse(f.List_FormSign);
                            f.List_FormSign.forEach(function (u) {
                                u.Count_FormSignUser = parseInt(u.Count_FormSignUser);
                                if (u.List_FormSignUser) {
                                    u.List_FormSignUser = JSON.parse(u.List_FormSignUser);
                                }
                            })
                        }
                    });
                    
                    if ($rootScope.Cache_Group_FormSign != null && $scope.Cache_Group_FormSign.length > 0) {
                        for (let cc = 0; cc < $rootScope.Cache_Group_FormSign.length; cc++) {
                            data[0].filter(x => x.Child_ID === $rootScope.Cache_Group_FormSign[cc].Child_ID).forEach(function (r) {
                                r.IsOpen = $rootScope.Cache_Group_FormSign[cc].IsOpen;
                            });
                        }
                    }
                    $scope.List_Data_Setup_Formteam = data[0];
                    ////-================================= Code Old =============================================
                    //var Temp = [];
                    //addToArray_FT(Temp, data[0], null, 0, 'STT');
                    //let filter_Parent = $filter('filter')(data[0], { Parent_ID: null }, true);
                    //for (let p = 0; p < filter_Parent.length; p++) {
                    //    for (let c = 0; c < Temp.length; c++) {
                    //        if (filter_Parent[p].Child_ID == Temp[c].Parent_ID) {
                    //            filter_Parent[p].IsSetup_Group += Temp[c].IsSetup_Group;
                    //        }
                    //    }
                    //    Temp.find(x => x.Child_ID === filter_Parent[p].Child_ID).Count_IsSetup = filter_Parent[p].IsSetup_Group;
                    //    Temp.find(x => x.Child_ID === filter_Parent[p].Child_ID).Count_Team;
                    //}
                    //if ($rootScope.Cache_Group_FormSign != null && $scope.Cache_Group_FormSign.length > 0) {
                    //    for (let cc = 0; cc < $rootScope.Cache_Group_FormSign.length; cc++) {
                    //        if (Temp.find(x => (x.Child_ID === $rootScope.Cache_Group_FormSign[cc].Child_ID && x.Parent_ID == null) || (x.Child_ID === $rootScope.Cache_Group_FormSign[cc].Child_ID && x.Parent_ID === $rootScope.Cache_Group_FormSign[cc].Parent_ID))) {
                    //            Temp.find(x => (x.Child_ID === $rootScope.Cache_Group_FormSign[cc].Child_ID && x.Parent_ID == null) || (x.Child_ID === $rootScope.Cache_Group_FormSign[cc].Child_ID && x.Parent_ID === $rootScope.Cache_Group_FormSign[cc].Parent_ID)).IsOpen = $rootScope.Cache_Group_FormSign[cc].IsOpen ?? false;
                    //        }

                    //        Temp.filter(x => x.Parent_ID === $rootScope.Cache_Group_FormSign[cc].Child_ID).forEach(function (r) {
                    //            r.IsOpen = true;
                    //        });
                    //    }
                    //}
                    //$scope.List_Data_Setup_Formteam = Temp;
                    ////-================================= Code Old =============================================
                    if ($scope.List_Data_Setup_Formteam != null) {
                        $scope.checkCount = $scope.List_Data_Setup_Formteam.length;
                    }

                    $scope.opition.Total = data[1][0].c;
                    //if (f || !$scope.opition.noOfPages) {
                    //    $scope.opition.Total = data[1][0].c;
                    //    $scope.opition.noOfPages = Math.ceil(data[1][0].c / $scope.opition.numPerPage);
                    //    $scope.opition.CheckAllG = false;
                    //    $scope.checkLen = 0;
                    //}
                }
            }).then(function () {
                closeswal();
            });
        };
        $scope.BindList_Formsign = function (f) {
            if (f) {
                $("#RequestCtr .row-content").animate({ scrollTop: 0 }, 400);
            }
            $scope.Viewdxtd = false;
            swal.showLoading();
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_List_Formsign", pas: [
                        { "par": "Form_ID", "va": $stateParams.form },
                        { "par": "Team_ID", "va": $stateParams.team },
                        { "par": "p", "va": $scope.opition.currentPage },
                        { "par": "pz", "va": $scope.opition.numPerPage },
                        { "par": "Status", "va": $scope.opition.Status },
                        { "par": "sort", "va": $scope.opition.sort },
                        { "par": "ob", "va": $scope.opition.ob },
                        { "par": "s", "va": $scope.opition.search },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    data[0].forEach(function (r) {
                        r.List_us_duyet = JSON.parse(r.List_us_duyet);
                        r.IsOpen = true;
                    });
                    $scope.List_Data_Formsign = data[0];
                    if (f || !$scope.opition.noOfPages) {
                        $scope.opition.Total = data[1][0].c;
                        $scope.opition.noOfPages = Math.ceil(data[1][0].c / $scope.opition.numPerPage);
                        $scope.opition.CheckAllG = false;
                        $scope.checkLen = 0;
                    }
                    $scope.SLA_Max = data[2][0].SLA_Max;
                    $scope.opition.Form_Name = data[3][0].Form_Name;
                }
            }).then(function () {
                closeswal();
            });
        };
        //==============================================

        // Modal Trình phê duyệt =======================
        $scope.OpenModalTrinhandDuyet = function (m, Status, TypeDuyet, tt) {
            swal.showLoading();
            ///Get phiếu duyệt ==========
            $scope.DataNextQT = [];
            $scope.isBH = null;
            var ArrPhieu = [];
            if (m) {
                ArrPhieu.push(m.Dexuat_ID);
                $scope.List_Data_Nhomform.filter(x => x.Dexuat_ID === m.Dexuat_ID).forEach(function (r) {
                    r.checked = true;
                });
            } else {
                $scope.List_Data_Nhomform.filter(x => x.checked === true).forEach(function (r) {
                    ArrPhieu.push(r.Dexuat_ID);
                });
            }
            if (ArrPhieu.length == 0) {
                Swal.fire({
                    icon: 'error',
                    type: 'error',
                    title: '',
                    text: "Bạn chưa chọn phiếu để trình."
                });
                return false;
            }
            //===========================

            if (Status === 0) { //Trạng thái đang dự thảo
                //Get chuyển theo quy trình
                $scope.dtitle = "Chuyển xử lý";
                if (TypeDuyet === 1) {
                    $scope.TypeDuyet = 1;
                    $http({
                        method: "POST",
                        url: "Trinh_Duyet_Hrm/Get_Quytrinh_ByNhansu",
                        data: {
                            t: $rootScope.login.tk,
                            Congty_ID: $rootScope.login.u.congtyID,
                            NhanSu_ID: $rootScope.login.u.NhanSu_ID,
                            Type: 1 //TypeQuytrinh = 1 Đề xuất tuyển dụng, chưa có 2,3
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        closeswal();
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error === 1) {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: "Có lỗi khi lấy dữ liệu quy trình!"
                            });
                            return false;
                        }
                        var data = res.data.dataQT;
                        $scope.Data_Quytrinh_ByNhansu = data;
                    }).then(function () {
                        $scope.isGet = 1;
                        if ($scope.Data_Quytrinh_ByNhansu == null || $scope.Data_Quytrinh_ByNhansu.length == 0) {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: "Chưa cấu hình quy trình duyệt."
                            });
                            return false;
                        }
                        else if ($scope.Data_Quytrinh_ByNhansu.length == 1) {
                            $scope.Trinhduyet = { NgayDuyet: new Date() };
                            $scope.chonQuytrinh($scope.Data_Quytrinh_ByNhansu[0]);
                            $scope.IsGetQuyTrinh = true;
                            $("#ModalTrinhandDuyet").modal("show");
                        }
                        else {
                            $scope.Trinhduyet = { NgayDuyet: new Date() };
                            $("#ModalTrinhandDuyet").modal("show");
                        }
                    });
                }
                //Get chuyển theo nhóm
                else if (TypeDuyet === 2) {
                    $scope.TypeDuyet = 2;
                    $http({
                        method: "POST",
                        url: "Trinh_Duyet_Hrm/Get_NhomDuyet_ByNhansu",
                        data: {
                            t: $rootScope.login.tk,
                            Congty_ID: $rootScope.login.u.congtyID,
                            NhanSu_ID: $rootScope.login.u.NhanSu_ID,
                            /*Type: 1*/ //TypeQuytrinh = 1 Đề xuất tuyển dụng, chưa có 2,3
                        },
                        contentType: 'application/json; charset=utf-8'
                    }).then(function (res) {
                        closeswal();
                        if (!$rootScope.checkToken(res)) return false;
                        if (res.data.error === 1) {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: "Có lỗi khi lấy dữ liệu quy trình!"
                            });
                            return false;
                        }
                        var data = res.data.dataND;
                        $scope.Data_Nhomduyet_ByNhansu = data;
                    }).then(function () {
                        $scope.isGet = 2;
                        if ($scope.Data_Nhomduyet_ByNhansu == null && $scope.Data_Nhomduyet_ByNhansu.length == 0) {
                            Swal.fire({
                                icon: 'error',
                                type: 'error',
                                title: '',
                                text: "Chưa cấu hình nhóm duyệt."
                            });
                            return false;
                        }
                        else if ($scope.Data_Nhomduyet_ByNhansu.length == 1) {
                            $scope.Trinhduyet = { NgayDuyet: new Date() };
                            $scope.chonNhomduyet($scope.Data_Nhomduyet_ByNhansu[0]);
                            $scope.IsGetNhomduyet = true;
                            $("#ModalTrinhandDuyet").modal("show");
                        }
                        else {
                            $scope.Trinhduyet = { NgayDuyet: new Date() };
                            $("#ModalTrinhandDuyet").modal("show");
                        }
                    });
                }
                //Get chuyển đích danh
                else if (TypeDuyet === 3) {
                    $scope.typeTL = null;
                    $scope.isGet = 3;
                    $scope.Nguoiduyet = { NhanSu_ID: null, IsDuyet: true, IsView: true, NgayDuyet: new Date(), IsType: true }
                    $scope.Nhansu = {};
                    $scope.Trinhduyet = { NgayDuyet: new Date() };
                    $scope.IsHT = null;
                    //$scope.goTagU();
                    closeswal();
                    $("#ModalTrinhandDuyet").modal("show");
                }
            }
            else if (Status === 1) { // Trạng thái đang chờ duyệt
                //Get duyệt multi phiếu
                if (ArrPhieu.length > 1) {
                    $scope.DuyetNhieu = true;
                    $scope.Trinhduyet = { NgayDuyet: new Date() };
                    closeswal();
                    if (TypeDuyet === 1)
                        $scope.dtitle = "Duyệt phiếu";
                    else if (TypeDuyet === 2)
                        $scope.dtitle = "Không duyệt";
                    $("#ModalTrinhandDuyet").modal("show");
                }
                //Get duyệt one phiếu
                else {
                    $scope.DuyetNhieu = false;
                    if (TypeDuyet === 1) {
                        $scope.dtitle = "Duyệt phiếu";
                        $http({
                            method: "POST",
                            url: "Trinh_Duyet_Hrm/getDuyetPhieu",
                            data: {
                                t: $rootScope.login.tk,
                                ArrPhieu: ArrPhieu,
                                NhanSu_ID: $rootScope.login.u.NhanSu_ID,
                            },
                            contentType: 'application/json; charset=utf-8'
                        }).then(function (res) {
                            closeswal();
                            if (res.data.error === 1) {
                                Swal.fire({
                                    icon: 'error',
                                    type: 'error',
                                    title: '',
                                    text: 'Có lỗi khi lấy quy trình!'
                                });
                                return false;
                            }
                            else {
                                $scope.DataNextQT = res.data.data;
                                if ($scope.DataNextQT != null && $scope.DataNextQT.length > 1)
                                    $scope.IsGetQuyTrinh = false;
                                else
                                    $scope.IsGetQuyTrinh = true;
                                $scope.typeTL = res.data.typeD;
                                var isBH = res.data.isBH;
                                if (isBH) {
                                    $scope.typeTL = null;
                                    $scope.isBH = true;
                                    $scope.dtitle = "Ban hành";
                                }
                                $scope.Trinhduyet = { NgayDuyet: new Date() };
                                $("#ModalTrinhandDuyet").modal("show");
                            }
                        });
                    }
                    else if (TypeDuyet === 2) {
                        $scope.isBH = false; //ko duyệt
                        $scope.DuyetNhieu = false;
                        $scope.Trinhduyet = { NgayDuyet: new Date() };
                        closeswal();
                        $scope.dtitle = "Không duyệt";
                        $("#ModalTrinhandDuyet").modal("show");
                    }
                }
            }
            else if (tt) {
                $scope.dtitle = "Ban hành";
            }
        };
        //Chọn quy trình duyệt
        $scope.chonQuytrinh = function (n) {
            swal.showLoading();
            $http({
                method: "POST",
                url: "Trinh_Duyet_Hrm/Get_NextQuytrinh",
                data: {
                    t: $rootScope.login.tk,
                    Congty_ID: $rootScope.login.u.congtyID,
                    QT_ID: n.QT_ID,
                    Type: 1,
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 2) {
                    $scope.IsGetQuyTrinh = true;
                    var dataNhomD = res.data.dataNhomD;
                    var typeTL = res.data.typeTL;
                    var dataNd = res.data.data;

                    if (dataNd == null && dataNhomD == null) {
                        $scope.dtitle = "Hoàn thành đề xuất";
                        $scope.typeTL = null;
                        $scope.isHTDX = true;
                        $scope.Duyetbh = {};
                        $scope.IsHT = true
                    }
                    else {
                        if (dataNhomD != null && dataNhomD.length > 0) {
                            $scope.DataNextQT = dataNhomD;
                        }
                        if (typeTL != null) {
                            $scope.typeTL = typeTL;
                        }
                        if (dataNd != null && dataNd.length > 0) {
                            $scope.Nguoiduyet = dataNd[0];
                        }
                    }
                }
                else {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: res.data.ms
                    });
                    $("#ModalTrinhandDuyet").modal("hide");
                    return false;
                }
            }).then(function () {
                closeswal();
            });
        };
        //Chọn nhóm duyệt
        $scope.chonNhomduyet = function (n) {
            swal.showLoading();
            $http({
                method: "POST",
                url: "Trinh_Duyet_Hrm/Get_NextNhomduyet",
                data: {
                    t: $rootScope.login.tk,
                    Congty_ID: $rootScope.login.u.congtyID,
                    NhomDuyet_ID: n.NhomDuyet_ID,
                    //Type: 1,
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 2) {
                    $scope.IsGetNhomduyet = true;
                    var dataNhomD = res.data.dataNhomD;
                    var typeTL = res.data.typeTL;
                    var dataNd = res.data.data;

                    if (dataNd == null && dataNhomD == null) {
                        $scope.dtitle = "Hoàn thành đề xuất";
                        $scope.typeTL = null;
                        $scope.isHTDX = true;
                        $scope.Duyetbh = {};
                        $scope.IsHT = true
                    }
                    else {
                        if (dataNhomD != null && dataNhomD.length > 0) {
                            $scope.DataNextQT = dataNhomD;
                        }
                        if (typeTL != null) {
                            $scope.typeTL = typeTL;
                        }
                        if (dataNd != null && dataNd.length > 0) {
                            $scope.Nguoiduyet = dataNd[0];
                        }
                    }
                }
                else {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: res.data.ms
                    });
                    $("#ModalTrinhandDuyet").modal("hide");
                    return false;
                }
            }).then(function () {
                closeswal();
            });
        };

        //Modal Duyệt phiếu ============================
        $scope.ClickTrinhDuyet = function (TypeDuyet, f) {
            swal.showLoading();
            var ArrPhieu = [];
            var Noidung = null;
            var Nguoinhan = null;
            var QT_ID = null;
            var NhomDuyet_ID = null;
            var IsDuyetPB = false;
            var TypeDuyet = null;
            if ($scope.Trinhduyet) {
                Noidung = $scope.Trinhduyet.Noidung ?? null;
            }
            $scope.List_Data_Nhomform.filter(x => x.checked).forEach(function (r) {
                ArrPhieu.push(r.Dexuat_ID);
            });

            if ($scope.opition.Status === '0') {
                //Set Nhóm duyệt or người duyệt
                if ($scope.DataNextQT != null && $scope.DataNextQT.length > 0 && $scope.DataNextQT[0].KieuDuyet == 1) {
                    if ($scope.DataNextQT[0].Users.length > 0) {
                        var i = -1;
                        do {
                            i++;
                            Nguoinhan = $scope.DataNextQT[0].Users[i].NhanSu_ID ?? null;
                        } while (Nguoinhan == $scope.login.u.NhanSu_ID);
                    } else {
                        Nguoinhan = $scope.DataNextQT[0].Users[0].NhanSu_ID ?? null;
                    }
                }
                if ($scope.DataNextQT != null && $scope.DataNextQT.length > 0) {
                    QT_ID = $scope.DataNextQT[0].QT_ID ?? null;
                    NhomDuyet_ID = $scope.DataNextQT[0].NhomDuyet_ID ?? $scope.DataNextQT[0].NhomDuyet_ID ?? null;
                    IsDuyetPB = $scope.DataNextQT[0].IsDuyetPB ?? false;
                }
                else if ($scope.Nguoiduyet != null) {
                    Nguoinhan = $scope.Nguoiduyet.NhanSu_ID ?? null;
                    QT_ID = $scope.Nguoiduyet.QT_ID ?? null;
                    NhomDuyet_ID = $scope.Nguoiduyet.NhomDuyet_ID ?? null;
                    IsDuyetPB = $scope.Nguoiduyet.IsDuyetPB ?? false;
                }
                //============================
                if (QT_ID == null && NhomDuyet_ID == null && Nguoinhan == null) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        title: '',
                        text: "Chưa xác định người nhận."
                    });
                    return false;
                }
                //Trình duyệt =========================
                $http({
                    method: "POST",
                    url: "Trinh_Duyet_Hrm/TrinhPhieu",
                    data: {
                        t: $rootScope.login.tk,
                        QT_ID: QT_ID,
                        NhomDuyet_ID: NhomDuyet_ID,
                        Nguoinhan: Nguoinhan,
                        ArrPhieu: ArrPhieu,
                        Noidung: Noidung,
                        Type: TypeDuyet,
                        IsDuyetPB: IsDuyetPB,
                        IsHT: $scope.IsHT
                    },
                    contentType: 'application/json; charset=utf-8'
                }).then(function (res) {
                    isSend = false;
                    if (res.data.error === 1) {
                        closeswal();
                        Swal.fire({
                            icon: 'error',
                            type: 'error',
                            title: '',
                            text: res.data.ms,
                        });
                    }
                    else {
                        $("#ModalTrinhandDuyet").modal("hide");
                        $scope.BindList_Nhomform(true);
                        //$scope.BindListCountVPP();
                        showtoastr('Đã cập nhật dữ liệu thành công!.');
                    }
                }, function (response) { // optional
                    isSend = false;
                    closeswal();
                });
                //===================================
            }
            else {
                //Duyệt phiếu =======================
                $http({
                    method: 'POST',
                    url: "Trinh_Duyet_Hrm/DuyetPhieu",
                    data: {
                        t: $rootScope.login.tk,
                        ArrPhieu: ArrPhieu,
                        NhanSu_ID: $rootScope.login.u.NhanSu_ID,
                        Noidung: $scope.Trinhduyet.Noidung,
                        isBH: $scope.isBH,
                    },
                    contentType: 'application/json; charset=utf-8'
                }).then(function (res) {

                    $scope.loadding = false;
                    closeswal();
                    $scope.checkLen = 0;
                    if (!$rootScope.checkToken(res)) return false;
                    if (res.data.error !== 0) {
                        Swal.fire({
                            icon: 'error',
                            type: 'error',
                            title: '',
                            text: 'Có lỗi khi ' + ($scope.isTralai != true ? "duyệt phiếu" : "trả lại") + ' !'
                        });
                        return false;
                    }
                    $("#ModalTrinhandDuyet").modal("hide");
                    $scope.BindList_Nhomform(true);
                    showtoastr(($scope.isTralai != true ? "Duyệt phiếu" : "Trả lại") + ' thành công!.');
                }, function (response) { // optional
                    $scope.loadding = false;

                });
                //===================================
            }
        };
        $scope.showChartSign = function (u) {
            $scope.ChartsSigns = u.Signs;
            $scope.Logs = [];
            $scope.getLichLog(u);
            $("#ModalChartSign").modal("show");
        };
        $scope.getLichLog = function (r) {
            $http({
                method: "POST",
                url: "/Home/callProc",
                data: {
                    t: $rootScope.login.tk,
                    proc: "Shrm_ListLog",
                    pas: [
                        { "par": "Lienket_ID", "va": r.Dexuat_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                var data = JSON.parse(res.data.data);
                data[0].forEach(function (r) {
                    if (r.Users) {
                        r.Users = JSON.parse(r.Users);
                    }
                });
                $scope.ChartsSigns = data[0];
                $scope.Logs = data[1];
            },
                function (response) {
                });
        };
        //==============================================

        //Region ================ Modal Export ================
        $scope.openModalExport = function (type) {
            $scope.listData_Column = [
                { "id": 1, "name": "Tên đề xuất", "column": "Tendexuat" },
                { "id": 2, "name": "Vị trí ứng tuyển", "column": "Vitri_ID" },
                { "id": 3, "name": "Tên phòng ban", "column": "Phongban_ID" },
                { "id": 4, "name": "Số lượng", "column": "Soluong" },
                { "id": 5, "name": "Hình thức làm việc", "column": "Hinhthuclamviec" },
                { "id": 6, "name": "Nơi làm việc", "column": "Noilamviec_ID" },
                { "id": 7, "name": "Mức lương từ", "column": "Luongtu" },
                { "id": 8, "name": "Mức lương đến", "column": "Luongden" },
                { "id": 9, "name": "Hạn tuyển từ", "column": "HantuyenTu" },
                { "id": 10, "name": "Hạn tuyển đến", "column": "HantuyenDen" },
                { "id": 11, "name": "Lý do tuyển", "column": "LydoTuyen" },
                { "id": 12, "name": "Mô tả công việc", "column": "Motacongviec" },
                { "id": 13, "name": "Tuổi từ", "column": "TuoiTu" },
                { "id": 14, "name": "Tuổi đến", "column": "TuoiDen" },
                { "id": 15, "name": "Giới tính", "column": "Gioitinh" },
                { "id": 16, "name": "Chiều cao", "column": "Chieucao" },
                { "id": 17, "name": "Cân nặng", "column": "Cannang" },
                { "id": 18, "name": "Trình độ", "column": "Trinhdo_ID" },
                { "id": 19, "name": "Kinh nghiệm", "column": "Kinhnghiem_ID" },
                { "id": 20, "name": "Người lập", "column": "Nguoilap_ID" },
                { "id": 21, "name": "Ngày lập", "column": "Ngaylap" },
            ];
            $scope.listData_Filter_Column = [];
            $scope.dtitle = "Xuất File Đề Xuất Tuyển Dụng";
            $("#ModalExport").modal("show");
        };
        $scope.push_Filter_DataColumn = function (item) {
            if (item != null) {
                $scope.listData_Filter_Column.push(item);
            }
            else {
                $scope.listData_Column.filter(a => $scope.listData_Filter_Column.filter(b => b.id === a.id).length == 0).forEach(function (r) {
                    $scope.listData_Filter_Column.push(r);
                });
            }
            $scope.listData_Column = $scope.listData_Column.filter(a => $scope.listData_Filter_Column.filter(b => b.id === a.id).length == 0);
        };
        $scope.remove_rowDataColumn = function (item) {
            if (item != null) {
                $scope.listData_Column.push(item);
            }
            else {
                $scope.listData_Filter_Column.filter(a => $scope.listData_Column.filter(b => b.id === a.id).length == 0).forEach(function (r) {
                    $scope.listData_Column.push(r);
                });
            }
            $scope.listData_Filter_Column = $scope.listData_Filter_Column.filter(a => $scope.listData_Column.filter(b => b.id === a.id).length == 0);
            $scope.listData_Column = $filter('orderBy')($scope.listData_Column, 'id');
        };
        function renderTable() {
            let data = $scope.List_Data_Nhomform;
            let data_filter = $scope.listData_Filter_Column;
            let countdatafilter = data_filter.length - 1;
            var htmltable = "";
            htmltable = "<font face='Times New Roman'>";
            htmltable += "<div><table border='0' colspan='1' cellpadding='10' style='min-width:1024px; margin:auto;'><tbody><tr><td style='padding:0px 15px;'><img src='" + $rootScope.fileUrl + $rootScope.login.u.logo + "' style='height:60px;width:auto;'/></td>";
            htmltable += "<td colspan='" + countdatafilter + "' style='text-align: center'><div><b style='font-size: 18px'>" + $scope.TudienDXTuyenDung[0][0].tenCongty + "</b></div> <div style='font-size: 16px'>" + $scope.TudienDXTuyenDung[0][0].tenkhongdau + "</div> <div>" + $scope.TudienDXTuyenDung[0][0].diaChi + "</div></td></tbody ></table>";
            htmltable += "<table border='0' cellpadding='5' style='min-width:1024px; margin:auto;'><tbody><tr><td colspan='" + data_filter.length + "'><hr/></td></tr></tbody></table></div>";
            htmltable += "<div><table border='0' cellpadding='10' style='min-width:1024px; margin:auto;'><thead><tr><th colspan='" + countdatafilter + "' align='center'><div class='col-md-12'><h3 style='font-weight:bold;text-align:center'>DANH SÁCH ĐỀ XUẤT TUYỂN DỤNG </h3></div></th></tr></thead></table>";
            htmltable += "<table border='1' cellpadding='10' style='min-width:1024px; margin:auto; font-size:14px;border-collapse:collapse'><thead>";
            htmltable += "<tr style='vertical-align: middle'>";
            if (data_filter != null && data_filter.length > 0) {
                htmltable += "<td style='font-weight:bold;font-size:14px;width:150px;text-align: center;'>STT</td>";
                data_filter.forEach(function (cl) {
                    if (cl.column === 'Tendexuat' || cl.column === 'Vitri_ID' || cl.column === 'Phongban_ID' || cl.column === 'Nguoilap_ID') htmltable += "<td style='font-weight:bold;font-size:14px;width:150px;text-align: left;'>" + cl.name + "</td>";
                    else htmltable += "<td style='font-weight:bold;font-size:14px;width:150px;text-align: center;'>" + cl.name + "</td>";
                });
                htmltable += "</tr></thead>";
                let index = 1;
                if (data.length > 0) {
                    htmltable += "<tbody>";
                    data.forEach(function (item) {
                        htmltable += "<tr>";
                        htmltable += "<td style='font-size:14px;width:150px;text-align: center;'>" + index + "</td>";
                        data_filter.forEach(function (cl) {
                            if (cl.column != "Vitri_ID" && cl.column != "Phongban_ID" && cl.column != "Hinhthuclamviec" && cl.column != "Noilamviec_ID" && cl.column != "Trinhdo_ID" && cl.column != "Kinhnghiem_ID" && cl.column != "Gioitinh" && cl.column != "Nguoilap_ID") {
                                if (item[cl.column] != null) {
                                    if (cl.column === "Tendexuat") htmltable += "<td style='font-size:14px;width:150px;text-align: left;'>" + item[cl.column] + "</td>";
                                    else htmltable += "<td style='font-size:14px;width:150px;text-align: center;'>" + item[cl.column] + "</td>";
                                }
                                else htmltable += "<td style='font-size:14px;width:150px;text-align: center;'></td>";
                            }
                            if (cl.column === "Vitri_ID") {
                                let checknull = false;
                                $scope.TudienDXTuyenDung[1].forEach(function (vt) {
                                    if (vt.VitriTD_ID == item[cl.column]) {
                                        checknull = true;
                                        htmltable += "<td style='font-size:14px;width:150px;text-align:left;'>" + vt.TenVitri + "</td>";
                                    }
                                });
                                if (!checknull) {
                                    htmltable += "<td style='font-size:14px;width:150px;text-align:left;'></td>";
                                }
                            }
                            if (cl.column === "Phongban_ID") {
                                let checknull = false;
                                $scope.TudienDXTuyenDung[2].forEach(function (pb) {
                                    if (pb.Phongban_ID == item[cl.column]) {
                                        checknull = true;
                                        htmltable += "<td style='font-size:14px;width:150px;text-align:left;'>" + pb.tenPhongban + "</td>";
                                    }
                                });
                                if (!checknull) {
                                    htmltable += "<td style='font-size:14px;width:150px;text-align:left;'></td>";
                                }
                            }
                            if (cl.column === "Hinhthuclamviec") {
                                let checknull = false;
                                $scope.ListHinhThucLV.forEach(function (ht) {
                                    if (ht.value == item[cl.column]) {
                                        checknull = true;
                                        htmltable += "<td style='font-size:14px;width:150px;text-align:center;'>" + ht.text + "</td>";
                                    }
                                });
                                if (!checknull) {
                                    htmltable += "<td style='font-size:14px;width:150px;text-align:center;'></td>";
                                }
                            }
                            if (cl.column === "Noilamviec_ID") {
                                let checknull = false;
                                $scope.TudienDXTuyenDung[3].forEach(function (dd) {
                                    if (dd.Diadanh_ID == item[cl.column]) {
                                        checknull = true;
                                        htmltable += "<td style='font-size:14px;width:150px;text-align:center;'>" + dd.TenDiadanh + "</td>";
                                    }
                                });
                                if (!checknull) {
                                    htmltable += "<td style='font-size:14px;width:150px;text-align:center;'></td>";
                                }
                            }
                            if (cl.column === "Trinhdo_ID") {
                                let checknull = false;
                                $scope.TudienDXTuyenDung[4].forEach(function (td) {
                                    if (td.TrinhdoHV_ID == item[cl.column]) {
                                        checknull = true;
                                        htmltable += "<td style='font-size:14px;width:150px;text-align:center;'>" + td.TenTrinhdohocvan + "</td>";
                                    }
                                });
                                if (!checknull) {
                                    htmltable += "<td style='font-size:14px;width:150px;text-align:center;'></td>";
                                }
                            }
                            if (cl.column === "Kinhnghiem_ID") {
                                let checknull = false;
                                $scope.ListExp.forEach(function (kn) {
                                    if (kn.value == item[cl.column]) {
                                        checknull = true;
                                        htmltable += "<td style='font-size:14px;width:150px;text-align:center;'>" + kn.text + "</td>";
                                    }
                                });
                                if (!checknull) {
                                    htmltable += "<td style='font-size:14px;width:150px;text-align:center;'></td>";
                                }
                            }
                            if (cl.column === "Gioitinh") {
                                let checknull = false;
                                $scope.ListSex.forEach(function (gt) {
                                    if (gt.value == item[cl.column]) {
                                        checknull = true;
                                        htmltable += "<td style='font-size:14px;width:150px;text-align:center;'>" + gt.text + "</td>";
                                    }
                                });
                                if (!checknull) {
                                    htmltable += "<td style='font-size:14px;width:150px;text-align:center;'></td>";
                                }
                            }
                            if (cl.column === "Nguoilap_ID") {
                                htmltable += "<td style='font-size:14px;width:150px;text-align:left;'>" + item.fullName + "</td>";
                            }
                        });
                        htmltable += "</tr>";
                        index = index + 1;
                    });
                    htmltable += "</tbody></table></div>";
                    return htmltable;
                }
            }
            return "";
        }
        $scope.ExportExcelDanhsach = function () {
            if ($scope.listData_Filter_Column == null || $scope.listData_Filter_Column.length == 0) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Bạn chưa chọn thông tin cần xuất !'
                });
                return false;
            }
            var htmltable = renderTable();
            var labelexecl = "Danh sách đề xuất tuyển dụng";
            var name = labelexecl;
            var style = "";
            var uri = 'data:application/vnd.ms-excel;base64,'
                , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><meta charset="utf-8"><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]-->' + style + '</head><body><table>{table}</table></body></html>'
                , base64 = function (s) { return window.btoa(unescape(encodeURIComponent(s))) }
                , format = function (s, c) { return s.replace(/{(\w+)}/g, function (m, p) { return c[p]; }) }
            var ctx = { worksheet: name || 'Worksheet', table: htmltable.replace(/\u00a0/g, " ") };
            var url = uri + base64(format(template, ctx));
            var a = document.createElement('a');
            document.body.appendChild(a);
            a.href = url;
            a.download = name + '.xls';
            a.click();
            setTimeout(function () { window.URL.revokeObjectURL(url); }, 0);
        };
        $scope.PrintDanhsach = function () {
            if ($scope.listData_Filter_Column == null || $scope.listData_Filter_Column.length == 0) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Bạn chưa chọn thông tin cần xuất !'
                });
                return false;
            }
            swal.showLoading();
            var htmltable = renderTable();
            var popupWin = window.open('', '_blank', 'width=1400,height=400');
            popupWin.document.open();
            popupWin.document.write('<html><head><link rel="stylesheet" type="text/css" href="style.css" /></head><body onload="window.print()">' + htmltable + '</body></html>');
            popupWin.document.close();
            closeswal();
        };
        //End Region ==========================================

        //Region ================== Modal choose user ================
        $scope.goUserTeam = function (nd) {
            switch ($stateParams.type) {
                case "5":
                    let filter = $scope.List_Data_Khaibaoteam.filter(x => x.active == true);
                    angular.forEach(filter, function (r) {
                        r.active = false;
                    });
                    $scope.IsGet_UserTeam = true;
                    nd.active = true;
                    $scope.GetTeam = nd;
                    $scope.Click_Get_UserTeam(nd);
                    //$scope.goTagU();
                    break;
                case "6":
                    if (($stateParams.form != null && $stateParams.form != "") || ($stateParams.team != null && $stateParams.team != "")) {
                        $scope.IsGet_UserFormSign = true;
                        $scope.GetFormSign = nd;
                        $scope.Click_Get_UserFormSign(nd);
                        //$scope.goTagU();
                        $scope.OpenModal_UserFormSign();
                    }
                    break;
            }
            
        };
        $scope.Click_Get_UserTeam = function (m) {
            swal.showLoading();
            $scope.List_Data_Userteam = [];
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_Get_Userteam", pas: [
                        { "par": "Team_ID", "va": m.Team_ID },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error === 1) {
                    Swal.fire({
                        type: 'error',
                        icon: 'error',
                        title: '',
                        text: 'Có lỗi khi load !'
                    });
                    return false;
                }
                var data = JSON.parse(res.data.data)[0];
                $scope.List_Data_Userteam = data;
            }).then(function () {
                closeswal();
            });
        };
        $scope.OpenModal_UserTeam = function () {
            $scope.utitle = "Cập nhật thành viên cho Team";
            $("#Modal_Edit_Userteam").modal("show");
        };
        $scope.Remove_Userteam = function (us, idx) {
            us.splice(idx, 1);
        };
        $scope.Click_Edit_Userteam = function (frm) {
            swal.showLoading();
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            Urlaction = "/Request/Update_Khaibaouser";
            var formData = new FormData();
            var obj = $scope.List_Data_Userteam;
            formData.append("t", $rootScope.login.tk);
            formData.append("Team_ID", $scope.GetTeam.Team_ID);
            formData.append("model", JSON.stringify(obj));
            $http.post(Urlaction, formData, {
                withCredentials: false,
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: angular.identity
            }).then(function (res) {
                closeswal();
                if (res.data.error == 1) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        text: res.data.ms
                    });
                    return false;
                }
                $scope.BindList_Khaibaoteam(true);
                $scope.Click_Get_UserTeam($scope.GetTeam);
                $("#Modal_Edit_Userteam").modal("hide");
                showtoastr('Đã cập nhật dữ liệu thành công!.');
            });
        };
        var users = [];
        $scope.showusersModal = function (f, index, idxarr, status) {
            $scope.idxFormSign = idxarr;
            $scope.statusFormSign = status;
            $rootScope.phongbans.filter(r => r.isCheck).forEach(function (r) {
                r.isCheck = false;
            });
            $rootScope.users.filter(r => r.isCheck).forEach(function (r) {
                r.isCheck = false;
            });
            $scope.vbphongbans = $rootScope.phongbans;
            $scope.vbusers = $rootScope.users;
            $scope.one = f;
            // set chọn user trong lấy list user config cho quy trình
            $scope.IndexListUser = index;
            $("#usersModal").modal("show");
        };
        $scope.choiceUser = function (users) {
            if ($scope.IndexListUser === 0) {
                //$scope.nguoiduyets = [];
                //users = users.filter(u => $scope.nguoiduyets.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                //$scope.nguoiduyets = $scope.nguoiduyets.concat(users);
                //$scope.Nhomform.searchU0 = null;
            }
            else if ($scope.IndexListUser === 1) {
                //users = users.filter(u => $scope.thamgiaKHs.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                //$scope.thamgiaKHs = $scope.thamgiaKHs.concat(users);
                //$scope.Nhomform.searchU1 = null;
            }
            else {
                switch ($stateParams.type) {
                    case "5":
                        users = users.filter(u => $scope.List_Data_Userteam.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                        users.forEach(function (r) {
                            r.IsType = 0;
                            r.Trangthai = true;
                        });
                        $scope.List_Data_Userteam = $scope.List_Data_Userteam.concat(users);
                        break;
                    case "6":
                        if ($stateParams.form || $stateParams.team) {
                            users = users.filter(u => $scope.List_Data_Userformsign.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                            users.forEach(function (r) {
                                r.IsType = 0;
                                r.Trangthai = true;
                            });
                            $scope.List_Data_Userformsign = $scope.List_Data_Userformsign.concat(users);
                        }
                        else {
                            if ($scope.statusFormSign) {
                                users = users.filter(u => $scope.List_Data_Formsign[$scope.idxFormSign].ListUser.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                                users.forEach(function (r) {
                                    r.IsType = 0;
                                    r.Trangthai = true;
                                    r.Parent_ID = $scope.idxFormSign + 1;
                                });
                                $scope.List_Data_Formsign[$scope.idxFormSign].ListUser = $scope.List_Data_Formsign[$scope.idxFormSign].ListUser.concat(users);
                            }
                            else {
                                users = users.filter(u => $scope.List_Data_Formsign[$scope.idxFormSign].ListUserFollow.findIndex(x => x.NhanSu_ID === u.NhanSu_ID) === -1);
                                users.forEach(function (r) {
                                    r.IsType = 3;
                                    r.Trangthai = true;
                                    r.Parent_ID = $scope.idxFormSign + 1;
                                });
                                $scope.List_Data_Formsign[$scope.idxFormSign].ListUserFollow = $scope.List_Data_Formsign[$scope.idxFormSign].ListUserFollow.concat(users);
                            }
                            $scope.RintimeQT();
                        }
                        break;
                }
                //add user group

                //$scope.filterUser = null;
                //$scope.searchU1 = "";;
                //$("input.ipautoUser1.true").focus();
                //$http({
                //    method: "POST",
                //    url: "Request/Add_UserTeam",
                //    data: { t: $rootScope.login.tk, Team_ID: $scope.GetTeam.Team_ID, u: $scope.UserTeams },
                //    contentType: 'application/json; charset=utf-8'
                //}).then(function (res) {
                //    if (!$rootScope.checkToken(res)) return false;
                //    if (res.data.error !== 1) {
                //        $scope.UserTeams.push(u);
                //        var i = $scope.noduyetUser.findIndex(x => x.NhanSu_ID === u.NhanSu_ID);
                //        if (i !== -1) {
                //            $scope.noduyetUser.splice(i, 1);
                //            $scope.Khaibaoteam.countUser += 1;

                //        }
                //        if ($scope.filterUser) {
                //            i = $scope.filterUser.findIndex(x => x.NhanSu_ID === u.NhanSu_ID);
                //            if (i !== -1) {
                //                $scope.filterUser.splice(i, 1);
                //            }
                //        }
                //        showtoastr("Nhân sự bạn chọn đã được thêm thành công.");
                //    } else {
                //        Swal.fire({
                //            icon: 'error',
                //            type: 'error',
                //            title: '',
                //            text: res.data.ms
                //        });
                //    }
                //});
            }
            $("#usersModal").modal("hide");
        };
        $scope.clickAddDuyetnguoi = function (u) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "GetRoleFunction", pas: [
                        { "par": "congtyID", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID", "va": u.NhanSu_ID },
                        { "par": "Module", "va": 'S015' }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {

                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0][0];
                    $scope.RoleDuyetnguoi = data;
                }

            }).then(function () {
                //if ($scope.RoleDuyetnguoi.Hrm_DeXuatTuyenDung_ChoDuyet) {
                $scope.Nhansu = u;
                $scope.Nguoiduyet.NhanSu_ID = u.NhanSu_ID;
                $scope.Nguoiduyet.Focus = false;
                //}
                //else {
                //    Swal.fire({
                //        icon: 'error',
                //        type: 'error',
                //        title: '',
                //        text: "Người bạn chọn không có quyền duyệt."
                //    });
                //    return false;
                //}
            });

        }
        $scope.removeUserD = function (us) {
            us.NhanSu_ID = null;
        };
        $scope.complete = function (string, m, $event, i) {
            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser" + i).focus();
                $event.preventDefault();
            }
            string = string.replace("@", "");
            if (m) {
                m.Focus0 = false;
                m.Focus1 = false;
                m.Focus2 = false;
            }
            m["Focus" + i] = true;
            var output = [];
            if (!string) {
                string = "";
            }
            var arrs = [];
            if ($scope.nguoiduyets != null && $scope.nguoiduyets.length > 0) {
                angular.forEach($scope.nguoiduyets, function (r) {
                    arrs.push(r)
                });
            }
            if ($scope.thamgiaKHs != null && $scope.thamgiaKHs.length > 0) {
                angular.forEach($scope.thamgiaKHs, function (r) {
                    arrs.push(r)
                });
            };

            angular.forEach(users, function (u) {
                if ((u.fullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.enFullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.tenTruyCap || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.phone + '').toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    if (m.NhanSu_ID != u.NhanSu_ID) {
                        var check = arrs.findIndex(x => x.NhanSu_ID == u.NhanSu_ID);
                        if (check == -1) {
                            output.push(u);
                        }
                    }
                }
            });

            $scope.filterUser = output;
        };
        $scope.goTagU = function (f) {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Shrm_Listuser", pas: [
                        { "par": "Congty_ID", "va": $rootScope.login.u.congtyID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    users = data[0];
                    $scope.filterUser = data[0];
                }
                if (f) {
                    $scope.Nguoiduyet.Focus = !$scope.Nguoiduyet.Focus;
                }
            });
        };
        $scope.handleKeyDown = function (m, $event, f) {

            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                //$("ul.autoUser").focus();
                $event.preventDefault();
                if ($scope.focusedIndex !== $scope.filterUser.length - 1) {
                    $scope.focusedIndex++;
                }
            }
            else if (keyCode === 38) {
                // Up
                $event.preventDefault();
                if ($scope.focusedIndex !== 0) {
                    $scope.focusedIndex--;
                }
            }
            else if (keyCode === 13 && $scope.focusedIndex >= 0) {
                // Enter
                $event.preventDefault();
                $scope.clickAddUser(m, $scope.filterUser[$scope.focusedIndex], f);
            }
        };
        $scope.ClearUser = function (f) {
            switch (f) {
                case 1:
                    $scope.xuly.TenXLC = null;
                    $scope.xuly.XLC = null;
                    break;
            }
        };
        $scope.completeDuyetnguoi = function (string, $event) {

            var keyCode = $event.keyCode;
            if (keyCode === 40) {
                // Down
                $("ul.autoUser").focus();
                $event.preventDefault();
            }
            string = string.replace("@", "");
            $scope.Focus1 = true;
            $scope.Focus2 = true;
            var output = [];
            if (!string) {
                string = "";
            }
            angular.forEach(users, function (u) {
                if ((u.fullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.enFullName || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.tenTruyCap || "").toLowerCase().indexOf(string.toLowerCase()) >= 0 || (u.phone + '').toLowerCase().indexOf(string.toLowerCase()) >= 0) {
                    output.push(u);
                }
            });
            $scope.filterUser = output;
        };
        ////====================== User nhóm duyệt =====================

        $scope.Click_Get_UserFormSign = function (m) {
            swal.showLoading();
            $scope.List_Data_Userteam = [];
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Srequest_Get_Userformsign", pas: [
                        { "par": "FormSign_ID", "va": m.FormSign_ID },
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error === 1) {
                    Swal.fire({
                        type: 'error',
                        icon: 'error',
                        title: '',
                        text: 'Có lỗi khi load !'
                    });
                    return false;
                }
                var data = JSON.parse(res.data.data)[0];
                angular.forEach(data, function (r) {
                    if (r.IsSLA) {
                        r.IsSLA = parseFloat(r.IsSLA).toFixed(1);
                    }
                });
                $scope.List_Data_Userformsign = data;
            }).then(function () {
                closeswal();
            });
        };
        $scope.OpenModal_UserFormSign = function () {
            $scope.ufstitle = "Cập nhật nhân sự nhóm duyệt";
            $("#Modal_Edit_UserFormsign").modal("show");
        };
        $scope.Remove_Userformsign = function (us, idx) {
            us.splice(idx, 1);
        };
        $scope.Click_Edit_Userformsign = function (frm) {
            swal.showLoading();
            if (!valid(frm)) {
                Swal.fire({
                    type: 'error',
                    icon: 'error',
                    title: '',
                    text: 'Vui lòng nhập thông tin trường bôi đỏ !'
                });
                return false;
            }
            Urlaction = "/Request/Update_Formsignuser";
            var formData = new FormData();
            var obj = $scope.List_Data_Userformsign;
            formData.append("t", $rootScope.login.tk);
            formData.append("FormSign_ID", $scope.GetFormSign.FormSign_ID);
            formData.append("model", JSON.stringify(obj));
            $http.post(Urlaction, formData, {
                withCredentials: false,
                headers: {
                    'Content-Type': undefined
                },
                transformRequest: angular.identity
            }).then(function (res) {
                closeswal();
                if (res.data.error == 1) {
                    Swal.fire({
                        icon: 'error',
                        type: 'error',
                        text: res.data.ms
                    });
                    return false;
                }
                $scope.BindList_Formsign(true);
                $scope.Click_Get_UserFormSign($scope.GetFormSign);
                $("#Modal_Edit_UserFormsign").modal("hide");
                showtoastr('Đã cập nhật dữ liệu thành công!.');
            });
        };

        ////===========================================================

        //End Region choose user ======================================

        //CKEDITOR.config.toolbar_MyCustomToolbar = [
        //    ['Bold', 'Italic', 'Underline', 'StrikeThrough', '-', 'Undo', 'Redo', '-', 'Cut', 'Copy', 'Paste',],
        //    ['NumberedList', 'BulletedList', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        //];
        //CKEDITOR.config.removePlugins = 'elementspath,save,font';
        //CKEDITOR.config.resize_enabled = false;

        $scope.Refresh = function () {
            $scope.opition = {
                search: "",
                search_ns: "",
                search_team: "",
                numPerPage: 50,
                currentPage: 1,
                sort: "STT",
                ob: "asc",
                Status: null,
                tenSort: "",
                tenTT: "Tất cả",
                tenStatus: "Tất cả",
                IsSearchBar: true,
                IsSearchBar_ns: true,
                IsSearchBar_team: true,
                CheckAllG: false,
                CheckAllG_PB: false,
                CheckAllG_T: false,
                n_Congty_ID: $rootScope.login.u.congtyID,
                //fress search
                ViTris: [],
                form_ids: [],
                NoiLamViecs: [],
                TrinhDos: [],
                mltu: null,
                mlden: null,
                HantuyenTu: null,
                HantuyenDen: null,
                //============
                s_ngaylap: null,
                s_vitri: null,
                s_hinhthuc: null,
                Total: 0,
                Pages: [50, 100, 1000, 5000]
            };
            $scope.checkLen = 0;
            $scope.ListDXTD = [];
            $scope.Viewdxtd = false;
            $scope.LoaiDuyet = null;
            $scope.GetTuDien();
            $scope.goTagU();
            
            switch ($stateParams.type) {
                case "1":
                    $scope.BindList_Nhomform(true);
                    break;
                case "2":
                    $scope.BindList_Mauform(true);
                    break;
                case "3":
                    $scope.BindList_Khaibaoform(true);
                    break;
                case "4":
                    $scope.BindList_Nhomteam(true);
                    break;
                case "5":
                    $scope.BindList_Khaibaoteam(true);
                    break;
                case "6":
                    if ($stateParams.form != null || $stateParams.team != null) $scope.BindList_Formsign(true);
                    else $scope.BindList_Setup_Formteam(true);
                    break;
            }
        };

        $scope.initOne = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "GetRoleFunction", pas: [
                        { "par": "congtyID", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID", "va": $rootScope.login.u.NhanSu_ID },
                        { "par": "Module", "va": 'S020' }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0][0];
                    $rootScope.ListDataRoleFunction = data;
                }
            }).then(function () {
                if ($rootScope.roles_Root || $rootScope.roles_Admin) {
                    $scope.opition.CheckAllG = false;
                    $scope.opition.CheckAllG_PB = false;
                    $scope.opition.CheckAllG_T = false;
                    $scope.checkLen = 0;
                    $scope.ListDXTD = [];
                    $scope.Viewdxtd = false;
                    $scope.LoaiDuyet = null;
                    $scope.IsShowMap = true;
                    $scope.GetTuDien();
                    $scope.goTagU();
                    switch ($stateParams.type) {
                        case "1":
                            if (location.href != (baseUrl + "#/khaibaothietlap/nhomdexuat/1")) {
                                location.href = baseUrl;
                            }
                            $rootScope.link = "nhomform";
                            $scope.$watch('opition.currentPage', $scope.BindList_Nhomform);
                            break;
                        case "2":
                            if (location.href != (baseUrl + "#/khaibaothietlap/maudexuat/2")) {
                                location.href = baseUrl;
                            }
                            $rootScope.link = "mauform";
                            $scope.$watch('opition.currentPage', $scope.BindList_Mauform);
                            break;
                        case "3":
                            if (location.href != (baseUrl + "#/khaibaothietlap/khaibaoform/3")) {
                                location.href = baseUrl;
                            }
                            $rootScope.link = "khaibaoform";
                            $scope.$watch('opition.currentPage', $scope.BindList_Khaibaoform);
                            break;
                        case "4":
                            if (location.href != (baseUrl + "#/khaibaothietlap/nhomteam/4")) {
                                location.href = baseUrl;
                            }
                            $rootScope.link = "nhomteam";
                            $scope.$watch('opition.currentPage', $scope.BindList_Nhomteam);
                            break;
                        case "5":
                            if (location.href != (baseUrl + "#/khaibaothietlap/khaibaoteam/5")) {
                                location.href = baseUrl;
                            }
                            $rootScope.link = "khaibaoteam";
                            $scope.$watch('opition.currentPage', $scope.BindList_Khaibaoteam);
                            break;
                        case "6":
                            if ($stateParams.form || $stateParams.team != null) {
                                $rootScope.link = "formsign";
                                $scope.$watch('opition.currentPage', $scope.BindList_Formsign);
                            }
                            else {
                                $rootScope.link = "formsetupteam";
                                $scope.$watch('opition.currentPage', $scope.BindList_Setup_Formteam);
                            }
                            break;
                    }
                }
                else {
                    location.href = baseUrl;
                }
            });
        };

        $scope.initOne();
    }]);