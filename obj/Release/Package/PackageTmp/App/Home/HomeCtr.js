﻿angular.module('HomeCtr', [])
    .controller("HomeCtr", ['$scope', '$rootScope', '$http', '$filter', 'Upload', '$window', '$interval', '$state', '$stateParams', '$timeout', function ($scope, $rootScope, $http, $filter, Upload, $window, $interval, $state, $stateParams, $timeout) {
        //load
        $scope.requestCount=function() {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_ThongkeCount", pas: [
                        { "par": "Congty_ID ", "va": null },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data)[0];
                    var oc = {};
                    var chart1 = [];
                    var chart2 = [];
                    var total = data.find(x => x.type === 0 && x.Trangthai === 100).count;
                    var colors = { '0': '#74b9ff', '1': '#33c9dc', '4': '#6dd230', '-2': '#f17ac7', '17': ' #f7dc6f ', '18': '#f5b041' };
                    data.filter(x => x.type===0).forEach(function (r) {
                        oc[r.Trangthai] = r.count;
                        if (r.Trangthai !== 5 && r.Trangthai !== 6 && r.Trangthai !== 7 && r.Trangthai !== 100 && r.Trangthai !== 2 && r.Trangthai !== 17 && r.Trangthai !== 18 && r.Trangthai != 101)
                            chart1.push({ text: r.TenTrangthai, total: Math.ceil(r.count * 100 / total), value: r.count, color: colors[r.Trangthai] });
                    });
                    $scope.RQCount0 = oc;
                    oc = {};
                    data.filter(x => x.type === 1).forEach(function (r) {
                        oc[r.Trangthai] = r.count;
                        if (r.Trangthai !== 5 && r.Trangthai !== 6 && r.Trangthai !== 7 && r.Trangthai !== 100 && r.Trangthai !== 2 && r.Trangthai !== 17 && r.Trangthai !== 18 && r.Trangthai != 101) 
                            chart2.push({ text: r.TenTrangthai, total: Math.ceil(r.count * 100 / total), value: r.count, color: colors[r.Trangthai] });
                    });
                    $rootScope.countCD = oc[1];
                    $scope.RQCount1 = oc;
                    var chart1 = AmCharts.makeChart("chartdivIn", {
                        "type": "pie",
                        "theme": "light",
                        "dataProvider": chart1,
                        "valueField": "value",
                        "titleField": "text",
                        "startEffect": "elastic",
                        "labelText": "[[value]]%",
                        "labelRadius": 15,
                        "startDuration": 2,
                        "radius": "30%",
                        "innerRadius": "60%",
                        "pullOutRadius": 25,
                        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
                        "angle": 0,
                        "allLabels": [{
                            "text": "",
                            "bold": true,
                            "x": '50%',
                            "y": "85%",
                            "rotation": 0,
                            "width": "100%",
                            "align": "middle"
                        }],
                        "export": {
                            "enabled": false
                        },
                        "colorField": "color",
                    });
                    var chart2 = AmCharts.makeChart("chartdivOut", {
                        "type": "pie",
                        "theme": "light",
                        "dataProvider": chart2,
                        "valueField": "value",
                        "titleField": "text",
                        "startEffect": "elastic",
                        "labelText": "[[value]]%",
                        "labelRadius": 15,
                        "startDuration": 2,
                        "radius": "30%",
                        "innerRadius": "60%",
                        "pullOutRadius": 25,
                        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
                        "angle": 0,
                        "allLabels": [{
                            "text": "",
                            "bold": true,
                            "x": '50%',
                            "y": "85%",
                            "rotation": 0,
                            "width": "100%",
                            "align": "middle"
                        }],
                        "export": {
                            "enabled": false
                        },
                        "colorField": "color",
                    });
                }
            });
        };
        $scope.Request_ListHome = function () {
            $http({
                method: "POST",
                url: "Home/CallProc",
                data: {
                    t: $rootScope.login.tk, proc: "Request_ListHome", pas: [
                        { "par": "Congty_ID ", "va": $rootScope.login.u.congtyID },
                        { "par": "NhanSu_ID ", "va": $rootScope.login.u.NhanSu_ID }
                    ]
                },
                contentType: 'application/json; charset=utf-8'
            }).then(function (res) {
                if (!$rootScope.checkToken(res)) return false;
                if (res.data.error !== 1) {
                    var data = JSON.parse(res.data.data);
                    $scope.FirstLoad = false;
                    data.forEach(function (tb) {
                        tb.forEach(function (r) {
                            r.objTrangthai = Trangthais.find(x => x.id == r.Trangthai);
                            if (r.Thanhviens) {
                                r.Thanhviens = JSON.parse(r.Thanhviens);
                                if (r.Signs) {
                                    r.Signs = JSON.parse(r.Signs);
                                    var tong = 0;
                                    var tc = 0;
                                    r.Signs.forEach(function (ro) {
                                        ro.Thanhviens = r.Thanhviens.filter(x => x.RequestSign_ID == ro.RequestSign_ID);
                                        if (!ro.IsHide) {
                                            if (ro.IsTypeDuyet == 0) {//Duyệt 1 trong nhiều
                                                ro.USign = ro.Thanhviens.filter(x => x.IsSign != 0);
                                                ro.IsShowTV = ro.Thanhviens.filter(x => x.IsSign > 0 && x.IsType != 4).length == 0;
                                                ro.Thanhviens = ro.Thanhviens.filter(x => x.IsSign == 0);
                                                tong += (ro.IsShowTV ? 0 : 1);
                                                tc += 1;
                                            } else {
                                                tong += ro.Thanhviens.filter(x => x.IsSign != 0 && !x.IsClose).length;
                                                tc += ro.Thanhviens.filter(x => !x.IsClose).length;
                                            }
                                        }

                                    });
                                    r.Tiendo = Math.ceil(tong * 100 / tc) || 0;
                                    r.color = renderColor(r.Tiendo);
                                    r.txtcolor = renderTxtColor(r.Tiendo);
                                }
                            }
                        });
                    });
                    $scope.choduyets = data[0].filter(x => x.TypeRP===1);
                    $scope.hoanthanhs = data[0].filter(x => x.TypeRP === 2);
                    $scope.quahans = data[0].filter(x => x.TypeRP === 3);
                }
            });
        };
        //Action
        $scope.Click_Link = function (trangthai, isinout) {
            location.href = baseUrl + "#/viewtonghop/" + trangthai;
        };
        $scope.goInfoRQ = function (r) {
            if ($scope.RequestMaster_ID != r.RequestMaster_ID)
                $scope.RequestMaster_ID = r.RequestMaster_ID;
            else {
                $scope.Uid = generateUUID();
            }
        };
        $scope.refershRQ = function () {
            init(); 
        };
        //function init
        function init() {
            $scope.FirstLoad = true;
            $scope.requestCount();
            $scope.Request_ListHome();
        }
        //first load
        init(); 
    }]);
