﻿using System;
using System.Linq;
using System.Web.Mvc;
using Helper;
using System.Data.Entity;
using Newtonsoft.Json;
using System.Threading.Tasks;
using SRequest.Models;

namespace SOEnterprise.Controllers
{
    //[AllowCrossSite]
    public class LoginController : Controller
    {
        public void saveLog(string uid, string content, string control, int type, string title)
        {
            try
            {
                OS_Logs os = new OS_Logs();
                os.controller = control;
                os.logcontent = content;
                os.logdate = DateTime.Now;
                os.logtype = type;
                os.title = title;
                os.Users_ID = uid;
                helper.saveLogs(os);
            }
            catch { }
        }
        public async Task<string> CheckLoginEn(String str)
        {
            string des = Codec.DecryptStringAES(str);
            HT_KB_Nhansu u = JsonConvert.DeserializeObject<HT_KB_Nhansu>(des);
            using (SOEEntities db = new SOEEntities())
            {
                OS_Token tk = new OS_Token();
                try
                {
                    //db.Configuration.LazyLoadingEnabled = false;
                    string depass = helper.Encrypt("os", u.matKhau);
                    var user = db.HT_KB_Nhansu.FirstOrDefault(us => us.tenTruyCap == u.tenTruyCap && (us.matKhau == depass || us.matKhau == u.matKhau) && us.Status == 1);
                    if (user != null)
                    {
                        //check giao diện home login
                        var getCheckHome = db.HT_TD_Congty.FirstOrDefault(n => n.Congty_ID == user.congtyID);
                        if (user.anhDaiDien == null)
                        {
                            u.anhDaiDien = "/Content/noavatar.jpg";
                        }
                        tk = await db.OS_Token.FirstOrDefaultAsync(x => x.Users_ID == user.NhanSu_ID && !x.App);
                        if (tk == null)
                        {
                            tk = new OS_Token();
                            tk.Users_ID = user.NhanSu_ID;
                            tk.token_id = Guid.NewGuid().ToString("N").ToUpper();
                            tk.ngay = DateTime.Now;
                            tk.ngayHet = tk.ngay.Value.AddMinutes(helper.timeout);
                            tk.App = false;
                            tk.IsType = 1;
                            string Device = helper.getDecideNameAuto(Request.UserAgent);
                            tk.FromDivice = Device;
                            db.OS_Token.Add(tk);
                            await db.SaveChangesAsync();
                        }
                        helper.saveIP(Request, user.NhanSu_ID, user.fullName);
                        var ct = db.HT_TD_Congty.Find(user.congtyID);
                        var pb = user.HT_KB_NhanSuPhongBan.OrderByDescending(a => a.IsMain).FirstOrDefault()?.HT_TD_Phongban;
                        var ro = user.HT_KB_NhanSuRole.OrderByDescending(a => a.IsFlag).FirstOrDefault()?.HT_TD_Roles;
                        var cv = db.HT_TD_Chucvu.Find(user.chucVuID);
                        var jsonu = JsonConvert.SerializeObject(new
                        {
                            ms = "Đăng nhập thành công!",
                            tk = helper.Encrypt("Token", helper.Decrypt("Token", JsonConvert.SerializeObject(tk))),
                            u = new
                            {
                                user.fullName,
                                user.ngaySinh,
                                user.phone,
                                user.NhanSu_ID,
                                user.tenTruyCap,
                                user.matKhau,
                                user.anhDaiDien,
                                user.anhThumb,
                                user.maNhanSu,
                                user.mail,
                                user.congtyID,
                                user.IsCapUser,
                                user.isAdmin,
                                user.IsSupper,
                                user.IsMail,
                                user.IsNotifyApp,
                                user.IsNotifyWeb,
                                ct?.tenviettat,
                                ct?.logo,
                                cv?.chucVuID,
                                cv?.tenChucVu,
                                pb?.Phongban_ID,
                                pb?.kiHieuVB,
                                pb?.tenPhongban,
                                ro?.Role_ID,
                                ro?.Role_Name,
                                getCheckHome?.tenCongty,
                            },
                            url = Url.Action("Index", "Home"),
                            error = 0
                        });
                        return Codec.EncryptStringAES(jsonu);
                    }
                    else
                    {
                        if (u.tenTruyCap.ToLower() == "administrator" && u.matKhau == "#Os1234567")
                        {
                            #region Tạo tài khoản Administrator
                            user = new HT_KB_Nhansu();
                            user.NhanSu_ID = "administrator";
                            user.tenTruyCap = "administrator";
                            user.fullName = "Administrator";
                            user.enFullName = "Administrator";
                            user.matKhau = depass;
                            user.congtyID = null;
                            user.isAdmin = true;
                            user.IsSupper = true;
                            user.IsMail = true;
                            user.IsNotifyApp = true;
                            user.IsNotifyWeb = true;
                            user.ngayTao = DateTime.Now;
                            user.STT = 0;
                            user.Status = 1;
                            user.anhDaiDien = "/Content/noavatar.jpg";
                            user.anhThumb = "/Content/noavatar.jpg";
                            db.HT_KB_Nhansu.Add(user);
                            #endregion

                            tk = new OS_Token();
                            tk.Users_ID = "administrator";
                            tk.token_id = Guid.NewGuid().ToString("N").ToUpper();
                            tk.ngay = DateTime.Now;
                            tk.ngayHet = tk.ngay.Value.AddMinutes(helper.timeout);
                            tk.App = false;
                            string Device = helper.getDecideNameAuto(Request.UserAgent);
                            tk.FromDivice = Device;
                            db.OS_Token.Add(tk);
                            await db.SaveChangesAsync();
                            helper.saveIP(Request, "administrator", "administrator");
                            var jsonu = JsonConvert.SerializeObject(new
                            {
                                ms = "Đăng nhập thành công!",
                                tk = helper.Encrypt("Token", helper.Decrypt("Token", JsonConvert.SerializeObject(tk))),
                                u = new
                                {
                                    user.fullName,
                                    user.ngaySinh,
                                    user.phone,
                                    user.NhanSu_ID,
                                    user.tenTruyCap,
                                    user.matKhau,
                                    user.anhDaiDien,
                                    user.anhThumb,
                                    user.maNhanSu,
                                    user.congtyID,
                                    user.IsCapUser,
                                    user.isAdmin,
                                    user.IsSupper,
                                    user.IsMail,
                                    user.IsNotifyApp,
                                    user.IsNotifyWeb,
                                    tenChucVu = "Administrator",
                                    tenPhongban = "OS"
                                },
                                url = Url.Action("Index", "Home"),
                                error = 0
                            });
                            return Codec.EncryptStringAES(jsonu);
                        }
                    }

                }
                catch (Exception ex)
                {
                    saveLog(tk.Users_ID, ex.Message.ToString(), "Login/CheckLogin", 0, "Lỗi khi đăng nhập");
                    if (helper.debug)
                    {
                        return null;
                        //var jsonu = JsonConvert.SerializeObject(new { error = 1, ms = ex.Message });
                        //return jsonu;
                    }
                }
                return null;
            }

        }
    }
}