﻿using Helper;
using Microsoft.ApplicationBlocks.Data;
using Models;
using Newtonsoft.Json;
using SRequest.Filter;
using SRequest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SRequest.Controllers
{
    [Compress]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        #region Cache
        [OutputCache(Duration = 900, VaryByParam = "*")]
        public async Task<JsonResult> SOE_List_Module(string t, string proc, List<sqlPar> pas)
        {
            using (SOEEntities db = new SOEEntities())
            {
                OS_Token tk = new OS_Token();
                try
                {
                    tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    string Connection = db.Database.Connection.ConnectionString;
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    var sqlpas = new List<SqlParameter>();
                    foreach (sqlPar p in pas)
                    {
                        sqlpas.Add(new SqlParameter("@" + p.par, p.va));
                    }
                    var arrpas = sqlpas.ToArray();
                    var task = Task.Run(() => SqlHelper.ExecuteDataset(Connection, proc, arrpas).Tables);
                    var tables = await task;
                    string JSONresult;
                    JSONresult = JsonConvert.SerializeObject(tables);
                    var jsonResult = Json(new { data = JSONresult }, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
                catch (Exception ex)
                {
                    helper.saveLog(tk.Users_ID, ex.Message.ToString(), "proc", 0, "Lỗi khi gọi thủ tục");
                    if (helper.debug)
                    {
                        return Json(new { error = 1, ms = ex.Message }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { error = 1 }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [OutputCache(Duration = 900, VaryByParam = "*")]
        public async Task<JsonResult> BindListUser(string t, string proc, List<sqlPar> pas)
        {
            using (SOEEntities db = new SOEEntities())
            {
                OS_Token tk = new OS_Token();
                try
                {
                    tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    string Connection = db.Database.Connection.ConnectionString;
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    var sqlpas = new List<SqlParameter>();
                    foreach (sqlPar p in pas)
                    {
                        sqlpas.Add(new SqlParameter("@" + p.par, p.va));
                    }
                    var arrpas = sqlpas.ToArray();
                    var task = Task.Run(() => SqlHelper.ExecuteDataset(Connection, proc, arrpas).Tables);
                    var tables = await task;
                    string JSONresult;
                    JSONresult = JsonConvert.SerializeObject(tables);
                    var jsonResult = Json(new { data = JSONresult }, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
                catch (Exception ex)
                {
                    helper.saveLog(tk.Users_ID, ex.Message.ToString(), "proc", 0, "Lỗi khi gọi thủ tục");
                    if (helper.debug)
                    {
                        return Json(new { error = 1, ms = ex.Message }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { error = 1 }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion

        public async Task<JsonResult> addFireBase(string t, OS_FireBase b)
        {
            using (SOEEntities db = new SOEEntities())
            {
                db.Configuration.LazyLoadingEnabled = false;
                try
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    string Connection = db.Database.Connection.ConnectionString;
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    try
                    {
                        var fb = await db.OS_FireBase.FirstOrDefaultAsync(a => a.TokenCMID == b.TokenCMID);
                        if (fb == null)
                        {
                            if (b.TokenCMID != null)
                            {
                                fb = new OS_FireBase();
                                fb.IDKey = helper.GenKey();
                                fb.token_id = tk.token_id;
                                fb.Users_ID = b.Users_ID;
                                fb.TokenCMID = b.TokenCMID;
                                fb.ngayThem = DateTime.Now;
                                fb.dName = b.dName;
                                fb.FName = b.FName;
                                fb.isType = b.isType;
                                db.OS_FireBase.Add(fb);
                            }
                        }
                        else if (fb.Users_ID != b.Users_ID)
                        {
                            if (b.dName != null && b.FName != null && b.isType != null)
                            {
                                fb.Users_ID = b.Users_ID;
                                fb.token_id = tk.token_id;
                                fb.ngayThem = DateTime.Now;
                                fb.dName = b.dName;
                                fb.FName = b.FName;
                                fb.isType = b.isType;
                            }
                        }
                        else
                        {
                            fb.ngayThem = DateTime.Now;
                        }
                        OS_WebAcess wa = new OS_WebAcess();
                        wa.WebAcess_ID = helper.GenKey();
                        wa.FromDivice = b.dName;
                        try
                        {
                            wa.FromIP = (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"]).Split(',')[0].Trim();
                        }
                        catch { wa.FromIP = ""; }
                        wa.IsTime = DateTime.Now;
                        wa.FullName = b.FName;
                        wa.Users_ID = b.Users_ID;
                        wa.IsStatus = false;
                        db.OS_WebAcess.Add(wa);
                        await db.SaveChangesAsync();
                    }
                    catch (Exception ex)
                    {
                        return Json(new { error = 1, ms = ex.Message }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    if (helper.debug)
                    {
                        return Json(new { error = 1, ms = ex.Message }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { error = 1 }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<ActionResult> updateThumb()
        {
            using (SOEEntities db = new SOEEntities())
            {
                var ns = await db.HT_KB_Nhansu.Where(a => a.anhDaiDien != null && a.anhDaiDien.Contains("NhanSu")).ToListAsync();
                foreach(var nhansu in ns)
                {
                    string anhDaiDien = nhansu.anhDaiDien;
                    string anhDaiDienthumb = "/Portals/NhanSu/Thumb/" + nhansu.anhDaiDien.Substring(nhansu.anhDaiDien.LastIndexOf("/")+1);
                    var check = Server.MapPath("~/" + anhDaiDien);
                    var anhthumbServer = Server.MapPath("~/" + anhDaiDienthumb);
                    helper.ResizeCopyImage(check, anhthumbServer, 120, 120, 90);
                    nhansu.anhThumb = anhDaiDienthumb;
                }
                await db.SaveChangesAsync();
            }
            return Content("ok");
        }

        public async Task<JsonResult> callProc(string t, string proc, List<sqlPar> pas)
        {
            using (SOEEntities db = new SOEEntities())
            {
                OS_Token tk = new OS_Token();
                try
                {
                    tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    string Connection = db.Database.Connection.ConnectionString;
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    var sqlpas = new List<SqlParameter>();
                    foreach (sqlPar p in pas)
                    {
                        sqlpas.Add(new SqlParameter("@" + p.par, p.va));
                    }
                    var arrpas = sqlpas.ToArray();
                    var task = Task.Run(() => SqlHelper.ExecuteDataset(Connection, proc, arrpas).Tables);
                    var tables = await task;
                    string JSONresult;
                    JSONresult = JsonConvert.SerializeObject(tables);
                    var jsonResult = Json(new { data = JSONresult }, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
                catch (DbEntityValidationException e)
                {
                    string contents = "";
                    foreach (var eve in e.EntityValidationErrors)
                    {
                        contents += ("Entity of type \"" + eve.Entry.Entity.GetType().Name + "\" in state \"" + eve.Entry.State + "\" has the following validation errors:");
                        foreach (var ve in eve.ValidationErrors)
                        {
                            contents += "- Property: \"" + ve.PropertyName + "\", Error: \"" + ve.ErrorMessage + "\"";
                        }
                    }
                    return Json(new
                    {
                        error = 1,
                        ms = contents
                    }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    var messages = new List<string>();
                    do
                    {
                        messages.Add(e.Message);
                        e = e.InnerException;
                    }
                    while (e != null);
                    var message = string.Join(" - ", messages);
                    return Json(new
                    {
                        error = 1,
                        ms = message
                    }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        public async Task<JsonResult> callProcNoPar(string t, string proc)
        {
            using (SOEEntities db = new SOEEntities())
            {
                OS_Token tk = new OS_Token();
                try
                {
                    tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    string Connection = db.Database.Connection.ConnectionString;
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }  
                    var task = Task.Run(() => SqlHelper.ExecuteDataset(Connection, proc).Tables);
                    var tables = await task;
                    string JSONresult;
                    JSONresult = JsonConvert.SerializeObject(tables);
                    var jsonResult = Json(new { data = JSONresult }, JsonRequestBehavior.AllowGet);
                    jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
                catch (Exception ex)
                {
                    helper.saveLog(tk.Users_ID, ex.Message.ToString(), "proc", 0, "Lỗi khi gọi thủ tục");
                    if (helper.debug)
                    {
                        return Json(new { error = 1, ms = ex.Message }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { error = 1 }, JsonRequestBehavior.AllowGet);
                }
            }
        }
    }
}