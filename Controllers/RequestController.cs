﻿using Helper;
using Microsoft.ApplicationBlocks.Data;
using Models;
using Newtonsoft.Json;
using SRequest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SRequest.Controllers
{
    public class RequestController : Controller
    {
        // GET: Request
        public ActionResult Index()
        {
            return View();
        }

        #region Nhomform
        [HttpPost]
        public async Task<JsonResult> Update_Nhomform(string t, Request_NhomForm model)
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }

                    if (model.NhomForm_ID == "-1" || model.NhomForm_ID == "" || model.NhomForm_ID == null)
                    {
                        model.NhomForm_ID = helper.GenKey();
                        model.Nguoitao = tk.Users_ID;
                        db.Request_NhomForm.Add(model);
                    }
                    else
                    {
                        db.Entry(model).State = EntityState.Modified;
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = "Có lỗi khi cập nhật nhóm form" }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> Del_Multi_Nhomform(string t, List<string> ids)
        {
            using (SOEEntities db = new SOEEntities())
            {
                try
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = helper.checkTokennoTask(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    foreach (var id in ids)
                    {
                        var da = await db.Request_NhomForm.FindAsync(id);
                        if (da != null)
                        {
                            db.Request_NhomForm.Remove(da);
                        }
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { error = 1, ms = "Vui lòng xóa dữ liệu liên quan của đợt tuyển dụng này." }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion
        #region Mauform
        [HttpPost]
        public async Task<JsonResult> Update_Mauform(string t, Request_MauForm model)
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }

                    if (model.MauForm_ID == "-1" || model.MauForm_ID == "" || model.MauForm_ID == null)
                    {
                        model.MauForm_ID = helper.GenKey();
                        model.Nguoitao = tk.Users_ID;
                        db.Request_MauForm.Add(model);
                    }
                    else
                    {
                        db.Entry(model).State = EntityState.Modified;
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = "Có lỗi khi cập nhật mẫu form" }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> Del_Multi_Mauform(string t, List<string> ids)
        {
            using (SOEEntities db = new SOEEntities())
            {
                try
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = helper.checkTokennoTask(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    foreach (var id in ids)
                    {
                        var da = await db.Request_MauForm.FindAsync(id);
                        if (da != null)
                        {
                            db.Request_MauForm.Remove(da);
                        }
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { error = 1, ms = "Vui lòng xóa dữ liệu liên quan của đợt tuyển dụng này." }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        public async Task<JsonResult> Update_MauFormD()
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    var t = Request.Form["t"];
                    var MauForm_ID = Request.Form["MauForm_ID"];
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", Request.Form["t"]));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    List<ListRequest_MauFormD> model = JsonConvert.DeserializeObject<List<ListRequest_MauFormD>>(Request.Form["modelD"]);
                    var model_mauform = await db.Request_MauFormD.Where(x => x.MauForm_ID == MauForm_ID).ToListAsync();
                    if (model_mauform.Count > 0)
                    {
                        db.Request_MauFormD.RemoveRange(model_mauform);
                    }
                    if (model.Count > 0)
                    {
                        List<Request_MauFormD> model_mauformD = new List<Request_MauFormD>();
                        foreach (var mf in model)
                        {
                            int stt = model_mauformD.Count + 1;
                            string MauFormD_ID = "";
                            if (mf.MauFormD_ID != null)
                            {
                                MauFormD_ID = mf.MauFormD_ID;
                            }
                            else
                            {
                                MauFormD_ID = helper.GenKey();
                                mf.MauFormD_ID = MauFormD_ID;
                            }
                            string fm_ID = null;
                            if (mf.IsParent_ID != null)
                            {
                                fm_ID = model.FirstOrDefault(x => x.index_ID != null && x.index_ID == mf.IsParent_ID)?.MauFormD_ID ?? null;
                            }
                            model_mauformD.Add(new Request_MauFormD()
                            {
                                MauFormD_ID = MauFormD_ID,
                                MauForm_ID = MauForm_ID,
                                TenTruong = mf.TenTruong,
                                KieuTruong = mf.KieuTruong,
                                IsLength = mf.IsLength,
                                STT = stt,
                                IsRequired = mf.IsRequired,
                                IsLabel = mf.IsLabel,
                                IsParent_ID = fm_ID == null ? mf.IsParent_ID : fm_ID,
                                IsType = mf.IsType,
                                IsClass = mf.IsClass,
                            });
                            stt++;
                        }
                        db.Request_MauFormD.AddRange(model_mauformD);
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public class ListRequest_MauFormD
        {
            public string MauFormD_ID { get; set; }
            public string MauForm_ID { get; set; }
            public string TenTruong { get; set; }
            public string KieuTruong { get; set; }
            public int IsLength { get; set; }
            public int STT { get; set; }
            public bool IsRequired { get; set; }
            public bool IsLabel { get; set; }
            public string IsParent_ID { get; set; }
            public int IsType { get; set; }
            public string index_ID { get; set; }
            public string IsClass { get; set; }
        }
        #endregion
        #region Khaibaoform
        [HttpPost]
        public async Task<JsonResult> Update_Khaibaoform(string t, string Congty_ID, Request_Form model, List<Request_FormSign> formsign, List<Request_FromSignUser_Table> formsignuser /*, List<Request_FromSignUser> formsignuserfollow*/)
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    if (model.Form_ID == "-1" || model.Form_ID == "" || model.Form_ID == null)
                    {
                        var checktrung = await db.Request_Form.Where(x => x.Congty_ID == Congty_ID && x.Form_Code == model.Form_Code).ToListAsync();
                        if (checktrung.Count > 0)
                        {
                            return Json(new { error = 1, ms = "Mã loại đề xuất đã tồn tại !" }, JsonRequestBehavior.AllowGet);
                        }
                        model.Form_ID = helper.GenKey();
                        model.Nguoitao = tk.Users_ID;
                        db.Request_Form.Add(model);
                        if (model.IsUseAll == true)
                        {
                            var model_team = await db.Request_Team.Where(x => x.Congty_ID == Congty_ID).ToListAsync();
                            if (model_team.Count > 0)
                            {
                                List<Request_FormTeam> formteam_new = new List<Request_FormTeam>();
                                foreach (var tm in model_team)
                                {
                                    formteam_new.Add(new Request_FormTeam()
                                    {
                                        FormTeam_ID = helper.GenKey(),
                                        Team_ID = tm.Team_ID,
                                        Form_ID = model.Form_ID,
                                        IsSLA = model.IsSLA ?? null,
                                        IsChangeQT = tm.Isloai == 1 ? true : tm.Isloai == 0 ? false : false,
                                        IsSkip = false,
                                        IsNoty = true,
                                        IsMail = true,
                                    });
                                }
                                db.Request_FormTeam.AddRange(formteam_new);
                            }
                        }
                    }
                    else
                    {
                        db.Entry(model).State = EntityState.Modified;
                        if (model.IsUseAll == true)
                        {
                            var model_team = await db.Request_Team.Where(x => x.Congty_ID == Congty_ID).ToListAsync();
                            if (model_team.Count > 0)
                            {
                                List<Request_FormTeam> formteam_new = new List<Request_FormTeam>();
                                var model_team_by_form = db.Request_FormTeam.Where(x => x.Form_ID == model.Form_ID).Select(x => x.Team_ID).ToList();
                                foreach (var tm in model_team.Where(x => !model_team_by_form.Contains(x.Team_ID)))
                                {
                                    var checkteam = db.Request_FormTeam.FirstOrDefault(x => x.Form_ID == model.Form_ID && x.Team_ID == tm.Team_ID);
                                    if (checkteam == null)
                                    {
                                        formteam_new.Add(new Request_FormTeam()
                                        {
                                            FormTeam_ID = helper.GenKey(),
                                            Team_ID = tm.Team_ID,
                                            Form_ID = model.Form_ID,
                                            IsSLA = model.IsSLA ?? null,
                                            IsChangeQT = tm.Isloai == 1 ? true : tm.Isloai == 0 ? false : false,
                                            IsSkip = false,
                                            IsNoty = true,
                                            IsMail = true,
                                        });
                                    }
                                }
                                db.Request_FormTeam.AddRange(formteam_new);
                            }
                        }
                    }
                    if (model.Form_ID != null)
                    {
                        var modelFormSign = db.Request_FormSign.Where(x => x.Form_ID == model.Form_ID).ToList();
                        if (modelFormSign.Count > 0)
                        {
                            db.Request_FormSign.RemoveRange(modelFormSign);
                            foreach(var fsu in modelFormSign)
                            {
                                var modeFormSignUser = db.Request_FromSignUser.Where(x => x.FormSign_ID == fsu.FormSign_ID).ToList();
                                db.Request_FromSignUser.RemoveRange(modeFormSignUser);
                            }
                        }
                    }
                    if (formsign != null && formsign.Count > 0)
                    {
                        List<Request_FormSign> new_modelFormSign = new List<Request_FormSign>();
                        var stt = 0;
                        foreach(var fs in formsign)
                        {
                            var GetFormSign_ID = helper.GenKey();
                            new_modelFormSign.Add(new Request_FormSign()
                            {
                                FormSign_ID = GetFormSign_ID,
                                Form_ID = model.Form_ID,
                                Team_ID = null,
                                GroupName = fs.GroupName,
                                IsTypeDuyet = model.IsQuytrinhduyet,
                                IsSkipOffline = false,
                                Nguoitao = tk.Users_ID,
                                Ngaytao = model.Ngaytao,
                                IsActive = true,
                                STT = stt++,
                            });
                            var sttns = 0;
                            if (formsignuser !=null && formsignuser.Count > 0)
                            {
                                List<Request_FromSignUser> new_modelFormSignUser = new List<Request_FromSignUser>();
                                foreach (var fsus in formsignuser.Where(x=> x.Parent_ID == fs.STT.ToString()))
                                {
                                    new_modelFormSignUser.Add(new Request_FromSignUser()
                                    {
                                        FormSignUser_ID = helper.GenKey(),
                                        FormSign_ID = GetFormSign_ID,
                                        NhanSu_ID = fsus.NhanSu_ID,
                                        Nguoitao = tk.Users_ID,
                                        Ngaytao = DateTime.Now,
                                        Trangthai = true,
                                        IsType = fsus.IsType,
                                        STT = sttns++,
                                    });
                                }
                                db.Request_FromSignUser.AddRange(new_modelFormSignUser);
                            }
                            //if (formsignuserfollow  != null && formsignuserfollow.Count > 0)
                            //{
                            //    List<Request_FromSignUser> new_modelFormSignUserFollow = new List<Request_FromSignUser>();
                            //    foreach (var fsus in formsignuserfollow)
                            //    {
                            //        new_modelFormSignUserFollow.Add(new Request_FromSignUser()
                            //        {
                            //            FormSignUser_ID = helper.GenKey(),
                            //            FormSign_ID = GetFormSign_ID,
                            //            NhanSu_ID = fsus.NhanSu_ID,
                            //            Nguoitao = tk.Users_ID,
                            //            Ngaytao = DateTime.Now,
                            //            Trangthai = true,
                            //            IsType = 3,
                            //            STT = sttns++,
                            //        });
                            //    }
                            //    db.Request_FromSignUser.AddRange(new_modelFormSignUserFollow);
                            //}
                        }
                        db.Request_FormSign.AddRange(new_modelFormSign);
                    }
                    
                    await db.SaveChangesAsync();
                    return Json(new { error = 0, Form_ID = model.Form_ID, IsUseAll = model.IsUseAll }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = "Có lỗi khi cập nhật form" }, JsonRequestBehavior.AllowGet);
            }
        }

        public class Request_FromSignUser_Table
        {
            public string Parent_ID { get; set; }
            public string FormSignUser_ID { get; set; }
            public string FormSign_ID { get; set; }
            public string NhanSu_ID { get; set; }
            public string Nguoitao { get; set; }
            public DateTime? Ngaytao { get; set; }
            public bool Trangthai { get; set; }
            public int STT { get; set; }
            public int IsType { get; set; }
            public float IsSLA { get; set; }
        }

        public async Task<JsonResult> Del_Multi_Khaibaoform(string t, List<string> ids)
        {
            using (SOEEntities db = new SOEEntities())
            {
                try
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = helper.checkTokennoTask(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    foreach (var id in ids)
                    {
                        var da = await db.Request_Form.FindAsync(id);
                        if (da != null)
                        {
                            db.Request_Form.Remove(da);
                        }
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { error = 1, ms = "Vui lòng xóa dữ liệu liên quan của đợt tuyển dụng này." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public async Task<JsonResult> Update_Formteam()
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    var t = Request.Form["t"];
                    var Form_ID = Request.Form["Form_ID"];
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", Request.Form["t"]));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    List<TeamsForm> model = JsonConvert.DeserializeObject<List<TeamsForm>>(Request.Form["model"]);
                    var model_team = await db.Request_FormTeam.Where(x => x.Form_ID == Form_ID).ToListAsync();
                    var model_form = db.Request_Form.Find(Form_ID);
                    if (model_team.Count > model.Count && model_form.IsUseAll == true)
                    {
                        model_form.IsUseAll = false;
                    }
                    if (model_team.Count > 0)
                    {
                        db.Request_FormTeam.RemoveRange(model_team);
                    }
                    if (model.Count > 0)
                    {
                        List<Request_FormTeam> model_formteam = new List<Request_FormTeam>();
                        foreach (var tm in model)
                        {
                            model_formteam.Add(new Request_FormTeam()
                            {
                                FormTeam_ID = helper.GenKey(),
                                Form_ID = Form_ID,
                                Team_ID = tm.Team_ID,
                                IsSLA = tm.IsSLA ?? null,
                                IsChangeQT = tm.IsChangeQT,
                                IsSkip = tm.IsSkip,
                                IsNoty = tm.IsNoty,
                                IsMail = tm.IsMail,
                            });
                        }
                        db.Request_FormTeam.AddRange(model_formteam);
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public class TeamsForm
        {
            public string Team_ID { get; set; }
            public float? IsSLA { get; set; }
            public bool IsChangeQT { get; set; }
            public bool IsSkip { get; set; }
            public bool IsNoty { get; set; }
            public bool IsMail { get; set; }
        }

        public async Task<JsonResult> Update_Trangthai(string t, string id, bool tt, int type, string form, string team)
        {
            using (SOEEntities db = new SOEEntities())
            {
                try
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = helper.checkTokennoTask(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    switch (type)
                    {
                        case 1:
                            var da_1 = await db.Request_NhomForm.FindAsync(id);
                            if (da_1 != null) { da_1.Trangthai = tt; }
                            break;
                        case 2:
                            var da_2 = await db.Request_MauForm.FindAsync(id);
                            if (da_2 != null) { da_2.Trangthai = tt; }
                            break;
                        case 3:
                            var da_3 = await db.Request_Form.FindAsync(id);
                            if (da_3 != null) { da_3.Trangthai = tt; }
                            break;
                        case 4:
                            var da_4 = await db.Request_NhomTeam.FindAsync(id);
                            if (da_4 != null) { da_4.Trangthai = tt; }
                            break;
                        case 5:
                            var da_5 = await db.Request_Team.FindAsync(id);
                            if (da_5 != null) { da_5.Trangthai = tt; }
                            break;
                        case 6:
                            if (!string.IsNullOrEmpty(form) || !string.IsNullOrEmpty(team))
                            {
                                var da_6 = await db.Request_FormSign.FindAsync(id);
                                if (da_6 != null) { da_6.IsActive = tt; }
                            }
                            else
                            {
                                var da_6 = await db.Request_Form.FindAsync(id);
                                if (da_6 != null) { da_6.Trangthai = tt; }
                            }
                            break;
                    }

                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { error = 0, ms = "Vui lòng xóa dữ liệu liên quan của Nhóm duyệt này." }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion
        #region Nhóm team
        [HttpPost]
        public async Task<JsonResult> Update_Nhomteam(string t, Request_NhomTeam model)
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }

                    if (model.NhomTeam_ID == "-1" || model.NhomTeam_ID == "" || model.NhomTeam_ID == null)
                    {
                        model.NhomTeam_ID = helper.GenKey();
                        model.Nguoitao = tk.Users_ID;
                        db.Request_NhomTeam.Add(model);
                    }
                    else
                    {
                        db.Entry(model).State = EntityState.Modified;
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = "Có lỗi khi cập nhật form" }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> Del_Multi_Nhomteam(string t, List<string> ids)
        {
            using (SOEEntities db = new SOEEntities())
            {
                try
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = helper.checkTokennoTask(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    foreach (var id in ids)
                    {
                        var da = await db.Request_NhomTeam.FindAsync(id);
                        if (da != null)
                        {
                            db.Request_NhomTeam.Remove(da);
                        }
                        var child = await db.Request_NhomTeam.Where(x => x.Nhomcha_ID == id).ToListAsync();
                        if (child.Count > 0)
                        {
                            db.Request_NhomTeam.RemoveRange(child);
                        }
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { error = 1, ms = "Vui lòng xóa dữ liệu liên quan của đợt tuyển dụng này." }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion
        #region Khai báo team
        [HttpPost]
        public async Task<JsonResult> Update_Khaibaoteam(string t, Request_Team model)
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }

                    if (model.Team_ID == "-1" || model.Team_ID == "" || model.Team_ID == null)
                    {
                        model.Team_ID = helper.GenKey();
                        model.Nguoitao = tk.Users_ID;
                        db.Request_Team.Add(model);
                    }
                    else
                    {
                        db.Entry(model).State = EntityState.Modified;
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = "Có lỗi khi cập nhật form" }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> Del_Multi_Khaibaoteam(string t, List<string> ids)
        {
            using (SOEEntities db = new SOEEntities())
            {
                try
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = helper.checkTokennoTask(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    foreach (var id in ids)
                    {
                        var da = await db.Request_Team.FindAsync(id);
                        if (da != null)
                        {
                            db.Request_Team.Remove(da);
                        }
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { error = 1, ms = "Vui lòng xóa dữ liệu liên quan của đợt tuyển dụng này." }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        [HttpPost]
        public async Task<JsonResult> Update_Khaibaouser()
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    var t = Request.Form["t"];
                    var Team_ID = Request.Form["Team_ID"];
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", Request.Form["t"]));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    List<UsersTeam> model = JsonConvert.DeserializeObject<List<UsersTeam>>(Request.Form["model"]);
                    var model_us = await db.Request_TeamUser.Where(x => x.Team_ID == Team_ID).ToListAsync();
                    if (model_us.Count > 0)
                    {
                        db.Request_TeamUser.RemoveRange(model_us);
                    }
                    if (model.Count > 0)
                    {
                        List<Request_TeamUser> model_user = new List<Request_TeamUser>();
                        var stt_us = 1;
                        foreach (var us in model)
                        {
                            model_user.Add(new Request_TeamUser()
                            {
                                TeamUser_ID = helper.GenKey(),
                                Team_ID = Team_ID,
                                Nguoitao = tk.Users_ID,
                                Ngaytao = DateTime.Now,
                                NhanSu_ID = us.NhanSu_ID,
                                IsType = us.IsType,
                                Trangthai = us.Trangthai,
                                STT = stt_us++,
                            });
                        }
                        db.Request_TeamUser.AddRange(model_user);
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }
        public class UsersTeam
        {
            public string NhanSu_ID { get; set; }
            public int IsType { get; set; }
            public bool Trangthai { get; set; }
            public float IsSLA { get; set; }
        }

        [HttpPost]
        public async Task<JsonResult> Update_User(string t, Request_TeamUser model)
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }

                    if (model.TeamUser_ID == "-1" || model.TeamUser_ID == "" || model.TeamUser_ID == null)
                    {
                        model.TeamUser_ID = helper.GenKey();
                        model.Nguoitao = tk.Users_ID;
                        db.Request_TeamUser.Add(model);
                    }
                    else
                    {
                        db.Entry(model).State = EntityState.Modified;
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = "Có lỗi khi cập nhật form" }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<JsonResult> Del_Multi_Khaibaouser(string t, List<string> ids)
        {
            using (SOEEntities db = new SOEEntities())
            {
                try
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = helper.checkTokennoTask(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    foreach (var id in ids)
                    {
                        var da = await db.Request_TeamUser.FindAsync(id);
                        if (da != null)
                        {
                            db.Request_TeamUser.Remove(da);
                        }
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { error = 1, ms = "Vui lòng xóa dữ liệu liên quan của đợt tuyển dụng này." }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        #endregion
        #region Form Sign
        [HttpPost]
        public async Task<JsonResult> Update_Formsign(string t, string Form_ID, string Team_ID, Request_FormSign model)
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }

                    if (model.FormSign_ID == "-1" || model.FormSign_ID == "" || model.FormSign_ID == null)
                    {
                        model.FormSign_ID = helper.GenKey();
                        model.Form_ID = Form_ID;
                        model.Team_ID = Team_ID == "" ? null : Team_ID;
                        model.Nguoitao = tk.Users_ID;
                        db.Request_FormSign.Add(model);
                    }
                    else
                    {
                        db.Entry(model).State = EntityState.Modified;
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = "Có lỗi khi cập nhật form" }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> Update_Trangthai_FormSign(string t, string id, bool tt)
        {
            using (SOEEntities db = new SOEEntities())
            {
                try
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = helper.checkTokennoTask(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    var da = await db.Request_FormSign.FindAsync(id);
                    if (da != null)
                    {
                        da.IsActive = tt;
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { error = 0, ms = "Vui lòng xóa dữ liệu liên quan của Nhóm duyệt này." }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        public async Task<JsonResult> Del_Multi_Formsign(string t, List<string> ids)
        {
            using (SOEEntities db = new SOEEntities())
            {
                try
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = helper.checkTokennoTask(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    foreach (var id in ids)
                    {
                        var da = await db.Request_FormSign.FindAsync(id);
                        if (da != null)
                        {
                            db.Request_FormSign.Remove(da);
                        }
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { error = 1, ms = "Vui lòng xóa dữ liệu liên quan của đợt tuyển dụng này." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public async Task<JsonResult> Update_Formsignuser()
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    var t = Request.Form["t"];
                    var FormSign_ID = Request.Form["FormSign_ID"];
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", Request.Form["t"]));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    List<UsersTeam> model = JsonConvert.DeserializeObject<List<UsersTeam>>(Request.Form["model"]);
                    var model_us = await db.Request_FromSignUser.Where(x => x.FormSign_ID == FormSign_ID).ToListAsync();
                    if (model_us.Count > 0)
                    {
                        db.Request_FromSignUser.RemoveRange(model_us);
                    }
                    if (model.Count > 0)
                    {
                        List<Request_FromSignUser> model_user = new List<Request_FromSignUser>();
                        var stt_us = 1;
                        foreach (var us in model)
                        {
                            model_user.Add(new Request_FromSignUser()
                            {
                                FormSignUser_ID = helper.GenKey(),
                                FormSign_ID = FormSign_ID,
                                Nguoitao = tk.Users_ID,
                                Ngaytao = DateTime.Now,
                                NhanSu_ID = us.NhanSu_ID,
                                IsType = us.IsType,
                                Trangthai = us.Trangthai,
                                IsSLA = us.IsSLA,
                                STT = stt_us++,
                            });
                        }
                        db.Request_FromSignUser.AddRange(model_user);
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> Update_FormD()
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    var t = Request.Form["t"];
                    var Form_ID = Request.Form["Form_ID"];
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", Request.Form["t"]));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    List<ListRequest_FormD> model = JsonConvert.DeserializeObject<List<ListRequest_FormD>>(Request.Form["modelD"]);
                    var model_form = await db.Request_FormD.Where(x => x.Form_ID == Form_ID).ToListAsync();
                    if (model_form.Count > 0)
                    {
                        db.Request_FormD.RemoveRange(model_form);
                    }
                    if (model.Count > 0)
                    {
                        List<Request_FormD> model_formD = new List<Request_FormD>();
                        foreach (var mf in model)
                        {
                            int stt = model_formD.Count + 1;
                            string FormD_ID = "";
                            if (mf.FormD_ID != null)
                            {
                                FormD_ID = mf.FormD_ID;
                            }
                            else
                            {
                                FormD_ID = helper.GenKey();
                                mf.FormD_ID = FormD_ID;
                            }
                            string fm_ID = null;
                            if (mf.IsParent_ID != null)
                            {
                                fm_ID = model.FirstOrDefault(x => x.index_ID != null && x.index_ID == mf.IsParent_ID)?.FormD_ID ?? null;
                            }
                            model_formD.Add(new Request_FormD()
                            {
                                FormD_ID = FormD_ID,
                                Form_ID = Form_ID,
                                TenTruong = mf.TenTruong,
                                KieuTruong = mf.KieuTruong,
                                IsLength = mf.IsLength,
                                STT = stt,
                                IsRequired = mf.IsRequired,
                                IsLabel = mf.IsLabel,
                                IsParent_ID = fm_ID == null ? mf.IsParent_ID : fm_ID,
                                IsType = mf.IsType,
                                IsClass = mf.IsClass,
                            });
                            stt++;
                        }
                        db.Request_FormD.AddRange(model_formD);
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = e.Message }, JsonRequestBehavior.AllowGet);
            }
        }

        public class ListRequest_FormD
        {
            public string FormD_ID { get; set; }
            public string Form_ID { get; set; }
            public string TenTruong { get; set; }
            public string KieuTruong { get; set; }
            public int IsLength { get; set; }
            public int STT { get; set; }
            public bool IsRequired { get; set; }
            public bool IsLabel { get; set; }
            public string IsParent_ID { get; set; }
            public int IsType { get; set; }
            public string index_ID { get; set; }
            public string IsClass { get; set; }
        }
        #endregion
        #region Setting Manage
        [HttpPost]
        public async Task<JsonResult> Update_SetupManage(string t, List<Request_Setting> model)
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    if(model.Count > 0)
                    {
                        foreach(var da in model)
                        {
                            var find_by_ID = db.Request_Setting.Find(da.Setting_ID);
                            if (find_by_ID != null)
                            {
                                find_by_ID.IsNumDevice = da.IsNumDevice;
                                find_by_ID.IsVerifySign = da.IsVerifySign;
                                find_by_ID.IsSkipWeb = da.IsSkipWeb;
                                find_by_ID.IsOneDevice = da.IsOneDevice;
                                find_by_ID.IsNguoilapHuyDexuat = da.IsNguoilapHuyDexuat;
                                find_by_ID.IsNguoilapXoaDexuat = da.IsNguoilapXoaDexuat;
                                find_by_ID.IsNguoilapTagFollow = da.IsNguoilapTagFollow;
                                find_by_ID.IsNguoilapXuly = da.IsNguoilapXuly;
                                find_by_ID.IsNguoiDuyetHuyDexuat = da.IsNguoiDuyetHuyDexuat;
                                find_by_ID.IsNguoiDuyetXoaDexuat = da.IsNguoiDuyetXoaDexuat;
                                find_by_ID.IsNguoiDuyetTagFollow = da.IsNguoiDuyetTagFollow;
                                find_by_ID.IsNguoiDuyetXuly = da.IsNguoiDuyetXuly;
                            }
                            else
                            {
                                Request_Setting st = new Request_Setting();
                                st.Setting_ID = helper.GenKey();
                                st.Congty_ID = da.Congty_ID;
                                st.IsNumDevice = da.IsNumDevice;
                                st.IsVerifySign = da.IsVerifySign;
                                st.IsSkipWeb = da.IsSkipWeb;
                                st.IsOneDevice = da.IsOneDevice;
                                st.IsNguoilapHuyDexuat = da.IsNguoilapHuyDexuat;
                                st.IsNguoilapXoaDexuat = da.IsNguoilapXoaDexuat;
                                st.IsNguoilapTagFollow = da.IsNguoilapTagFollow;
                                st.IsNguoilapXuly = da.IsNguoilapXuly;
                                st.IsNguoiDuyetHuyDexuat = da.IsNguoiDuyetHuyDexuat;
                                st.IsNguoiDuyetXoaDexuat = da.IsNguoiDuyetXoaDexuat;
                                st.IsNguoiDuyetTagFollow = da.IsNguoiDuyetTagFollow;
                                st.IsNguoiDuyetXuly = da.IsNguoiDuyetXuly;
                                db.Request_Setting.Add(st);
                            }
                        }
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = "Có lỗi khi cập nhật form" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region Setting Number
        #region Quản lý file
        [HttpPost]
        public async Task<JsonResult> Update_FolderFile(string t, Request_Folder model)
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }

                    if (model.Folder_ID == "-1" || model.Folder_ID == "" || model.Folder_ID == null)
                    {
                        model.Folder_ID = helper.GenKey();
                        model.Nguoitao = tk.Users_ID;
                        model.Ngaytao = DateTime.Now;
                        model.STT = db.Request_Folder.Count() + 1;
                        db.Request_Folder.Add(model);
                    }
                    else
                    {
                        db.Entry(model).State = EntityState.Modified;
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = "Có lỗi khi cập nhật nhóm form" }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> Del_Folder(string t, string id, List<string> arrChild)
        {
            using (SOEEntities db = new SOEEntities())
            {
                try
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = helper.checkTokennoTask(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }

                    var da_p = await db.Request_Folder.FindAsync(id);
                    if (da_p != null)
                    {
                        db.Request_Folder.Remove(da_p);
                        var filelink_p = await db.Request_Folder_File.Where(x => x.Folder_ID == id).ToListAsync();
                        if (filelink_p.Count > 0)
                        {
                            db.Request_Folder_File.RemoveRange(filelink_p);
                        }
                    }
                    if (arrChild.Count > 0)
                    {
                        foreach(var ids in arrChild)
                        {
                            var da_c = await db.Request_Folder.FindAsync(ids);
                            if (da_c != null)
                            {
                                db.Request_Folder.Remove(da_c);
                                var filelink_c = await db.Request_Folder_File.Where(x => x.Folder_ID == ids).ToListAsync();
                                if (filelink_c.Count > 0)
                                {
                                    db.Request_Folder_File.RemoveRange(filelink_c);
                                }
                            }
                        }
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception e)
                {
                    return Json(new { error = 1, ms = "Vui lòng xóa dữ liệu liên quan của đợt tuyển dụng này." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public async Task<JsonResult> Update_LinkFolderFile(string t, string Folder_ID, List<Request_Folder_File> arr)
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }

                    if (arr.Count > 0)
                    {
                        List<Request_Folder_File> model = new List<Request_Folder_File>();

                        foreach(var ids in arr)
                        {
                            model.Add(new Request_Folder_File()
                            {
                                Folder_File_ID = helper.GenKey(),
                                Folder_ID = Folder_ID,
                                FileID = ids.FileID,
                            });
                        }
                        db.Request_Folder_File.AddRange(model);
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = "Có lỗi khi cập nhật nhóm form" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        [HttpPost]
        public async Task<JsonResult> Update_Settingnumber(string t, List<Request_So> model,string Congty_ID)
        {
            try
            {
                using (SOEEntities db = new SOEEntities())
                {
                    OS_Token tk = JsonConvert.DeserializeObject<OS_Token>(helper.Decrypt("Token", t));
                    int vt = await helper.checkToken(tk);
                    if (vt != 1)
                    {
                        return Json(new { error = 1, token = vt }, JsonRequestBehavior.AllowGet);
                    }
                    if (model.Count > 0)
                    {
                        var stt = 1;
                        foreach (var da in model)
                        {
                            var find_by_ID = db.Request_So.Find(da.SettingSo_ID);
                            if (find_by_ID != null)
                            {
                                find_by_ID.Congty_ID = Congty_ID;
                                find_by_ID.IDKey = da.IDKey;
                                find_by_ID.CotThongtin = da.CotThongtin;
                                find_by_ID.Sokytu = da.Sokytu;
                                find_by_ID.Ngancach = da.Ngancach;
                                find_by_ID.Sudung = da.Sudung;
                                find_by_ID.Tudongsinh = da.Tudongsinh;
                                find_by_ID.STT = stt++;
                            }
                            else
                            {
                                Request_So so = new Request_So();
                                so.SettingSo_ID = helper.GenKey();
                                so.Congty_ID = Congty_ID;
                                so.IDKey = da.IDKey;
                                so.CotThongtin = da.CotThongtin;
                                so.Sokytu = da.Sokytu;
                                so.Ngancach = da.Ngancach;
                                so.Sudung = da.Sudung;
                                so.Tudongsinh = da.Tudongsinh;
                                find_by_ID.STT = stt++;
                                db.Request_So.Add(so);
                            }
                        }
                    }
                    await db.SaveChangesAsync();
                    return Json(new { error = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception e)
            {
                return Json(new { error = 1, ms = "Có lỗi khi cập nhật form" }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}